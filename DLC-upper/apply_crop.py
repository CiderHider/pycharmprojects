import deeplabcut
import os
import numpy as np


def extract_crop(config_path):
    crop_template = {'_R_': [],
                     '_L_': [],
                     '_Side_': [],
                     '_Top_': []
                     }
    config_data = deeplabcut.auxiliaryfunctions.read_config(config_path)
    for video_path in config_data['video_sets']:
        file_name = os.path.split(video_path)[1]
        for key in crop_template:
            if key in file_name:
                np.append(crop_template[key], config_data['video_sets'][video_path]['crop'], axis=1)
                break
    return crop_template
