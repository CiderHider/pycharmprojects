% File to generate 3D coordinates from DeepLabCut into SIMI
% Written by Rafael Ornelas Kobayashi - v00
% Editted by Mayo - v01 and after
% Version History
% v00 - works on windows for TDM without filtering or leg rejection, poorly formatted and written
% v01 - made a bit more readable, works with leg rejection
% v02 - works with Mac as well now, but only for one crossing
% v03 - works for multiple crossings now
% v04 - better plotting figure
% NOTE: Deep4Simi is able to track as many features as there are, because
% it's a structure rather than a matrix. this version only tracks a set
% number of features.

clear all

Right_Flag=false;
Left_Flag=false;

%%%%%%%%%%%%%%%% parameters to define %%%%%%%%%%%%%%%%%%%

%P-value for thresholding DLC likelihood
p=0.998;

%Establishing video resolution:
x_Res=964;
y_Res=400;

% 1 for full body, 2 for left side, 3 for the right side
model=1;

%Multiple selection flag
another='Yes';
cuenta=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Opening file
while strcmp(another,'Yes')==1
    [name0, path0]=uigetfile('*.csv');
    task_names{cuenta}=name0;
    task_paths{cuenta}=path0;
    another = questdlg('Do you want to select another file?','Multiple files selector','Yes','No','No');
    cuenta=cuenta+1;
end

for multiple_files=1:length(task_names)
    
    %Assigning name for multiple files
    name=task_names{multiple_files};
    path=task_paths{multiple_files};
    
    
    %Begining of program
    nameUnderscore=strfind(name,'_');
    trialtype=name(nameUnderscore(2)+1:nameUnderscore(3)-1); %Identifying which type of trial is it
    wrong=false;
    
    %Deleting opposite leg markers selection
    %deleting = input('Do you want apply opposite leg rejection? y/n','s');
    deleting = 'y';
    
    %Low-Pass filtering selection
    % filtering = input('Do you want to low-pass filter the data? y/n','s');
    filtering = 'n';
    
    for number=1:8  %for the 8 cameras
        guion=strfind(name,'-');
        name(guion+1)=num2str(number);    %Creating name for the excel file to read
        
        try
            Data=tdfread([path name],',');  %Reading as struct
        catch
            fprintf(['\nFile not found:  ' name])
            wrong=true; %True if file was not succesdully open
        end
        
        % Get rid of unneccesary text
        if wrong==false
            
            %creating a structure with all data from the table
            clear likelihood newMatrix matrix
            likelihood_count=1;
            matrix_count=1;
            Matrix_Struct=[];
            fields=fieldnames(Data);
            
            for column=2:1:length(fields)
                Table_column=getfield(Data,fields{column});
                header=Table_column(1,:);
                header(find(header==' '))=''; %Removing white spaces
                subheader=Table_column(2,:);
                subheader(find(subheader==' '))=''; %Removing white spaces
                %Removing '-' signs (if any)
                if isempty(find(header=='-'))==0
                    header(find(header=='-'))='';
                end
                
                Matrix_Struct=setfield(Matrix_Struct,header,subheader,str2num(Table_column(3:length(Table_column),:)));
            end
            
            fields=fieldnames(Matrix_Struct); %Reading fields inside the structure
            
            %If there are tail markers, overwrite crest coordinates with tail coordinates
            if length(fields)>10
                Matrix_Struct.Leftcrest.x=Matrix_Struct.Lefttail.x;
                Matrix_Struct.Leftcrest.y=Matrix_Struct.Lefttail.y;
                Matrix_Struct.Leftcrest.likelihood=Matrix_Struct.Lefttail.likelihood;
                Matrix_Struct.Rightcrest.x=Matrix_Struct.Righttail.x;
                Matrix_Struct.Rightcrest.y=Matrix_Struct.Righttail.y;
                Matrix_Struct.Rightcrest.likelihood=Matrix_Struct.Righttail.likelihood;
            end
            
            %Create matrix of coordinates (Normalized to resolution)
            matrix=[getfield(Matrix_Struct,fields{1},'x')/x_Res,getfield(Matrix_Struct,fields{1},'y')/y_Res];
            for j=2:1:10
                matrix=[matrix,getfield(Matrix_Struct,fields{j},'x')/x_Res,getfield(Matrix_Struct,fields{j},'y')/y_Res];
            end
            
            %Create matrix of likelihoods
            likelihood=[getfield(Matrix_Struct,fields{1},'likelihood'),getfield(Matrix_Struct,fields{1},'likelihood')];
            for j=2:1:10
                likelihood=[likelihood,getfield(Matrix_Struct,fields{j},'likelihood'),getfield(Matrix_Struct,fields{j},'likelihood')];
            end
            
            %Converting matrix array to cell (so it can have empty spaces)
            matrix= num2cell(matrix);
            
            %Deleting cell values that have a likelihood below the treshold
            for i=1:20
                empty_cells=find(likelihood(:,i)<p);
                for j=1:length(empty_cells)
                    matrix{empty_cells(j),i}='';
                end
            end
            
            fprintf(['\nImporting file ' num2str(number) '...  completed!'])
            
            %Deleting outliers from the oposite side
            clear empty_index left_side right_side;
            
            if strcmp(deleting,'y')==1
                
                if isempty(strfind(trialtype,'T'))==0 && isempty(strfind(trialtype,'M'))==0  %TM section
                    
                    for i=1:size(matrix,2)
                        index=1;
                        for j=1:size(matrix,1)
                            if isempty(matrix{j,i})==1
                                empty_index(index,i)=j+1;  %J+1 because the video starts at 0 and the matrix at 1
                                index=index+1;
                            end
                        end
                    end
                    
                    %Concatenating amount of empty markers per side to determine the dominant side
                    left_side=[empty_index(:,1); empty_index(:,2); empty_index(:,3); empty_index(:,4); empty_index(:,5); empty_index(:,6); empty_index(:,7); empty_index(:,8); empty_index(:,9); empty_index(:,10)];
                    left_side(find(left_side==0))='';
                    right_side=[empty_index(:,11); empty_index(:,12); empty_index(:,13); empty_index(:,14); empty_index(:,15); empty_index(:,16); empty_index(:,17); empty_index(:,18); empty_index(:,19); empty_index(:,20)];
                    right_side(find(right_side==0))='';
                    
                    if length(left_side)>=length(right_side)
                        for i=1:10
                            for j=1:length(matrix)
                                matrix{j,i}='';
                            end
                        end
                    else
                        for i=11:20
                            for j=1:length(matrix)
                                matrix{j,i}='';
                            end
                        end
                    end
                end %End of TM section
                
                
                if strcmp(trialtype,'CORR')==1 || strcmp(trialtype,'LADi')==1 || strcmp(trialtype,'OBS')==1 || strcmp(trialtype,'STA')==1  %Start of CORR, LADi section
                    
                    
                    %Empty cell array creation
                    empty_cell_array=cell(size(matrix,1),size(matrix,2));
                    
                    %Cameras configuration
                    if number==1
                        Cameras=fopen('Cameras.txt');
                        if Cameras>0
                            fprintf('\r\nCurrent Cameras configuration: ');
                            for i=1:8
                                read=fgetl(Cameras);
                                Cam(i,1)=read;
                                fprintf(['\r\nInitial visible leg in Camera ' i ': ' Cam(i,1)]);
                            end
                            Change_Configuration=input('\r\nModify configuration? (y/n)','s');
                        end
                        if Cameras<=0 || Change_Configuration=='y'
                            Cameras=fopen('Cameras.txt','w');
                            for i=1:8
                                Side = input(['\r\nDefine initial visible leg (L/R) for Camera ' num2str(i) ': \r\n'],'s');
                                fprintf(Cameras,[Side '\r\n']);
                                Cam(i,1)=Side;
                            end
                            fprintf('\r\nCameras configuration has been stored');
                        end
                    end
                    
                    %Selecting points for side L
                    figure('Name','Left and Right leg selections')
                    if Cam(number)=='L' && Left_Flag==false
                        index=1;
                        for i=1:20
                            for j=1:size(matrix,1)
                                if isempty(matrix{j,i})==1 %Adding zeroes to filter without changing vector's lenght
                                    vector(j)=NaN;
                                    empty_index(index,i)=j;
                                    index=index+1;
                                else
                                    vector(j)=matrix{j,i};
                                end
                            end
                            newMatrix(:,i)=vector';
                            
                            if i < 10 && mod(i,2) % odd numbers less than 10 (Left leg)
                                subplot(2,1,1);
                                plot(1:length(vector),vector,'-b','LineWidth',2);
                                xlim([1,length(vector)])
                                hold on
                                title({'Select start-end points of LEFT, then RIGHT leg',...
                                'Left click on left leg, the start and end of each crossing',...
                                'Then Right click on right leg, the start and end of each crossing',...
                                'Then click enter'})
                                ylabel('Left Leg')
                            elseif i > 10 && mod(i,2) % odd numbers more than 10 (Right leg)
                                subplot(2,1,2);
                                plot(1:length(vector),vector,'-r','LineWidth',2);
                                xlim([1,length(vector)])
                                hold on
                                ylabel('Right Leg')
                            end     
                        end
                        
                        set(gcf,'Position',[1  50 1280 605])
                        fprintf('\r\nSelect begining and end point for left side (left click) and right side (right click)');
                        [equisL bla button]= ginput(); %Select only for the first record
                        close all
                        Left_Flag=true;
                        
                        %For multiple crossings
                        left_button=find(button==1);
                        button_account=1;
                        for multiple_cross=1:2:length(find(button==1)) %number of crossings
                            crossingsL_L(button_account,:)=[equisL(left_button(multiple_cross)),equisL(left_button(multiple_cross+1))];
                            button_account=button_account+1;
                        end
                        
                        right_button=find(button==3);
                        button_account=1;
                        for multiple_cross=1:2:length(find(button==3)) %number of crossings
                            crossingsR_L(button_account,:)=[equisL(right_button(multiple_cross)),equisL(right_button(multiple_cross+1))];
                            button_account=button_account+1;
                        end
                        
                    end
                    
                    %Selecting points for side R
                    if Cam(number)=='R' && Right_Flag==false
                        index=1;
                        for i=1:20
                            for j=1:size(matrix,1)
                                if isempty(matrix{j,i})==1 %Adding zeroes to filter without changing vector's lenght
                                    vector(j)=NaN;
                                    empty_index(index,i)=j;
                                    index=index+1;
                                else
                                    vector(j)=matrix{j,i};
                                end
                            end
                            newMatrix(:,i)=vector';
                            
                            if i < 10 && mod(i,2) % odd numbers less than 10 (Left leg)
                                subplot(2,1,1);
                                plot(1:length(vector),vector,'-b','LineWidth',2);
                                xlim([1,length(vector)])
                                hold on
                                title({'Select start-end points of LEFT, then RIGHT leg',...
                                'Left click on left leg, the start and end of each crossing',...
                                'Then Right click on right leg, the start and end of each crossing',...
                                'Then click enter'})
                                ylabel('Left Leg')
                            elseif i > 10 && mod(i,2) % odd numbers more than 10 (Right leg)
                                subplot(2,1,2);
                                plot(1:length(vector),vector,'-r','LineWidth',2);
                                xlim([1,length(vector)])
                                hold on
                                ylabel('Right Leg')
                            end     
                        end
                        
                        
                        set(gcf,'Position',[1  50 1280 605])
                        fprintf('\r\nSelect begining and end point for left side (left click) and right side (right click)');
                        [equisR bla button]= ginput(); %Select only for the first record
                        close all
                        Right_Flag=true;
                        
                        
                        %For multiple crossings
                        left_button=find(button==1);
                        button_account=1;
                        for multiple_cross=1:2:length(find(button==1)) %number of crossings
                            crossingsL_R(button_account,:)=[equisR(left_button(multiple_cross)),equisR(left_button(multiple_cross+1))];
                            button_account=button_account+1;
                        end
                        
                        right_button=find(button==3);
                        button_account=1;
                        for multiple_cross=1:2:length(find(button==3)) %number of crossings
                            crossingsR_R(button_account,:)=[equisR(right_button(multiple_cross)),equisR(right_button(multiple_cross+1))];
                            button_account=button_account+1;
                        end
                        
                    end
                    close all
                    
                    %Preparing the crossings to add to the empty cell array
                    
                    if Cam(number,1)=='L'
                        equisL=crossingsL_L;
                        equisR=crossingsR_L;
                    else
                        equisL=crossingsL_R;
                        equisR=crossingsR_R;
                    end
                    
                    %Adding left markers
                    for cross_number=1:1:size(equisL,1)
                        cross2add=[round(equisL(cross_number,1)):1:round(equisL(cross_number,2))];
                        
                        for i=1:1:10
                            for j=1:1:length(cross2add)
                                empty_cell_array{cross2add(j),i}=matrix{cross2add(j),i};
                            end
                        end
                        
                    end
                    
                    %Adding right markers
                    for cross_number=1:1:size(equisR,1)
                        cross2add=[round(equisR(cross_number,1)):1:round(equisR(cross_number,2))];
                        
                        for i=11:1:20
                            for j=1:1:length(cross2add)
                                empty_cell_array{cross2add(j),i}=matrix{cross2add(j),i};
                            end
                        end
                        
                    end
                    
                    matrix=empty_cell_array;
                    
                end %End of CORR_LADi
                
            end %end of if strcmp(deleting,'y')==1
            
            
            %%%%%Low-pass filtering
            
            if strcmp(filtering,'y')==1
                
                %Creating a new matrix with NaN values in empty spaces
                for i=1:20
                    for j=1:size(matrix,1)
                        if isempty(matrix{j,i})==1 %Adding zeroes to filter without changing vector's lenght
                            vector(j)=NaN;
                        else
                            vector(j)=matrix{j,i};
                        end
                    end
                    newMatrix(:,i)=vector';
                end
                
                %Removing outliers
                for k=1:size(matrix,2)
                    [peaks,times]=findpeaks(diff(newMatrix(:,k)),'MinPeakDistance',40);
                    PeaksIndex=find(peaks>=mean(peaks)*2);
                    Outliers=times(PeaksIndex);
                    for i=1:length(Outliers)
                        try
                            newMatrix(Outliers(i)-4:Outliers(i)+3,k)=NaN;
                        catch
                            newMatrix(1:Outliers(i)+3,k)=NaN;
                        end
                    end
                    newMatrix(:,k)=fillmissing(newMatrix(:,k),'spline');
                end
                
                %Designing filter
                LowpassFreq = 14;
                LPFiltOrder = 4;
                SampleRate = 100;
                [b_LP,a_LP] = butter(LPFiltOrder, LowpassFreq / (SampleRate / 2),'low');
                
                %Applying low pass filter
                for i=1:size(newMatrix,2)
                    try
                        matrix_Filtered(:,i) = filtfilt(b_LP, a_LP, newMatrix(:,i));
                    catch
                        newVector=newMatrix(:,i);
                        while length(newVector) > length(matrix_Filtered(:,i))
                            newVector(length(newVector))='';
                        end
                        matrix_Filtered(:,i) = filtfilt(b_LP, a_LP, newVector);
                    end
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Patching tracking errors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                Patched_Matrix=matrix_Filtered;
                
                %%High passsing to adjust baseline
                HighpassFreq = 0.14;
                LPFiltOrder = 4;
                [b_HP,a_HP] = butter(LPFiltOrder, HighpassFreq / (SampleRate / 2),'high');
                
                for multirun=1:2 %for multiple run (to correct upper and lower pikes
                    
                    for k=1:size(Patched_Matrix,2)%To filter each marker (single run)
                        
                        if multirun==1
                            marker = filtfilt(b_HP, a_HP, matrix_Filtered(:,k));
                            [picos,tiempos]=findpeaks(-marker); %To correct bottom pikes
                        elseif multirun==2
                            marker = Patched_Matrix(:,k);
                            [picos,tiempos]=findpeaks(marker);    %For upper pikes
                        end
                        
                        %Separating peaks in 2 clusters using k-means clustering
                        Kmeans_error=0;
                        
                        try
                            [idx,C] = kmeans(picos,2);
                        catch
                            Kmeans_error=1;
                        end
                        
                        if Kmeans_error~=1
                            
                            Outliers_cluster=find(C(:)==min(C));
                            Outliers_indexes=find(idx==Outliers_cluster);
                            Indexes2Delete=tiempos(Outliers_indexes);
                            
                            %Deleting peaks from data
                            [NewPicos,NewTiempos]=findpeaks(marker); %calculate all peaks on normal filtered signal
                            for i=1:length(Indexes2Delete)
                                for m=1:length(NewTiempos)-1
                                    if NewTiempos(m)<=Indexes2Delete(i) && NewTiempos(m+1)>=Indexes2Delete(i) %Finding peaks prior and after outlier peak
                                        if NewTiempos(m+1)-NewTiempos(m)<=25 % <25 to ensure you dont delete important pikes from normal walking during baseline change
                                            Patched_Matrix(NewTiempos(m)+1:NewTiempos(m+1)-1,k)=NaN;
                                        end
                                    end
                                end
                            end
                            
                            Patched_Matrix(:,k)=fillmissing(Patched_Matrix(:,k),'spline');
                            
                        end
                    end %Patching ending (single run)
                    
                end %multirun end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                
                %Overwritting filtered data on matrix
                for i=1:size(matrix,2)
                    empty_column=empty_index(:,i);
                    empty_column(find(empty_column==0))='';
                    for j=1:size(Patched_Matrix,1)
                        if isempty(find(empty_column==j))==1
                            matrix{j,i}=Patched_Matrix(j,i);
                        else
                            matrix{j,i}='';
                        end
                    end
                end
                
            end %End of filtering section
            
            
            %%
            
            
            %%%%%%%%Writing .p file
            % if wrong==false
            
            name2save=['RawData_' num2str(number) '.txt'];
            if strcmp(filtering,'y')==1
                name2save=['RawData_Filtered' num2str(number) '.txt'];
            end
            Raw_Data=fopen([path name2save],'w');
            fprintf(Raw_Data, 'FileType \t RawData');
            fprintf(Raw_Data, '\r\nVersion 150');
            fprintf(Raw_Data, '\r\nName \t Raw data');
            fprintf(Raw_Data, '\r\nSamples %d',length(matrix));
            fprintf(Raw_Data, '\r\nTimeOffset \t 0.000000');
            fprintf(Raw_Data, '\r\nSamplesPerSecond \t 100.000000');
            
            if model == 1  %Full body
                fprintf(Raw_Data, '\r\nCount \t 10 \r\n');
                fprintf(Raw_Data, '\r\n21001 \t 21001 \t 21000 \t 21000 \t 21100 \t 21100 \t 21200 \t 21200 \t 21300 \t 21300 \t 31001 \t 31001 \t 31000 \t 31000 \t 31100 \t 31100 \t 31200 \t 31200 \t 31300 \t 31300');
                fprintf(Raw_Data, '\r\nperineum bone left \t perineum bone left \t left hip \t left hip \t left knee \t left knee \t left ankle-bone \t left ankle-bone \t left foot \t left foot \t perineum bone right \t perineum bone right \t right hip \t right hip \t right knee \t right knee \t right ankle-bone \t right ankle-bone \t right foot \t right foot \r\n');
                for k=1:length(matrix)
                    for j=1:20
                        if isempty(matrix{k,j})==1
                            fprintf(Raw_Data,'  ');
                        else
                            fprintf(Raw_Data,'%f ',matrix{k,j});
                        end
                        fprintf(Raw_Data,'\t ');
                        %fprintf(Raw_Data, '\%f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f \t %f', [matrix{k,1} matrix{k,2} matrix{k,3} matrix{k,4} matrix{k,5} matrix{k,6} matrix{k,7} matrix{k,8} matrix{k,9} matrix{k,10} matrix{k,11} matrix{k,12} matrix{k,13} matrix{k,14} matrix{k,15} matrix{k,16} matrix{k,17} matrix{k,18} matrix{k,19} matrix{k,20}]);
                    end
                    fprintf(Raw_Data,'\r\n');
                end
                
            elseif model ==2 %Left
                fprintf(Raw_Data, '\r\nCount \t 5 \r\r\n');
                fprintf(Raw_Data, '\r\n21001 \t 21001 \t 21000 \t 21000 \t 21100 \t 21100 \t 21200 \t 21200 \t 21300 \t 21300');
                fprintf(Raw_Data, '\r\nperineum bone left \t perineum bone left \t left hip \t left hip \t left knee \t left knee \t left ankle-bone \t left ankle-bone \t left foot \t left foot');
            elseif model==3  %Right
                fprintf(Raw_Data, '\r\nCount \t 5 \r\r\n');
                fprintf(Raw_Data, '\r\n31001 \t 31001 \t 31000 \t 31000 \t 31100 \t 31100 \t 31200 \t 31200 \t 31300 \t 31300');
                fprintf(Raw_Data, '\r\nperineum bone right \t perineum bone right \t right hip \t right hip \t right knee \t right knee \t right ankle-bone \t right ankle-bone \t right foot \t right foot');
                
            end
            
            fclose(Raw_Data);
            
            oldfolder=cd;
            %     cd ..\..\..\..\..\..\..\..\..
            cd(path);
            
            % Rename
            if ~isunix
                command=['REN ' name2save ' ' name2save(1:length(name2save)-3) 'p'];
            else
                command=['mv ' name2save ' ' name2save(1:length(name2save)-3) 'p'];
            end
            
            status=system(command);
            
            % Delete files
            if status ~= 0
                if ~isunix
                    system(['del RawData_' num2str(number) '.p']);
                else
                    system(['rm RawData_' num2str(number) '.p']);
                end
                status=system(command);
                fprintf("\nFile succesfully overwritten!");
            end
            
        end %end of if false
        
        wrong=false;
        cd(oldfolder);
        
    end
    close all
    fprintf("\nAll files have been succesfully exported!");
    
    %%To make it executable:
    %  mcc -m Deep2Simi_11
    
    
end %For for multiple comparisons end
