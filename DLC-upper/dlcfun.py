import deeplabcut
import os
# Usefull lines
# deeplabcut.select_cropping_area(config_path)
# deeplabcut.extract_frames(config_path,mode = 'automatic/manual',algo = 'uniform/kmeans', userfeedback = False, crop = True/False)
#


def dlc_init():
    # User Info
    video_dir = '/home/aaron/Documents/DLCTraining/Videos'
    working_dir = '/home/aaron/Documents/DLCTraining/'
    project_name = 'Upper_limb'
    experimenter = 'Aaron'

    # Generated Info
    video_list = [os.path.join(video_dir, x) for x in os.listdir(video_dir)]

    # Create DLC project
    config_path = deeplabcut.create_new_project(project_name, experimenter, video_list, working_directory=working_dir,
                                                copy_videos=False)
    # deeplabcut.add_new_videos(path_yaml, video_list, copy_videos=False)

    # add bodyparts and skeleton to config file
    config_data = deeplabcut.auxiliaryfunctions.read_config(config_path)
    config_data['bodyparts'] = ['Shoulder', 'Elbow', 'Wrist_U', 'Wrist_R', 'Thumb_base', 'Thumb_mid', 'Thumb_tip',
                                'Index_base', 'Index_mid', 'Index_tip']
    config_data['skeleton'] = [['Shoulder', 'Elbow'],
                               ['Elbow', 'Wrist_U'],
                               ['Elbow', 'Wrist_R'],
                               ['Wrist_U', 'Wrist_R'],
                               ['Wrist_R', 'Thumb_base'],
                               ['Thumb_base', 'Thumb_mid'],
                               ['Thumb_mid', 'Thumb_tip'],
                               ['Wrist_R', 'Index_base'],
                               ['Index_base', 'Index_mid'],
                               ['Index_mid', 'Index_tip']
                               ]
    config_data['numframes2pick'] = 15
    config_data['dotsize'] = 4
    deeplabcut.auxiliaryfunctions.write_config(config_path, config_data)
    return config_path


def apply_crop(config_path):
    crop_template = {'_R_': [0, 1920, 0, 1080],
                     '_L_': [0, 1920, 0, 1080],
                     '_Side_': [0, 1280, 0, 720],
                     '_Top_': [0, 1280, 0, 720]
                     }
    config_data = deeplabcut.auxiliaryfunctions.read_config(config_path)
    for video_path in config_data['video_sets']:
        file_name = os.path.split(video_path)[1]
        for key in crop_template:
            if key in file_name:
                config_data['video_sets'][video_path]['crop'] = crop_template[key]
                break
    deeplabcut.auxiliaryfunctions.write_config(config_path, config_data)
