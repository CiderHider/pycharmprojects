import deeplabcut
import os

def dlc_init():
    # User Info
    video_dir = '/media/aaron/DATA1/trainHH0603'
    working_dir = '/home/aaron/Documents/DLCHH0306'
    project_name = 'Arm_China'
    experimenter = 'Aaron'

    # Generated Info
    video_list = [os.path.join(video_dir, x) for x in os.listdir(video_dir)]

    # Create DLC project
    config_path = deeplabcut.create_new_project(project_name, experimenter, video_list, working_directory=working_dir,
                                                copy_videos=False)
    # deeplabcut.add_new_videos(path_yaml, video_list, copy_videos=False)

    # add bodyparts and skeleton to config file
    config_data = deeplabcut.auxiliaryfunctions.read_config(config_path)
    config_data['bodyparts'] = ['Shoulder', 'Mid_arm', 'Elbow', 'Wrist_U', 'Wrist_R']
    config_data['skeleton'] = [['Shoulder', 'Elbow'],
                               ['Elbow', 'Wrist_U'],
                               ['Elbow', 'Wrist_R'],
                               ['Wrist_U', 'Wrist_R'],
                               ['Shoulder', 'Mid_arm'],
                               ['Mid_arm', 'Elbow']
                               ]
    config_data['numframes2pick'] = 15
    config_data['dotsize'] = 3
    deeplabcut.auxiliaryfunctions.write_config(config_path, config_data)
    return config_path


def apply_crop(config_path):
    crop_template = {'_R_': [0, 1920, 0, 1080],
                     '_L_': [0, 1920, 0, 1080],
                     '_Side_': [0, 1280, 0, 720],
                     '_Top_': [0, 1280, 0, 720]
                     }
    config_data = deeplabcut.auxiliaryfunctions.read_config(config_path)
    for video_path in config_data['video_sets']:
        file_name = os.path.split(video_path)[1]
        for key in crop_template:
            if key in file_name:
                config_data['video_sets'][video_path]['crop'] = crop_template[key]
                break
    deeplabcut.auxiliaryfunctions.write_config(config_path, config_data)
