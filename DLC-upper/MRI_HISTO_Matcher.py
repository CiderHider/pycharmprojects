import numpy as np


def closest(array, value):
    the_index = np.abs((array - value)).argmin()
    return the_index


n_histo = 36
n_mri = 23
histo = np.linspace(0, 100, n_histo)
mri = np.linspace(0, 100, n_mri)
selected = []

for mri_slize in mri:
    idx = closest(histo, mri_slize)
    selected += [idx+1]
