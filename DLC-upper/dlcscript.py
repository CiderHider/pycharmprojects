import deeplabcut
import dlcfun
import hdf2simi


# print(os.path.abspath(inspect.getfile(deeplabcut.analyze_videos))) # get function location

#Initialisation

# extract frames
deeplabcut.extract_frames(config_path, algo='kmeans', userfeedback=False)
# manual extraction
# deeplabcut.extract_frames(config_path, mode='manual')

# Label frames
deeplabcut.label_frames(config_path)

# Train
deeplabcut.create_training_dataset(config_path)
deeplabcut.train_network(config_path)

deeplabcut.evaluate_network(config_path)

# deeplabcut.analyze_videos(config_path, ["path/to/folder/with/actual/videos"])
# gpu sometimes breaks... (cpu usage high and no GPU usage resulting in very slow analysis) --> restart pc
deeplabcut.analyze_videos(config_path, ['/media/aaron/Nut9000/DLCVidsArianna/used'])
deeplabcut.analyze_videos(config_path, ['/media/aaron/Nut9000/DLCVidsArianna/used'], videotype='MP4')

deeplabcut.create_labeled_video(config_path, ["videos"], draw_skeleton=True, trailpoints=7, save_frames=True, destfolder="pathtoFolder")

#deeplabcut.analyze_videos_converth5_to_csv('/media/alex/experimentaldata/cheetahvideos','.mp4')
#deeplabcut.analyze_videos_converth5_to_csv()

hdf2simi.convert_h5_to_simi()