"""
Convert HDF (H5) to simi readable RawData format (.p)
Author: Aaron Brändli

Repurposing of
analyze_videos_converth5_to_csv(videopath, videotype=".avi")
in
DeepLabCut2.0 Toolbox (deeplabcut.org)
© A. & M. Mathis Labs
https://github.com/AlexEMG/DeepLabCut
Please see AUTHORS for contributors.
https://github.com/AlexEMG/DeepLabCut/blob/master/AUTHORS
Licensed under GNU Lesser General Public License v3.0
"""
import os
from pathlib import Path
import pandas as pd
import numpy as np

from deeplabcut.utils import auxiliaryfunctions


def convert_h5_to_simi(videopath, videotype=".avi", p_cutoff=0.8, sample_rate=100):
    """
    By default the output poses (when running analyze_videos) are stored as MultiIndex Pandas Array, which contains the name of the network, body part name, (x, y) label position \n
    in pixels, and the likelihood for each frame per body part. These arrays are stored in an efficient Hierarchical Data Format (HDF) \n
    in the same directory, where the video is stored. If the flag save_as_csv is set to True, the data is also exported as comma-separated value file. However,
    if the flag was *not* set, then this function allows the conversion of all h5 files to csv files (without having to analyze the videos again)!
    This functions converts hdf (h5) files to the comma-separated values format (.csv), which in turn can be imported in many programs, such as MATLAB, R, Prism, etc.
    Parameters
    ----------
    videopath : string
        A strings containing the full paths to videos for analysis or a path to the directory where all the videos with same extension are stored.
    videotype: string, optional
        Checks for the extension of the video in case the input to the video is a directory.\nOnly videos with this extension are analyzed. The default is ``.avi``
    Examples
    --------
    Converts all pose-output files belonging to mp4 videos in the folder '/media/alex/experimentaldata/cheetahvideos' to csv files.
    deeplabcut.analyze_videos_converth5_to_csv('/media/alex/experimentaldata/cheetahvideos','.mp4')
    """
    failed_files = []
    start_path = os.getcwd()
    os.chdir(videopath)
    Videos = [
        fn
        for fn in os.listdir(os.curdir)
        if (videotype in fn) and ("_labeled.mp4" not in fn)
    ]  # exclude labeled-videos!

    Allh5files = [
        fn for fn in os.listdir(os.curdir) if (".h5" in fn) and ("resnet" in fn)
    ]

    for video in Videos:
        vname = Path(video).stem
        # Is there a scorer for this?
        PutativeOutputFiles = [fn for fn in Allh5files if vname in fn]
        for pfn in PutativeOutputFiles:
            scorer = pfn.split(vname)[1].split(".h5")[0]
            if "DLC" in scorer or "DeepCut" in scorer:
                try:
                    DC = pd.read_hdf(pfn, "df_with_missing")
                except KeyError:
                    print(f"File: {pfn} could not be handled")
                    failed_files.append(pfn)
                    continue

                n_markers = DC.shape[1] // 3
                n_samples = DC.shape[0]

                # Resort markers/bodyparts into simi configuration and extract Simi style data
                simi_sorted_bodyparts = ["Shoulder", "Elbow", "Mid_arm", "Wrist_U", "Wrist_R"]
                DC_sorted = DC.reindex(simi_sorted_bodyparts, axis="columns", level=1)
                simi_data = convert_to_simidata(DC_sorted, p_cutoff)

                # Create Simi RawFile format
                simi_header = "FileType\tRawData\n" \
                              "Version\t150\n" \
                              "Name\tRaw data\n" \
                              "Samples\t{}\n" \
                              "TimeOffset\t0\n" \
                              "SamplesPerSecond\t{}\n" \
                              "Count\t{}\n\n" \
                              "1000000\t1000000\t" \
                              "1000002\t1000002\t" \
                              "1000003\t1000003\t" \
                              "1000004\t1000004\t" \
                              "1000005\t1000005\n" \
                              "Shoulder L\tShoulder L\t" \
                              "Elbow L\tElbow L\t" \
                              "Mid-arm L\tMid-arm L\t" \
                              "Wrist UL\tWrist UL\t" \
                              "Wrist RL\tWrist RL\n" \
                              "".format(n_samples, sample_rate, n_markers)

                # Write Simi RawFile
                print(f"Converting to SIMI-file (.p): {pfn}")
                # np.savetxt(pfn.split(".h5")[0] + ".p", to_simi, delimiter='\t', fmt='%.5f')
                # np.savetxt(pfn.split(".h5")[0] + ".p", simi_data.values,
                #            delimiter='\t', fmt='%.5f', header=simi_header, comments='')
                # simi_data.to_csv(pfn.split(".h5")[0] + ".p", sep="\t", na_rep="", float_format='%.5f', header=pd_header, index=False)
                with open(pfn.split(".h5")[0] + ".p", 'w') as file:
                    file.write(simi_header)
                    simi_data.to_csv(file, header=False, index=False, sep="\t", na_rep="", float_format='%.5f',
                                     mode="a")

    os.chdir(str(start_path))
    print(f"Finished converting pose files to .p format. The following files could not be handled: \n {failed_files}")
    return failed_files


def convert_to_simidata(input_data, p_cutoff):
    # get SIMI data
    simi_data = input_data.drop(columns='likelihood', level=2)
    # extract likelyhood and x/y coordinates
    likelihood = input_data.xs('likelihood', 1, level=2, drop_level=False)
    # set scale to 0-1 for SIMI
    simi_data.iloc[:, simi_data.columns.get_level_values(2) == 'x'] /= 640
    simi_data.iloc[:, simi_data.columns.get_level_values(2) == 'y'] /= 480
    simi_data.iloc[:, simi_data.columns.get_level_values(2) == 'x'] = \
        np.where(likelihood > p_cutoff, simi_data.loc[:, simi_data.columns.get_level_values(2) == 'x'], np.nan)
    simi_data.iloc[:, simi_data.columns.get_level_values(2) == 'y'] = \
        np.where(likelihood > p_cutoff, simi_data.loc[:, simi_data.columns.get_level_values(2) == 'y'], np.nan)

    return simi_data
