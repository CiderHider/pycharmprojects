import os

import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from nipype.interfaces import fsl
from nipype import Node, Workflow, Function
import glob
from nipype.interfaces.dcm2nii import Dcm2niix
import nibabel as nib
from scipy.ndimage import center_of_mass
from numpy.linalg import norm

"""
Data
"""

mni_voxel_size = 0.25 # voxel size in mm
# Data from Shiqi Sum
df_dist = pd.DataFrame(data={'subject': ['Rey', 'Padme', 'Jyn', 'Leia'],
                             'center_a': [[147, 170, 99], [], [152, 175, 87], [156, 183, 81]],
                             'center_b': [[148, 173, 107], [151, 181, 91], [155, 178, 94], [156, 187, 88]],
                             'shift': [[1, 10, 0], [-4, 0, 0], [-1, 3, 0], [-2, -8, 0]],
                             'target_est_ps': [[147, 177, 90], [147, 179, 86], [148, 175, 79], [154, 182, 92]]
                             })

planning_dir = '/mnt/Shared/BRAIN_Alignment/Subjects'
target_files = glob.glob(os.path.join(planning_dir, r'**', 'apply_transforms_target_atlas', r'*.nii.gz'),
                         recursive=True)
lesion_files = glob.glob(os.path.join(planning_dir, r'**', 'apply_transforms_annot_atlas', r'*_1_flirt.nii.gz'),
                         recursive=True)

df_temp = pd.DataFrame()
for target_file in target_files:
    subject = (target_file.replace(planning_dir, '')).split(os.sep)[1]
    nifti = nib.load(target_file)
    mri = np.array(nifti.get_fdata())
    center = center_of_mass(mri)
    df_line = pd.DataFrame(data={'subject': subject, 'planning': [center]})
    df_temp = pd.concat([df_temp, df_line], axis=0)

df_temp2 = pd.DataFrame()
for lesion_file in lesion_files:
    subject = os.path.basename(lesion_file).split('_')[0]
    nifti = nib.load(lesion_file)
    mri = np.array(nifti.get_fdata())
    center = center_of_mass(mri)
    df_line = pd.DataFrame(data={'subject': subject, 'lesion': [center]})
    df_temp2 = pd.concat([df_temp2, df_line], axis=0)

df_dist = df_dist.set_index('subject')
df_dist = df_dist.join([df_temp.set_index('subject'), df_temp2.set_index('subject')])
df_dist = df_dist.applymap(np.asarray)
df_dist = df_dist.dropna()
df_dist['delta_cb'] = df_dist['center_b'] - df_dist['planning']
df_dist['delta_cb_norm'] = df_dist['delta_cb'].apply(norm)
df_dist['delta_center'] = df_dist['target_est_ps'] - (df_dist['planning'] + df_dist['shift'])
df_dist['delta_center_norm'] = df_dist['delta_center'].apply(norm)

df_dist['delta_lesion'] = df_dist['lesion'] - df_dist['planning']
df_dist['delta_lesion_norm'] = df_dist['delta_lesion'].apply(norm)

variance = np.std(df_dist['planning'].to_numpy(), axis=0)
average = np.mean(df_dist['planning'].to_numpy(), axis=0)
# df_melt = df_dist.melt(value_vars=['center_a', 'center_b', 'planning'])
# sns.scatterplot(df_melt, hue='variable')
# plt.show()


df_dist_mm = df_dist*mni_voxel_size
