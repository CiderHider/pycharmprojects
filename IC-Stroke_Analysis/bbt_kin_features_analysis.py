import os.path
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns


def color_palette_mapper(df_input, mapping_column, color_maps, map_id, col_suffix='', max_spacing=False):
    color_map = color_maps[map_id]
    if '#' in color_map:
        color_palette = sns.color_palette(f"light:{color_map}", as_cmap=True)  # .reversed()
    else:
        color_palette = sns.color_palette(color_map, as_cmap=True)

    color_list = color_palette(np.linspace(0, 0.75, df_input.shape[0]))
    ser_out = pd.Series(list(map(tuple, color_list)), index=df_input.index)
    # ser_out = df_input[mapping_column].map(color_palette)
    return ser_out.to_frame(f"color_mapping{col_suffix}")


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/ProcessedData/BBT/'

output_dir = os.path.join(root_dir, 'Figures')

ref_bbt_features = pd.read_csv(os.path.join(root_dir, 'RefBBT.csv'), sep=';')

jyn_features = pd.read_csv(os.path.join(root_dir, 'JYN_Features.csv'), sep=',')
jyn_features['monkey'] = 'JYN'
leia_features = pd.read_csv(os.path.join(root_dir, 'LEIA_Features.csv'), sep=',')
leia_features['monkey'] = 'LEIA'
padme_features = pd.read_csv(os.path.join(root_dir, 'PADME_Features.csv'), sep=',')
padme_features['monkey'] = 'PADME'

df_all = jyn_features.append(leia_features.append(padme_features))

keep_same = {'dates', 'monkey'}
df_all.columns = df_all.columns.map(lambda x: 'feat_' + x if x not in keep_same else x)
list_features = df_all.drop(columns=keep_same).columns.to_list()
df_feat_melted = df_all.melt(id_vars=keep_same, value_vars=list_features, var_name='features',
                             value_name='feature_value')


# Merge data with the reference information
df_bbt = ref_bbt_features.join(df_feat_melted.set_index(['monkey', 'dates']), on=['monkey', 'dates'])

# Calculate necessary parameters
df_bbt['dates'] = pd.to_datetime(df_bbt['dates'], format='%Y%m%d')
df_bbt['lesion'] = pd.to_datetime(df_bbt['lesion'], format='%Y%m%d')
df_bbt['t_delta'] = df_bbt['dates'] - df_bbt['lesion']
df_bbt['t_days'] = df_bbt['t_delta'].dt.days
df_bbt['groups'] = np.where(df_bbt['t_days'] < 0, 0, df_bbt['t_days'])

# Bin data into healthy-chronic
# status_precise = pd.to_timedelta([-100, 0, 15, 30, 90, 300], 'D')
# status_labels = ['healthy', 'acute', 'late acute', 'early chronic', 'late chronic']
status_precise = pd.to_timedelta([-100, 0, 30, 90, 300], 'D')
status_labels = ['intact', 'acute', 'subacute', 'chronic']
df_bbt['t_binned'] = pd.cut(df_bbt['t_delta'], bins=status_precise, labels=status_labels)

df_bbt['status_labels'] = df_bbt['t_binned']

# If multiple columns should be excluded from melt
# filter_col = [col for col in df_bbt if col.startswith('feat_')]
# list = df_bbt[filter_col].columns.to_list()

'''
# Create independant colors per severity and status
severity = ['mild', 'moderate', 'severe']
df_color = pd.DataFrame(index=pd.MultiIndex.from_product([severity, status_labels])).reset_index()
df_color = df_color.reset_index()
df_color.columns = ['plt_color', 'severity', 'status_labels']
# join with main dataframe
df_bbt = df_bbt.join(df_color.set_index(['severity', 'status_labels']), on=['severity', 'status_labels'])
'''

status_labels = ['intact', 'acute', 'subacute', 'chronic']
color_palettes = {'JYN': '#F0DF0D', 'PADME': '#088980', 'LEIA': '#F0DF0D', 'REY': '#088980'}
severity_colors = {'mild': '#F0DF00', 'moderate': '#19D3C5'}

# Create Color specification Dataframe
status_index = pd.MultiIndex.from_product([status_labels, df_bbt['monkey'].unique()], names=['t_binned', 'monkey'])
df_status = pd.DataFrame(index=status_index, columns=['status_precise']).reset_index()
df_status['status_id'] = np.concatenate(
    [([x] * len(df_status.iloc[:, 1].unique())) for x in range(len(df_status.iloc[:, 0].unique()))], axis=0)
df_status['status_id_unique'] = df_status.index



df_status['status_precise'] = df_status.groupby('monkey').apply(
    lambda x: color_palette_mapper(x, 'status_id', color_palettes, x.name))

df_status.loc[df_status['t_binned'] == 'intact', ['status_precise']] = \
    pd.Series([(0.41960784, 0.42352941, 0.42745098, 1.0) for _ in df_status.index])

df_all = df_bbt.join(df_status.set_index(['t_binned', 'monkey']), on=['t_binned', 'monkey'])
df_colors = (df_all[['status_precise', 'status_id_unique', 'monkey']]).drop_duplicates().sort_values(
    by=['status_id_unique']).reset_index(drop=True)

markers = {'JYN': 'o', 'LEIA': 'p', 'PADME': 'v'}
markers_df = pd.DataFrame.from_dict(markers,orient='index',columns=['marker_type']).rename_axis('monkey').reset_index()
df_colors=df_colors.join(markers_df.set_index('monkey'), on=['monkey'])
'''
g = sns.catplot(x="monkey", y="feature_value",
                hue='status_id_unique', col="features", col_wrap=4, width=0.9,
                data=df_all, kind="violin", sharey=False, inner=None, aspect=3, height=2,
                palette=df_colors['status_precise'].to_list())
plt.savefig(os.path.join(output_dir, 'BBT_AllFeatures.svg'), format='svg')
#plt.show()
# Plot the lines
'''


df_toplot = df_all.set_index(['monkey', 't_binned'])
df_toplot['unique-idf'] = df_toplot.index
df_toplot = df_toplot.reset_index()
'''
g = sns.catplot(x='unique-idf', y="feature_value",
                hue='status_id_unique',col="features", col_wrap=6,
                data=df_toplot, kind="strip",sharey=False,width=0.8,
                palette=df_colors['status_precise'].to_list())
plt.savefig(os.path.join(output_dir, 'BBT_AllFeaturesStripPlot.svg'), format='svg')
'''
'''
g = sns.catplot(x='unique-idf', y="feature_value",
                hue='status_id_unique', col="features", col_wrap=4,
                data=df_all, kind="swarm", sharey=False, s=1,
                palette=df_colors['status_precise'].to_list())
plt.savefig(os.path.join(output_dir, 'BBT_AllFeaturesSwarmPlot.svg'), format='svg')
'''


df_all = df_all.sort_values(by=['monkey'],ascending=False)
#df_all_part=df_all[(df_all['t_binned']=='intact') | (df_all['t_binned']=='chronic')]
#df_new = df_all_part[df_all_part['monkey']!='LEIA']
#df_colors2=df_colors[df_colors['marker_type']!='p']

g = sns.catplot(x='monkey', y="feature_value",
                hue='status_id_unique', col="features", col_wrap=5,
                data=df_all, kind="point", sharey=False, aspect=1.5, height=3, dodge=True,
                linestyles='-', palette=df_colors['status_precise'].to_list(),
                markers=df_colors['marker_type'].to_list(),
                )
plt.savefig(os.path.join(output_dir, 'BBT_AllFeaturesPointPlot15.svg'), format='svg')
plt.savefig(os.path.join(output_dir, 'BBT_AllFeaturesPointPlot15.pdf'), format='pdf')

'''
lf=df_all['features'].unique()

df_all_part=df_all[(df_all['t_binned']=='intact') | (df_all['t_binned']=='chronic')]

for ii in lf:
    df_plot=df_all_part[df_all_part['features'] == ii]
    df_plot.sort_values(by='groups',inplace=True)
    g = sns.catplot(x='t_binned', y="feature_value",
                    hue='status_id_unique', col="monkey", col_wrap=2,
                    style='status_id_unique',
                    data=df_plot, kind="point", sharey=False,
                    aspect=1, height=3, dodge=True,
                    palette=df_colors['status_precise'].to_list(),
                    markers=df_colors['marker_type'].to_list(), join=False
                    )
    g
    plt.savefig(os.path.join(output_dir, (str(ii)+'BBT_AllFeatures.svg')), format='svg')

    plt.show()
'''