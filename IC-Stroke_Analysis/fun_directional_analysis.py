# Imports
import os
import glob
import re
import tkinter as tk
from tkinter import filedialog
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from scipy import stats as st
from scipy.sparse import csr_matrix
from scipy.signal import argrelextrema
from scipy.special import i0
from sklearn.cluster import KMeans
from sklearn.neighbors import KernelDensity
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import silhouette_score

from skimage.color import rgb2hsv, label2rgb, rgb2gray
from skimage import io
from skimage.util import img_as_ubyte

import time

"""
Functions
"""


def get_image_filenames(file_list, file_identifier, file_extension='tiff'):
    dataframe_files = pd.DataFrame()
    dataframe_files[file_identifier] = [file for file in file_list if re.search(rf"{file_identifier}.*", file)]
    dataframe_files['color'] = dataframe_files[file_identifier].str.extract(f".*_(.*).{file_extension}")
    dataframe_files['slide_nr'] = dataframe_files[file_identifier].str.extract(f".*_(\d+).*.{file_extension}")
    return dataframe_files


def check_file_path(data_path):
    if not os.path.exists(data_path):
        data_path = os.path.expanduser("~")
        root = tk.Tk()
        root.withdraw()
        data_path = filedialog.askdirectory(title=f"Chose alternative to given path: {data_path}", initialdir=data_path)
    return data_path


def get_file_list_df(data_path):
    # Get data to analyze

    file_list = glob.glob(os.path.join(data_path, '*.tiff'))
    filename_list = [os.path.split(file)[1] for file in file_list]
    filename_list.sort()

    dataframe_files = get_image_filenames(filename_list, 'ImageRAW').set_index(['color', 'slide_nr'])
    df_direction = (get_image_filenames(filename_list, 'ColorAnalysis')).set_index(['color', 'slide_nr'])
    df_mask = (get_image_filenames(filename_list, 'Mask')).set_index(['color', 'slide_nr'])
    df_wmgm_mask = (get_image_filenames(filename_list, 'WMGM')).set_index(['slide_nr'])
    dataframe_files = dataframe_files.join([df_direction, df_mask])
    dataframe_files = dataframe_files.reset_index(level=0).join(df_wmgm_mask['WMGM']).reset_index()
    dataframe_files.columns = ['slide_nr', 'color', 'filename_raw', 'filename_direction', 'filename_fiber_mask',
                               'filename_wm_gm_mask']
    dataframe_files['directory'] = data_path
    return dataframe_files


def rgb2label(array):
    array = rgb2gray(array)
    array = np.unique(array, return_inverse=True)[1].reshape(array.shape)
    return array


def collect_data(dataframe_files, threshold_hsv_vol=0.0, show_mask=False, subject_name=None):
    df_combined = pd.DataFrame()
    for row in dataframe_files.itertuples(index=False):
        print(f"Processing Slide {row.slide_nr} Color {row.color}")
        raw_image = io.imread(os.path.join(row.directory, row.filename_raw))
        # Get direction analysis image and convert to HSV space for clustering
        direction_image = io.imread(os.path.join(row.directory, row.filename_direction))
        direction_image = rgb2hsv(direction_image)

        # Get GM WM mask image and select white matter (value = 0)
        gm_wm_mask_image = io.imread(os.path.join(row.directory, row.filename_wm_gm_mask))
        if gm_wm_mask_image.ndim > 2:
            raise Exception(
                f"Mask GM mask file was read with multiple dims: {gm_wm_mask_image.ndim}! Check file and tifffile version")

        gm_wm_mask_image = np.where(gm_wm_mask_image == 0, 1, 0)  # Set all pixels with 0 to 1 and all other pixels to 0

        # Get fiber mask image and keep mask of interest
        fiber_mask_image = io.imread(os.path.join(row.directory, row.filename_fiber_mask))
        if fiber_mask_image.ndim > 2:
            raise Exception(
                f"Mask GM mask file was read with multiple dims: {fiber_mask_image.ndim}! Check file and tifffile version")

        if row.color == 'red':
            keep = 1
        elif row.color == 'green':
            keep = 1
        elif row.color == 'FR' and subject_name == 'Rey':
            keep = 0
            print('kept channel 0')
        elif row.color == 'FR':
            keep = 1
        elif row.color == 'BF':
            keep = 1
        else:
            raise Exception(f"Mask color in filename not found: {row.filename_fiber_mask}")
        fiber_mask_image = np.where(fiber_mask_image == keep, 1, 0)

        mask_image = fiber_mask_image * gm_wm_mask_image
        if show_mask:
            plt.figure()
            plt.imshow(mask_image)
            plt.show()

        pixel_location = np.transpose(np.nonzero(mask_image))
        df_temp = pd.DataFrame(pixel_location, columns=['height', 'width'])
        df_temp['raw'] = raw_image[np.nonzero(mask_image)]
        direction_colors = direction_image[np.nonzero(mask_image)]
        df_temp[['direction_h', 'direction_s', 'direction_v']] = direction_colors

        if threshold_hsv_vol:
            df_temp = df_temp.loc[df_temp['direction_v'] > threshold_hsv_vol]
        df_temp[['color', 'slide_nr']] = [row.color, row.slide_nr]

        df_combined = pd.concat([df_combined, df_temp], axis=0)

    # Bin Data and cluster it according to hsv color model --> maybe drop s and/or v
    df_combined = df_combined.reset_index()
    return df_combined


def partial_z_score(dataframe, z_score_list):
    assert isinstance(z_score_list, list), 'Provide a List of keys for partial_zscoring'
    if not z_score_list:
        df_zscore = st.zscore(dataframe)
    else:
        df_zscore = pd.DataFrame(index=dataframe.index)
        for column in z_score_list:
            df_zscore[column] = st.zscore(dataframe[column])
    return df_zscore


def get_clusters(dataframe, max_clusters=5, plot_silhouette=False, verbose=False):
    sil = []
    label_list = []
    for k in range(2, max_clusters + 1):
        if verbose:
            t_start = time.time()
        kmeans = KMeans(n_clusters=k).fit(dataframe)
        labels = kmeans.labels_
        sil.append(silhouette_score(dataframe, labels, metric='euclidean'))
        label_list.append(labels)

        if verbose:
            print(time.time() - t_start)
    max_score = max(sil)
    n_clusters = sil.index(max_score) + 2
    print(f"Max silhouette score: {max_score} @ index: {n_clusters}")
    best_labels = label_list[sil.index(max_score)]

    if plot_silhouette:
        plt.figure()
        plt.plot(sil)
        plt.show()
    return best_labels, n_clusters


def get_height_clusters(dataframe, previous_cluster_vector=np.array([]), plot_silhouette=False, zscore=True,
                        max_clusters=5):
    data = dataframe.copy()
    height_bin = data['bins'].iloc[0]

    print(f"Clustering height bin {height_bin}")
    if zscore:
        cluster_data = partial_z_score(data, ['direction_h', 'direction_s', 'direction_v'])
    else:
        cluster_data = data[['direction_h', 'direction_s', 'direction_v']]

    # Run optimal cluster search with Sillhouette score if no optimal number has been determined for the active bin
    if not previous_cluster_vector.size or not previous_cluster_vector[height_bin]:
        best_labels = get_clusters(cluster_data, plot_silhouette=plot_silhouette, max_clusters=max_clusters)[0]
    else:
        n_clusters = previous_cluster_vector[height_bin]
        print(f"Height bin {height_bin} - previously selected cluster number has been kept: {n_clusters}")
        kmeans = KMeans(n_clusters=int(n_clusters), random_state=42).fit(cluster_data)
        best_labels = kmeans.labels_
    # Store label data instead and access this instead of running another fit

    ser_clusters = pd.DataFrame(index=dataframe.index, data={'cluster': best_labels})

    return ser_clusters


def get_KDE_circular(series, vm_kappa=25, n_bins=1024, plot_it=True):
    """
    Circular KDE (Van Mises)
    Fast KDE estimation functions taken from https://stackoverflow.com/users/836995/rudolfbyker:
    https://stackoverflow.com/questions/28839246/scipy-gaussian-kde-and-circular-data
    vonmises_fft_kde() and vonmises_pdf()
    """

    # vm_kappa = 25  # to be adjusted --> plugin rule

    pi_adjusted = (series - 0.5) * 2 * np.pi  # Rescale from [0,1] to [-pi,pi]
    vm_kde_bins, vm_kde = vonmises_fft_kde(pi_adjusted, kappa=vm_kappa, n_bins=n_bins)

    mins = argrelextrema(vm_kde, np.less, mode='wrap')[0]
    maxs = argrelextrema(vm_kde, np.greater, mode='wrap')[0]

    # find flat extremas (find_peaks does not wrap) and append them to min/max list respectively
    diff = np.diff(vm_kde)
    plateaus = np.where(diff == 0)[0]
    for plateau in plateaus:
        one_shift = 1
        if plateau == n_bins:
            one_shift = -1

        if diff[plateau + one_shift] > 0:
            mins = np.append(mins, plateau)
        elif diff[plateau + one_shift] < 0:
            maxs = np.append(maxs, plateau)
        else:
            raise Exception(
                f"Long plateau in Kernel density estimate")
    maxs = np.sort(maxs)
    mins = np.sort(mins)

    # plt.figure()
    # plt.plot(vm_kde)
    # plt.show()

    edges = np.concatenate(([0], mins, [n_bins]))

    if np.max(maxs) > np.max(mins):
        labels = np.append(maxs[-1], maxs)
    else:
        labels = np.append(maxs, maxs[0])
    # series.index = series
    # Breaks if too high n_bins -> only single min to 2 maxs
    bins = pd.cut(series, bins=edges / n_bins, labels=labels / n_bins, include_lowest=True, ordered=False)
    if plot_it:
        # hist_n = np.histogram(pi_adjusted, bins=1024)[0]
        # plt.figure()
        plt.plot(vm_kde_bins, vm_kde)
        plt.plot(vm_kde_bins[mins], vm_kde[mins], 'rx')
        plt.plot(vm_kde_bins[maxs], vm_kde[maxs], 'gx')
        # plt.plot(vm_kde_bins, hist_n / np.max(hist_n))
        # plt.show()

    return bins


def get_KDE_linear(series, kde_bandwidth=None):
    vector = series.to_numpy().reshape(-1, 1)
    test_length = 100
    vector_test = np.linspace(0, 1, test_length).reshape(-1, 1)
    if kde_bandwidth is None:
        # Scott Bandwidth estimation for vector (1D)
        scott = vector.shape[0] ** (-1. / 5)
        kde = KernelDensity(bandwidth=scott).fit(vector)
    elif kde_bandwidth == 'cv':
        params = {'bandwidth': np.linspace(0.04, 0.1, 5)}
        grid = GridSearchCV(KernelDensity(), params, cv=5, n_jobs=-2)
        grid.fit(vector)
        print(f"best bandwidth: {grid.best_estimator_.bandwidth}")

        # use the best estimator to compute the kernel density estimate
        kde = grid.best_estimator_
    else:
        kde = KernelDensity(bandwidth=kde_bandwidth).fit(vector)

    estimate = kde.score_samples(vector_test)
    mins = argrelextrema(estimate, np.less)[0]
    maxs = argrelextrema(estimate, np.greater)[0]

    edges = np.concatenate(([0], mins / 100, [1]))
    # series.index = series

    print('beep')
    # bins = pd.cut(series, bins=edges, labels=maxs/test_length, include_lowest=True)
    """
    Problems with bins on edges (14th bin for example BUT NEEDS TO BE CIRCULAR ANYWAY!!!
    https://stackoverflow.com/questions/28839246/scipy-gaussian-kde-and-circular-data
    """
    plt.figure()
    plt.plot(vector_test, estimate)
    plt.plot(vector_test[mins], estimate[mins], 'rx')
    plt.plot(vector_test[maxs], estimate[maxs], 'gx')
    plt.show()

    # return bins


def vonmises_pdf(x, mu, kappa):
    return np.exp(kappa * np.cos(x - mu)) / (2. * np.pi * i0(kappa))


def vonmises_fft_kde(data, kappa, n_bins):
    bins = np.linspace(-np.pi, np.pi, n_bins + 1, endpoint=True)
    hist_n, bin_edges = np.histogram(data, bins=bins)
    bin_centers = np.mean([bin_edges[1:], bin_edges[:-1]], axis=0)
    kernel = vonmises_pdf(
        x=bin_centers,
        mu=0,
        kappa=kappa
    )
    kde = np.fft.fftshift(np.fft.irfft(np.fft.rfft(kernel) * np.fft.rfft(hist_n)))
    kde /= np.trapz(kde, x=bin_centers)
    return bin_centers, kde


def initialize_clusters(ref_file, subject, color, threshold, n_bins, keep_old_clusters=True):
    if keep_old_clusters and os.path.isfile(ref_file):
        df_cluster_ref = pd.read_pickle(ref_file)
        cluster_list = np.zeros(n_bins)
        if (subject, color, threshold, n_bins) in df_cluster_ref.columns:
            list_imported = df_cluster_ref[subject, color, threshold, n_bins].to_list()[0]
            cluster_list[:len(list_imported)] = list_imported
    else:
        cluster_list = np.zeros(n_bins)
    return cluster_list


def write_clusters_ref(cluster_list, file_ref, subject, color, threshold, n_bins):
    if os.path.isfile(file_ref):
        df_ref = pd.read_pickle(file_ref)
    else:
        df_ref = pd.DataFrame()
    df_ref[subject, color, threshold, n_bins] = [cluster_list]
    pd.to_pickle(df_ref, file_ref)


def write_cluster_overlay_images(dataframe_files, dataframe_data, dir_save,
                                 cluster_key='cluster_alt', save_png=True, save_tiff=False):
    if not os.path.exists(dir_save):
        os.makedirs(dir_save)

    # Create unique keys
    dataframe_clusters = dataframe_data.copy()
    dataframe_clusters['unique_keys'] = pd.factorize(dataframe_clusters[cluster_key])[0] + 1

    for row in dataframe_files.itertuples(index=False):
        # Load raw image
        raw_image = io.imread(os.path.join(row.directory, row.filename_raw))

        # Get Image subset
        df_image = dataframe_clusters.loc[dataframe_clusters['slide_nr'] == row.slide_nr]

        # Drop pixels if not assigned
        df_image = df_image.dropna(subset=[cluster_key])

        # Create overlay mask from dataframe
        overlay_image = csr_matrix((df_image['unique_keys'], (df_image['height'], df_image['width'])),
                                   raw_image.shape).toarray()
        # Create labeled image
        labeled_image = label2rgb(overlay_image, raw_image, bg_label=0)

        # Get n_bins
        n_bins = len(dataframe_clusters['height_bins'].unique())

        if save_tiff:
            # Save the overlay image as .tiff
            filename_save = '_'.join(
                [f"ClusterOverlay_bins-{n_bins}"] + row.filename_raw.split('_')[1:])
            io.imsave(os.path.join(dir_save, filename_save), labeled_image, check_contrast=False)
            print(f"Saved labeled Image to {os.path.join(dir_save, filename_save)}")

        if save_png:
            # Save the overlay image as .png
            filename_save_png = '_'.join(
                [f"ClusterOverlay_bins-{n_bins}"] + row.filename_raw.split('_')[1:])
            filename_save_png = '.'.join([filename_save_png.split('.')[0], 'png'])
            labeled_image_ubyte = img_as_ubyte(labeled_image)
            io.imsave(os.path.join(dir_save, filename_save_png), labeled_image_ubyte, check_contrast=False)
            print(f"Saved labeled Image to {os.path.join(dir_save, filename_save_png)}")


def get_duplicates(input_list):
    seen = {}
    duplicates = []
    for element in input_list:
        if element not in seen:
            seen[element] = 1
        else:
            if seen[element] == 1:
                duplicates.append(element)
            seen[element] += 1
    return duplicates


def split_list(list, ind):
    return list[0:ind], list[ind:]


def circular_distance(alpha, beta):
    # Get smaller arc length min(alpha-beta, 2Pi-(alpha-beta))
    return np.pi - abs(np.pi - abs(alpha - beta))


def join_KDE_bins_circular(dataframe, join_sigma=1.5, print_joining=False):
    data = dataframe.copy()
    n_height_bins = len(data['height_bins'].unique())
    # Convert [0,1] to radian
    data[['direction_bins', 'direction_std']] = data[['direction_bins', 'direction_std']] * 2 * np.pi

    # Create np.ufunc from circular distance
    ufunc_circ_dist = np.frompyfunc(circular_distance, 2, 1)

    # Initiate needed variables
    data['neuron_bundle'] = np.nan
    letters = [chr(x) for x in range(ord('a'), ord('z') + 1)]
    open_letters = letters + [x + y for x in letters for y in letters]
    height_grouped = data.groupby(['height_bins'])
    for height_ind in range(n_height_bins):
        current_group = height_grouped.groups[height_ind]
        if height_ind == 0:
            # Initiate first group assignment
            data.loc[current_group, 'neuron_bundle'], open_letters = split_list(open_letters, len(current_group))

        else:
            # Assign letters to current height bin if the fall into peak+-std of previous bins,
            # split and assign new ones where necessary

            # Initiate needed variables
            current_peaks = data.loc[current_group]['direction_bins'].to_numpy()
            previous_group = height_grouped.groups[height_ind - 1]
            previous_peaks = data.loc[previous_group]['direction_bins'].to_numpy()
            previous_std = data.loc[previous_group]['direction_std'].to_numpy()

            # check if current bins fall into peak+-std of previous bins
            # tester = abs(ufunc_circ_dist.outer(current_peaks, previous_peaks))
            bin_continuation = (abs(ufunc_circ_dist.outer(current_peaks,
                                                          previous_peaks)) - join_sigma * previous_std) < 0
            current_matches, previous_matches = np.where(bin_continuation)

            # Print for monitoring
            if print_joining:
                print(f"Bin joining at: {height_ind}")
                print(bin_continuation)

                # Initiate needed variables
            current_bundle = [''] * len(current_group)
            # Assign letters to splitting groups
            if len(np.unique(current_matches)) != len(current_matches):

                # UNSURE HOW TO HANDLE COMBINATION
                # Example
                # Previous:     0   1   2   3
                # Connections:  | \ |      /
                # Current:      0   1     2
                # Current implementation: p2 ends, c2 inherits p3, c0 inherits p0, c1 inherits p0 AND p1
                # Alternative p2 ends, c2 inherits p3, c0 NEW branch, c1 inherits p0 AND p1
                duplicate_list = get_duplicates(current_matches)
                for duplicate in duplicate_list:
                    index_list = [index for index, value in enumerate(current_matches) if value == duplicate]
                    previous_labels = data.loc[
                        previous_group[previous_matches[index_list]], 'neuron_bundle']
                    current_bundle[duplicate] = '_'.join(previous_labels.to_list())
                    bin_continuation[duplicate, :] = False

                current_matches, previous_matches = np.where(bin_continuation)

            # Assign letters to non-splitting, continuing groups
            for ii, current_match in enumerate(current_matches):
                current_bundle[current_match] = data.loc[previous_group[previous_matches[ii]], 'neuron_bundle']
                # [ if i in previous_matches
                #               else x for i, x in enumerate(current_bundle)]
            # Assign letters to new groups
            n_unassigned = current_bundle.count('')
            if n_unassigned > 0:
                new_letters, open_letters = split_list(open_letters, n_unassigned)
                current_bundle = [new_letters.pop(0) if x == '' else x for x in current_bundle]

            # Write current letters to dataframe
            data.loc[current_group, 'neuron_bundle'] = current_bundle

    # Join 'neuron_bundle' with original dataframe
    dataframe['neuron_bundle'] = data['neuron_bundle']
    return dataframe


def run_KDE_analysis(dataframe, n_kde_bins=20, bin_key='height', plot_it=True, reverse_order=False, join_sigma=1.5):
    try:
        print(f"Subject: {dataframe['subject'].iat[0]} Color:{dataframe['color'].iat[0]}")
    except KeyError:
        pass

    # Cut data frame in n_cluster_bins bins along height of full image stack
    kde_bin_key = f"{bin_key}_bins"
    kde_bins = pd.cut(dataframe[bin_key], bins=n_kde_bins, labels=False)
    if reverse_order:
        kde_bins[:] = kde_bins[::-1]

    dataframe = dataframe.join([kde_bins.rename(kde_bin_key)])

    print('Running kernel density estimates...')
    if plot_it:
        plt.figure()
    dataframe['direction_bins'] = dataframe['direction_h'].groupby(dataframe[kde_bin_key]).apply(
        lambda x: get_KDE_circular(x, plot_it=plot_it))
    if plot_it:
        plt.show()
    dataframe_grouped = dataframe.groupby([kde_bin_key, 'direction_bins'])['direction_h'].apply(
        lambda x: st.circstd(x, high=1)).rename('direction_std').reset_index()

    print('Joining clusters...')
    dataframe_grouped = join_KDE_bins_circular(dataframe_grouped, join_sigma=join_sigma)

    print('Merging Dataframes...')
    dataframe = dataframe.join(dataframe_grouped.set_index([kde_bin_key, 'direction_bins']),
                               on=[kde_bin_key, 'direction_bins'])
    print('Done')
    return dataframe


def aggregate_bins(dataframe, grouping=None, analysis_key='raw', plot_dir=None):
    # Aggregate according to input
    if grouping is None:
        grouping = ['color', 'height_bins', 'neuron_bundle']

    grouped = dataframe.groupby(grouping)
    density = grouped[analysis_key].sum()
    area = grouped[analysis_key].count()

    # Get first line of each group
    df_grouped = grouped.first()

    # Join with aggregates
    df_grouped = df_grouped.join([density.rename('density'), area.rename('area')])
    df_grouped = df_grouped.reset_index()
    # Plot if directory is passed
    if plot_dir is not None:
        plot_dir = check_file_path(plot_dir)
        df_grouped.groupby('color').apply(lambda x: plot_densities(x, ['density', 'area'], plot_dir, grouping))

    return df_grouped


def plot_densities(dataframe, modes, save_dir, grouping):
    for mode in modes:
        plt.figure()
        plt.title(f"{dataframe['subject'].iat[0]} Color: {dataframe['color'].iat[0]}")
        sns.scatterplot(data=dataframe, x='height_bins', y=mode, hue='neuron_bundle')
        plt.legend(bbox_to_anchor=(1.01, 1), borderaxespad=0)
        if isinstance(grouping, list):
            plt_grp = ''.join([item.title()[:2] for item in grouping])
        else:
            plt_grp = grouping.title()[:2]

        plt.savefig(os.path.join(save_dir, f"IC_Color-{dataframe['color'].iat[0]}_Mode-{mode}_Group-{plt_grp}.png"))
        plt.show()


"""
Deprecated
"""
# # Connect clusters between planes
# df_cluster = df_red.groupby(['bins', 'cluster'])[
#     ['direction_h', 'direction_s', 'direction_v']].mean().dropna()
# df_cluster['cluster_alt'] = get_clusters(partial_z_score(df_cluster, ['direction_h', 'direction_s', 'direction_v']),
#                                          max_clusters=10)[0]
# df_cluster['cluster_associated'] = get_clusters(df_cluster[['direction_h', 'direction_s', 'direction_v']],
#                                                 max_clusters=df_red['cluster'].max() + 1)[0]
# df_red = df_red.join(df_cluster.drop(['direction_h', 'direction_s', 'direction_v'], axis=1), on=['bins', 'cluster'])

# # Show pre plane association clusters
# plt.figure()
# sns.scatterplot(data=df_cluster, x='direction_h', y='direction_v', hue='bins')
# plt.show()
#
# # Plot new association of cluster means
# color_list = ['bins', 'cluster_associated', 'cluster_alt']
# df_meta = df_cluster.reset_index()
# for coloring in color_list:
#     plotty_3d = df_cluster[['direction_h', 'direction_s', 'direction_v']].to_numpy()
#     fig = px.scatter_3d(
#         plotty_3d, x=0, y=1, z=2,
#         color=df_meta[coloring], labels={'color': coloring},
#         color_discrete_sequence=nrc.base_11, template='plotly_dark',
#         size_max=18
#     )
#     fig.show()
#
# # plt.figure()
# # sns.scatterplot(data=df_red, x='width', y=-df_red['height'], hue='cluster')
# # plt.show()
#
# # Full brain
# # color_list = ['bins', 'cluster_associated', 'cluster_alt']
# # for coloring in color_list:
# #     plotty_3d = df_red[['width', 'height', 'slide_nr']].to_numpy()
# #     fig = px.scatter_3d(
# #         plotty_3d, x=0, y=1, z=2,
# #         color=df_red[coloring], labels={'color': coloring},
# #         color_discrete_sequence=nrc.base_11, template='plotly_dark',
# #         size_max=18
# #     )
# #     fig.show()

# for height_ind in reversed(range(n_cluster_bins)):
#     current_group = height_grouped.groups[height_ind]
#
#     if height_ind == n_cluster_bins - 1:
#         df_direction_bins.loc[current_group]['neuron_bundle'] = np.arange(len(current_group), dtype=int)
#     else:
#         # Get previous group (!!inverse range!!)
#         current_peaks = df_direction_bins.loc[current_group]['direction_bins'].to_numpy()
#
#         previous_group = height_grouped.groups[height_ind + 1]
#         previous_peaks = df_direction_bins.loc[previous_group]['direction_bins'].to_numpy()
#         previous_std = df_direction_bins.loc[previous_group]['direction_std'].to_numpy()
#
#         # check if current bins fall into peak+-std of previous bins
#         bin_continuation = (abs(np.subtract.outer(current_peaks, previous_peaks)) - previous_std) < 0
#         matches = np.where(bin_continuation)
#
#         current_bundle = np.empty(len(current_group))
#         current_bundle.fill(np.nan)
#         if len(np.unique(matches[0])) != len(matches[0]):
#             print(f"problem {height_ind} --> how to merge properly")
#             print(bin_continuation)
#         # else:
#         #     current_bundle[matches[0]] = df_direction_bins.loc[previous_group[matches[1]], 'neuron_bundle']
#         #     df_direction_bins.loc[current_group] = current_bundle

"""
Linear KDE
"""
# Scotts factor --> maybe implement Silverman (effective N calculation)
# scotty = df_red.shape[0] ** (-1. / 5)
# testo = df_red['direction_h'].groupby(df_red['bins']).apply(lambda x: get_KDE(x, kde_bandwidth=scotty))
# hello = pd.DataFrame(testo.to_list(), columns=['mins', 'maxs'])

"""
K-Means Clustering
"""
#
# # Check for previous clustering data on specific filtering and bin number
# n_cluster_array = initialize_clusters(cluster_ref_file, 'Merida', 'red', filter_hsv_v, n_cluster_bins,
#                                       keep_old_clusters=do_get_previous_clustering)
#
# # Cluster data at each height slice
# ser_clusters = df_red[['bins', 'direction_h', 'direction_s', 'direction_v']].groupby(bins).apply(
#     lambda x: get_height_clusters(x, n_cluster_array))
# df_red = pd.concat([df_red, ser_clusters], axis=1)
#
# # Save optimal cluster number for second pass with same settings
# cluster_ref_array = (df_red.groupby(['bins'])['cluster'].max() + 1).to_numpy()
# write_clusters_ref(cluster_ref_array, cluster_ref_file, 'Merida', 'red', filter_hsv_v, n_cluster_bins)
#
