import os.path

import pandas as pd
import numpy as np
import fun_kinematic as fk
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.signal import deconvolve
from numpy.linalg import norm

def calculate_rom(ang):
    res = ang.max()-ang.min()
    return res

root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/FilesForKinematic/'
# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/FilesForKinematic'

df_allfeatures = pd.read_pickle(os.path.join(root_path, 'markers_dynamicfeautres_20200609_Padme_Brain_Injury11.pkl'))
df_alltrials = pd.read_pickle(os.path.join(root_path, 'trials_20200609_Padme_Brain_Injury11.pkl'))

df_all = df_alltrials.join(df_allfeatures)
df_trials = df_all.dropna(subset=[('meta', 'new_trials')])
df_trials=df_trials[['meta','angles','dyfeatures']]
#df_trials=df_trials.droplevel('marker', axis=1)

dd=df_trials[['meta']].droplevel('marker', axis=1)
#itv = dd[(dd['event_analog']=='start_mov') | (dd['event_analog']=='grasp')].index.tolist()
#itr = dd.index[(dd['event_analog']=='start_mov') | (dd['event_analog']=='grasp')]
itt = dd.index[~((dd['event_analog']=='start_mov') | (dd['event_analog']=='grasp'))]

dd['phase'] = dd['event_analog']
dd.loc[itt, 'phase'] = np.nan
#dd['phase'] = dd['phase'].fillna(method='ffill').fillna(method='bfill')
#dd.loc[dd.index[(dd['phase']=='start_mov')], 'phase'] = 'reach'

data_phase = dd['phase'].fillna(method='ffill').fillna(method='bfill')
data_phase[data_phase=='start_mov'] = 'reach'

df_trials[('meta','data_phase')]= data_phase
del dd, itt

df_workingon = df_trials.droplevel('marker', axis=1)
#aa=df_trials.groupby([('meta','trial_analog'),('meta','data_phase')])[['angles']].max()
aa=df_trials.groupby([('meta','trial_analog'),('meta','data_phase')])[[('angles','mcp_thumb')]].apply(lambda x: calculate_rom(x))
aa=aa.unstack()


selected_columns = df_trials[[('meta','trial_analog'),('meta','data_phase')]]
selected_columns = selected_columns.droplevel('marker', axis=1)
df_angles = selected_columns.join(df_trials.angles)

bb = df_angles.groupby(['trial_analog','data_phase'])['mcp_thumb'].apply(lambda x: calculate_rom(x)).unstack()


col_names = df_trials.angles.columns
df_res = pd.DataFrame(index=df_angles['trial_analog'].unique())
for cc in col_names:
    single_rom = df_angles.groupby(['trial_analog','data_phase'])[cc].apply(lambda x: calculate_rom(x)).unstack()
    single_rom=single_rom.rename(columns={"grasp": "rom_grasp_"+cc, "reach": "rom_reach_"+cc})
    df_res = df_res.join(single_rom)
