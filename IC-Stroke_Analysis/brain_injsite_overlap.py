import nibabel as nib
import os
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from skimage.morphology import erosion, dilation, opening, closing, white_tophat
from skimage.morphology import black_tophat, skeletonize, convex_hull_image
from skimage.morphology import disk
from scipy import ndimage


def np_from_nii(file):
    nifti = nib.load(file)
    np_array = np.array(nifti.get_fdata())
    return np_array


def calculate_overlap(x, f_list):
    vol1 = np_from_nii(x)
    intersection = []
    for ii in f_list:
        vol2 = np_from_nii(ii)
        vol1 = np.where(vol1 > 0, 1, 0)
        vol2 = np.where(vol2 > 0, 1, 0)
        unique_vol1, counts_vol1 = np.unique(vol1, return_counts=True)
        summed_vol = vol1 + vol2
        unique_sum, counts_sum = np.unique(summed_vol, return_counts=True)
        #print(counts_sum.size)
        if counts_sum.size == 3:
            aa = counts_sum[2] / counts_vol1[1]
            intersection.append(aa)
        else:
            intersection.append(1)

    #print('Infunct')
    return intersection


def flip_volume(input_df, out):
    nifti = nib.load(input_df.path)
    np_array = np.array(nifti.get_fdata())
    if input_df.mri_flip == 'yes':
        flipped_array = np_array[::-1, :, :]
    else:
        flipped_array = np_array

    example_nifti = nib.load(input_df.path)
    nifti = nib.Nifti1Image(flipped_array, example_nifti.header.get_sform(), header=example_nifti.header)
    nifti_file = os.path.join(out, f"{input_df['monkey']}_{input_df['region']}_injection.nii.gz")
    nib.save(nifti, nifti_file)
    return input_df


#path = '/Users/muscalu/Downloads/Histology_Data/Registration/Lesion'
root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/injection_volumes'
ref_file = pd.read_csv(os.path.join(root_path, 'monkey_ref_sheet.csv'), sep=';')
files_path = os.path.join(root_path,'registered_vol_flipped','PMd')

filter_phrase = '.nii.gz'
file_filter = os.path.join(files_path, '**', f"*{filter_phrase}")
file_list = glob.glob(file_filter, recursive=True)

df = pd.DataFrame({'path': file_list})
df['file_name'] = df['path'].apply(os.path.basename)
df['monkey'] = df['file_name'].str.extract(r'^([a-zA-Z0-9]+)')
df['region'] = df['file_name'].apply(lambda x: x.split('_')[1])
df = df.join(ref_file.set_index('monkey'), on=['monkey'])


subjects_list = df.monkey.to_list()
subjects_list.sort(reverse=True)
df_overlap = df.copy()
df_overlap.set_index('monkey', inplace=True)
df_overlap.sort_index(ascending=False, inplace=True)
file_list.sort(reverse=True)
df_overlap[subjects_list] = df_overlap['path'].apply(lambda x: calculate_overlap(x, file_list))

df_all = df_overlap.iloc[:, -6:]
df_all = round(df_all.transpose(), 2)

sns.heatmap(data=df_all, vmin=0, vmax=1, annot=True, cmap='gist_gray_r')
output_dir = os.path.join(root_path,'FigInjCorrelation')
plt.savefig(os.path.join(output_dir, "PMdInjOverlap.svg"))
plt.show()
