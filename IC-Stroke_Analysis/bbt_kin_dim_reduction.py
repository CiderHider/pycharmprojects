import os.path
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from numpy.linalg import norm
import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from scipy.spatial import distance
import umap
import scipy.stats as ss
import statsmodels.api as sa
import scikit_posthocs as sp


def data_preprocessing(bbt_data, time_unit, monkey_name):
    if monkey_name == 'all':
        bbt_data = bbt_data.copy()
    else:
        bbt_data = bbt_data[bbt_data['monkey'] == monkey_name]

    # Get Numeric data
    prep_data = bbt_data.select_dtypes(include=['int64', 'float64']).drop(columns=['trails_success', 'groups'])
    # drop rows with little valid data and after the columns containing NaNs
    prep_data = prep_data.dropna(thresh=len(bbt_data.columns) // 2).dropna(axis='columns')

    # Scale data (zscore)
    data_scaled = StandardScaler().fit_transform(prep_data)

    return bbt_data, data_scaled, time_unit, monkey_name


def calc_umap(orig_data, data_scaled, to_plot, u_n, out, out_name):
    if to_plot:
        # Insert mapping into dataframes
        mapper = umap.UMAP(n_neighbors=u_n)

        # Insert embedding into old dataframe at corresponding row
        umap_data = orig_data.copy()
        umap_data[['UMAP1', 'UMAP2']] = mapper.fit_transform(data_scaled)
        orig_data.loc[:, ['UMAP1', 'UMAP2']] = umap_data[['UMAP1', 'UMAP2']]
        fig, ax = plt.subplots(2, 2, sharex=True, figsize=(20, 20))
        sns.scatterplot(data=orig_data, x='UMAP1', y='UMAP2', hue='monkey', ax=ax[0, 0],
                        palette="magma")  # , size='Delta_t')
        sns.scatterplot(data=orig_data, x='UMAP1', y='UMAP2', hue='status', ax=ax[0, 1], palette="magma")
        sns.scatterplot(data=orig_data, x='UMAP1', y='UMAP2', hue='monkey', style='status', ax=ax[1, 0],
                        palette="magma")
        sns.scatterplot(data=orig_data, x='UMAP1', y='UMAP2', hue='groups', style='monkey', size='status', ax=ax[1, 1],
                        palette="magma")
        plt.savefig(os.path.join(out, (out_name + '.svg')), format='svg')
        plt.show()


def plot_pca_results(df_input, pca_extracted, fe_names, to_plot, monkey_name, ti_unit, out):
    # Calculate the centroids for plotting
    centroids = df_input[['PC1', 'PC2', 'PC3', 'groups']].groupby(['groups']).mean().reset_index()
    ax = sns.relplot(data=df_input, x='PC1', y='PC2', hue='groups', palette="magma", alpha=.5)
    bx = sns.scatterplot(data=centroids, x='PC1', y='PC2', hue='groups', palette="magma", s=200, legend=False)
    bx.set_title('PCA_' + monkey_name)
    plt.gcf().set_size_inches(12, 12)
    plt.savefig(os.path.join(out, (monkey_name + ti_unit + '_pca2dim' + '.svg')), format='svg')
    plt.show()

    loadings = pca_extracted.components_ * (np.sqrt(pca_extracted.explained_variance_).transpose().reshape(-1, 1))
    sns.heatmap(loadings.transpose(), vmin=-1, vmax=1, annot=True, cmap="vlag",
                xticklabels=['PC1', 'PC2', 'PC3'], yticklabels=fe_names)
    plt.gcf().set_size_inches(6, 10)
    plt.savefig(os.path.join(out, (monkey_name + ti_unit + '_coeffspca2dim' + '.svg')), format='svg')
    plt.show()


def calc_pca_pro_monkey(df_input, data_scaled_pca):
    # Calculate the PCA
    monkey_bbt = df_input.copy()
    pca_data = PCA(n_components=3)
    monkey_bbt[['PC1', 'PC2', 'PC3']] = pca_data.fit_transform(data_scaled_pca)
    print('Explained variation per principal component: {}'.format(pca_data.explained_variance_ratio_))
    return monkey_bbt, pca_data


def calc_euclid_dist(df_input, to_plot, monkey_name, ti_unit, out):
    df_input = df_input.copy()
    prep_euclid = df_input.select_dtypes(include=['int64', 'float64']).drop(columns=['trails_success'])
    pre_mean = prep_euclid[prep_euclid['groups'] == 0].mean(axis=0).to_numpy()

    # vec2=np.array(pre_mean).reshape(-1,1)
    prep_euclid = prep_euclid.transpose()
    # vec1=prep_euclid.iloc[:,1].to_numpy().reshape(-1,1)
    # ar=distance.cdist(vec1,vec2,'euclidean') #not working
    # dst = distance.euclidean(vec1,vec2)
    # dist = np.linalg.norm(vec1-vec2)

    # euclid_matrix = np.linalg.norm(prep_euclid.sub(pre_mean['pre_mean'], axis=0))

    df_input['euclid_dist'] = prep_euclid.apply(lambda x: np.linalg.norm(x - pre_mean))
    ax = sns.barplot(data=df_input, y='euclid_dist', x='t_binned', palette="magma")
    ax.set_title('Distance from intact in ' + monkey_name + ' over ' + ti_unit)
    ax.set_ylim([0, 2500])
    plt.savefig(os.path.join(out, (monkey_name + ti_unit + 'bbt_y_mean_distance' + '.svg')), format='svg')
    #plt.show()

    df_input.to_csv(os.path.join(out, (monkey_name + ti_unit + 'bbt_euclid_distance' + '.csv')))

    # Statistics...
    data = [df_input.loc[ids, 'euclid_dist'].values for ids in df_input.groupby('groups').groups.values()]
    H, p = ss.kruskal(*data)
    print('p-value'.format(p))
    sp.posthoc_conover(df_input, val_col='euclid_dist', group_col='groups', p_adjust='holm')

    pc = sp.posthoc_conover(df_input, val_col='euclid_dist', group_col='groups')
    heatmap_args = {'linewidths': 0.25, 'linecolor': '0.5', 'clip_on': False, 'square': True,
                    'cbar_ax_bbox': [0.80, 0.35, 0.04, 0.3]}
    sp.sign_plot(pc, **heatmap_args)
    plt.savefig(os.path.join(out, (monkey_name + ti_unit + '_significance' + '.svg')), format='svg')
    plt.show()


def color_palette_mapper(df_input, mapping_column, color_maps, map_id, col_suffix='', max_spacing=False):
    color_map = color_maps[map_id]
    if '#' in color_map:
        color_palette = sns.color_palette(f"light:{color_map}", as_cmap=True)  # .reversed()
    else:
        color_palette = sns.color_palette(color_map, as_cmap=True)

    color_list = color_palette(np.linspace(0, 0.75, df_input.shape[0]))
    ser_out = pd.Series(list(map(tuple, color_list)), index=df_input.index)
    # ser_out = df_input[mapping_column].map(color_palette)
    return ser_out.to_frame(f"color_mapping{col_suffix}")


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/ProcessedData/BBT/'
#root_dir = 'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\IC-Stroke_KinematicData\Data-Analysis\ProcessedData\BBT'

output_dir = os.path.join(root_dir, 'Figures')

ref_bbt_features = pd.read_csv(os.path.join(root_dir, 'RefBBT.csv'), sep=';')

jyn_features = pd.read_csv(os.path.join(root_dir, 'JYN_Features.csv'), sep=',')
jyn_features['monkey'] = 'JYN'
leia_features = pd.read_csv(os.path.join(root_dir, 'LEIA_Features.csv'), sep=',')
leia_features['monkey'] = 'LEIA'
padme_features = pd.read_csv(os.path.join(root_dir, 'PADME_Features.csv'), sep=',')
padme_features['monkey'] = 'PADME'

df_all = jyn_features.append(leia_features.append(padme_features))
df_bbt = ref_bbt_features.join(df_all.set_index(['monkey', 'dates']), on=['monkey', 'dates'])

df_bbt['dates'] = pd.to_datetime(df_bbt['dates'], format='%Y%m%d')
df_bbt['lesion'] = pd.to_datetime(df_bbt['lesion'], format='%Y%m%d')
df_bbt['t_delta'] = df_bbt['dates'] - df_bbt['lesion']
df_bbt['t_days'] = df_bbt['t_delta'].dt.days
df_bbt['groups'] = np.where(df_bbt['t_days'] < 0, 0, df_bbt['t_days'])

# Bin data into healthy-chronic
# status_precise = pd.to_timedelta([-100, 0, 15, 30, 90, 300], 'D')
# status_labels = ['intact', 'acute', 'late acute', 'early chronic', 'late chronic']
status_precise = pd.to_timedelta([-100, 0, 30, 90, 300], 'D')
status_labels = ['intact', 'acute', 'early chronic', 'late chronic']
df_bbt['t_binned'] = pd.cut(df_bbt['t_delta'], bins=status_precise, labels=status_labels)

df_bbt = df_bbt.dropna()
df_all = df_all.drop(columns=['dates', 'monkey'])
df_bbt = df_bbt.reset_index(drop=True)

# Cerate Color specification Dataframe
status_index = pd.MultiIndex.from_product([status_labels, df_bbt['monkey'].unique()], names=['t_binned', 'monkey'])
df_status = pd.DataFrame(index=status_index, columns=['status_precise']).reset_index()
df_status['status_id'] = np.concatenate(
    [([x] * len(df_status.iloc[:, 1].unique())) for x in range(len(df_status.iloc[:, 0].unique()))], axis=0)
df_status['status_id_unique'] = df_status.index

# Some variables
features_names = list(df_all.columns)
calc_plots = True

print("Display Results for ALL with days as time unit - mean feature")
df_mean_all = df_bbt.copy()
df_mean_all = df_mean_all.groupby(['monkey', 't_days', 'status']).mean().reset_index()
df_temp, scaled_df, timeU, monkey_n = data_preprocessing(df_mean_all, 't_days', 'all')
df_out, pca_data = calc_pca_pro_monkey(df_temp, scaled_df)
# Plot the pca results
sns.scatterplot(data=df_out, x='PC1', y='PC2', hue='groups', style='monkey',
                palette="magma", s=200, legend=True)
plt.gcf().set_size_inches(8, 8)
plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)
plt.savefig(os.path.join(output_dir, ('PCA_ALLGruppedFeatures' + '.svg')), format='svg')
plt.show()

loadings = pca_data.components_ * (np.sqrt(pca_data.explained_variance_).transpose().reshape(-1, 1))
sns.heatmap(loadings.transpose(), vmin=-1, vmax=1, annot=True, cmap="vlag",
            xticklabels=['PC1', 'PC2', 'PC3'], yticklabels=features_names)
plt.gcf().set_size_inches(6, 10)
plt.savefig(os.path.join(output_dir, ('PCALoadings_ALLGruppedFeatures' + '.svg')), format='svg')
plt.show()

# print("Dispay Results for All in One")
df_temp, scaled_df, timeU, monkey_n = data_preprocessing(df_bbt, 't_days', 'all')
df_out_all, pca_data_all = calc_pca_pro_monkey(df_temp, scaled_df)

calc_euclid_dist(df_out_all[df_out_all['monkey'] == 'JYN'], calc_plots, 'JYN_All', timeU, output_dir)
calc_euclid_dist(df_out_all[df_out_all['monkey'] == 'PADME'], calc_plots, 'PADME_All', timeU, output_dir)
calc_euclid_dist(df_out_all[df_out_all['monkey'] == 'LEIA'], calc_plots, 'LEIA_All', timeU, output_dir)

print("Dispay Results for All in One_2")
color_palettes = {'JYN': '#F0DF0D', 'PADME': '#088980', 'LEIA': '#F0DF0D'}
# severity_colors = ['#F0DF00', '#19D3C5']
severity_colors = {'mild': '#F0DF00', 'moderate': '#19D3C5'}
# markers = {'JYN': 'o', 'PADME': r"$\triangledown$", 'LEIA': r"$\bigcirc$"}
markers = {'JYN': 'o', 'PADME': r"v", 'LEIA': r"p"}

df_status['status_precise'] = df_status.groupby('monkey').apply(
    lambda x: color_palette_mapper(x, 'status_id', color_palettes, x.name))

df_status.loc[df_status['t_binned'] == 'intact', ['status_precise']] = \
    pd.Series([(0.41960784, 0.42352941, 0.42745098, 1.0) for _ in df_status.index])
# [(0.73333333, 0.72941176, 0.72941176, 1.0) for _ in df_status.index])  # '#BBBABA'

df_out_all = df_out_all.join(df_status.set_index(['t_binned', 'monkey']), on=['t_binned', 'monkey'])
df_colors = (df_out_all[['status_precise', 'status_id_unique', 'monkey']]).drop_duplicates().sort_values(
    by=['status_id_unique']).reset_index(drop=True)

df_out_all['monkey_markers'] = df_out_all['monkey'].map(markers)

df_centroids = df_out_all.groupby(['t_binned', 'monkey']).mean()
df_centroids_obj = df_out_all.select_dtypes(include=object).groupby(['t_binned', 'monkey']).first()
df_centroids = df_centroids.join(df_centroids_obj).reset_index()

'''
Plot mixed PCA with marginals
'''
plt.figure()
g = sns.jointplot(
    data=df_out_all, x='PC1', y='PC2',
    hue='status_id_unique', palette=df_colors['status_precise'].to_list(),
    height=7,
    joint_kws=dict(style=df_out_all['monkey'], markers=list(markers.values()), style_order=list(markers.keys()),
                   alpha=1, s=25),
    marginal_kws=dict(fill=False),
    legend=True
)

# g.plot_joint(sns.scatterplot, data=df_centroids,  # x='PC1', y='PC2',
#              hue='status_id_unique', palette=df_centroids['status_precise'].to_list(),
#              style='monkey', markers=list(markers.values()), style_order=list(markers.keys()),
#              s=100, alpha=1)

# In case individual point management independant of hue
for row in df_centroids.itertuples():
    g.ax_joint.scatter(row.PC1, row.PC2, color=row.status_precise, marker=row.monkey_markers, s=100, edgecolor='#54565B')

plt.savefig(os.path.join(output_dir, 'joint_PCA_ALL.svg'), format='svg')
plt.show()

'''
Plot Animal wise PCA with marginals
'''
for subject in df_colors['monkey'].unique():
    # Set non focused animals to grey
    df_subject_colors = df_colors.copy()
    df_subject_colors.loc[~(df_subject_colors['monkey'] == subject), ['status_precise']] = \
        pd.Series([(0.85882353, 0.85882353, 0.86666667, 1.0) for _ in df_subject_colors.index])

    # Get subject specific centroids
    df_subject_centroids = df_centroids.loc[df_centroids['monkey'] == subject]

    # get zoom
    pc1 = df_out_all.loc[df_out_all['monkey'] == subject, 'PC1']
    pc2 = df_out_all.loc[df_out_all['monkey'] == subject, 'PC2']
    margin = 0.1
    x_zoom = (pc1.min() * (1 + margin), pc1.max() * (1 + margin))
    y_zoom = (pc2.min() * (1 + margin), pc2.max() * (1 + margin))

    # Plot
    plt.figure()
    g = sns.jointplot(
        data=df_out_all, x='PC1', y='PC2',
        hue='status_id_unique', palette=df_subject_colors['status_precise'].to_list(),
        height=7,
        xlim=x_zoom, ylim=y_zoom,
        joint_kws=dict(style=df_out_all['monkey'], markers=list(markers.values()), style_order=list(markers.keys()),
                       alpha=0.7, s=25),
        marginal_kws=dict(fill=False),
        legend=True
    )

    # In case individual point management independant of hue
    for row in df_subject_centroids.itertuples():
        g.ax_joint.scatter(row.PC1, row.PC2, color=row.status_precise, marker=row.monkey_markers, s=100, edgecolor='#54565B')

    plt.savefig(os.path.join(output_dir, f"joint_PCA_{subject}.svg"), format='svg')
    plt.show()

print("Display Results for UMAPs")
df_temp, scaled_df, timeU, monkey_n = data_preprocessing(df_bbt, 't_days', 'all')
umap_neighbours = 50
calc_umap(df_bbt, scaled_df, False, umap_neighbours, output_dir, 'UMAP_ALL')

'''
Deprecated


sns.scatterplot(data=df_out_all[df_out_all['monkey'] == 'JYN'], x='PC1', y='PC2', hue='groups',
                palette="magma")
sns.scatterplot(data=df_out_all[df_out_all['monkey'] == 'LEIA'], x='PC1', y='PC2', hue='groups',
                palette="viridis")
sns.scatterplot(data=df_out_all[df_out_all['monkey'] == 'PADME'], x='PC1', y='PC2', hue='groups',
                palette="mako")
plt.gcf().set_size_inches(15, 15)
plt.savefig(os.path.join(output_dir, ('PCA_ALL_v2' + '.svg')), format='svg')
plt.show()

print("Display Results for PADME with days as timeUnit")
df_temp, scaled_df, timeU, monkey_n = data_preprocessing(df_bbt, 't_days', 'PADME')
df_out, pca_data = calc_pca_pro_monkey(df_temp, scaled_df)
plot_pca_results(df_out, pca_data, features_names, calc_plots, monkey_n, timeU, output_dir)
calc_euclid_dist(df_out, calc_plots, monkey_n, timeU, output_dir)

print("Display Results for JYN with days as time unit")
df_temp, scaled_df, timeU, monkey_n = data_preprocessing(df_bbt, 't_days', 'JYN')
df_out, pca_data = calc_pca_pro_monkey(df_temp, scaled_df)
plot_pca_results(df_out, pca_data, features_names, calc_plots, monkey_n, timeU, output_dir)
calc_euclid_dist(df_out, calc_plots, monkey_n, timeU, output_dir)

print("Display Results for LEIA with days as time unit")
df_temp, scaled_df, timeU, monkey_n = data_preprocessing(df_bbt, 't_days', 'LEIA')
df_out, pca_data = calc_pca_pro_monkey(df_temp, scaled_df)
plot_pca_results(df_out, pca_data, features_names, calc_plots, monkey_n, timeU, output_dir)
calc_euclid_dist(df_out, calc_plots, monkey_n, timeU, output_dir)


sns.set(style = "darkgrid")
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = df_out[df_out['monkey']=='JYN']['PC1']
y = df_out[df_out['monkey']=='JYN']['PC2']
z = df_out[df_out['monkey']=='JYN']['PC3']
ax.set_xlabel("PC1")
ax.set_ylabel("PC2")
ax.set_zlabel("PC3")
ax.scatter(x, y, z)
plt.show()
'''
