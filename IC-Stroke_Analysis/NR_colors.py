base_3 = ['#FA525B',
          '#F0DF0D',
          '#19D3C5'
          ]

base_4 = ['#F0DF0D',
          '#19D3C5',
          '#372367',
          '#FA525B'
          ]

base_5 = ['#F0DF0D',
          '#19D3C5',
          '#372367',
          '#969899',
          '#FA525B'
          ]

base_6 = ['#F0DF0D',
          '#19D3C5',
          '#372367',
          '#FA525B',
          '#6C005F',
          '#088980'
          ]

base_11 = ['#088980',
           '#6C005F',
           '#A40A11',
           '#FE9e53',
           '#218716',
           '#FFB11E',
           '#B8E600',
           '#F0DF0D',
           '#19D3C5',
           '#372367',
           '#FA525B'
           ]

greys_4 = ['#757679',
           '#969899',
           '#BBBABA',
           '#DBDBDD'
           ]

cont_3 = base_3
cont_4 = base_4
cont_5 = base_5
cont_6 = base_6
cont_11 = base_11.reverse()

heat_r = ['#088980',
          '#FDFDFD',
          '#000000'
          ]

heat_b = ['#A31B21',
          '#FDFDFD',
          '#000000'
          ]

heat_y = ['#F0DF0D',
          '#FDFDFD',
          '#000000'
          ]
