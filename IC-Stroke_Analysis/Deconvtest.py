from matplotlib import pyplot as plt
import numpy as np
import cvxpy as cp
from scipy import signal as sig
import pandas as pd


test_path = '/home/aaron/Desktop/testa.csv'
test_df = pd.read_csv(test_path)
orig = test_df['Above'].to_numpy()
orig = orig-min(orig)

# Curve to build signal out of
gauss = [1.14062601e-28, 8.01089493e-23, 1.36393531e-17, 5.62965111e-13, 5.63305721e-09, 1.36641247e-05, 8.03515837e-03, 1.14546557e+00, 3.95862723e+01, 3.31651641e+02, 6.73588500e+02, 3.31651641e+02, 3.95862723e+01, 1.14546557e+00, 8.03515837e-03, 1.36641247e-05, 5.63305721e-09, 5.62965111e-13, 1.36393531e-17, 8.01089493e-23, 1.14062601e-28]
gauss = np.array([x for x in gauss if x > (max(gauss)/1000)])
# Test data to match with
# orig = np.zeros(69)
# orig[[4, 10, 41, 42, 60]] = 1

conv = sig.convolve(gauss, orig, 'full')

deconvolved = sig.deconvolve(conv, gauss)

deco = [abs(x) if abs(x) < 5 else 0 for x in deconvolved[0]]

fig, ax = plt.subplots(2,2)
ax[0, 0].plot(orig)
ax[0, 1].plot(conv)
ax[1, 0].plot(gauss)
ax[1, 1].plot(deco)
plt.show()




"""
# Define Problem and Parameters
# peaks = cp.Variable(len(orig))#, boolean=True)
peaks = cp.Variable(len(orig), boolean=True)

constraints = [np.sum(peaks) <= len(orig),
               np.sum(peaks) >= 1
               ]
objective = cp.Minimize(cp.sum_squares(cp.conv(gauss, peaks)[0:-(len(gauss)-1)][0]-orig))
#Alternate formulation, does not work as peaks don't seem to get initiated (None)
# objective = cp.Minimize(cp.sum_squares(np.convolve(peaks.value, gauss, 'same')-orig))
prob = cp.Problem(objective, constraints)

# Solve
opt_val = prob.solve(solver='ECOS_BB')

# Info dump and plot
opt_peaks = peaks.value
print(f"Optimal value: {opt_val}")
print("Optimal var:")
print(opt_peaks)

plt.figure()
plt.plot(orig)
plt.plot(gauss)
plt.plot(np.convolve(peaks.value, gauss, 'same'))
plt.show()
"""