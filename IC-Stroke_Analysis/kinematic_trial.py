import os.path

import pandas as pd
import numpy as np
import fun_kinematic as fk
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.signal import deconvolve
from numpy.linalg import norm

"""
df_markers access:
Marker xyz => df_marker['SHO']
Marker x => df_marker['SHO', 'x']
all x => df_marker.xs('x', axis=1, level=1)

Metadata  of trials
df_marker['meta']
"""

# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/FilesForKinematic/'
# root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/FilesForKinematic/'
root_path = r'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\FilesForKinematic'

test_mat = os.path.join(root_path, '20200609_Padme_Brain_Injury11_Struct.mat')
test_c3d = os.path.join(root_path, '20200609_Padme_Brain_Injury11.c3d')
angle_reference = root_path

data_folder = root_path
path_markers = os.path.join(data_folder, 'data_markers.pkl')
path_analog = os.path.join(data_folder, 'data_analog.pkl')
path_events = os.path.join(data_folder, 'data_events.pkl')
parquet_file_paths = [path_markers, path_analog]
mat_dict = fk.load_mat(test_mat)
df_events_mat = fk.get_events_mat(mat_dict['structTrials'])
keep_old_df = True

# Import data from c3d files
if all(os.path.isfile(x) for x in parquet_file_paths) and keep_old_df:
    df_markers = pd.read_pickle(path_markers)
    df_analog = pd.read_pickle(path_analog)
    # df_events = pd.read_parquet(path_parquet_events)

else:
    df_markers, df_analog, df_events = fk.import_c3d(test_c3d, save_path=data_folder)

# Linearly interpolate marker data according to time vector (index)
df_markers = df_markers.interpolate(method='time')

# Get trials from marked events
df_events_mat = fk.get_trials(df_events_mat['trial_mat']).to_frame(name='trial_mat').join([df_events_mat['event_mat']])
df_events_mat.columns = pd.MultiIndex.from_product([['meta'], df_events_mat.columns.tolist()])
df_markers = df_markers.join([df_events_mat])

df_on_off = fk.get_paired_on_off(df_analog['Voltage.Kuka_Trigger'])
df_markers = pd.merge_asof(df_markers, df_on_off, left_index=True, right_index=True,
                           tolerance=pd.Timedelta("10ms"))
df_markers[('meta', 'trial_analog')] = fk.get_trials(df_markers[('meta', 'trial_analog')])
df_markers[('meta', 'index_analog')] = df_markers.groupby(('meta', 'trial_analog'), dropna=False).apply(
    lambda x: fk.reindex_rel(x)).droplevel(0)

# Save pickle
save_path, file_name = os.path.split(test_c3d)
file_name = file_name.split('.')[0]
df_markers.to_pickle(os.path.join(save_path, f"markers_{file_name}.pkl"))
df_analog.to_pickle(os.path.join(save_path, f"analog_{file_name}.pkl"))

# test = df_markers.
# trials = df_markers.groupby(level=['xyz'], axis=1).apply(lambda x: plotter(x))

# plt.figure()
# plotting = df_markers.reset_index()
# sns.lineplot(data=df_markers, x=df_markers.index, y='SHO')

# event_list = ['beginningOfMovement', 'Pulling', 'endOfMovement', 'LightON', 'LightOFF', 'Grasping']
#
# df_concat = pd.DataFrame(columns=event_list)
# for trial in event_array:
#     df_temp = pd.DataFrame({key: [trial[key]] for key in event_list})
#     df_concat = pd.concat([df_concat, df_temp], ignore_index=True)

# df_test = df_markers.dropna(subset=[('meta', 'trial_analog')])
# angle_test = get_angles(df_test[['WRA', 'F12', 'F13']], 'flexion')
# plt.figure()
# plt.plot(angle_test)
# plt.show()

# Calculate angles of interest
df_ref = pd.read_csv(os.path.join(angle_reference, 'angle_ref.csv'))
for row in df_ref.itertuples(index=False):
    angle_list = [row.marker_1, row.marker_2, row.marker_3, row.marker_4]
    angle_list = [x for x in angle_list if isinstance(x, str)]
    angles = fk.get_angles(df_markers[angle_list], angle_mode=row.joint_mode,
                           inverse=row.inverse)
    if row.unclip == 'unclip':
        angles = fk.unclip_angles(angles)

    df_markers[('angles', row.angle_name)] = angles
    # angular velocity
    df_markers[('dyfeatures', f"ang_vel_{row.angle_name}")] = np.append([0], np.diff(angles))

df_markers_deg = df_markers['angles'] / np.pi * 180
maxs = df_markers_deg.max(axis=0)
mins = df_markers_deg.min(axis=0)
stds = df_markers_deg.std(axis=0)
df_check = maxs.to_frame('max').join([mins.rename('min'), stds.rename('std')])

# Calculate velocity, acceleration, jerk of the wrist markers :WRB
df_markers[('dyfeatures', 'velocity')] = np.append([0], np.sqrt(
    np.power(np.diff(df_markers[('WRB', 'x')]), 2) + np.power(np.diff(df_markers[('WRB', 'y')]), 2) + np.power(
        np.diff(df_markers[('WRB', 'z')]), 2)))

df_markers[('dyfeatures', 'acceleration')] = np.append([0], np.diff(df_markers[('dyfeatures', 'velocity')]))
df_markers[('dyfeatures', 'jerk')] = np.append([0], np.diff(df_markers[('dyfeatures', 'acceleration')]))

# Calculate distance between points
df_ref_dist = pd.read_csv(os.path.join(root_path, 'dist_points.csv'))
for row in df_ref_dist.itertuples(index=False):
    points_list = [row.marker_1, row.marker_2]
    df_markers[('dyfeatures', f"dist_{row.dist_name}")] = fk.get_points_distances(df_markers[points_list])
df_markers[('dyfeatures', 'hand_opening')] = norm(
    np.cross(fk.get_vectors(df_markers[['F13', 'F22']]), fk.get_vectors(df_markers[['F13', 'F53']]))) / 2

save_path = root_path

df_trials = df_markers[['angles', 'dyfeatures']]

file_name = 'dynamicfeautres_20200609_Padme_Brain_Injury11'
df_trials.to_pickle(os.path.join(save_path, f"markers_{file_name}.pkl"))

# Plotting angles for testing
angles = df_markers[['angles', 'meta']].copy()
melted_angles = angles.reset_index().melt(id_vars=['index', ('meta', 'trial_analog'), ('meta', 'index_analog')],
                                          value_name='angle').set_index(
    'index')
melted_angles.columns = [col[1] if isinstance(col, tuple) else col for col in melted_angles.columns]
melted_angles = melted_angles.loc[melted_angles['marker'] == 'angles']
melted_angles = melted_angles.dropna()
melted_angles['angle'] = melted_angles['angle'].convert_dtypes()  # the melt seems to convert angles to objects...
plt.plot()
g = sns.FacetGrid(data=melted_angles, row='xyz', hue='trial_analog')
# g.map_dataframe(sns.scatterplot, x='index_analog', y='angle')
g.map_dataframe(sns.lineplot, x='index_analog', y='angle', ci=None)
plt.show()
