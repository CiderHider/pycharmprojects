import pandas as pd
import numpy as np
from glob import glob
import os
import seaborn as sns
from matplotlib import pyplot as plt


def color_palette_mapper(df_input, mapng_column, color_maps, map_id, col_suffix='', max_spacing=False):
    color_map = color_maps[map_id]
    if '#' in color_map:
        color_palette = sns.color_palette(f"light:{color_map}", as_cmap=True)  # .reversed()
    else:
        color_palette = sns.color_palette(color_map, as_cmap=True)

    color_list = color_palette(np.linspace(0, 0.75, df_input.shape[0]))
    ser_out = pd.Series(list(map(tuple, color_list)), index=df_input.index)
    # ser_out = df_input[mapping_column].map(color_palette)
    return ser_out.to_frame(f"color_mapping{col_suffix}")


# Set data path
data_folder = 'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\Brinkman'
files = glob(os.path.join(data_folder, '*.csv'))

out_figures = 'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\IC-Stroke_KinematicData\Data-Analysis\Figures\Performance'

# Collect Data
df_brinkman = pd.DataFrame()
for file in files:
    # Read and clean csv
    df_temp = pd.read_csv(file, skiprows=3)
    df_temp = df_temp.loc[df_temp['Date'] > 0]
    # Adjust dtypes
    df_temp['Date'] = pd.to_datetime(df_temp['Date'], format='%Y%m%d')
    for idx in [1, 2]:
        df_temp[df_temp.columns[-idx]] = pd.to_numeric(df_temp[df_temp.columns[-idx]], errors='coerce')

    # subject data
    info_extra = pd.read_csv(file, nrows=2, dtype=str, header=None)[2].to_list()
    df_temp['monkey'] = info_extra[0].upper()
    df_temp['lesion'] = pd.to_datetime(info_extra[1])

    # concatenate files
    df_brinkman = pd.concat([df_brinkman, df_temp])

df_brinkman.reset_index(drop=True)

df_brinkman['t_delta'] = df_brinkman['Date'] - df_brinkman['lesion']
df_brinkman['t_days'] = df_brinkman['t_delta'].dt.days
df_brinkman['groups'] = np.where(df_brinkman['t_days'] < 0, 0, df_brinkman['t_days'])

# Bin data into healthy-chronic
# status_precise = pd.to_timedelta([-100, 0, 15, 30, 90, 300], 'D')
# status_labels = ['healthy', 'acute', 'late acute', 'early chronic', 'late chronic']
status_precise = pd.to_timedelta([-100, 0, 30, 90, 300], 'D')
status_labels = ['intact', 'acute', 'subacute', 'chronic']
df_brinkman['t_binned'] = pd.cut(df_brinkman['t_delta'], bins=status_precise, labels=status_labels)
df_brinkman = df_brinkman.dropna(subset=['t_binned'])
# Set color and marker palettes
color_palettes = {'JYN': '#F0DF0D', 'PADME': '#088980', 'LEIA': '#F0DF0D', 'REY': '#088980', 'HH03': '#FA525B',
                  'HH06': '#FA525B', 'HH11': '#FA525B'}
severity_colors = {'mild': '#F0DF00', 'moderate': '#19D3C5', 'severe': '#FA525B'}
markers = {'JYN': 'o', 'PADME': 'v', 'LEIA': 'p', 'REY': '^', 'HH03': 'H', 'HH06': 'h', 'HH11': '*'}

# Create Color specification Dataframe
status_index = pd.MultiIndex.from_product([status_labels, df_brinkman['monkey'].unique()], names=['t_binned', 'monkey'])
df_status = pd.DataFrame(index=status_index, columns=['status_precise']).reset_index()
df_status['status_id'] = np.concatenate(
    [([x] * len(df_status.iloc[:, 1].unique())) for x in range(len(df_status.iloc[:, 0].unique()))], axis=0)
df_status['status_id_unique'] = df_status.index

df_status['status_precise'] = df_status.groupby('monkey').apply(
    lambda x: color_palette_mapper(x, 'status_id', color_palettes, x.name))

df_status.loc[df_status['t_binned'] == 'intact', ['status_precise']] = \
    pd.Series([(0.41960784, 0.42352941, 0.42745098, 1.0) for _ in df_status.index])

df_brinkman = df_brinkman.join(df_status.set_index(['t_binned', 'monkey']), on=['t_binned', 'monkey'])
df_colors = (df_brinkman[['status_precise', 'status_id_unique', 'monkey']]).drop_duplicates().sort_values(
    by=['status_id_unique']).reset_index(drop=True)

plt.figure()
sns.scatterplot(data=df_brinkman, x='t_days', y='Successfull grasp rate',
                hue='status_id_unique', palette=df_colors['status_precise'].to_list(),
                style='monkey', markers=list(markers.values()), style_order=list(markers.keys()),
                s=100, alpha=1)
plt.xlim(-50, 250)
plt.gcf().set_size_inches(10, 6)

plt.savefig(os.path.join(out_figures, f"Brinkman_Performance.svg"))
plt.show()

plt.figure()
sns.scatterplot(data=df_brinkman, x='t_days', y='Grasps per minute',
                hue='status_id_unique', palette=df_colors['status_precise'].to_list(),
                style='monkey', markers=list(markers.values()), style_order=list(markers.keys()),
                s=100, alpha=1)
plt.xlim(-50, 250)
plt.gcf().set_size_inches(10, 6)

plt.savefig(os.path.join(out_figures, f"Brinkman_gpm_Performance.svg"))
plt.show()