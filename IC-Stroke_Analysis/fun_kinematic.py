import pandas as pd
from scipy.io import loadmat, matlab
import numpy as np
import os.path
from matplotlib import pyplot as plt
import seaborn as sns
from numpy.linalg import norm


def import_c3d(filename, data_types=('POINT', 'ANALOG', 'EVENT'), save_path=None):
    from ezc3d import c3d
    """ Reads Vicon c3d files and returns it in pd.Dataframes

        Parameters
        ----------
        filename: String
           path to c3d file

        data_types: tuple of Strings
            data_types to be returned: 'POINT', 'ANALOG' and 'EVENT'
            Default: ('POINT', 'ANALOG', 'EVENT')

        Returns
        -------
        [df_point, df_analog, df_event]

        df_point: pd.Dataframe()
            VICON marker data

        df_analog: pd.Dataframe()
            Analog data streams

        df_event: pd.Dataframe()
            Event time points

        """
    # Read c3d file
    c3d_file = c3d(filename)

    """
    Get marker data
    """
    if 'POINT' in data_types and 'POINT' in c3d_file['parameters'].keys():
        # Get marker labels
        frame_labels = c3d_file['parameters']['POINT']['LABELS']['value']

        # Extract and reshape marker data
        frame_data = c3d_file['data']['points']
        frame_data = frame_data.reshape(frame_data.shape[0] * frame_data.shape[1], frame_data.shape[2])[
                     :len(frame_labels) * 3, :]

        # Define multi index for marker data
        frame_multi_columns = pd.MultiIndex.from_product([['x', 'y', 'z'], frame_labels],
                                                         names=['xyz', 'marker']).swaplevel()

        # Create Dataframe
        df_markers = pd.DataFrame(data=frame_data.T)
        df_markers.columns = frame_multi_columns
        # df_point = pd.DataFrame(data=frame_data, index=frame_multi_index).swaplevel().transpose()

        # Set index = frame times
        frame_frequency = c3d_file['parameters']['POINT']['RATE']['value'][0]
        # df_point.index = pd.to_datetime(df_point.index * 1000 / frame_frequency, unit='ms')
        df_markers.index = pd.to_timedelta(df_markers.index * 1000 / frame_frequency, unit='ms')

        if save_path:
            path_markers = os.path.join(save_path, 'data_markers.pkl')
            df_markers.to_pickle(path_markers)
    else:
        df_markers = None
        print('No marker data found')

    """
    Get analog data
    """
    if 'ANALOG' in data_types and 'ANALOG' in c3d_file['parameters'].keys():
        analog_labels = c3d_file['parameters']['ANALOG']['LABELS']['value']

        analog_data = c3d_file['data']['analogs']
        analog_data = analog_data.reshape((analog_data.shape[1], analog_data.shape[2]))
        df_analog = pd.DataFrame(data=analog_data, index=analog_labels).transpose()

        # Set index = frame times
        analog_frequency = c3d_file['parameters']['ANALOG']['RATE']['value'][0]
        # df_analog.index = pd.to_datetime(df_analog.index * 1000 / analog_frequency, unit='ms')
        df_analog.index = pd.to_timedelta(df_analog.index * 1000 / analog_frequency, unit='ms')
        if save_path:
            path_analog = os.path.join(save_path, 'data_analog.pkl')
            df_analog.to_pickle(path_analog)
    else:
        df_analog = None
        print('No analog data found')

    """
    Get event data
    """
    if 'EVENT' in data_types and 'LABELS' in c3d_file['parameters']['EVENT'].keys():
        event_list = c3d_file['parameters']['EVENT']['LABELS']['value']
        event_data = c3d_file['parameters']['EVENT']['TIMES']['value'].T
        event_time = event_data[:, 0] * 60 + event_data[:, 1]
        # df_event = pd.DataFrame({'event': event_list, 'time': pd.to_datetime(event_time, unit='s')})
        df_event = pd.DataFrame({'event': event_list, 'time': pd.to_timedelta(event_time, unit='s')})
        if save_path:
            path_event = os.path.join(save_path, 'data_event.pkl')
            df_event.to_parquet(path_event)
    else:
        df_event = None
        print('No event data found')

    return [df_markers, df_analog, df_event]


def load_mat(filename):
    """
    This function should be called instead of direct scipy.io.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects

    Parameters
        ----------
        filename: String
           path to mat file

        Returns
        -------

        mat_dict: dictionary
            dictionary with pandas compatible imports


    Author: Jeff Lin
    https://stackoverflow.com/users/12899265/jeff-lin
    """

    def _check_vars(d):
        """
        Checks if entries in dictionary are mat-objects. If yes
        todict is called to change them to nested dictionaries
        """
        for key in d:
            if isinstance(d[key], matlab.mio5_params.mat_struct):
                d[key] = _todict(d[key])
            elif isinstance(d[key], np.ndarray):
                d[key] = _toarray(d[key])
        return d

    def _todict(matobj):
        """
        A recursive function which constructs from matobjects nested dictionaries
        """
        d = {}
        for strg in matobj._fieldnames:
            elem = matobj.__dict__[strg]
            if isinstance(elem, matlab.mio5_params.mat_struct):
                d[strg] = _todict(elem)
            elif isinstance(elem, np.ndarray):
                d[strg] = _toarray(elem)
            else:
                d[strg] = elem
        return d

    def _toarray(ndarray):
        """
        A recursive function which constructs ndarray from cellarrays
        (which are loaded as numpy ndarrays), recursing into the elements
        if they contain matobjects.
        """
        if ndarray.dtype != 'float64':
            elem_list = []
            for sub_elem in ndarray:
                if isinstance(sub_elem, matlab.mio5_params.mat_struct):
                    elem_list.append(_todict(sub_elem))
                elif isinstance(sub_elem, np.ndarray):
                    elem_list.append(_toarray(sub_elem))
                else:
                    elem_list.append(sub_elem)
            return np.array(elem_list)
        else:
            return ndarray

    data = loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_vars(data)


def get_events_mat(event_array,
                   event_list=('beginningOfMovement', 'Pulling', 'endOfMovement', 'LightON', 'LightOFF', 'Grasping'),
                   polling_frequency=100):
    df_events = pd.DataFrame(columns=event_list)
    for trial in event_array:
        df_temp = pd.DataFrame({key: [trial[key]] for key in event_list})
        df_events = pd.concat([df_events, df_temp], axis=0, ignore_index=True)

    df_events['trial_mat'] = df_events.index
    df_events = df_events.melt(id_vars=['trial_mat'], var_name='event_mat', value_name='frame_mat')
    # df_events.index = pd.to_datetime(df_events['frame_mat'] / polling_frequency, unit='s')
    df_events.index = pd.to_timedelta(df_events['frame_mat'] / polling_frequency, unit='s')
    df_events = df_events.sort_index()
    return df_events


def plotter(dataframe):
    if dataframe.name == 'meta':
        return
    else:
        # Plot Slide by Slide fiber counts
        # melted = dataframe.droplevel('xyz', axis=1).melt()
        melted = dataframe.melt()
        g = sns.FacetGrid(melted, col='asfd', row="Monkey", hue='Above_Below', aspect=3)
        g.map(sns.barplot, 'Slide_Nr', 'Neuron_Count')
        # plt.savefig('Above_Below.png')
        plt.show()


def get_trials(ser_trials, sample_rate='10ms'):
    ser_trials = ser_trials.dropna()
    bfill = ser_trials.resample(sample_rate).bfill()
    ffill = ser_trials.resample(sample_rate).ffill()
    ser_resampled = bfill.where(bfill == ffill)
    return ser_resampled


def reindex_rel(dataframe):
    dataframe.index.name = 'old_index'
    dataframe = dataframe.reset_index()
    dataframe['rel_index'] = dataframe['old_index'] - dataframe['old_index'].iat[0]
    dataframe = dataframe.set_index('old_index')
    return dataframe['rel_index']


def get_angles(points, angle_mode=None, inverse=None):
    # Get markers to calculate angles with
    m = points.columns.unique(level='marker')
    # get vectors for angle calculation
    vector1 = (points[m[1]] - points[m[0]]) / norm(points[m[1]] - points[m[0]], axis=1).reshape(-1, 1)
    vector2 = (points[m[2]] - points[m[1]]) / norm(points[m[2]] - points[m[1]], axis=1).reshape(-1, 1)

    if len(m) == 4:
        vector3 = (points[m[3]] - points[m[1]]) / norm(points[m[3]] - points[m[1]], axis=1).reshape(-1, 1)
        cross = np.cross(vector1, vector3)
        cross_norm = cross / norm(cross, axis=1).reshape(-1, 1)
        vector1 = cross_norm

    if inverse == 'inverse':
        vector1 = -vector1

    # Get timepoint wise dot product and angles
    dot_prod = (vector1 * vector2).sum(axis=1)
    raw_angles = np.arccos(dot_prod)

    if angle_mode == 'standard' or angle_mode is None:
        return raw_angles
    elif angle_mode == 'abduction':
        return raw_angles - np.pi / 2
    else:
        raise Exception(f"{angle_mode} - angle_mode not implemented")


def unclip_angles(data, shift=np.pi / 4, period=np.pi):
    # Unclips angles going below period floor and reappearing at period ceiling
    # previously clipped angles will be negative
    return (data + shift) % period - shift


def get_ROM(data):
    return max(data) - min(data)


def get_points_distances(points):
    # Get markers to calculate angles with
    n = points.columns.unique(level='marker')
    # get the points and calculate the distance
    dist = norm(points[n[1]] - points[n[0]], axis=1).reshape(-1, 1)
    return dist


def get_vectors(points):
    # Get markers to calculate angles with
    n = points.columns.unique(level='marker')
    # get the points and calculate the distance
    vec = (points[n[1]] - points[n[0]]) / norm(points[n[1]] - points[n[0]], axis=1).reshape(-1, 1)
    return vec


# Get on/off
def get_paired_on_off(ser_signal, on_duration=100, off_duration=200):
    on_signal = pd.Timedelta(f"{on_duration} ms")
    off_signal = pd.Timedelta(f"{off_duration} ms")
    signal = ser_signal.diff()
    on_off_series = signal[signal < -2] + signal[signal > 2]
    on_off_series = on_off_series.index.to_series().diff().shift(-1)
    on_off_series = on_off_series.round('100 ms')
    on_off = pd.DataFrame(columns=pd.MultiIndex.from_product([['meta'], ['light_on', 'light_off']]))
    on_off['meta', 'light_on'] = on_off_series.index.where(on_off_series == on_signal)
    on_off['meta', 'light_off'] = on_off_series.index.where(on_off_series == off_signal)

    # Get trial reaching from on to off from analog data
    next_off = on_off['meta', 'light_off'].bfill().rename('light_off')
    valid_starts = on_off['meta', 'light_on'].dropna().rename('light_on')
    interval_candidates = valid_starts.to_frame().join(next_off)
    df_intervals = interval_candidates.groupby('light_off')['light_on'].max().reset_index()
    df_intervals = df_intervals.melt(ignore_index=False).reset_index().set_index('value').sort_index()
    df_intervals.columns = pd.MultiIndex.from_tuples([('meta', 'trial_analog'), ('meta', 'event_analog')])

    return df_intervals
