import os.path
import pandas as pd
import numpy as np
import glob

from matplotlib import pyplot as plt
import seaborn as sns
from scipy.stats import linregress


def color_palette_mapper(df_input, mapping_column, color_maps, map_id, col_suffix='', max_spacing=False):
    color_map = color_maps[map_id]
    if '#' in color_map:
        color_palette = sns.color_palette(f"light:{color_map}", as_cmap=True)  # .reversed()
    else:
        color_palette = sns.color_palette(color_map, as_cmap=True)

    color_list = color_palette(np.linspace(0, 0.75, df_input.shape[0]))
    ser_out = pd.Series(list(map(tuple, color_list)), index=df_input.index)
    # ser_out = df_input[mapping_column].map(color_palette)
    return ser_out.to_frame(f"color_mapping{col_suffix}")


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/ProcessedData'
histo_ref = pd.read_csv(os.path.join(root_dir, 'histo_values.csv'), sep=',')
recovery_ref = pd.read_csv(os.path.join(root_dir, 'recovery_plateau.csv'), sep=';')
output_dir = os.path.join(root_dir, 'Figures')


color_palettes = {'JYN': '#F0DF0D', 'PADME': '#088980', 'LEIA': '#F0DF0D', 'REY': '#088980'}
g = sns.catplot(x="monkey", y="days_recovery", col="object",
                data=recovery_ref, kind="bar", height=4, aspect=.7,
                order=['Rey',"Padme","Jyn","Leia"])

plt.savefig(os.path.join(root_dir, "RecoveredPerformance.svg"), format='svg')
plt.show()

markers = {'Jyn': 'o', 'Padme': 'v', 'Leia': 'p', 'Rey': '^'}

markers_df = pd.DataFrame.from_dict(markers,orient='index',columns=['marker_type']).rename_axis('monkey').reset_index()
recovery_ref=recovery_ref.join(markers_df.set_index('monkey'), on=['monkey'])


df_all = recovery_ref.join(histo_ref.set_index('monkey'),on='monkey')

df_part = df_all[(df_all["brain_region"]=='M1') & (df_all["object"]=='SC')]


slope, intercept, r, p, se = linregress(df_part.days_recovery, df_part.density_norm)
print('days_recovery = {:.2f} * density_norm + {:.2f}'.format(slope, intercept))
print('p = {:.4f}, r = {:.4f}'.format(p, r))
sns.lmplot(y='density_norm', x='days_recovery', data=df_part,ci=None,
           order=1, scatter_kws={"s": 80})
plt.show()

df_all.to_csv('PerfvsHisto.csv')