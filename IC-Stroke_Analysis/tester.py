import pandas as pd
import numpy as np

col1 = np.zeros(10)
col2 = np.zeros(10)
col1[[0, 1, 5, 8]] = 1
col2[[3, 6, 7, 9]] = 1

df = pd.DataFrame({'start': col1, 'stop': col2})

ends = df.index.to_series().where(df['stop'].ne(0))
starts = df.index.to_series().where(df['start'].ne(0))
next_end = ends.bfill().rename('end')
valid_starts = starts.dropna().rename('start')
valid_starts = starts.dropna().rename('start').dropna()
valid_starts = starts.dropna().rename('start')
candidates = valid_starts.to_frame().join(next_end, how='left')
intervals = candidates.groupby('end')['start'].max().reset_index().astype(int)

