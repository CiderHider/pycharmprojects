import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from glob import glob
import os


def get_distances(df_input):
    ser_out = df_input - df_input.mean(axis=0)
    ser_out = (ser_out ** 2).sum(axis=1)
    return ser_out


landmarks = ['Eye_R', 'Eye_L', 'AC', 'V4', 'Ga', 'CCp']
data_folder = 'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\MRI\Frame_consistency'
files = glob(os.path.join(data_folder, '**/*.fcsv'))

df_frame = pd.DataFrame()
for file in files:
    df_temp = pd.read_csv(file, skiprows=2)
    df_temp['subject'] = os.path.basename(file).split('_')[0]
    df_frame = pd.concat([df_frame, df_temp])

df_frame = df_frame.reset_index(drop=True)

id_col = df_frame.columns.to_list()[0]

df_frame = df_frame.loc[~(df_frame['subject'] == 'Rey')]
df_frame = df_frame.loc[df_frame[id_col] < 7]
df_frame = df_frame[['subject', id_col, 'x', 'y', 'z']]

# df_frame['distance'] = df_frame.groupby(['subject', id_col])['x', 'y', 'z'].apply(lambda x: get_distances(x))
ser_dist = df_frame.groupby(['subject', id_col]).apply(lambda x: get_distances(x))
df_frame = df_frame.join(ser_dist.reset_index(level=[0, 1], drop=True).rename('distance'))

df_mean = df_frame.groupby(['subject', id_col]).mean().reset_index()

plt.figure()
g = sns.catplot(data=df_mean, x=id_col, y='distance', hue='subject') #, markers=['o', 'p', 'v', 'v'])
g.set_xticklabels(landmarks)
plt.savefig(os.path.join(data_folder, 'Frame_distances.svg'))
plt.show()

dist_mean = df_mean['distance'].mean()
dist_std = df_mean['distance'].std()
