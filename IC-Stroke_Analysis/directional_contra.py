# Imports
from fun_directional_analysis import *
# import plotly.io as pio
"""
SETTINGS
"""
n_width_bins = 40
keep_old_df = True
do_get_previous_clustering = False
save_images = False

# Rendering 3d Plots in Browser
# pio.renderers.default = 'browser'

# File references
on_laptop = True
on_win_desktop = False

"""
SCRIPT
"""

# def collect_da_data(data_path='/media/aaron/Squirrel500/00_Merida/MeridaBDAAnnotation/output/DirectionAnalysis'):
if on_laptop:
    data_folder = '/media/aaron/Squirrel500/Merida/MeridaBDAAnnotation/output/DirectionAnalysis'
elif on_win_desktop:
    data_folder = 'F:\Merida\MeridaBDAAnnotation\output\DirectionAnalysis'
else:
    data_folder = ''

data_folder = check_file_path(data_folder)

print('Getting image file list...')
df_files = get_file_list_df(data_folder)
print('Done')

# Assimilate data or read it from existing file
print('Collecting Data...')
path_parquet = os.path.join(data_folder, 'data.parquet')
if os.path.isfile(path_parquet) and keep_old_df:
    df_all = pd.read_parquet(path_parquet)
    print('Imported saved Parquet file')
else:
    df_all = collect_data(df_files)
    df_all.to_parquet(path_parquet)
    print('Data import complete. Saved to Parquet file for upcoming executions.')

# Get color specific Data
df_red = df_all.loc[df_all['color'] == 'red'].copy()
df_green = df_all.loc[df_all['color'] == 'green'].copy()

# Run kernel density analysis
df_red = run_KDE_analysis(df_red, n_kde_bins=n_width_bins)
df_green = run_KDE_analysis(df_green, n_kde_bins=n_width_bins)

# Plot results
modes = ['raw', 'area']
groupings = ['width_bins', ['width_bins', 'neuron_bundle']]
dir_figures = os.path.join(data_folder, 'Figures')
if not os.path.isdir(dir_figures):
    os.mkdir(dir_figures)
for combination in [(m, g) for m in modes for g in groupings]:
    plot_bins(df_red, mode=combination[0], grouping=combination[1], save_dir=dir_figures)
    plot_bins(df_green, mode=combination[0], grouping=combination[1], save_dir=dir_figures)

# Save overlay Images
if save_images:
    df_files_red = df_files.loc[df_files['color'] == 'red']
    write_cluster_overlay_images(df_files_red, df_red, os.path.join(data_folder, 'Output'),
                                 cluster_key='neuron_bundle')
    df_files_green = df_files.loc[df_files['color'] == 'green']
    write_cluster_overlay_images(df_files_green, df_green, os.path.join(data_folder, 'Output'),
                                 cluster_key='neuron_bundle')
