import os.path
import pandas as pd
import numpy as np
import glob

from matplotlib import pyplot as plt
import seaborn as sns


def color_palette_mapper(df_input, mapping_column, color_maps, map_id, col_suffix='', max_spacing=False):
    color_map = color_maps[map_id]
    if '#' in color_map:
        color_palette = sns.color_palette(f"light:{color_map}", as_cmap=True)  # .reversed()
    else:
        color_palette = sns.color_palette(color_map, as_cmap=True)

    color_list = color_palette(np.linspace(0, 0.75, df_input.shape[0]))
    ser_out = pd.Series(list(map(tuple, color_list)), index=df_input.index)
    # ser_out = df_input[mapping_column].map(color_palette)
    return ser_out.to_frame(f"color_mapping{col_suffix}")


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/ProcessedData'
root_dir_files = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/ProcessedData/Performance'
monkey_ref = pd.read_csv(os.path.join(root_dir, 'MonkeyRef.csv'), sep=';')
output_dir = os.path.join(root_dir, 'Figures')

file_list = glob.glob(os.path.join(root_dir_files, '*.csv'))

df_all = pd.DataFrame()
for file in file_list:
    file_name = os.path.split(file)[1]
    data = pd.read_csv(file, sep=';')
    data['monkey'] = file_name.split('_')[0]
    data['task'] = file_name.split('_')[1]
    df_all = df_all.append(data)

# Merge data with the reference information
df_all = df_all.join(monkey_ref.set_index(['monkey']), on=['monkey'])

# Calculate necessary parameters
df_all['dates'] = pd.to_datetime(df_all['dates'], format='%Y%m%d')
df_all['lesion'] = pd.to_datetime(df_all['lesion'], format='%Y%m%d')
df_all['t_delta'] = df_all['dates'] - df_all['lesion']
df_all['t_days'] = df_all['t_delta'].dt.days
df_all['groups'] = np.where(df_all['t_days'] < 0, 0, df_all['t_days'])

# Bin data into healthy-chronic
status_precise = pd.to_timedelta([-100, 0, 30, 90, 300], 'D')
status_labels = ['intact', 'acute', 'late acute', 'chronic']
df_all['t_binned'] = pd.cut(df_all['t_delta'], bins=status_precise, labels=status_labels)
df_all['success_rate'] = df_all['Success'] / df_all['totalAttempts']
df_all['success_rate'] = df_all['success_rate'].fillna(0)

color_palettes = {'JYN': '#F0DF0D', 'PADME': '#088980', 'LEIA': '#F0DF0D', 'REY': '#088980'}
severity_colors = {'mild': '#F0DF00', 'moderate': '#19D3C5'}
markers = {'JYN': 'o', 'PADME': 'v', 'LEIA': 'p', 'REY': '^'}

# Create Color specification Dataframe
status_index = pd.MultiIndex.from_product([status_labels, df_all['monkey'].unique()], names=['t_binned', 'monkey'])
df_status = pd.DataFrame(index=status_index, columns=['status_precise']).reset_index()
df_status['status_id'] = np.concatenate(
    [([x] * len(df_status.iloc[:, 1].unique())) for x in range(len(df_status.iloc[:, 0].unique()))], axis=0)
df_status['status_id_unique'] = df_status.index


df_status['status_precise'] = df_status.groupby('monkey').apply(
    lambda x: color_palette_mapper(x, 'status_id', color_palettes, x.name))

df_status.loc[df_status['t_binned'] == 'intact', ['status_precise']] = \
    pd.Series([(0.41960784, 0.42352941, 0.42745098, 1.0) for _ in df_status.index])

df_all = df_all.join(df_status.set_index(['t_binned', 'monkey']), on=['t_binned', 'monkey'])
df_colors = (df_all[['status_precise', 'status_id_unique', 'monkey']]).drop_duplicates().sort_values(
    by=['status_id_unique']).reset_index(drop=True)

tasks = ['SS','SC','PN','BBT']

for ii in tasks:
    if ii == 'BBT':
        df_colors = df_colors.drop(df_colors[df_colors.monkey =='REY'].index)

    df_to_plot = df_all[df_all['task']==ii]
    ax=sns.scatterplot(data=df_to_plot, x=df_to_plot.t_days, y=df_to_plot.success_rate,
                    hue='status_id_unique', palette=df_colors['status_precise'].to_list(),
                    style='monkey', markers=list(markers.values()), style_order=list(markers.keys()),
                    s=100, alpha=1)
    plt.xlim(-50, 250)


    plt.gcf().set_size_inches(10, 6)
    ax.set_title(str('Success Rate'+ii))
    out_figures = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/Figures/Performance'
    plt.savefig(os.path.join(out_figures, f"{ii}_Performance.svg"), format='svg')
    plt.savefig(os.path.join(out_figures, f"{ii}_Performance.pdf"), format='pdf')

    plt.show()


for ii in tasks:
    if ii == 'BBT':
        df_colors = df_colors.drop(df_colors[df_colors.monkey =='REY'].index)

    df_to_plot = df_all[df_all['task']==ii].copy()
    df_to_plot = df_to_plot[df_to_plot['monkey']=='PADME']
    ax=sns.barplot(data=df_to_plot, x=df_to_plot.t_days, y=df_to_plot.success_rate,
                    palette=df_colors['status_precise'].to_list(),
                    alpha=1)
    plt.gcf().set_size_inches(10, 6)
    ax.set_title(str('Success Rate'+ii))
    out_figures = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/Figures/Performance'
    #plt.savefig(os.path.join(out_figures, f"{ii}_REYBarPerformance.svg"), format='svg')
    plt.show()



strategy_input = os.path.join(root_dir,'BBT','ForPython','Strategies')
file_list_str = glob.glob(os.path.join(strategy_input, '*.csv'))

df_st = pd.DataFrame()
for file in file_list_str:
    file_name = os.path.split(file)[1]
    data = pd.read_csv(file, sep=';')
    #data=data.T.reset_index().rename(columns={data.index.name:'movement'})
    #data=data.sum(data.iloc[:, -3:])
    data=data.sum(axis=0)
    data = data.reset_index(name='nr_mov')
    data=data.rename({'index': 'strategy'}, axis='columns')
    data['date'] = file_name.split('_')[0]
    data['monkey'] = file_name.split('_')[1]


data['percent_strategy']=data['nr_mov']/data['nr_mov'].sum(axis=0)

ax = sns.catplot(data=data, x="strategy", y="percent_strategy",
                 kind="bar", height=3, aspect=1);

plt.ylim(0, 0.5)
out_figures = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/Figures/Performance'
plt.savefig(os.path.join(out_figures, "PadmeStrategy.svg"), format='svg')
plt.show()

new_df = df_all[['t_days', 'monkey', 'task','severity','t_binned','success_rate']].copy()
new_df.to_csv('Performance.csv')