import pandas as pd
import numpy as np
import os
import glob
import SimpleITK as sitk
from skimage import io
from skimage.external.tifffile import TiffFile
import nibabel as nib
import re


def transform_annotation(array, transform):
    if transform == 0:
        return array
    sitk_image = sitk.GetImageFromArray(array)
    sitk_transformed = sitk.Resample(sitk_image, sitk_image, transform, sitk.sitkNearestNeighbor, 0.0)
    array_transformed = sitk.GetArrayFromImage(sitk_transformed)
    return array_transformed


def get_annotation_nifti(dataframe, annotation_key='image_annotation', transform_key='transform', right_hemi=True):
    annotation_shape = dataframe.loc[0, annotation_key].shape
    n_annotations = min(annotation_shape)
    idx_min = annotation_shape.index(n_annotations)
    print(f"Annotation Channels: {n_annotations} - on Axis: {idx_min}")

    slice_list = [slice(None)] * len(annotation_shape)
    for idx_annotation in range(n_annotations):
        slice_list[idx_min] = idx_annotation
        df_transform = dataframe[[annotation_key, transform_key]].copy()
        df_transform[annotation_key] = df_transform.apply(lambda x: x[annotation_key][tuple(slice_list)], axis=1)
        df_transform[annotation_key] = pad_images(df_transform[annotation_key])
        ser_annotation = df_transform.apply(
            lambda x: transform_annotation(x[annotation_key], x[transform_key]), axis=1)

        stack = np.array(ser_annotation.to_list())
        example_file_name = dataframe.loc[0, 'file_name_annotation']
        save_dir = dataframe.loc[0, 'directory_annotation']
        subject_name = re.search("^[a-zA-Z]+", example_file_name).group(0)
        example_file = os.path.join(save_dir, example_file_name)
        nifti = create_nifti(stack, example_file, right_hemi=right_hemi)
        file_name_nifti = f"{subject_name}_annotation_{idx_annotation}.nii.gz"
        nib.save(nifti, os.path.join(save_dir, file_name_nifti))


def collect_data_registration(directory, file_identifier='', file_extension='tif', get_images=True):
    file_filter = os.path.join(directory, rf"*{file_identifier}*.{file_extension}")
    file_list = glob.glob(file_filter)
    df_data = pd.DataFrame()
    df_data['file_name'] = [os.path.split(file)[1] for file in file_list]
    df_data['directory'] = directory
    df_data['slide_nr'] = df_data['file_name'].str.extract(f".*_(\d+).*.{file_extension}")
    if get_images:
        df_data['image'] = df_data.apply(lambda x: io.imread(os.path.join(x['directory'], x['file_name'])), axis=1)
    return df_data


def stack_percentile_mean(array, percentile=5, axis=0):
    rest_axis = list(range(array.ndim))
    rest_axis.remove(axis)
    percentile_l = percentile
    percentile_u = 100 - percentile
    # Get Percentiles per image (z-stack)
    percentile_list = (np.percentile(array, (percentile_l, percentile_u), axis=rest_axis)).T

    n_images = array.shape[axis]
    mean_list = np.zeros(n_images)
    for image_idx in range(n_images):
        image = array[image_idx]
        p_lower, p_upper = percentile_list[image_idx]
        mask = np.where((image > p_lower) & (image <= p_upper), True, False)
        mean_list[image_idx] = np.mean(image, where=mask)
    return mean_list


def multiply_along_axis(array, vector, axis):
    """
    Multiply n-dimensional array by 1-d vector along given axis
    adjusted from Neinstein's answer on
    https://stackoverflow.com/questions/30031828/multiply-numpy-ndarray-with-1d-array-along-a-given-axis
    """
    # ensure we're working with Numpy arrays
    array = np.array(array)
    vector = np.array(vector)

    # shape check
    if axis >= array.ndim:
        raise np.AxisError(axis, array.ndim)
    if array.shape[axis] != vector.size:
        raise ValueError(
            "Length of 'A' along the given axis must be the same as B.size"
        )

    # np.broadcast_to puts the new axis as the last axis, so
    # we swap the given axis with the last one, to determine the
    # corresponding array shape. np.swapaxes only returns a view
    # of the supplied array, so no data is copied unnecessarily.
    shape = np.swapaxes(array, array.ndim - 1, axis).shape

    # Broadcast to an array with the shape as above. Again,
    # no data is copied, we only get a new look at the existing data.
    vector_broad = np.broadcast_to(vector, shape)

    # Swap back the axes. As before, this only changes our "point of view".
    vector_broad = np.swapaxes(vector_broad, array.ndim - 1, axis)

    return array * vector_broad


def rigid_registration(array_moving, array_fixed, verbose=False, get_transform=False):
    moving = sitk.GetImageFromArray(array_moving)
    fixed = sitk.GetImageFromArray(array_fixed)

    # Initialize registration parameters
    initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                          moving,
                                                          sitk.Similarity2DTransform(),
                                                          sitk.CenteredTransformInitializerFilter.GEOMETRY)

    registration_method = sitk.ImageRegistrationMethod()

    # Similarity metric settings.
    registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.01)
    registration_method.SetInterpolator(sitk.sitkLinear)

    # Optimizer settings.
    registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                      numberOfIterations=10000,
                                                      convergenceMinimumValue=1e-6,
                                                      convergenceWindowSize=500)
    registration_method.SetOptimizerScalesFromPhysicalShift()
    registration_method.SetInitialTransform(initial_transform, inPlace=False)

    # Execute Registration
    rigid_transform = registration_method.Execute(fixed, moving)
    if verbose:
        print(f"Final metric value: {registration_method.GetMetricValue()}")
        print(f"Optimizer's stopping condition, {registration_method.GetOptimizerStopConditionDescription()}")

    # Apply Transform
    transformed = sitk.Resample(moving, fixed, rigid_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
    array_transformed = sitk.GetArrayFromImage(transformed)

    if get_transform:
        return array_transformed, rigid_transform
    else:
        return array_transformed


def pad_to_shape(image, shape_target):
    y, x = np.subtract(shape_target, image.shape)
    padder = ((y // 2, y // 2 + y % 2),
              (x // 2, x // 2 + x % 2))
    image_padded = np.pad(image, padder, mode='constant')
    return image_padded


def pad_images(ser_images, padding=100):
    ser_shapes = ser_images.apply(lambda x: x.shape)
    max_shape = pd.DataFrame(ser_shapes.to_list()).max()
    ser_padded = ser_images.apply(lambda x: pad_to_shape(x, max_shape + 2 * padding))
    return ser_padded


def create_nifti(array_zyx, example_file, right_hemi=True, voxel_depth=300):
    # Get Metadata of tif
    with TiffFile(example_file) as tif:
        assert tif.is_imagej
        # get image resolution from TIFF tags
        tags = tif.pages[0].tags
        x_resolution = tags['x_resolution'].value
        y_resolution = tags['y_resolution'].value

    # Swap axis to conform to MNI MRI coordinates
    array_zyx = array_zyx.swapaxes(0, 1)
    array_zyx = array_zyx.swapaxes(0, 2)
    array_zyx = np.flip(array_zyx, 2)
    if not right_hemi:
        array_zyx = np.flip(array_zyx, 0)

    # Set Resolution
    x_size = x_resolution[1] / x_resolution[0]
    y_size = y_resolution[1] / y_resolution[0]
    voxel_size = [y_size, voxel_depth, x_size]
    voxel_diagonal = np.diag(voxel_size + [1])
    nifti_image = nib.Nifti1Image(array_zyx, voxel_diagonal)
    nifti_image.header['xyzt_units'] = 3  # NIFTI_UNITS_MICRON : 3
    nifti_image.header['pixdim'][1:4] = np.array(voxel_size) / 1000  # Change to Micron
    # nifti_image.header['extents'] = 16384
    # nifti_image.header['regular'] = 'r'
    return nifti_image


def associate_tiff_to_MRI(file_list, mri_start, mri_stop):
    file_list.sort()

    n_files = len(file_list)
    nii_pos = [x + 1 for x in range(n_files)]
    vector_MRI = np.linspace(mri_start, mri_stop, num=n_files).round()
    association = pd.DataFrame({'File_names': file_list, 'Histo_nii_pos': nii_pos, 'MRI_slice': vector_MRI})

    return association
