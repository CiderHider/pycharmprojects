"""
NAME: Plot Above Below

DESCRIPTION: Processes and Plots the Fiberdensity/Fibercount of passing fibers from fluorescent microscopy images.
The Input data is expected to be in .csv format. One per slide/condition

AUTHOR: Aaron Brändli
Date: December 2020

"""

# Concat instead of Append
# check for return lengths of functions --> if null make same size as input

# Imports
import os
import glob
import re
import tkinter as tk
from tkinter import filedialog
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import signal as sig
from scipy import stats as st
from sklearn.cluster import KMeans
#import cvxpy as cvx

# Script settings
tubeness_wanted = 'Off'
find_cluster_n = True
use_test_data = False
log_scale_clustering = True
take_n_smallest_widths = 2

# Deconvolution Parameters see below... change
plot_it = False  # Needs a crap-ton of ram and CPU

# Data_Set specific Settings and Variables
if use_test_data:
    data_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/QuPathStuff/AboveBelowMod/Test/'
    n_clusters_kmeans = 3
else:
    data_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/QuPathStuff/AboveBelowMod/All/'
    n_clusters_kmeans = 5

monkey_sheet_dir = "/home/aaron/Dropbox/Documents/Work/Histology/MatlabBrain/"
monkey_sheet_path = "/home/aaron/Dropbox/Documents/Work/Histology/MatlabBrain/MonkeyReferenceSheet.csv"


def peak_finder(signal, threshold_s, name=''):
    # Print group name if provided
    if name:
        print(f"Peaks: {name}")

    # Initiate Dataframe
    df_peaks = pd.DataFrame(index=signal.index)
    for key in signal.columns:
        column_keys = [f"Peaks_{key}", f"Peak_Width_{key}"]
        df_key = pd.DataFrame(index=signal.index, columns=['p', 'w'])
        peak_list, properties = sig.find_peaks(signal[key], height=threshold_s)
        half_widths = sig.peak_widths(signal[key], peak_list, rel_height=0.5)[0]
        df_key['p'].iloc[peak_list] = properties['peak_heights']
        df_key['w'].iloc[peak_list] = half_widths
        df_key.columns = column_keys
        df_peaks = pd.concat([df_peaks, df_key], axis=1)

    return df_peaks.astype(float)


def deconvolver(signal, filter, name=''):
    # Print group name if provided
    if name:
        print(f"Deconvolve: {name}")
    # Deconvolve signal
    deconvolved_signal = signal.apply(lambda x: sig.deconvolve(x, filter)[1], axis=0)
    deconvolved_signal = deconvolved_signal.reindex(index=signal.index, fill_value=0)
    # Rename Columns
    deconvolved_signal.columns = ['Deconv_' + column_name for column_name in signal.columns]

    return deconvolved_signal


def partial_zscore(dataframe, z_score_list):
    assert isinstance(z_score_list, list), 'Provide a List of keys for partial_zscoring'
    if not z_score_list:
        # df_zscore = dataframe.apply(lambda x: (x-x.min())/(x.max() - x.min()), axis=0)
        df_zscore = st.zscore(dataframe)
    else:
        df_zscore = pd.DataFrame(index=dataframe.index)
        for column in z_score_list:
            df_zscore[column] = st.zscore(dataframe[column])
    return df_zscore


# Collect Data in Dataframe
if not os.path.exists(data_path):
    data_path = os.path.expanduser("~")
if not os.path.exists(monkey_sheet_dir):
    monkey_sheet_dir = os.path.expanduser("~")

# Get data to analyze
root = tk.Tk()
root.withdraw()
data_folder = filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=data_path)
if not os.path.exists(monkey_sheet_path):
    monkey_sheet_path = filedialog.askopenfilename(title="Select the Monkey Reference Sheet.",
                                                   initialdir=monkey_sheet_dir)

df_monkey_sheet = pd.read_csv(monkey_sheet_path)
staining_color_list = df_monkey_sheet['Color'].dropna().unique().tolist()
file_list = glob.glob(os.path.join(data_folder, '*.csv'))
file_list.sort()
df = pd.DataFrame()

# Collect data
for file in file_list:
    df_raw = pd.read_csv(file)
    df_temp = pd.DataFrame()
file_path, file_name = os.path.split(file)

tubeness_file = (re.search('Tube(.*).csv', file_name)).group(1)
# if tubeness_file != tubeness_wanted:
#     continue

monkey_name = (re.search('(.*)_Above', file_name)).group(1)
staining_color = [color for color in staining_color_list if (color in file_name)]
# print(staining_color)
# staining_color = (re.search('\d{2}_(.*)_Tube', file_name)).group(1)
region = df_monkey_sheet.loc[
    (df_monkey_sheet['Monkey'] == monkey_name) & (df_monkey_sheet['Color'] == staining_color[0]), "Region"].tolist()
threshold = df_monkey_sheet.loc[
    (df_monkey_sheet['Monkey'] == monkey_name) & (
            df_monkey_sheet['Color'] == staining_color[0]), "Threshold"].tolist()
df_temp['Above'] = df_raw['Above']
df_temp['Below'] = df_raw['Below']
df_temp['Background'] = df_raw['Background']
df_temp['Monkey'] = monkey_name
df_temp['Tubeness'] = tubeness_file
df_temp['Slide_Nr'] = int((re.search(r'\d+', file_name)).group(0))
df_temp['File_Name'] = file_name
df_temp['File_Path'] = file_path
df_temp['Color'] = staining_color[0]
df_temp['Region'] = region[0]
df_temp['Threshold'] = threshold[0]
df_temp['X'] = df_raw.index

# get Peaks etc
peak_df = peak_finder(df_temp[['Above', 'Below']], df_temp['Threshold'].iloc[0])
df_temp = pd.concat([df_temp, peak_df], axis=1)
# Append to full dataframe
df = df.append(df_temp, ignore_index=True)

# Get Scope of data to be analyzed
monkey_list = df['Monkey'].unique()
slide_nrs = set(df['Slide_Nr'].unique())

# Fill in missing slides (Empty, 1 row) to produce even dataset for ease of plotting
df_augmenter = df.loc[df['X'] == 0]
df_augmenter = df_augmenter.set_index(["Monkey", "Region", "Slide_Nr"]).unstack(fill_value=0).stack().reset_index()
df_augmenter = df_augmenter[df.columns.tolist()]
df_augmenter[['File_Name', 'File_Path', 'Color', 'Tubeness']] = 'Augment'
df_augmenter = df_augmenter.loc[df_augmenter['Above'] == 0]
df = df.append(df_augmenter, ignore_index=True)
# Reset Index -> homogenize dataset
df.reset_index()

# Group dataset into single Slides to analyze --> (extract Above Below and Threshold)
grouped = df.groupby(['Monkey', 'Region', 'Slide_Nr'], sort=False)

# Get accumulated Data (group needs to be reinitiated bc of added columns!)
grouped_counts = df.groupby(['Monkey', 'Region', 'Slide_Nr'], as_index=False)[['Peaks_Above', 'Peaks_Below']].count()

# # Plot Slide by Slide fiber counts
# melted_counts = grouped_counts.melt(
#     id_vars=['Monkey', 'Region', 'Slide_Nr'], value_vars=['Peaks_Above', 'Peaks_Below'],
#     var_name='Above_Below', value_name='Neuron_Count')
# g = sns.FacetGrid(melted_counts, col="Region", row="Monkey", hue='Above_Below', aspect=3)
# g.map(sns.barplot, 'Slide_Nr', 'Neuron_Count')
# plt.savefig('Above_Below.png')
# # plt.show()
#
# # Plot overall slide counts
# grouped_sums = grouped_counts.groupby(['Monkey', 'Region'], as_index=False)[['Peaks_Above', 'Peaks_Below']].sum()
# grouped_sums['Ratio'] = grouped_sums['Peaks_Below'] / grouped_sums['Peaks_Above']
# grouped_sums = grouped_sums.set_index(["Monkey", "Region"]).unstack(fill_value=0).stack().reset_index()
# f = sns.FacetGrid(grouped_sums, row='Region', aspect=3)
# f.map(sns.barplot, 'Monkey', 'Ratio')
# plt.savefig('Above_Below_Ratio.png')
# # plt.show()

if plot_it:
    for monkey in monkey_list:
        melted_monkey = df.loc[df['Monkey'] == monkey].melt(
            id_vars=['Monkey', 'Region', 'Slide_Nr', 'X'], value_vars=['Above', 'Below'],
            var_name='Above_Below', value_name='Intensity')
        melted_monkey['Peaks'] = df.loc[df['Monkey'] == monkey].melt(
            id_vars=['Monkey', 'Region', 'Slide_Nr', 'X'], value_vars=['Peaks_Above', 'Peaks_Below'],
            var_name='Above_Below', value_name='Peaks')['Peaks']
        g = sns.FacetGrid(melted_monkey, col="Region", row="Slide_Nr", hue='Above_Below')
        g.map(sns.lineplot, 'X', 'Intensity')
        g.map(sns.scatterplot, 'X', 'Peaks')
        # sns.lineplot(x='X', y='Above', hue='Slide_Nr', data=padme)
        # sns.swarmplot(x='Slide_Nr', y='Above', data=df[df['Tubeness'] == ('Off')])
        plt.show()

# Plot Amplitudes vs widths

melted_peaks = df.melt(value_vars=['Peaks_Above', 'Peaks_Below'], value_name='Peaks', var_name='Above_Below')
melted_widths = df.melt(value_vars=['Peak_Width_Above', 'Peak_Width_Below'], value_name='Width', var_name='Above_Below')
melted_pw = pd.concat([melted_peaks, melted_widths['Width']], axis=1).dropna()
melted_pw = melted_pw.drop((melted_pw[melted_pw['Peaks'] == 0]).index)

# plt.figure()
# sns.scatterplot(data=melted_pw, x='Width', y='Peaks', hue='Above_Below')
# plt.show()

plt.figure()
sns.displot(melted_pw, x='Width', y='Peaks', hue='Above_Below')  # , binwidth=5)
plt.show()

cluster_data = melted_pw[['Peaks', 'Width']]
if log_scale_clustering:
    np.log10(cluster_data)
cluster_data = partial_zscore(cluster_data, ['Width'])

if find_cluster_n:
    # get optimal clusternumber for K means using Elbow method --> optimal n at 'knee'
    n_clusters = range(1, 20)
    kmeans = [KMeans(n_clusters=i) for i in n_clusters]
    score = [kmeans[i].fit(cluster_data).score(cluster_data) for i in range(len(kmeans))]
    plt.plot(n_clusters, score)
    plt.xlabel('Number of Clusters')
    plt.ylabel('Score')
    plt.title('Elbow Curve')
    plt.savefig('KMeans_opt_cluster_Nr.png')
    plt.show()

kmeans = KMeans(n_clusters=n_clusters_kmeans, random_state=42).fit(cluster_data)

melted_pw['Cluster'] = kmeans.labels_
shapes = melted_pw.groupby(['Cluster'])[['Peaks', 'Width']].median()

plt.figure()
sns.displot(data=melted_pw, x='Width', y='Peaks', hue='Cluster', log_scale=log_scale_clustering)  # , binwidth=5)
sns.scatterplot(data=shapes, x='Width', y='Peaks')
plt.savefig('Spike_Type_Clustering.png')
plt.show()

plt.figure()
sns.displot(melted_pw, x='Width', y='Peaks', hue='Cluster')  # , binwidth=5)
plt.show()

selected_shapes = shapes.sort_values(by='Width').iloc[np.arange(take_n_smallest_widths)]

gauss_kernel_diam = 10
kernel_x = np.arange(-gauss_kernel_diam, gauss_kernel_diam + 1)
filter_gauss = selected_shapes.apply(lambda x: x['Peaks'] * np.exp(-4 * np.log(2) * (kernel_x / x['Width']) ** 2),
                                     axis=1)
# filter_gauss = filter_gauss.apply(lambda x: x/np.sum(x))

plt.figure()
for i in np.arange(take_n_smallest_widths):
    plt.plot(filter_gauss.iloc[i])
plt.show()

"""
Straight deconvolution does not seem to give the insight wanted
"""
# Deconvolve Slida data with estimated Neuron with (Gauss)
# gauss_kernel_width = 21
# gauss_sigma = 1
# kernel_x = np.linspace(-4, 4, gauss_kernel_width)
# filter_gauss = np.exp(-(kernel_x / float(gauss_sigma)) ** 2)
#
# deconvolved = grouped.apply(lambda x: deconvolver(x[['Above', 'Below']], filter_gauss.iloc[0]))  # , x.name))
# deconvolved.index = df.index  # Used to get rid of multiindex... check if consistent order
# df = pd.concat([df, deconvolved], axis=1)

"""
Integer Pprogramming approach to chose the right curve to approximate signal
CVXPY
"""
