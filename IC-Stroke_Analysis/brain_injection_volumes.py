import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import os


folder = 'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\HistoProcessedData\injection_volumes'
df_injection = pd.read_csv(os.path.join(folder, 'Injection_Volume_Summary.csv'))

plt.figure()
sns.barplot(data=df_injection, x='brain_region', y='vol_tot', ci=None)
sns.swarmplot(data=df_injection, x='brain_region', y='vol_tot', hue='monkey')
plt.savefig(os.path.join(folder, 'Injection_volumes.svg'))
plt.show()