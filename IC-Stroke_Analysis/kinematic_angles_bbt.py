import os.path

import pandas as pd
import numpy as np
import fun_kinematic as fk
from numpy.linalg import norm

"""
df_markers access:
Marker xyz => df_marker['SHO']
Marker x => df_marker['SHO', 'x']
all x => df_marker.xs('x', axis=1, level=1)

Metadata  of trials
df_marker['meta']
"""


def get_angles(points, angle_mode=None, inverse=None, v_static_norm=[0, 0, 1]):
    # redirect to secondary function in case of suppination calculation
    if angle_mode == 'suppination':
        return get_angle_suppination(points)

    # Get markers to calculate angles with
    m = points.columns.unique(level='marker')
    # get vectors for angle calculation
    vector1 = (points[m[1]] - points[m[0]]) / norm(points[m[1]] - points[m[0]], axis=1).reshape(-1, 1)

    if len(m) > 2:
        vector2 = (points[m[2]] - points[m[1]]) / norm(points[m[2]] - points[m[1]], axis=1).reshape(-1, 1)
    else:
        vector2 = pd.DataFrame(columns=vector1.columns, index=vector1.index, data=[v_static_norm] * vector1.shape[0])

    if len(m) == 4:
        vector3 = (points[m[3]] - points[m[1]]) / norm(points[m[3]] - points[m[1]], axis=1).reshape(-1, 1)
        # Get normal vector to plane spanned by v1 and v3
        cross = np.cross(vector1, vector3)
        cross_norm = cross / norm(cross, axis=1).reshape(-1, 1)
        vector1 = cross_norm

    if inverse == 'inverse':
        vector1 = -vector1

    # Get timepoint wise dot product and angles
    dot_prod = (vector1 * vector2).sum(axis=1)
    raw_angles = np.arccos(dot_prod)

    if angle_mode == 'standard' or angle_mode is None:
        return raw_angles
    elif angle_mode == 'abduction':
        return raw_angles - np.pi / 2
    else:
        raise Exception(f"{angle_mode} - angle_mode not implemented")


def get_angle_suppination(points):
    # Get markers to calculate angles with
    m = points.columns.unique(level='marker')
    # Get mid point of wrist
    points_ab = (points[m[2]] + points[m[3]]) / 2
    # get vectors for angle calculation (naming kept from general function)
    vector1 = (points[m[1]] - points[m[0]]) / norm(points[m[1]] - points[m[0]], axis=1).reshape(-1, 1)
    vector3 = (points_ab - points[m[1]]) / norm(points_ab - points[m[1]], axis=1).reshape(-1, 1)
    # Get normal vector to plane spanned by v1 and v3
    cross = np.cross(vector1, vector3)
    cross_norm = cross / norm(cross, axis=1).reshape(-1, 1)
    vector1 = cross_norm

    # Get wrist vector
    vector_ab = (points[m[3]] - points[m[2]]) / norm(points[m[3]] - points[m[2]], axis=1).reshape(-1, 1)

    # Get timepoint wise dot product and angles
    dot_prod = (vector1 * vector_ab).sum(axis=1)
    raw_angles = np.arccos(dot_prod)
    return raw_angles - np.pi / 2


# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/FilesForKinematic/'
# root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/FilesForKinematic/'
root_path = r'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\FilesForKinematic'

test_mat = os.path.join(root_path, '20200609_Padme_Brain_Injury11_Struct.mat')
test_c3d = os.path.join(root_path, '20200609_Padme_Brain_Injury11.c3d')
angle_reference = root_path

data_folder = root_path
path_markers = os.path.join(data_folder, 'data_markers.pkl')
path_analog = os.path.join(data_folder, 'data_analog.pkl')
path_events = os.path.join(data_folder, 'data_events.pkl')
parquet_file_paths = [path_markers, path_analog]
mat_dict = fk.load_mat(test_mat)
df_events_mat = fk.get_events_mat(mat_dict['structTrials'])
keep_old_df = True

# Import data from c3d files
if all(os.path.isfile(x) for x in parquet_file_paths) and keep_old_df:
    df_markers = pd.read_pickle(path_markers)


else:
    df_markers, df_analog, df_events = fk.import_c3d(test_c3d, save_path=data_folder)

# Linearly interpolate marker data according to time vector (index)
df_markers = df_markers.interpolate(method='time')

df_ref = pd.read_csv(os.path.join(angle_reference, 'angle_ref_bbt.csv'))
for row in df_ref.itertuples(index=False):
    angle_list = [row.marker_1, row.marker_2, row.marker_3, row.marker_4]
    angle_list = [x for x in angle_list if isinstance(x, str)]
    angles = get_angles(df_markers[angle_list], angle_mode=row.joint_mode,
                        inverse=row.inverse)

    df_markers[('angles', row.angle_name)] = angles
    # angular velocity
    df_markers[('dyfeatures', f"ang_vel_{row.angle_name}")] = np.append([0], np.diff(angles))

df_markers_deg = df_markers['angles'] / np.pi * 180
