import os
from nipype.interfaces import fsl
from nipype import Node, Workflow, Function
import glob
from nipype.interfaces.dcm2nii import Dcm2niix
import nibabel as nib

"""
Functions
"""


def writer(in_file):  # Can be exchanged for DataSink
    return in_file


def hard_swap_axis(in_file, axis=0):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    array = np.flip(array, axis=axis)
    flipped_hemi = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_axSwap.nii.gz")
    nib.save(flipped_hemi, out_file)
    return out_file


"""
Inputs
"""
# # Centroid for BET [pixels]- Jyn: [100,100,150], Leia: [100, 110, 170], Padme: [100, 100, 175]
# bet_center = [100, 70, 175]
# # Brain radius [pixels]
# bet_radius = 37

input_dirs = [
    '/mnt/Shared/MRI/Planning/Jyn',
    '/mnt/Shared/MRI/Planning/Leia',
    '/mnt/Shared/MRI/Planning/Padme',
    # '/mnt/Shared/MRI/Planning/Rey',
]

input_atlas = r'/mnt/Shared/BRAIN_Alignment/Atlas/Fascicularis_MNI/cyno_18_model-MNI.nii'
input_atlas_mask = r'/mnt/Shared/BRAIN_Alignment/Atlas/Fascicularis_MNI/Brain-label.nii.gz'
"""
Computation nodes
"""

# frame_bet_one = Node(fsl.BET(
#     center=bet_center,
#     radius=bet_radius
# ),
#     name='frame_BET_one')

# frame_flirt = Node(fsl.FLIRT(cost='mutualinfo',
#                              # cost_func='mutualinfo',
#                              dof=12,
#                              output_type="NIFTI_GZ",
#                              ),
#                    name='Frame_FLIRT')

atlas_flirt = Node(fsl.FLIRT(
    cost='normcorr',
    # cost='mutualinfo',
    dof=12,
    output_type="NIFTI_GZ",
),
    name='Atlas_FLIRT')

frame_apply_mask = Node(fsl.maths.ApplyMask(output_type="NIFTI_GZ"),
                        name='apply_brain_mask_frame')

atlas_apply_mask = Node(fsl.maths.ApplyMask(output_type="NIFTI_GZ"),
                        name='apply_brain_mask_atlas')

frame_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('z', '-y', 'x')),
    name='frame_swap_dim'
)

mask_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('z', '-y', 'x')),
    name='mask_swap_dim'
)

target_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('z', '-y', 'x')),
    name='target_swap_dim'
)

xfm_target_to_atlas = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                           name='apply_transforms_target_atlas')

""" Input Nodes """

mni_atlas = Node(name='mni_Atlas',
                 interface=Function(input_names=["in_file"],
                                    output_names=["out_file"],
                                    function=writer)
                 )

frame_MRI = Node(name='frame_MRI',
                 interface=Function(input_names=["in_file"],
                                    output_names=["out_file"],
                                    function=writer)
                 )

target = Node(name='targets',
              interface=Function(input_names=["in_file"],
                                 output_names=["out_file"],
                                 function=writer)
              )

write_frame = Node(name='write_frame',
                   interface=Function(input_names=["in_file"],
                                      output_names=["out_file"],
                                      function=writer)
                   )
"""
Run
"""
for input_dir in input_dirs:
    wf = Workflow(name="Frame_alignment")

    wf.connect([
        # Apply skull stripping masks
        (mni_atlas, atlas_apply_mask, [("out_file", "in_file")]),
        (frame_MRI, frame_apply_mask, [("out_file", "in_file")]),
        # Swap dims brain and target
        (frame_apply_mask, frame_swap_dim, [("out_file", "in_file")]),
        (target, target_swap_dim, [("out_file", "in_file")]),
        # FLIRT frame with post-mortem MRI
        (frame_swap_dim, atlas_flirt, [("out_file", "in_file")]),
        (atlas_apply_mask, atlas_flirt, [("out_file", "reference")]),
        # Write FLIRT
        (atlas_flirt, write_frame, [("out_file", "in_file")]),
        # Apply transforms to target --> atlas
        (atlas_flirt, xfm_target_to_atlas, [("out_matrix_file", "in_matrix_file")]),
        (target_swap_dim, xfm_target_to_atlas, [("out_file", "in_file")]),
        (mni_atlas, xfm_target_to_atlas, [("out_file", "reference")]),
    ])

    # Atlas
    mni_atlas.inputs.in_file = input_atlas
    atlas_apply_mask.inputs.mask_file = input_atlas_mask
    wf.base_dir = input_dir
    # Targets
    # Target nifti
    target.inputs.in_file = glob.glob(os.path.join(input_dir, 'Target', r'*.nii.gz'))[0]

    # Frame MRI
    frame_MRI.inputs.in_file = glob.glob(os.path.join(input_dir, 'NII', r'*.nii.gz'))[0]

    # Brain Mask
    frame_apply_mask.inputs.mask_file = glob.glob(os.path.join(input_dir, 'Masks', r'*Brain*.nii.gz'))[0]

    # Apply artifact mask if it exists
    artifact_file = glob.glob(os.path.join(input_dir, 'Masks', r'*Ignore*.nii.gz'))
    if artifact_file:
        wf.connect(mask_swap_dim, "out_file", atlas_flirt, "in_weight")
        mask_swap_dim.inputs.in_file = artifact_file[0]
    # Write overview and detailed graph to dot and png
    wf.write_graph("workflow_graph.dot", graph2use='flat')

    # # Run sequentially
    # wf.run()

    # Run it in parallel
    allocated_cpus = os.cpu_count() - 1
    wf.run('MultiProc', plugin_args={'n_procs': allocated_cpus})
