# IO Imports
import os
import glob
import datetime as dt
from tkinter import filedialog
from tkinter import *

# Data Processing Imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

# ML Imports
import sklearn
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_validate
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures

# Collect Data in Dataframe
initial_path = '/home/aaron/Dropbox/Documents/Work/Data Analysis/Corellation'
if not os.path.exists(initial_path):
    initial_path = os.path.dirname(os.path.realpath(__file__))
root = Tk()
root.withdraw()
data_path = filedialog.askopenfilename(initialdir=initial_path, title='Select File to Analyze')
root.destroy()

df = pd.read_csv(data_path)

#  Prepare Data
y = df['meanPerSession']
df_names = pd.get_dummies(df['Monkey'])
df_X_select = df.drop(['Monkey', 'SNR', 'Putamen', 'Caudate', 'meanPerSession', 'stdPerSession'], axis=1)
X = pd.concat([df_names, df_X_select], axis=1)

# Split Data into Test and Trainingset
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=10)

# Define Linear Models to test
ml_models = {'lm': LinearRegression(),
             'lm_deg2': Pipeline([('poly_transformer', PolynomialFeatures(degree=2)),
                                  ('lm', LinearRegression())]),
             'lm_deg3': Pipeline([('poly_transformer', PolynomialFeatures(degree=3)),
                                  ('lm', LinearRegression())]),
             'lm_deg4': Pipeline([('poly_transformer', PolynomialFeatures(degree=4)),
                                  ('lm', LinearRegression())]),
             'lm_deg5': Pipeline([('poly_transformer', PolynomialFeatures(degree=5)),
                                  ('lm', LinearRegression())])
             }
# Fold Split for Cross validation
kf_results = []
kfs = KFold(n_splits=5, shuffle=True, random_state=42)
for i_f, (ix_train, ix_test) in enumerate(kfs.split(X_train)):
    # Loop over models
    for mod_name, mod in ml_models.items():
        # Define training and testing folds
        X_training_folds = X_train.iloc[ix_train]
        y_training_folds = y_train.iloc[ix_train]
        X_test_fold = X_train.iloc[ix_test]
        y_test_fold = y_train.iloc[ix_test]
        # Fit the model on the training folds
        mod.fit(X_training_folds, y_training_folds)
        # Test on both the training and testing folds to check for over-/under-fitting
        y_pred_train = mod.predict(X_training_folds)
        y_pred_test = mod.predict(X_test_fold)
        # R2
        kf_results.append({'model': mod_name, 'fold': i_f, 'stage': 'train', 'scorer': 'r2',
                           'val': r2_score(y_training_folds, y_pred_train)})
        kf_results.append({'model': mod_name, 'fold': i_f, 'stage': 'test', 'scorer': 'r2',
                           'val': r2_score(y_test_fold, y_pred_test)})
        # MSE
        kf_results.append({'model': mod_name, 'fold': i_f, 'stage': 'train', 'scorer': 'MSE',
                           'val': -mean_squared_error(y_training_folds, y_pred_train)})
        kf_results.append({'model': mod_name, 'fold': i_f, 'stage': 'test', 'scorer': 'MSE',
                           'val': -mean_squared_error(y_test_fold, y_pred_test)})
kf_results_df = pd.DataFrame(kf_results)

# Get cv train AND test scores
cv_scores = {}
for mod_name in ml_models.keys():
    cv_scores[mod_name] = cross_validate(ml_models[mod_name], X_train, y_train, cv=kfs,
                                         scoring=['r2', 'neg_mean_squared_error'],
                                         return_train_score=True, n_jobs=-1)


def crossval_to_df(cv_dict):
    crossval_results = []
    for model in cv_dict.keys():
        for scorer in cv_dict[model].keys():
            if scorer.startswith('train_'):
                score = scorer.replace('train_', '')
                for i_val, val in enumerate(cv_dict[model][scorer]):
                    crossval_results.append({'model': model, 'fold': i_val, 'stage': 'train',
                                             'scorer': score, 'val': val})
            elif scorer.startswith('test_'):
                score = scorer.replace('test_', '')
                for i_val, val in enumerate(cv_dict[model][scorer]):
                    crossval_results.append({'model': model, 'fold': i_val, 'stage': 'test',
                                             'scorer': score, 'val': val})
    return pd.DataFrame(crossval_results)


crossval_df = crossval_to_df(cv_scores)

for mod_name in crossval_df['model'].unique():
    kf_df = crossval_df.loc[crossval_df['model'] == mod_name]
    with sns.plotting_context("notebook", font_scale=1.2):
        g = sns.catplot(x="fold", y="val", hue="stage", col="scorer", data=crossval_df,
                        kind="bar", sharey=False, height=3, aspect=1.5, ci=None)
        g.fig.suptitle(mod_name, y=1.05)
    plt.show()


# #cat_cols_to_encode = ['country', 'gender']
# cols_to_scale = list(df_X_select.columns)
# preprocessor = make_column_transformer(
#    # (OneHotEncoder(drop='if_binary'), cat_cols_to_encode),
#     (StandardScaler(), cols_to_scale),
#     remainder='passthrough'
# )
#
# lm_pipeline = make_pipeline(
#     preprocessor,
#     LinearRegression()
# )
#
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=10)
#
# lm_pipeline.fit(X_train, y_train)
#
# y_pred = np.array(lm_pipeline.predict(X_test))
# # Scores
# recall_lr = rs(y_test, y_pred)
# precision_lr = ps(y_test, y_pred)
# f1_lr = fs(y_test, y_pred)
# acc_lr = lm_pipeline.score(X_test, y_test)
# print('*** Evaluation metrics for test dataset ***')
# print(f'Recall: {recall_lr:0.2f}, Precision Score: {precision_lr:0.2f}')
# print(f'F1 Score: {f1_lr:0.2f}, Accuracy: {acc_lr:0.2f}')
