import os
from nipype.interfaces import fsl
from nipype import Node, Workflow, Function
import glob
from nipype.interfaces.dcm2nii import Dcm2niix
import nibabel as nib

"""
Functions
"""


def writer(in_file):  # Can be exchanged for DataSink
    return in_file


def hard_swap_axis(in_file, axis=0):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    array = np.flip(array, axis=axis)
    flipped_hemi = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_axSwap.nii.gz")
    nib.save(flipped_hemi, out_file)
    return out_file


"""
Inputs
"""
# Centroid for BET [pixels]- Jyn: [100,100,150], Leia: [100, 110, 170], Padme: [100, 100, 175]
bet_center = [100, 70, 175]
# Brain radius [pixels]
bet_radius = 37

input_dirs = [
    # '/mnt/Shared/MRI/Planning/Jyn',
    # '/mnt/Shared/MRI/Planning/Leia',
    '/mnt/Shared/MRI/Planning/Padme',
    # '/mnt/Shared/MRI/Planning/Rey',
]

input_atlas = r'/mnt/Shared/BRAIN_Alignment/Atlas/Fascicularis_MNI/cyno_18_model-MNI.nii'

"""
Computation nodes
"""

atlas_bet = Node(fsl.BET(),
                 name='atlas_BET')

frame_bet_one = Node(fsl.BET(
    center=bet_center,
    radius=bet_radius
),
    name='frame_BET_one')

frame_bet_two = Node(fsl.BET(
    center=bet_center,
    radius=bet_radius + 10
),
    name='frame_BET_two')

frame_flirt = Node(fsl.FLIRT(cost='mutualinfo',
                             # cost_func='mutualinfo',
                             dof=12,
                             output_type="NIFTI_GZ",
                             ),
                   name='Frame_FLIRT')

atlas_flirt = Node(fsl.FLIRT(cost_func='normcorr',
                             # cost='normcorr',  # Might be unneccessary or detrimental
                             dof=12,
                             output_type="NIFTI_GZ",
                             ),
                   name='Atlas_FLIRT')

atlas_flirt_one = Node(fsl.FLIRT(cost_func='mutualinfo',
                                 # cost='normcorr',  # Might be unneccessary or detrimental
                                 dof=6,
                                 output_type="NIFTI_GZ",
                                 searchr_x=[-5, 5],  # Diana: 0, 15, 5
                                 searchr_y=[-20, 20],
                                 searchr_z=[-15, 15],
                                 ),
                       name='Atlas_FLIRT_x')

frame_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('z', '-y', 'x')
),
    name='frame_swap_dim'
)

"""
Input Nodes
"""

mni_atlas = Node(name='mni_Atlas',
                 interface=Function(input_names=["in_file"],
                                    output_names=["out_file"],
                                    function=writer)
                 )

ind_MRI = Node(name='ind_MRI',
               interface=Function(input_names=["in_file"],
                                  output_names=["out_file"],
                                  function=writer)
               )

frame_MRI = Node(name='frame_MRI',
                 interface=Function(input_names=["in_file"],
                                    output_names=["out_file"],
                                    function=writer)
                 )

targets = Node(name='targets',
               interface=Function(input_names=["in_file"],
                                  output_names=["out_file"],
                                  function=writer)
               )

"""
Set up Workflow
"""

wf = Workflow(name="Frame_alignment")

wf.connect([
    # (mni_atlas, atlas_bet, [("out_file", "in_file")]),
    (frame_MRI, frame_swap_dim, [("out_file", "in_file")]),
    (frame_swap_dim, frame_bet_one, [("out_file", "in_file")]),
    (frame_swap_dim, frame_bet_two, [("out_file", "in_file")]),
    # FLIRT frame with post-mortem MRI
    (frame_bet_two, atlas_flirt, [("out_file", "in_file")]),
    (mni_atlas, atlas_flirt, [("out_file", "reference")]),
    # FLIRT frame with post-mortem MRI
    (frame_bet_one, atlas_flirt_one, [("out_file", "in_file")]),
    (mni_atlas, atlas_flirt_one, [("out_file", "reference")]),
])

# Atlas
mni_atlas.inputs.in_file = input_atlas

"""
Run
"""
for input_dir in input_dirs:
    wf.base_dir = input_dir
    # Targets
    target_folder = os.path.join(input_dir, 'Target')
    target_filter = r'*.nii.gz'
    input_target = glob.glob(os.path.join(target_folder, target_filter), recursive=False)
    targets.iterables = ("in_file", input_target)

    # Frame MRI
    frame_MRI.inputs.in_file = glob.glob(os.path.join(input_dir, 'NII', r'*.nii.gz'))[0]

    # Write overview and detailed graph to dot and png
    wf.write_graph("workflow_graph.dot", graph2use='flat')

    # # Run sequentially
    # wf.run()

    # Run it in parallel
    allocated_cpus = os.cpu_count() - 1
    wf.run('MultiProc', plugin_args={'n_procs': allocated_cpus})
