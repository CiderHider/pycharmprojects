import os.path
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import glob


def color_palette_mapper(df_input, mapping_column, color_maps, map_id, col_suffix='', max_spacing=False):
    color_map = color_maps[map_id]
    if '#' in color_map:
        color_palette = sns.color_palette(f"light:{color_map}", as_cmap=True)  # .reversed()
    else:
        color_palette = sns.color_palette(color_map, as_cmap=True)

    color_list = color_palette(np.linspace(0, 0.75, df_input.shape[0]))
    ser_out = pd.Series(list(map(tuple, color_list)), index=df_input.index)
    # ser_out = df_input[mapping_column].map(color_palette)
    return ser_out.to_frame(f"color_mapping{col_suffix}")


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/ProcessedData/'
monkey_ref = pd.read_csv(os.path.join(root_dir, 'MonkeyRef.csv'), sep=';')


files_dir = os.path.join(root_dir,'ResultsKinematic/ForPython/Features_noPip/')
output_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/IC-Stroke_KinematicData/Data-Analysis/Figures/'


file_list = glob.glob(os.path.join(files_dir, '*.csv'))

df_all = pd.DataFrame()
for file in file_list:
    file_name = os.path.split(file)[1]
    data = pd.read_csv(file, sep=',')
    data['monkey'] = file_name.split('_')[0]
    df_all = df_all.append(data)


keep_same = {'dates', 'monkey'}
df_all.columns = df_all.columns.map(lambda x: 'feat_' + x if x not in keep_same else x)
list_features = df_all.drop(columns=keep_same).columns.to_list()
df_feat_melted = df_all.melt(id_vars=keep_same, value_vars=list_features, var_name='features',
                             value_name='feature_value')

# Merge data with the reference information
df_all = monkey_ref.join(df_feat_melted.set_index(['monkey']), on=['monkey'])


# Calculate necessary parameters
df_all['dates'] = pd.to_datetime(df_all['dates'], format='%Y%m%d')
df_all['lesion'] = pd.to_datetime(df_all['lesion'], format='%Y%m%d')
df_all['t_delta'] = df_all['dates'] - df_all['lesion']
df_all['t_days'] = df_all['t_delta'].dt.days
df_all['groups'] = np.where(df_all['t_days'] < 0, 0, df_all['t_days'])

# Bin data into healthy-chronic
# status_precise = pd.to_timedelta([-100, 0, 15, 30, 90, 300], 'D')
# status_labels = ['healthy', 'acute', 'late acute', 'early chronic', 'late chronic']
status_precise = pd.to_timedelta([-100, 0, 30, 90, 300], 'D')
status_labels = ['intact', 'acute', 'subacute', 'chronic']
df_all['t_binned'] = pd.cut(df_all['t_delta'], bins=status_precise, labels=status_labels)
df_all['status_labels'] = df_all['t_binned']

color_palettes = {'JYN': '#F0DF0D', 'PADME': '#088980', 'LEIA': '#F0DF0D', 'REY': '#088980'}
severity_colors = {'mild': '#F0DF00', 'moderate': '#19D3C5'}

# Create Color specification Dataframe
status_index = pd.MultiIndex.from_product([status_labels, df_all['monkey'].unique()], names=['t_binned', 'monkey'])
df_status = pd.DataFrame(index=status_index, columns=['status_precise']).reset_index()
df_status['status_id'] = np.concatenate(
    [([x] * len(df_status.iloc[:, 1].unique())) for x in range(len(df_status.iloc[:, 0].unique()))], axis=0)
df_status['status_id_unique'] = df_status.index



df_status['status_precise'] = df_status.groupby('monkey').apply(
    lambda x: color_palette_mapper(x, 'status_id', color_palettes, x.name))

df_status.loc[df_status['t_binned'] == 'intact', ['status_precise']] = \
    pd.Series([(0.41960784, 0.42352941, 0.42745098, 1.0) for _ in df_status.index])

df_all = df_all.join(df_status.set_index(['t_binned', 'monkey']), on=['t_binned', 'monkey'])
df_colors = (df_all[['status_precise', 'status_id_unique', 'monkey']]).drop_duplicates().sort_values(
    by=['status_id_unique']).reset_index(drop=True)

markers = {'JYN': 'o', 'PADME': 'v', 'LEIA': 'p', 'REY': '^'}
markers_df = pd.DataFrame.from_dict(markers,orient='index',columns=['marker_type']).rename_axis('monkey').reset_index()
df_colors=df_colors.join(markers_df.set_index('monkey'), on=['monkey'])

'''
g = sns.catplot(x="monkey", y="feature_value",
                hue='status_id_unique', col="features", col_wrap=6, width=0.8,
                data=df_all, kind="violin", sharey=False, inner=None,aspect=5,height=2,
                palette=df_colors['status_precise'].to_list())
plt.savefig(os.path.join(output_dir, 'RobotSS_AllFeaturesViolinPlot.svg'), format='svg')
#plt.show()
'''


df_all = df_all.sort_values(by=['monkey'],ascending=False)
'''
g = sns.catplot(x='monkey', y="feature_value",
                hue='status_id_unique', col="features", col_wrap=5,
                style='status_id_unique',
                data=df_all, kind="point", sharey=False,
                aspect=1, height=3, dodge=True,
                palette=df_colors['status_precise'].to_list(),
                markers=df_colors['marker_type'].to_list()
                )
plt.savefig(os.path.join(output_dir, 'RobotSS_AllFeaturesPointPlot1.svg'), format='svg')
'''

df_all =df_all[(df_all['t_binned']!='acute')&(df_all['t_binned']!='subacute')]

g = sns.catplot(x='monkey', y="feature_value",
                hue='status_id_unique', col="features", col_wrap=5,
                style='status_id_unique',
                data=df_all, kind="point", sharey=False,
                aspect=1, height=3, dodge=True,
                palette=df_colors['status_precise'].to_list(),
                markers=df_colors['marker_type'].to_list(),join=False
                )
plt.savefig(os.path.join(output_dir, 'RobotSS_AllFeaturesAdjusted.svg'), format='svg')
plt.savefig(os.path.join(output_dir, 'RobotSS_AllFeaturesAdjusted.pdf'), format='pdf')

plt.show()
