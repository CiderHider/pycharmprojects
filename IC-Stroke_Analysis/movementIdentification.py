import pandas as pd
import numpy as np
from fun_kinematic import import_c3d, load_mat, get_events_mat
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.signal import deconvolve


def plotter(dataframe):
    if dataframe.name == 'meta':
        return
    else:
        # Plot Slide by Slide fiber counts
        # melted = dataframe.droplevel('xyz', axis=1).melt()
        melted = dataframe.melt()
        g = sns.FacetGrid(melted, col='asfd', row="Monkey", hue='Above_Below', aspect=3)
        g.map(sns.barplot, 'Slide_Nr', 'Neuron_Count')
        # plt.savefig('Above_Below.png')
        plt.show()


def get_trials(ser_trials, sample_rate='10ms'):
    bfill = ser_trials.resample(sample_rate).bfill()
    ffill = ser_trials.resample(sample_rate).ffill()
    ser_resampled = bfill.where(bfill == ffill)
    return ser_resampled


test_mat = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/FilesForKinematic/20200609_Padme_Brain_Injury11_Struct.mat'
test_c3d = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/FilesForKinematic/20200609_Padme_Brain_Injury11.c3d'

mat_dict = load_mat(test_mat)
df_events_mat = get_events_mat(mat_dict['structTrials'])

df_markers, df_analog, df_events = import_c3d(test_c3d)
df_events_mat = get_trials(df_events_mat['trial_mat']).to_frame(name='trial_mat').join([df_events_mat['event_mat']])
df_events_mat.columns = pd.MultiIndex.from_product([['meta'], df_events_mat.columns.tolist()])
df_markers = df_markers.join([df_events_mat])

# Get on/off
on_signal = pd.Timedelta('100 ms')
off_signal = pd.Timedelta('200 ms')
signal = df_analog['Voltage.Kuka_Trigger'].diff()
on_off_series = signal[signal < -2] + signal[signal > 2]
on_off_series = on_off_series.index.to_series().diff().shift(-1)
on_off_series = on_off_series.round('100 ms')
on_off = pd.DataFrame(columns=pd.MultiIndex.from_product([['meta'], ['on', 'off']]))
on_off['meta', 'on'] = on_off_series.index.to_series().where(on_off_series == on_signal)
on_off['meta', 'off'] = on_off_series.index.to_series().where(on_off_series == off_signal)

# Get trial reaching from on to off from analog data
next_off = on_off['meta', 'off'].bfill().rename('off')
valid_starts = on_off['meta', 'on'].dropna().rename('on')
interval_candidates = valid_starts.to_frame().join(next_off)
intervals = interval_candidates.groupby('off')['on'].max().reset_index()
intervals = intervals.melt(ignore_index=False).reset_index().set_index('value').sort_index()
trial_analog = get_trials(intervals['index']).to_frame()
trial_analog.columns = pd.MultiIndex.from_tuples([('meta', 'trial_analog')])
df_markers = pd.merge_asof(df_markers, trial_analog, left_index=True, right_index=True,
                           tolerance=pd.Timedelta("10ms"))

# test = df_markers.
# trials = df_markers.groupby(level=['xyz'], axis=1).apply(lambda x: plotter(x))

# plt.figure()
# plotting = df_markers.reset_index()
# sns.lineplot(data=df_markers, x=df_markers.index, y='SHO')

# event_list = ['beginningOfMovement', 'Pulling', 'endOfMovement', 'LightON', 'LightOFF', 'Grasping']
#
# df_concat = pd.DataFrame(columns=event_list)
# for trial in event_array:
#     df_temp = pd.DataFrame({key: [trial[key]] for key in event_list})
#     df_concat = pd.concat([df_concat, df_temp], ignore_index=True)

