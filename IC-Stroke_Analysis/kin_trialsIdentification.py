import os.path
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.signal import deconvolve
import scipy as sc
import pandas as pd


def calculate_robot_min(sig):
    # print(sig)
    # s = sig[::-1]
    id_s = sig.idxmin()
    # print(id_s)
    return id_s


def get_trials(ser_trials, sample_rate='10ms'):
    ser_trials = ser_trials.dropna()
    bfill = ser_trials.resample(sample_rate).bfill()
    ffill = ser_trials.resample(sample_rate).ffill()
    ser_resampled = bfill.where(bfill == ffill)
    return ser_resampled


root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/FilesForKinematic/'
# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/FilesForKinematic'

analogDF = pd.read_pickle(os.path.join(root_path, 'analog_20200609_Padme_Brain_Injury11.pkl'))
df_markers = pd.read_pickle(os.path.join(root_path, 'markers_20200609_Padme_Brain_Injury11.pkl'))

lightOn = df_markers.index[df_markers['meta', 'event_analog'] == 'light_on']  # without .loc
lightOff = df_markers.index[df_markers['meta', 'event_analog'] == 'light_off']

df = df_markers['WRB']
thX = 0.8 * df_markers['WRB', 'x'].max()  # frame 5584 - wrist aligned with the bar
df['x'].loc[df['x'] <= thX] = thX

peaksX, _ = sc.signal.find_peaks(df['x'], height=df['x'].max() * 0.95)
# merge a series to a dataframe transform it to df,
# adjust column names to match the multilevel dataframe names=('marker', 'xyz') = levels
df_peaksX = df['x'][peaksX].to_frame()
df_peaksX.columns = pd.MultiIndex.from_tuples([('meta', 'peaks_x')], names=('marker', 'xyz'))
df_markers = df_markers.join(df_peaksX)

'''
plt.figure()
a = df.melt(var_name="xyz", value_name="Timing", ignore_index=False)
# sns.lineplot(data=a, x=a.index, y='Timing', hue='xyz')
plt.plot(df['x'].to_numpy())
plt.plot(peaksX, df['x'][peaksX], "x")
# plt.plot(lightOn.to_numpy().astype(int)/10000000, df['x'][lightOn.to_numpy().astype(int)/10000000],"x")
plt.show()
'''
# take light on and light off moments
lightOnOff = df_markers.index[
    (df_markers['meta', 'event_analog'] == 'light_on') | (
            df_markers['meta', 'event_analog'] == 'light_off')]  # without .loc

# Calculate each time the signal in x passes the threshold
signal_x = df_markers[('WRB', 'x')] > thX
changes_x = signal_x ^ signal_x.shift()
sig_changes = changes_x.to_frame()
frames_of_change = sig_changes.index[sig_changes[('WRB', 'x')] == True].to_frame()
start_x = frames_of_change.iloc[::2]
end_x = frames_of_change.iloc[1::2]

df_markers.loc[start_x.index, ('meta', 'event_analog')] = 'start_x'
df_markers.loc[end_x.index, ('meta', 'event_analog')] = 'end_x'
# df_markers.loc[df_peaksX.index, ('meta', 'event_analog')] = 'grasp'
df_markers.loc[start_x.index, ('meta', 'pass_th_x')] = True

# Calculate each time the signal passes the threshold
lOnOff = pd.IntervalIndex.from_tuples(list(map(tuple, lightOnOff.to_numpy().reshape((-1, 2)))))
robot_local_min = analogDF.groupby(pd.cut(analogDF.index, lOnOff, include_lowest=True))['Voltage.Kuka_ID'].apply(
    lambda x: calculate_robot_min(x))
new_robot_df = pd.DataFrame(index=robot_local_min, data={'robot_min': ['robot_min']}).resample('10ms').first()

pull_moment = new_robot_df.index[new_robot_df['robot_min'] == 'robot_min']  # without .loc
df_markers.loc[pull_moment, ('meta', 'event_analog')] = 'pulling'

# Check if the identify start movement is in the light on light off interval or outside
meta_df = df_markers['meta']
cc = meta_df.groupby(['trial_analog'])['pass_th_x'].first().to_frame().reset_index()
cc = cc.fillna(False)
cc.columns = pd.MultiIndex.from_tuples([('meta', 'trial_analog'), ('meta', 'inside_interval')], names=('marker', 'xyz'))
df_markers = df_markers.join(cc.set_index(('meta', 'trial_analog')), on=[('meta', 'trial_analog')])
# pass_cc = cc.loc[cc[('meta', 'inside_interval')] == True]
pass_cc = df_markers['meta', 'trial_analog'].where(df_markers['meta', 'pass_th_x'] == True)
df_markers.loc[pass_cc.dropna().index, ('meta', 'event_analog')] = 'start_mov'

# Get the starting point when it is outside the interval
fail_cc = cc.loc[cc[('meta', 'inside_interval')] == False]
for row in fail_cc.itertuples(index=False):
    fail_index = df_markers.loc[df_markers[('meta', 'trial_analog')] == row[0]].index[0]
    df_before_index = df_markers.loc[:fail_index, [('meta', 'trial_analog'), ('meta', 'pass_th_x')]]
    true_before = df_before_index.loc[df_before_index[('meta', 'pass_th_x')] == True]
    returned_index = \
        true_before[true_before[('meta', 'pass_th_x')] == True & true_before[('meta', 'trial_analog')].isnull()].index[
            -1]
    df_markers.loc[returned_index, ('meta', 'event_analog')] = 'start_mov'
    df_markers.loc[returned_index, ('meta', 'trial_analog')] = row[0]
    print(fail_index)

df_markers[('meta', 'trial_analog')] = get_trials(df_markers[('meta', 'trial_analog')])
# Check the grasping moments
meta_df = df_markers['meta']
# meta_df['peaks_x']=meta_df['peaks_x'].astype(bool)
cp = meta_df.groupby([meta_df.index, 'peaks_x'])['trial_analog'].first().to_frame().reset_index().dropna().set_index(
    'level_0')

cpp = cp.drop_duplicates('trial_analog')
df_markers.loc[cpp.index, ('meta', 'event_analog')] = 'grasp'

# Split in columns
df_dummies = pd.get_dummies(df_markers[('meta', 'event_analog')]).astype(bool)
df_dummies.columns = pd.MultiIndex.from_product([['meta'], df_dummies.columns])
df_markers = df_markers.join(df_dummies)

# Check the data
check_df = df_markers['meta']
check_df = check_df[check_df['grasp'] == True]


check_df = df_markers['meta']
# rr=check_df['event_analog'].isin(['start_mov','light_off']) # works
# t=get_trials(check_df.loc[check_df['event_analog'].isin(['start_mov','light_off']),'trial_analog'])
check_df['new_trials'] = get_trials(
    check_df.loc[check_df['event_analog'].isin(['start_mov', 'light_off']), 'trial_analog'])  # works with warning

'''
aa = get_trials(df_markers.loc[
                    df_markers[('meta', 'event_analog')].isin([('meta', 'start_mov'), ('meta', 'light_off')]), (
                    'meta', 'trial_analog')])
'''
# Adjust the trials to match the new start
add_trial_df = check_df['new_trials'].to_frame()
add_trial_df.columns = pd.MultiIndex.from_tuples([('meta', 'new_trials')], names=('marker', 'xyz'))
df_markers = df_markers.join(add_trial_df)

#df_trials = df_markers.dropna(subset=[('meta', 'new_trials')])

save_path = root_path
file_name = 'trials_20200609_Padme_Brain_Injury11'
df_markers.to_pickle(os.path.join(save_path, f"markers_{file_name}.pkl"))


