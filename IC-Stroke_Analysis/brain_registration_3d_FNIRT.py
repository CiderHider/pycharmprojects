import os
from nipype.interfaces import fsl
from nipype import Node, Workflow, Function
import glob
from nipype.interfaces.image import Reorient

"""
Functions
"""


def writer(in_file):  # Can be exchanged for DataSink
    return in_file


def resampled_bet(in_file, subtractor=42):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    array = np.where(array >= subtractor, array - subtractor, 0)
    subtracted = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_resampled.nii.gz")
    nib.save(subtracted, out_file)
    return out_file


def hard_swap_axis(in_file, axis=0):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    array = np.flip(array, axis=axis)
    flipped_hemi = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_axSwap.nii.gz")
    nib.save(flipped_hemi, out_file)
    return out_file


def get_hemi_mask(in_file, idx_divider=None, axis=0, left=True):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    if idx_divider is None:
        idx_divider = array.shape[axis] // 2
    slice_list = [slice(None)] * array.ndim
    if left:
        slice_list[axis] = slice(idx_divider)
    else:
        slice_list[axis] = slice(idx_divider, array.shape[axis])
    array[tuple(slice_list)] = 0
    masked_hemi = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_hemi.nii.gz")
    nib.save(masked_hemi, out_file)
    return out_file


def invert_signal(in_file, ceiling=100):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    array = np.where(array > 0, ceiling - array, 0)
    inverted = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_inv.nii.gz")
    nib.save(inverted, out_file)
    return out_file


"""
Inputs
"""

# ATLAS
input_atlas = r'/mnt/Shared/BRAIN_Alignment/Atlas/Fascicularis_MNI/cyno_18_model-MNI.nii'

# # MERIDA
# input_histo = r'/mnt/Shared/Merida/MeridaBDAAnnotation/output/DirectRegistration/Mean/Merida_z-norm_histo.nii.gz'
# input_mri = r'/mnt/Shared/MRI/b0_forAaron/s_20210115_01_b0_dn_Merida.nii'
#
#
# annotation_folder = r'/mnt/Shared/Merida/MeridaBDAAnnotation/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = r'/mnt/Shared/BRAIN_Alignment/Subjects/Merida'

# # # VAIANA
# input_histo = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/DirectRegistration/Mean/Vaiana_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20210129_02_b0_dn_gc_Vaiana.nii'
#
# annotation_folder = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Vaiana'


# # # PADME
# input_histo = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/DirectRegistration/Mean/Padme_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20201023_01_b0_dn_Padme.nii'
#
# annotation_folder = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Padme'

# # LEIA
# input_histo = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/DirectRegistration/Mean/Leia_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200720_01_b0_dn_gc_Leia.nii'
#
# annotation_folder = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)

# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Leia'

# # JYN
input_histo = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/DirectRegistration/Mean/Jyn_z-norm_histo.nii.gz'
input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200605_01_b0_dn_gc_Jyn.nii'

annotation_folder = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/Registration/'
annotation_filter = r'*_annotation_*.nii.gz'
input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)

output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Jyn'

# # # REY
# input_histo = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/DirectRegistration/Mean/Rey_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20191018_01_b0_dn_gc_Rey.nii'
#
# annotation_folder = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/Registration'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Rey'

# # # # HH11
# input_histo = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/DirectRegistration/Mean/HH11_z-norm_histo.nii.gz'
# #
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200221_01_b0_dn_gc_HH11.nii'
#
# annotation_folder = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/Registration'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH11'

# # # HH05
# input_histo = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/DirectRegistration/Mean/HH_z-norm_histo.nii.gz'
#
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200214_01_b0_dn_gc_HH05.nii'
#
# annotation_folder = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/Registration'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH05'

# # # HH06
# input_histo = ''
#
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200110_01_b0_dn_gc_HH06.nii'
#
# annotation_folder = ''
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH06'

# # # HH03
# input_histo = ''
#
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200117_01_b0_dn_gc_HH03.nii'
#
# annotation_folder = ''
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH03'


# Create output directory
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

# Set Variables for Workflow
division_axis = 0
division_index = 48
mask_left = False  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana, HH11 Lesion
do_turn_MRI = True  # True for Jyn and Rey, HH11
lr_is_flipped = True
swapping_axis = 1
"""
Set up Nodes and workflow
"""
hemi_mask = Node(name='hemi_mask',
                 interface=Function(input_names=["in_file", "idx_divider", "axis", "left"],
                                    output_names=["out_file"],
                                    function=get_hemi_mask)
                 )

swap_axis = Node(name='hard_swap_axis',
                 interface=Function(input_names=["in_file", "axis"],
                                    output_names=["out_file"],
                                    function=hard_swap_axis)
                 )

resample = Node(name='resample_bet',
                interface=Function(input_names=["in_file", "axis"],
                                   output_names=["out_file"],
                                   function=resampled_bet)
                )
get_inverted = Node(name='invert_signal',
                    interface=Function(input_names=["in_file", "axis"],
                                       output_names=["out_file"],
                                       function=invert_signal)
                    )

fsl_bet = Node(fsl.BET(),
               name='BET')

histo_flirt = Node(fsl.FLIRT(cost='mutualinfo',
                             # cost_func='mutualinfo',
                             dof=9,  # Vaiana 9
                             output_type="NIFTI_GZ",
                             # out_file="histo_registered.nii.gz",
                             searchr_x=[-5, 5],  # Diana: 0, 15, 5
                             searchr_y=[-20, 20],
                             searchr_z=[-15, 15]),
                   name='Histo_FLIRT')

atlas_flirt = Node(fsl.FLIRT(cost_func='mutualinfo',
                             cost='mutualinfo',  # Might be unneccessary or detrimental
                             dof=12,
                             output_type="NIFTI_GZ",
                             ),
                   name='Atlas_FLIRT')

atlas_fnirt = Node(fsl.FNIRT(intensity_mapping_model='global_non_linear_with_bias',
                             subsampling_scheme=[8, 4, 2, 2],
                             output_type="NIFTI_GZ"),
                   name='FNIRT')

histo_xfm_to_annot = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                          name='apply_histo_transform_to_annot')

atlas_xfm_to_annot = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                          name='apply_atlas_transform_to_annot')

atlas_xfm_to_histo = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                          name='apply_atlas_transform_to_histo')

mni_atlas = Node(name='mni_Atlas',
                 interface=Function(input_names=["in_file"],
                                    output_names=["out_file"],
                                    function=writer)
                 )

ind_MRI = Node(name='ind_MRI',
               interface=Function(input_names=["in_file"],
                                  output_names=["out_file"],
                                  function=writer)
               )

histo_annot = Node(name='histo_annotations',
                   interface=Function(input_names=["in_file"],
                                      output_names=["out_file"],
                                      function=writer)
                   )

histo = Node(name='histo',
             interface=Function(input_names=["in_file"],
                                output_names=["out_file"],
                                function=writer)
             )

orientation_writer = Node(name='writer',
                          interface=Function(input_names=["in_file"],
                                             output_names=["out_file"],
                                             function=writer)
                          )

MRI_writer = Node(name='write_reg_MRI',
                  interface=Function(input_names=["in_file"],
                                     output_names=["out_file"],
                                     function=writer)
                  )
FNIRT_writer = Node(name='write_fnirt_MRI',
                  interface=Function(input_names=["in_file"],
                                     output_names=["out_file"],
                                     function=writer)
                  )

turn_MRI = Node(fsl.utils.SwapDimensions(
    new_dims=('-x', '-y', 'z')),
    name='turn_MRI_180'
)

MRI_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('-y', '-x', '-z')),
    name='MRI_swap_dim'
)

concat_xfm = Node(fsl.utils.ConvertXFM(
    concat_xfm=True),
    name='Concat_xfm'
)

apply_warp_annot = Node(fsl.ApplyWarp(),
                        name='Apply_warp_to_Annot'
                        )

apply_warp_histo = Node(fsl.ApplyWarp(),
                        name='Apply_warp_to_Histo'
                        )

"""
Set up workflow
"""
# Provide initiation files for Nodes
mni_atlas.inputs.in_file = input_atlas
histo.inputs.in_file = input_histo
ind_MRI.inputs.in_file = input_mri
histo_annot.iterables = ("in_file", input_annotations)
# Hemisphere masking input
swap_axis.inputs.axis = swapping_axis
hemi_mask.inputs.axis = division_axis
hemi_mask.inputs.idx_divider = division_index
hemi_mask.inputs.left = mask_left

# Connect Nodes for Workflow
wf = Workflow(name="individual_reg", base_dir=output_dir)

if lr_is_flipped:
    wf.connect([
        (ind_MRI, swap_axis, [("out_file", "in_file")]),
        (swap_axis, orientation_writer, [("out_file", "in_file")])
    ])
else:
    wf.connect([
        (ind_MRI, orientation_writer, [("out_file", "in_file")])
    ])

if do_turn_MRI:
    wf.connect([
        # Turn 180 degrees around z axis
        (orientation_writer, turn_MRI, [("out_file", "in_file")]),
        # Swap dims of individual MRI to match MNI atlas
        (turn_MRI, MRI_swap_dim, [("out_file", "in_file")])
    ])
else:
    wf.connect([
        # Swap dims of individual MRI to match MNI atlas
        (orientation_writer, MRI_swap_dim, [("out_file", "in_file")])
    ])

wf.connect([
    # individual MRI masking and registration of histo volume to masked MRI
    (MRI_swap_dim, hemi_mask, [("out_file", "in_file")]),
    (hemi_mask, histo_flirt, [("out_file", "reference")]),
    (histo, histo_flirt, [("out_file", "in_file")]),
    # skullstripping of MNI atlas and registration of individual MRI to stripped atlas
    (mni_atlas, fsl_bet, [("out_file", "in_file")]),
    (MRI_swap_dim, atlas_flirt, [("out_file", "in_file")]),
    (fsl_bet, atlas_flirt, [("out_file", "reference")]),
    # invert atlas
    (fsl_bet, get_inverted, [("out_file", "in_file")]),
    # FNIRT individual MRI to stripped-inverse atlas
    (get_inverted, atlas_fnirt, [("out_file", "ref_file")]),
    (atlas_flirt, atlas_fnirt, [("out_matrix_file", "affine_file")]),
    (MRI_swap_dim, atlas_fnirt, [("out_file", "in_file")]),
    # Write resampled BET --> DATASINK
    (fsl_bet, resample, [('out_file', 'in_file')]),
    # Write registered MRI --> DATASINK
    (atlas_flirt, MRI_writer, [("out_file", "in_file")]),
    # Write registered MRI --> DATASINK
    (atlas_fnirt, FNIRT_writer, [("warped_file", "in_file")]),
    # Concatenate flirt transformations
    (histo_flirt, concat_xfm, [("out_matrix_file", "in_file")]),
    (atlas_flirt, concat_xfm, [("out_matrix_file", "in_file2")]),
    # apply transform and warp to annotations volumes
    (concat_xfm, apply_warp_annot, [("out_file", "premat")]),
    (histo_annot, apply_warp_annot, [("out_file", "in_file")]),
    (atlas_fnirt, apply_warp_annot, [("field_file", "field_file")]),
    (mni_atlas, apply_warp_annot, [("out_file", "ref_file")]),
    # apply transform and warp to histo volume
    (concat_xfm, apply_warp_histo, [("out_file", "premat")]),
    (histo, apply_warp_histo, [("out_file", "in_file")]),
    (atlas_fnirt, apply_warp_histo, [("field_file", "field_file")]),
    (mni_atlas, apply_warp_histo, [("out_file", "ref_file")]),
])

# Write overview and detailed graph to dot and png
wf.write_graph("workflow_graph.dot", graph2use='flat')

"""
Run Workflow
"""
# # Run sequentially
# wf.run()

# Run it in parallel
allocated_cpus = os.cpu_count() - 1
wf.run('MultiProc', plugin_args={'n_procs': allocated_cpus})
