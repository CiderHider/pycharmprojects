"""
NAME: UMAP Kinematic Data

DESCRIPTION: Maps Kinematic Static features to umap space and visualizes them

AUTHOR: Aaron Brändli
Date: April 2021

"""

# Concat instead of Append
# check for return lengths of functions --> if null make same size as input

# Imports
import os
import glob
import tkinter as tk
from tkinter import filedialog
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import umap
from sklearn.preprocessing import StandardScaler
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import ListedColormap

# Functions
def get_monkey_state(dataframe, acute_chronic = 30):
    delta_t = dataframe['Delta_t'].iloc[0].days
    if delta_t <= 0:
        state = 'Baseline'
    elif delta_t <= acute_chronic:
        state = 'Acute'
    else:
        state = 'Chronic'
    return state


""" Script parameters """
data_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/KinematicFILES_StaticFeatures'
ref_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/Monkey_Reference_sheet.csv'
plot_3D = True
# Neighbouring parameter for UMAP: lower-local, higher-broad
umap_neighbours = 20


# Collect Data in Dataframe
if not os.path.exists(data_path):
    data_path = os.path.expanduser("~")

# Get data to analyze
root = tk.Tk()
root.withdraw()
data_folder = filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=data_path)
if not os.path.exists(ref_path):
    ref_path = filedialog.askopenfilename(title="Select the Monkey Reference Sheet.", initialdir=data_path)

file_list = glob.glob(os.path.join(data_folder, '*.csv'))
file_list.sort()
df = pd.DataFrame()

# Reference sheet
ref_sheet = pd.read_csv(ref_path)

# Collect data
for file in file_list:
    df_raw = pd.read_csv(file)
    df_temp = pd.DataFrame()
    file_path, file_name = os.path.split(file)

    file_part_list = file_name.split("_")
    date = pd.to_datetime(file_part_list[0], format='%Y%m%d')
    df_raw['Date'] = date
    monkey_name = file_part_list[1]
    df_raw['Monkey'] = monkey_name
    df_raw['Object'] = file_part_list[2]
    df_raw['Resistance'] = file_part_list[3]
    severity = ref_sheet['Severity'].loc[ref_sheet['Monkey'] == monkey_name].values
    df_raw['Severity'] = severity[0]
    lesion_date = ref_sheet['Lesion_date'].loc[ref_sheet['Monkey'] == monkey_name].values
    lesion_date = pd.to_datetime(lesion_date[0], format='%Y%m%d')
    df_raw['Lesion_date'] = lesion_date
    df_raw['Delta_t'] = date-lesion_date
    state = get_monkey_state(df_raw)
    df_raw['State'] = state
    df_raw['Monkey_state'] = f"{monkey_name}_{state}"
    # Append to full dataframe
    df = df.append(df_raw, ignore_index=True)

# Get Numeric data
umap_data = df.select_dtypes(include=['int64', 'float64'])
# drop rows with little valid data and after the columns containing NaNs
umap_data = umap_data.dropna(thresh=len(df.columns)//2).dropna(axis='columns')

#Scale data (zscore)
umap_data_scaled = StandardScaler().fit_transform(umap_data)

# Insert mapping into dataframes
mapper = umap.UMAP(n_neighbors=umap_neighbours)

# Insert embedding into old dataframe at corresponding row
umap_data[['UMAP1', 'UMAP2']] = mapper.fit_transform(umap_data_scaled)
df.loc[:, ['UMAP1', 'UMAP2']] = umap_data[['UMAP1', 'UMAP2']]

fig, ax = plt.subplots(2, 2, sharex=True, figsize=(20, 20))
sns.scatterplot(data=df, x='UMAP1', y='UMAP2', hue='Monkey', ax=ax[0, 0]) #, size='Delta_t')
sns.scatterplot(data=df, x='UMAP1', y='UMAP2', hue='Severity', ax=ax[0, 1])
sns.scatterplot(data=df, x='UMAP1', y='UMAP2', hue='Monkey_state', style='Severity', ax=ax[1, 0])
sns.scatterplot(data=df, x='UMAP1', y='UMAP2', hue='Severity', style='Monkey', size='State', ax=ax[1, 1])
plt.show()

if plot_3D:
    # Insert mapping into dataframes
    mapper = umap.UMAP(n_neighbors=umap_neighbours, n_components=3)

    # Insert embedding into old dataframe at corresponding row
    umap_data[['UMAP1', 'UMAP2', 'UMAP3']] = mapper.fit_transform(umap_data_scaled)
    df.loc[:, ['UMAP1', 'UMAP2', 'UMAP3']] = umap_data[['UMAP1', 'UMAP2', 'UMAP3']]

