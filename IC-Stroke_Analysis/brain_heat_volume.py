import nibabel as nib
import os
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def np_from_nii(file):
    nifti = nib.load(file)
    np_array = np.array(nifti.get_fdata())
    return np_array

'''
def flip_volume(vol):
    return vol[::-1,:,:]
'''

def get_sum(x):
    return x.sum()


filter_phrase = '_flirt_flirt.nii.gz'
path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN_Alignment/Subjects'
root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN_Alignment/'
output_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN_Alignment/Heatmaps/HeatMapVolumes'
output_path_each = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN_Alignment/Heatmaps/HeatMapVolumesSeverity'

ref_path =  '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN_Alignment/ReferenceFiles'
monkey_reference = pd.read_csv(os.path.join(ref_path, 'monkey_ref_sheet.csv'), sep=';')

file_filter = os.path.join(path, '**', f"*{filter_phrase}")
file_list = glob.glob(file_filter, recursive=True)

df = pd.DataFrame({'path': file_list})
df['file_name'] = df['path'].apply(os.path.basename)
df['subject'] = df['file_name'].str.extract(r'^([a-zA-Z0-9]+)')
df['annotation'] = df['file_name'].str.extract(f"(\d+){filter_phrase}")
df = df.dropna()


df['volume'] = df['path'].apply(lambda x: np_from_nii(x))

'''
df['volume'] = np.where(((df['subject'] == 'Merida') | (df['subject'] == 'Vaiana')), np.flip(df['volume'], axis=0),
                        df['volume'])
'''


df['volume'] = np.where(((df['subject'] == 'Merida') | (df['subject'] == 'Vaiana')
                         | (df['subject'] == 'HH05')
                         | (df['subject'] == 'HH11')
                         ),
                        df['volume'].apply(lambda x: x[::-1,:,:]), df['volume'])

df = df.join(monkey_reference.set_index('monkey'), on=['subject'])

annotation_heatmaps = df.groupby('annotation')['volume'].apply(
    lambda x: get_sum(x))  # just putting .sum() does not seem to work...

example_nifti = nib.load(df.loc[0, 'path'])
for annotation_heatmap in annotation_heatmaps.iteritems():
    nifti = nib.Nifti1Image(annotation_heatmap[1], example_nifti.header.get_sform(), header=example_nifti.header)
    nifti_file = os.path.join(output_path, f"annotation_{annotation_heatmap[0]}_heat_map.nii.gz")
    nib.save(nifti, nifti_file)
    print(f"Saved to: {nifti_file}")

#Lesion pro severity
df_lesion_ann = df[df['annotation']=="1"]
annotation_severity_heatmaps = df_lesion_ann.groupby(['annotation','severity'])['volume'].apply(
        lambda x: get_sum(x)) # just putting .sum() does not seem to work...

ann_severity_heatmaps=annotation_severity_heatmaps.to_frame().reset_index()

example_nifti = nib.load(df_lesion_ann.loc[2, 'path'])
for ah in ann_severity_heatmaps.itertuples():
    nifti = nib.Nifti1Image(ah.volume, example_nifti.header.get_sform(), header=example_nifti.header)
    nifti_file = os.path.join(output_path_each, f"annotation_{ah.severity}_heat_map.nii.gz")
    nib.save(nifti, nifti_file)
    print(f"Saved to: {nifti_file}")
