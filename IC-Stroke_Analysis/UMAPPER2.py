"""
NAME: UMAP Kinematic Data

DESCRIPTION: Maps Kinematic Static features to umap space and visualizes them

AUTHOR: Aaron Brändli
Date: April 2021

"""

# Concat instead of Append
# check for return lengths of functions --> if null make same size as input

# Imports
import os
import glob
import tkinter as tk
from tkinter import filedialog
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import umap
from sklearn.preprocessing import StandardScaler
import plotly.express as px
import plotly.io as pio
import NR_colors as nrc

pio.renderers.default = 'browser'


# Functions
def get_monkey_state(dataframe, acute_chronic=30):
    delta_t = dataframe['Delta_t'].iloc[0].days
    if delta_t <= 0:
        state = 'Baseline'
    elif delta_t <= acute_chronic:
        state = 'Acute'
    else:
        state = 'Chronic'
    return state


def umap_and_plot(scaled_data, reference_df: pd.DataFrame(), label_variation: list, dimensions=2, neighbours=20):
    # Check for compatible plotting
    assert dimensions == 3 or dimensions == 2, 'Set Dimensions to 2 or 3'

    # Define Umap mapping
    mapper = umap.UMAP(n_neighbors=neighbours, n_components=dimensions)
    # Insert embedding into old dataframe at corresponding row
    umapped = mapper.fit_transform(scaled_data)

    # Plot a UMAP for each label variation
    for coloring in label_variation:
        # 3D or 2D UMAP
        if dimensions == 3:
            fig = px.scatter_3d(
                umapped, x=0, y=1, z=2,
                color=reference_df[coloring], labels={'color': coloring},
                color_discrete_sequence=nrc.base_11, template='plotly_dark'
            )
        else:
            fig = px.scatter(
                umapped, x=0, y=1,
                color=reference_df[coloring], labels={'color': coloring},
                color_discrete_sequence=nrc.base_11, template='plotly_dark'
            )

        fig.show()


""" Script parameters """
data_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/KinematicFILES_StaticFeatures'
ref_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/Monkey_Reference_sheet.csv'

# Plot Parameters
plot_2D = True
plot_3D = False
coloring_list = ['Monkey', 'Monkey_state', 'Severity']
# Neighbouring parameter for UMAP: lower-local, higher-broad
umap_neighbours = 5

# Collect Data in Dataframe
if not os.path.exists(data_path):
    data_path = os.path.expanduser("~")

# Get data to analyze
root = tk.Tk()
root.withdraw()
data_folder = filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=data_path)
if not os.path.exists(ref_path):
    ref_path = filedialog.askopenfilename(title="Select the Monkey Reference Sheet.", initialdir=data_path)

file_list = glob.glob(os.path.join(data_folder, '*.csv'))
file_list.sort()
df = pd.DataFrame()

# Reference sheet
ref_sheet = pd.read_csv(ref_path)

# Collect data
for file in file_list:
    df_raw = pd.read_csv(file)
    df_temp = pd.DataFrame()
    file_path, file_name = os.path.split(file)

    file_part_list = file_name.split("_")
    date = pd.to_datetime(file_part_list[0], format='%Y%m%d')
    df_raw['Date'] = date
    monkey_name = file_part_list[1]
    df_raw['Monkey'] = monkey_name
    df_raw['Object'] = file_part_list[2]
    df_raw['Resistance'] = file_part_list[3]
    severity = ref_sheet['Severity'].loc[ref_sheet['Monkey'] == monkey_name].values
    df_raw['Severity'] = severity[0]
    lesion_date = ref_sheet['Lesion_date'].loc[ref_sheet['Monkey'] == monkey_name].values
    lesion_date = pd.to_datetime(lesion_date[0], format='%Y%m%d')
    df_raw['Lesion_date'] = lesion_date
    df_raw['Delta_t'] = date - lesion_date
    state = get_monkey_state(df_raw)
    df_raw['State'] = state
    df_raw['Monkey_state'] = f"{monkey_name}_{state}"
    # Append to full dataframe
    df = df.append(df_raw, ignore_index=True)

# drop rows with little valid data and after the columns containing NaNs
df_clean = df.dropna(thresh=len(df.columns) // 2).dropna(axis='columns')
# df_clean.reset_index()
# Get Numeric data
umap_data = df_clean.select_dtypes(include=['int64', 'float64'])
# Scale data (zscore)
umap_data_scaled = StandardScaler().fit_transform(umap_data)

if plot_2D:
    umap_and_plot(umap_data_scaled, df_clean, coloring_list)

if plot_3D:
    umap_and_plot(umap_data_scaled, df_clean, coloring_list, dimensions=3)
