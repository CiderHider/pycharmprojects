import nibabel as nib
import os
import glob
import pandas as pd
import numpy as np


def np_from_nii(file):
    nifti = nib.load(file)
    np_array = np.array(nifti.get_fdata())
    return np_array


def get_sum(x):
    return x.sum()


filter_phrase = '_flirt_flirt.nii.gz'
path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN_Alignment/Subjects'
file_filter = os.path.join(path, '**', f"*{filter_phrase}")
file_list = glob.glob(file_filter, recursive=True)

df = pd.DataFrame({'path': file_list})
df['file_name'] = df['path'].apply(os.path.basename)
df['subject'] = df['file_name'].str.extract(r'^([a-zA-Z]+)')
df['annotation'] = df['file_name'].str.extract(f"(\d+){filter_phrase}")
df = df.dropna()
df['volume'] = df['path'].apply(lambda x: np_from_nii(x))

annotation_heatmaps = df.groupby('annotation')['volume'].apply(
    lambda x: get_sum(x))  # just putting .sum() does not seem to work...

example_nifti = nib.load(df.loc[0, 'path'])
for annotation_heatmap in annotation_heatmaps.iteritems():
    nifti = nib.Nifti1Image(annotation_heatmap[1], example_nifti.header.get_sform(), header=example_nifti.header)
    nifti_file = os.path.abspath(f"annotation_{annotation_heatmap[0]}_heat_map.nii.gz")
    nib.save(nifti, nifti_file)
    print(f"Saved to: {nifti_file}")
