import os
from nipype.interfaces import fsl
from nipype import Node, Workflow, Function
import glob
from nipype.interfaces.image import Reorient

"""
Functions
"""


def writer(in_file):  # Can be exchanged for DataSink
    return in_file


def resampled_bet(in_file, subtractor=42):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    array = np.where(array >= subtractor, array - subtractor, 0)
    subtracted = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_resampled.nii.gz")
    nib.save(subtracted, out_file)
    return out_file


def hard_swap_axis(in_file, axis=0):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    array = np.flip(array, axis=axis)
    flipped_hemi = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_axSwap.nii.gz")
    nib.save(flipped_hemi, out_file)
    return out_file


def get_hemi_mask(in_file, idx_divider=None, axis=0, left=True):
    import nibabel as nib
    import numpy as np
    from os.path import abspath, basename
    nifti = nib.load(in_file)
    array = np.array(nifti.get_fdata())
    if idx_divider is None:
        idx_divider = array.shape[axis] // 2
    slice_list = [slice(None)] * array.ndim
    if left:
        slice_list[axis] = slice(idx_divider)
    else:
        slice_list[axis] = slice(idx_divider, array.shape[axis])
    array[tuple(slice_list)] = 0
    masked_hemi = nib.Nifti1Image(array, nifti.header.get_sform(), header=nifti.header)
    in_file_name = basename(in_file).split('.')[0]
    out_file = abspath(f"{in_file_name}_hemi.nii.gz")
    nib.save(masked_hemi, out_file)
    return out_file


"""
Inputs
"""

# ATLAS
input_atlas = r'/mnt/Shared/BRAIN_Alignment/Atlas/Fascicularis_MNI/cyno_18_model-MNI.nii'

# # MERIDA
# input_histo = r'/mnt/Shared/Merida/MeridaBDAAnnotation/output/DirectRegistration/Mean/Merida_z-norm_histo.nii.gz'
# input_mri = r'/mnt/Shared/MRI/b0_forAaron/s_20210115_01_b0_dn_Merida.nii'
#
#
# annotation_folder = r'/mnt/Shared/Merida/MeridaBDAAnnotation/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = r'/mnt/Shared/BRAIN_Alignment/Subjects/Merida'
# mask_left = False  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI = False  # True for Jyn and Rey, HH11
# reconstruct_planning = False

# # # VAIANA
# input_histo = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/DirectRegistration/Mean/Vaiana_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20210129_02_b0_dn_gc_Vaiana.nii'
#
# annotation_folder = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Vaiana'
# mask_left = False  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI = False  # True for Jyn and Rey, HH11
# reconstruct_planning = False


# # PADME
# input_histo = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/DirectRegistration/Mean/Padme_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20201023_01_b0_dn_Padme.nii'
#
# annotation_folder = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Padme'
# mask_left = True  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI = False  # True for Jyn and Rey, HH11
#
# reconstruct_planning = True
# # reconstruction dir
# frame_input_dir = '/mnt/Shared/MRI/Planning/Padme'
# # Centroid for BET [pixels]
# bet_center = [100, 70, 175]
# # Brain radius [pixels]
# bet_radius = 37

# # LEIA
# input_histo = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/DirectRegistration/Mean/Leia_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200720_01_b0_dn_gc_Leia.nii'
#
# annotation_folder = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/Registration/'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Leia'
# mask_left = True  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI = False  # True for Jyn and Rey, HH11
#
# reconstruct_planning = True
# # reconstruction dir
# frame_input_dir = '/mnt/Shared/MRI/Planning/Leia'
# # Centroid for BET [pixels]
# bet_center = [100, 110, 170]
# # Brain radius [pixels]
# bet_radius = 35

# # JYN
input_histo = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/DirectRegistration/Mean/Jyn_z-norm_histo.nii.gz'
input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200605_01_b0_dn_gc_Jyn.nii'

annotation_folder = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/Registration/'
annotation_filter = r'*_annotation_*.nii.gz'
input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)

output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Jyn'
mask_left = True  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
do_turn_MRI = True  # True for Jyn and Rey, HH11

reconstruct_planning = True
# reconstruction dir
frame_input_dir = '/mnt/Shared/MRI/Planning/Jyn'
# Centroid for BET [pixels]
bet_center = [100, 100, 150]
# Brain radius [pixels]
bet_radius = 35

# # # REY
# input_histo = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/DirectRegistration/Mean/Rey_z-norm_histo.nii.gz'
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20191018_01_b0_dn_gc_Rey.nii'
#
# annotation_folder = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/Registration'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/Rey'
# mask_left = True  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI = True  # True for Jyn and Rey, HH11
#
# reconstruct_planning = True
# # reconstruction dir
# frame_input_dir = '/mnt/Shared/MRI/Planning/Rey'
# # Centroid for BET [pixels]
# bet_center = [100, 100, 150]
# # Brain radius [pixels]
# bet_radius = 35

# # # # HH11
# input_histo = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/DirectRegistration/Mean/HH11_z-norm_histo.nii.gz'
# #
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200221_01_b0_dn_gc_HH11.nii'
#
# annotation_folder = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/Registration'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH11'
# mask_left =   # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI = True  # True for Jyn and Rey, HH11
#
# reconstruct_planning = false

# # # HH05
# input_histo = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/DirectRegistration/Mean/HH_z-norm_histo.nii.gz'
#
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200214_01_b0_dn_gc_HH05.nii'
#
# annotation_folder = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/Registration'
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH05'
# mask_left =  # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI =  # True for Jyn and Rey, HH11
#
# reconstruct_planning = false

# # # HH06
# input_histo = ''
#
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200110_01_b0_dn_gc_HH06.nii'
#
# annotation_folder = ''
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH06'
# mask_left =   # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI =  # True for Jyn and Rey, HH11
#
# reconstruct_planning = false

# # # HH03
# input_histo = ''
#
# input_mri = '/mnt/Shared/MRI/b0_forAaron/s_20200117_01_b0_dn_gc_HH03.nii'
#
# annotation_folder = ''
# annotation_filter = r'*_annotation_*.nii.gz'
# input_annotations = glob.glob(os.path.join(annotation_folder, annotation_filter), recursive=False)
#
# output_dir = '/mnt/Shared/BRAIN_Alignment/Subjects/HH03'
# mask_left =   # Mask right if false, True: Jyn, Leia, Padme, Rey; False: Merida, Vaiana
# do_turn_MRI =   # True for Jyn and Rey, HH11
#
# reconstruct_planning = false

# Create output directory
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

# Set Variables for Workflow
division_axis = 0
division_index = 48
lr_is_flipped = True
swapping_axis = 1

"""
Set up Nodes and workflow
"""
# Ungrouped
hemi_mask = Node(name='hemi_mask',
                 interface=Function(input_names=["in_file", "idx_divider", "axis", "left"],
                                    output_names=["out_file"],
                                    function=get_hemi_mask)
                 )

resample = Node(name='resample_bet',
                interface=Function(input_names=["in_file", "axis"],
                                   output_names=["out_file"],
                                   function=resampled_bet)
                )

# Skull stripping
fsl_bet = Node(fsl.BET(),
               name='BET')

frame_bet = Node(fsl.BET(
    center=bet_center,
    radius=bet_radius
),
    name='frame_BET')

# Linear registration
histo_flirt = Node(fsl.FLIRT(cost='mutualinfo',
                             # cost_func='mutualinfo',
                             dof=9,  # Vaiana 9
                             output_type="NIFTI_GZ",
                             # out_file="histo_registered.nii.gz",
                             searchr_x=[-5, 5],  # Diana: 0, 15, 5
                             searchr_y=[-20, 20],
                             searchr_z=[-15, 15]),
                   name='Histo_FLIRT')

atlas_flirt = Node(fsl.FLIRT(cost='mutualinfo',
                             cost_func='mutualinfo',
                             dof=12,
                             output_type="NIFTI_GZ",
                             ),
                   name='Atlas_FLIRT')

frame_flirt = Node(fsl.FLIRT(cost='mutualinfo',
                             # cost_func='mutualinfo',
                             dof=12,
                             output_type="NIFTI_GZ",
                             ),
                   name='Frame_FLIRT')

# Data handling and sinks
mni_atlas = Node(name='mni_Atlas',
                 interface=Function(input_names=["in_file"],
                                    output_names=["out_file"],
                                    function=writer)
                 )

ind_MRI = Node(name='ind_MRI',
               interface=Function(input_names=["in_file"],
                                  output_names=["out_file"],
                                  function=writer)
               )

histo_annot = Node(name='histo_annotations',
                   interface=Function(input_names=["in_file"],
                                      output_names=["out_file"],
                                      function=writer)
                   )

histo = Node(name='histo',
             interface=Function(input_names=["in_file"],
                                output_names=["out_file"],
                                function=writer)
             )

orientation_writer = Node(name='writer',
                          interface=Function(input_names=["in_file"],
                                             output_names=["out_file"],
                                             function=writer)
                          )

MRI_writer = Node(name='write_reg_MRI',
                  interface=Function(input_names=["in_file"],
                                     output_names=["out_file"],
                                     function=writer)
                  )

frame_writer = Node(name='write_reg_frame_MRI',
                    interface=Function(input_names=["in_file"],
                                       output_names=["out_file"],
                                       function=writer)
                    )

frame_MRI = Node(name='frame_MRI',
                 interface=Function(input_names=["in_file"],
                                    output_names=["out_file"],
                                    function=writer)
                 )

target = Node(name='target',
              interface=Function(input_names=["in_file"],
                                 output_names=["out_file"],
                                 function=writer)
              )

# Dimension alignment
turn_MRI = Node(fsl.utils.SwapDimensions(
    new_dims=('-x', '-y', 'z')),
    name='turn_MRI_180'
)

MRI_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('-y', '-x', '-z')),
    name='MRI_swap_dim'
)

swap_axis = Node(name='hard_swap_axis',
                 interface=Function(input_names=["in_file", "axis"],
                                    output_names=["out_file"],
                                    function=hard_swap_axis)
                 )

frame_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('z', '-y', 'x')),
    name='frame_swap_dim'
)

target_swap_dim = Node(fsl.utils.SwapDimensions(
    new_dims=('z', '-y', 'x')),
    name='target_swap_dim'
)

# Concatenate transformations
concat_xfm = Node(fsl.utils.ConvertXFM(
    concat_xfm=True),
    name='concat_xfm'
)

concat_xfm_target = Node(fsl.utils.ConvertXFM(
    concat_xfm=True),
    name='concat_xfm_target'
)

# Apply transforms
xfm_annot_to_atlas = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                          name='apply_transforms_annot_atlas')

xfm_histo_to_atlas = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                          name='apply_transforms_histo_atlas')

xfm_target_to_PM = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                        name='apply_transforms_target_PM')

xfm_target_to_atlas = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                           name='apply_transforms_target_atlas')

xfm_frame_to_atlas = Node(fsl.preprocess.ApplyXFM(apply_xfm=True),
                          name='apply_transforms_frame_atlas')

"""
Set up workflow
"""
# Provide initiation files for Nodes
mni_atlas.inputs.in_file = input_atlas
histo.inputs.in_file = input_histo
ind_MRI.inputs.in_file = input_mri
histo_annot.iterables = ("in_file", input_annotations)
# Hemisphere masking input
swap_axis.inputs.axis = swapping_axis
hemi_mask.inputs.axis = division_axis
hemi_mask.inputs.idx_divider = division_index
hemi_mask.inputs.left = mask_left

# Connect Nodes for Workflow
wf = Workflow(name="individual_reg", base_dir=output_dir)

if lr_is_flipped:
    wf.connect([
        (ind_MRI, swap_axis, [("out_file", "in_file")]),
        (swap_axis, orientation_writer, [("out_file", "in_file")])
    ])
else:
    wf.connect([
        (ind_MRI, orientation_writer, [("out_file", "in_file")])
    ])

if do_turn_MRI:
    wf.connect([
        # Turn 180 degrees around z axis
        (orientation_writer, turn_MRI, [("out_file", "in_file")]),
        # Swap dims of individual MRI to match MNI atlas
        (turn_MRI, MRI_swap_dim, [("out_file", "in_file")])
    ])
else:
    wf.connect([
        # Swap dims of individual MRI to match MNI atlas
        (orientation_writer, MRI_swap_dim, [("out_file", "in_file")])
    ])

wf.connect([
    # individual MRI masking and registration of histo volume to masked MRI
    (MRI_swap_dim, hemi_mask, [("out_file", "in_file")]),
    (hemi_mask, histo_flirt, [("out_file", "reference")]),
    (histo, histo_flirt, [("out_file", "in_file")]),
    # skullstripping of MNI atlas and registration of individual MRI to stripped atlas
    (mni_atlas, fsl_bet, [("out_file", "in_file")]),
    (MRI_swap_dim, atlas_flirt, [("out_file", "in_file")]),
    (fsl_bet, atlas_flirt, [("out_file", "reference")]),
    # Write resampled BET --> DATASINK
    (fsl_bet, resample, [('out_file', 'in_file')]),
    # Write registered MRI --> DATASINK
    (atlas_flirt, MRI_writer, [("out_file", "in_file")]),
    # Concatenate flirt transformations
    (histo_flirt, concat_xfm, [("out_matrix_file", "in_file")]),
    (atlas_flirt, concat_xfm, [("out_matrix_file", "in_file2")]),
    # apply transforms to annotation volumes
    (concat_xfm, xfm_annot_to_atlas, [("out_file", "in_matrix_file")]),
    (histo_annot, xfm_annot_to_atlas, [("out_file", "in_file")]),
    (mni_atlas, xfm_annot_to_atlas, [("out_file", "reference")]),
    # apply transforms to histo volume
    (concat_xfm, xfm_histo_to_atlas, [("out_file", "in_matrix_file")]),
    (histo, xfm_histo_to_atlas, [("out_file", "in_file")]),
    (mni_atlas, xfm_histo_to_atlas, [("out_file", "reference")])
])

if reconstruct_planning:
    # Target nifti
    target.inputs.in_file = glob.glob(os.path.join(frame_input_dir, 'Target', r'*.nii.gz'))[0]

    # Frame MRI
    frame_MRI.inputs.in_file = glob.glob(os.path.join(frame_input_dir, 'NII', r'*.nii.gz'))[0]
    wf.connect([
        # Align frame MRI and target with atlas coordinates
        (frame_MRI, frame_swap_dim, [("out_file", "in_file")]),
        (target, target_swap_dim, [("out_file", "in_file")]),
        # Skull strip frame MRI
        (frame_swap_dim, frame_bet, [("out_file", "in_file")]),
        # FLIRT frame with post-mortem MRI
        (frame_bet, frame_flirt, [("out_file", "in_file")]),
        (MRI_swap_dim, frame_flirt, [("out_file", "reference")]),
        # Concatenate transforms for target
        (frame_flirt, concat_xfm_target, [("out_matrix_file", "in_file")]),
        (atlas_flirt, concat_xfm_target, [("out_matrix_file", "in_file2")]),
        # Apply transforms to target --> atlas
        (frame_flirt, xfm_target_to_PM, [("out_matrix_file", "in_matrix_file")]),
        (target_swap_dim, xfm_target_to_PM, [("out_file", "in_file")]),
        (mni_atlas, xfm_target_to_PM, [("out_file", "reference")]),
        # Apply transforms to target --> atlas
        (concat_xfm_target, xfm_target_to_atlas, [("out_file", "in_matrix_file")]),
        (target_swap_dim, xfm_target_to_atlas, [("out_file", "in_file")]),
        (mni_atlas, xfm_target_to_atlas, [("out_file", "reference")]),
        # Write registered MRI --> DATASINK
        (frame_flirt, frame_writer, [("out_file", "in_file")]),
        # Apply transforms to frame --> atlas
        (concat_xfm_target, xfm_frame_to_atlas, [("out_file", "in_matrix_file")]),
        (frame_swap_dim, xfm_frame_to_atlas, [("out_file", "in_file")]),
        (mni_atlas, xfm_frame_to_atlas, [("out_file", "reference")]),
    ])

# Write overview and detailed graph to dot and png
wf.write_graph("workflow_graph.dot", graph2use='flat')

"""
Run Workflow
"""
# # Run sequentially
# wf.run()

# Run it in parallel
allocated_cpus = os.cpu_count() - 1
wf.run('MultiProc', plugin_args={'n_procs': allocated_cpus})
