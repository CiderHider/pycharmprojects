import numpy as np
import os
from skimage.filters import threshold_yen, threshold_otsu
from fun_directional_analysis import check_file_path
import nibabel as nib
import re
import registration_functions as rf
'''
Laptop
'''
# dir_raw = '/media/aaron/Squirrel500/Merida/MeridaBDAAnnotation/output/DirectRegistration'
# dir_annotation = '/media/aaron/Squirrel500/Merida/MeridaBDAAnnotation/output/Registration'

# ## Vaiana
# dir_raw = '/media/aaron/Squirrel500/00_Vaiana/Vaiana_NoBDA_Annotations/output/DirectRegistration/Mean'
# dir_annotation = '/media/aaron/Squirrel500/00_Vaiana/Vaiana_NoBDA_Annotations/output/Registration'
# directory = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/QuPathStuff/QuPathProjects/Leia/USED/Leia_BDAProject_MarkedGM/output/DirectRegistration/Mean'
# directory = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/QuPathStuff/QuPathProjects/Jyn/Jyn_BDAProject_MarkedGM/output/DirectRegistration/Mean'
## Padme old
# dir_raw = '/media/aaron/Squirrel500/Padme/Padme_BDAProject_AboveBelow_Reg/output/DirectRegistration/Mean'
# dir_annotation = '/media/aaron/Squirrel500/Padme/Padme_BDAProject_AboveBelow_Reg/output/Registration'
# # Padme new
# dir_raw = '/media/aaron/Squirrel500/Padme/Padme_Reg/output/DirectRegistration/Mean'
# dir_annotation = '/media/aaron/Squirrel500/Padme/Padme_Reg/output/Registration'
## Leia
# dir_raw = '/media/aaron/Nut9000/Leia/LeiaQP/output/DirectRegistration/Mean'
# dir_annotation = '/media/aaron/Nut9000/Leia/LeiaQP/output/Registration'
# ## Jyn
# dir_raw = '/media/aaron/Nut9000/Jyn/Jyn_BDAProject_MarkedGM_Reg/output/DirectRegistration/Mean'
# dir_annotation = '/media/aaron/Nut9000/Jyn/Jyn_BDAProject_MarkedGM_Reg/output/Registration'
dir_raw = r'/media/aaron/Nut9000/Jyn/Jyn_BDAProject_MarkedGM_Reg/output/DirectRegistration/Mean'
dir_annotation = r'/media/aaron/Nut9000/Jyn/Jyn_BDAProject_MarkedGM_Reg/output/Registration'

'''
Linuxhome
'''
# # hh03
# dir_raw = '/mnt/Shared/HH03/Registration/HH03_BDA_QuPathProject/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/HH03/Registration/HH03_BDA_QuPathProject/output/Registration'
#
# # hh05
# dir_raw = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/Registration'
#
# # # hh06
# # dir_raw = ''
# # dir_annotation = ''
#
# # # hh11
# dir_raw = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/Registration'
#
# # Merida
# dir_raw = '/mnt/Shared/Merida/MeridaBDAAnnotation/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Merida/MeridaBDAAnnotation/output/Registration'
# #
# # Vaiana
# dir_raw = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/Registration'

# # Leia
# dir_raw = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/Registration'
#
# Jyn
dir_raw = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/DirectRegistration/Mean'
dir_annotation = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/Registration'

# # Padme
# dir_raw = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/Registration'
#
# Rey
# dir_raw = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/Registration'


'''
Linuxhome
'''
# # hh03
# dir_raw = '/mnt/Shared/HH03/Registration/HH03_BDA_QuPathProject/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/HH03/Registration/HH03_BDA_QuPathProject/output/Registration'
#
# # hh05
# dir_raw = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/HH05/Registration/HH05_LesionBDA_QuPathProject/output/Registration'
#
# # # hh06
# # dir_raw = ''
# # dir_annotation = ''
#
# # # hh11
# dir_raw = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/HH11/Registration/HH11_BDA_Project/output/Registration'
#
# # Merida
# dir_raw = '/mnt/Shared/Merida/MeridaBDAAnnotation/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Merida/MeridaBDAAnnotation/output/Registration'
# #
# # Vaiana
# dir_raw = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Vaiana/Vaiana_NoBDA_Annotations/output/Registration'

# # Leia
# dir_raw = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Leia/Registration/Leia_BDAProject_MarkedGM/output/Registration'
#
# Jyn
dir_raw = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/DirectRegistration/Mean'
dir_annotation = '/mnt/Shared/Jyn/Registration/Jyn_BDAProject_MarkedGM_Reg/output/Registration'

# # Padme
# dir_raw = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Padme/Registration/Padme_BDAProject_AboveBelow_Reg/output/Registration'
#
# Rey
# dir_raw = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/DirectRegistration/Mean'
# dir_annotation = '/mnt/Shared/Rey/Rey_QuPath_BDA_BIOP_Class/output/Registration'


dir_raw = check_file_path(dir_raw)
dir_annotation = check_file_path(dir_annotation)

# Store intermediate results for debugging (very memory intensive!)
save_intermediate_results = False
# z-stack axis, default 0
image_axis = 0
# Alignment reference slide nr, if None:middle is taken
# Merida: 35, Vaiana: 36/31, Padme: 33, Leia: 43, Jyn: 34, Rey: HH11:39 HH05:46
seed_slide_nr = 34
# Flip z-stack order if set to False
invert_slices = False
# False: Vaiana, Merida, HH11Lesion
is_right_hemi = True
# Remove high signal artifacts
remove_artifacts = False


"""
Skript
"""
print('Collecting Data...')
df_histo = rf.collect_data_registration(dir_raw)
df_histo = df_histo.sort_values(by=['slide_nr'], ascending=invert_slices, ignore_index=True)
df_annotation = rf.collect_data_registration(dir_annotation, file_identifier='MultiChannel')
df_histo = df_histo.join(df_annotation.set_index('slide_nr'), on='slide_nr', rsuffix='_annotation')
print('Preprocessing...')
ser_padded = rf.pad_images(df_histo['image'])
ser_padded = rf.pad_images(df_histo['image'])

if save_intermediate_results:
    df_histo['image_padded'] = ser_padded
# stack = np.array(df_histo['image_padded'].to_list())
stack = np.array(ser_padded.to_list())

# Median Intensity adjustment
list_image_mean = rf.stack_percentile_mean(stack, axis=image_axis)
if np.isnan(list_image_mean.sum()):
    raise ValueError('An Image seems to be broken --> check for nans in list_image_mean')
stack_mean = np.mean(list_image_mean)
stack_multiplyer = stack_mean / list_image_mean
# Multiply image along axis
stack = rf.multiply_along_axis(stack, stack_multiplyer, axis=image_axis)

# Thresholding to remove background (histo)
# threshold = threshold_triangle(stack)
threshold = threshold_otsu(stack)
stack_thr = stack * (stack > threshold)  # High pass threshold

if save_intermediate_results:
    df_histo['image_thresholded'] = stack_thr.tolist()

print('Alignment...')
if seed_slide_nr is None:
    array_seed = stack.shape[image_axis] // 2
else:
    array_seed = df_histo.index[df_histo['slide_nr'].astype('int32') == seed_slide_nr].item()

# Initialize registration iteration
list_neg = range(array_seed, -1, -1)
list_pos = range(array_seed, stack.shape[image_axis])
list_reg = list(zip(list_neg, list_neg[1:])) + list(zip(list_pos, list_pos[1:]))

# Volume alignment
transform_list = [0] * stack_thr.shape[0]
for idx_fixed, idx_moving in list_reg:
    print(f"{idx_moving} to {idx_fixed}")
    stack_thr[idx_moving], transform_list[idx_moving] = rf.rigid_registration(stack_thr[idx_moving],
                                                                              stack_thr[idx_fixed], verbose=True,
                                                                              get_transform=True)

# Thresholding to remove artifacts (histo)
if remove_artifacts:
    threshold = threshold_yen(stack_thr, nbins=32)
    stack_thr = np.where(stack_thr < threshold, stack_thr, stack_mean)
    # stack_thr = stack_thr * (stack_thr < threshold)  # Low pass threshold

# Save to df
df_histo['image_aligned'] = stack_thr.tolist()
df_histo['transform'] = transform_list

print('Creating Nifti Volume...')
example_file_name = df_histo.loc[0, 'file_name']
subject_name = re.search("^[a-zA-Z]+", example_file_name).group(0)
example_file = os.path.join(dir_raw, example_file_name)
nifti = rf.create_nifti(stack_thr, example_file, right_hemi=is_right_hemi)

if invert_slices:
    info = 'z-inverse_'
else:
    info = 'z-norm_'

file_name_nifti = f"{subject_name}_{info}histo.nii.gz"
nib.save(nifti, os.path.join(dir_raw, file_name_nifti))

print('Creating Annotation volumes (NIFTI)...')
rf.get_annotation_nifti(df_histo, right_hemi=is_right_hemi)
print('Done')

