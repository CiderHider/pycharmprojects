import nibabel as nib
import os
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from skimage.morphology import erosion, dilation, opening, closing, white_tophat
from skimage.morphology import black_tophat, skeletonize, convex_hull_image
from skimage.morphology import disk
from scipy import ndimage


def np_from_nii(file):
    nifti = nib.load(file)
    np_array = np.array(nifti.get_fdata())
    return np_array


def calculate_overlap(x, f_list):
    vol1 = np_from_nii(x)
    check_vol1 = os.path.basename(x).split('_')[0][1]
    if check_vol1 == 'H':
        vol1 = vol1[::-1, :, :]

    intersection = []
    for ii in f_list:
        # print(ii)
        check_vol2 = os.path.basename(ii).split('_')[0][1]
        vol2 = np_from_nii(ii)
        if check_vol2 == 'H':
            vol2 = vol2[::-1, :, :]

        vol1 = np.where(vol1 > 0, 1, 0)
        vol2 = np.where(vol2 > 0, 1, 0)
        unique_vol1, counts_vol1 = np.unique(vol1, return_counts=True)
        summed_vol = vol1 + vol2
        unique_sum, counts_sum = np.unique(summed_vol, return_counts=True)
        # print(counts_sum.size)
        if counts_sum.size == 3:
            aa = counts_sum[2] / counts_vol1[1]
            intersection.append(aa)
        else:
            intersection.append(1)

    # print('Infunct')
    return intersection


def flip_volume(input_df, out):
    nifti = nib.load(input_df.path)
    np_array = np.array(nifti.get_fdata())
    flipped_array = np_array[::-1, :, :]

    example_nifti = nib.load(input_df.path)
    nifti = nib.Nifti1Image(flipped_array, example_nifti.header.get_sform(), header=example_nifti.header)
    nifti_file = os.path.join(out, f"{input_df['subject']}_heat_map_flipped.nii.gz")
    nib.save(nifti, nifti_file)


path = '/Users/muscalu/Downloads/Histology_Data/Registration/Lesion'

flip_export = False
export_overlap = True

filter_phrase = '_annotation_1_flirt_flirt.nii.gz'
file_filter = os.path.join(path, '**', f"*{filter_phrase}")
file_list = glob.glob(file_filter, recursive=True)

df = pd.DataFrame({'path': file_list})
df['file_name'] = df['path'].apply(os.path.basename)
df['subject'] = df['file_name'].str.extract(r'^([a-zA-Z0-9]+)')

if export_overlap:
    subjects_list = df.subject.to_list()
    subjects_list.sort(reverse=True)
    df_overlap = df.copy()
    df_overlap.set_index('subject', inplace=True)
    df_overlap.sort_index(ascending=False, inplace=True)
    file_list.sort(reverse=True)
    df_overlap[subjects_list] = df_overlap['path'].apply(lambda x: calculate_overlap(x, file_list))

    df_all = df_overlap.iloc[:, -6:]
    df_all = round(df_all.transpose(), 2)

    sns.heatmap(data=df_all, vmin=0, vmax=1, annot=True, cmap='gist_gray_r')
    output_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/LESION'
    plt.savefig(os.path.join(output_dir, "LesionOverlap.svg"))
    plt.show()


if flip_export:
    china_df = df[df['subject'].str.contains('HH')]

    output_flipped = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/LESION/FlippedVol'
    china_df.apply(lambda x: flip_volume(x, output_flipped), axis=1)


'''
#pened = ndimage.binary_opening(test_vol, structure=np.ones((3,3)))
dimension = test_vol.shape

for ii in range(0,dimension[1]):
    test_vol[:, ii, :] = dilation(test_vol[:,ii,:], disk(3))
    #print(rr.shape)
    #test_vol[:, ii, :] = rr
    #print(ii)

bb =  opening(test_vol[:,0,:], disk(5))
plt.imshow(test_vol[:,0,:], cmap='gray')
plt.imshow(bb, cmap='gray')
plt.show()


example_nifti = nib.load(path)

nifti = nib.Nifti1Image(test_vol, example_nifti.header.get_sform(), header=example_nifti.header)
nifti_file = os.path.join(out_path, f"Padme_annotation_7_dilation3.nii.gz")
nib.save(nifti, nifti_file)
print(f"Saved to: {nifti_file}")

'''
