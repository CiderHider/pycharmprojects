# Imports
import pandas as pd
# import pingouin as pg

from fun_directional_analysis import *

# import plotly.io as pio

"""
To CHECK

pi-periodic von mises distribution instead of 2pi and rescale

check circular STD

smoothing value kappa (25 atm)

couchy distribution??

plot 'continuous' descending tract instead of height binned?!
"""

"""
SETTINGS
"""
n_height_bins = 20
join_sigma = 1.7  # 1.5 is standard

keep_old_df = True
save_images = False

colors_to_analyze = ['red', 'green', 'FR', 'BF']
# colors_to_analyze = ['FR']


# File references
machine = 1  # 0: Ubuntu/Laptop, 1: win Desktop

fix_leia = True
fix_HH05 = True
fix_merida = True

reduce_below = True
if reduce_below:
    reduce_below_str = '_reduced'
else:
    reduce_below_str = ''

"""
SCRIPT
"""

# def collect_da_data(data_path='/media/aaron/Squirrel500/00_Merida/MeridaBDAAnnotation/output/DirectionAnalysis'):
if machine == 0:
    data_folders = '/media/aaron/Squirrel500/Merida/MeridaBDAAnnotation/output/DirectionAnalysis'
    subject_reference = ''
elif machine == 1:
    data_folders = [
        r'F:\Jyn\Registration\Jyn_BDAProject_MarkedGM_Reg\output\DirectionAnalysis',
        r'F:\Leia\Registration\Leia_BDAProject_MarkedGM\output\DirectionAnalysis',
        r'F:\Padme\Registration\Padme_BDAProject_AboveBelow_Reg\output\DirectionAnalysis',
        r'F:\Vaiana\Registration\Vaiana_NoBDA_Annotations\output\DirectionAnalysis',
        r'F:\Merida\Registration\MeridaBDAAnnotation\output\DirectionAnalysis',
        r'F:\Rey\Registration\Rey_QuPath_BDA_BIOP\output\DirectionAnalysis',
        r'F:\Rey\Rey_antiGFP_QP_Project\output\DirectionAnalysis',
        r'F:\HH05\IC\HH05_Contra_Healthy_QuPath\output\DirectionAnalysis',
    ]
    # subject_reference = r'F:\Dropbox (Personal)\Documents\Work\Histology\MatlabBrain\MonkeyReferenceSheet.csv'
    subject_reference = r'F:\Graphics\monkey_histo_reference_sheet.csv'
    dir_combined = r'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\HistoProcessedData\Directional_analysis'
else:
    data_folders = ''
    subject_reference = ''

subject_reference = check_file_path(subject_reference)
dir_combined = check_file_path(dir_combined)
df_reference = pd.read_csv(subject_reference)

# Initialize combination dataframe for combined plotting:
df_all_combined = pd.DataFrame()
for data_folder in data_folders:
    data_folder = check_file_path(data_folder)

    # Get Monkey Name
    subject_name = os.path.normpath(data_folder).split(os.path.sep)[1]

    print(f"Processing Subject: {subject_name}")

    print('Getting image file list...')
    df_files = get_file_list_df(data_folder)
    print('Done')

    # Assimilate data or read it from existing file
    print('Collecting Data...')
    df_subject = pd.DataFrame()
    for color in colors_to_analyze:
        path_parquet = os.path.join(data_folder, f"{color}_data.parquet")
        if os.path.isfile(path_parquet) and keep_old_df:
            df_color = pd.read_parquet(path_parquet)
            print(f"Imported saved Parquet file: {subject_name} {color}")
        else:
            df_color = collect_data(df_files.loc[df_files['color'] == color], subject_name=subject_name)
            df_color.to_parquet(path_parquet)
            print(f"Data import complete. Saved to Parquet file for upcoming executions: {subject_name} {color}")

        df_subject = pd.concat([df_subject, df_color], axis=0)

    # Add subject name to df
    df_subject['subject'] = subject_name
    # Run kernel density analysis
    df_subject = df_subject.groupby('color').apply(
        lambda x: run_KDE_analysis(x, n_kde_bins=n_height_bins, join_sigma=join_sigma))
    df_subject = df_subject.reset_index(drop=True)

    # Aggregate and plot results
    grouping = ['color', 'height_bins', 'neuron_bundle']
    dir_figures = os.path.join(data_folder, 'Figures')
    if not os.path.isdir(dir_figures):
        os.mkdir(dir_figures)
    df_subject_results = aggregate_bins(df_subject, grouping=grouping, plot_dir=dir_figures)

    # Create df for combined plotting
    df_subject_results = df_subject_results.reset_index()
    df_all_combined = pd.concat([df_all_combined, df_subject_results])

    # Save overlay Images
    if save_images:
        for color in colors_to_analyze:
            write_cluster_overlay_images(df_files.loc[df_files['color'] == color],
                                         df_subject.loc[df_subject['color'] == color],
                                         os.path.join(data_folder, 'Output'),
                                         cluster_key='neuron_bundle')

df_all_combined = df_all_combined.join(df_reference.set_index(['subject', 'color']), on=['subject', 'color'])

# Set density equal to area for DAB measures
if 'BF' in colors_to_analyze:
    mask = df_all_combined['color'] == 'BF'
    df_all_combined.loc[mask, ['density']] = [df_all_combined.loc[mask, ['area']]]

# Get dominant bundle and combine with sub bundles
bundles = (df_all_combined.groupby(['subject', 'brain_region', 'neuron_bundle'])['area'].sum()).reset_index()
max_bundles = (bundles.groupby(['subject', 'brain_region'])['area'].max()).reset_index()
max_bundles = max_bundles.join(bundles.set_index(['subject', 'brain_region', 'area']),
                               on=['subject', 'brain_region', 'area'])
max_bundles = max_bundles.drop(columns=['area'])
df_all_combined = df_all_combined.join(max_bundles.set_index(['subject', 'brain_region']),
                                       on=['subject', 'brain_region'], rsuffix='_temp')
df_all_combined['CST'] = df_all_combined.apply(lambda x: x.neuron_bundle_temp in x.neuron_bundle, axis=1)

if fix_leia:
    # Leia fix, disjointed
    leia_below_mask = (df_all_combined['color'] == 'red') & \
                      (df_all_combined['neuron_bundle'] == 'j') & \
                      (df_all_combined['subject'] == 'Leia')
    df_all_combined.loc[leia_below_mask, 'CST'] = True

if fix_merida:
    # Leia fix, disjointed
    leia_below_mask = (df_all_combined['color'] == 'green') & \
                      (df_all_combined['neuron_bundle'] == 'l') & \
                      (df_all_combined['subject'] == 'Merida')
    df_all_combined.loc[leia_below_mask, 'CST'] = True

if fix_HH05:
    # HH05 green fix, wrong initial bundle taken
    HH05_green_mask = (df_all_combined['color'] == 'green') & \
                      (df_all_combined['neuron_bundle'].str.contains('a')) & \
                      (df_all_combined['subject'] == 'HH05')
    df_all_combined.loc[HH05_green_mask, 'CST'] = True

df_all_combined = df_all_combined.drop(columns=['neuron_bundle_temp'])

# Plot all
# Get dominant tracts only and normalize (max normalization)
df_plot = df_all_combined.loc[df_all_combined['CST']].reset_index(drop=True)
# Sum adjacent tracts
df_plot = (df_plot.groupby(['subject', 'brain_region', 'height_bins'], as_index=False)[['density', 'area']].sum(
    numeric_only=True))
df_plot = df_plot.join(
    df_reference[['subject', 'brain_region', 'severity']].set_index(['subject', 'brain_region']),
    on=['subject', 'brain_region']).drop_duplicates()

# Normalize
df_plot['area_norm'] = df_plot.groupby(['subject', 'brain_region'])['area'].apply(lambda x: x / x.max())
df_plot['density_norm'] = df_plot.groupby(['subject', 'brain_region'])['density'].apply(lambda x: x / x.max())
# df_plot['area_norm'] = df_plot.groupby(['subject', 'brain_region'])['area'].apply(
#     lambda x: x / np.mean(np.sort(x)[-3:]))
# df_plot['density_norm'] = df_plot.groupby(['subject', 'brain_region'])['density'].apply(
#     lambda x: x / np.mean(np.sort(x)[-3:]))

# Plot variants of normalizations
plot_columns = ['area_norm', 'density_norm']
for plot_column in plot_columns:
    plt.figure()
    g = sns.relplot(data=df_plot, col='brain_region', x=plot_column, y='height_bins', hue='subject')
    # invert y axis in all n plots
    for ax in g.axes[0]:
        ax.invert_yaxis()
        ax.fill_between([0, max(df_plot[plot_column])], 0.435 * n_height_bins, 0.56 * n_height_bins, color='red',
                        alpha=.7)
    g.set_titles("Region: {col_name}")
    # Set super title and shift subplots down
    g.fig.suptitle(f"IC: {plot_column}")
    plt.subplots_adjust(top=0.85)
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_{plot_column}{reduce_below_str}.png"))
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_{plot_column}{reduce_below_str}.svg"))
    plt.show()

# Plot groups (line) (x-y axis inversion not supported by seaborn!)
for plot_column in plot_columns:
    plt.figure()
    g = sns.relplot(data=df_plot, col='brain_region', y=plot_column, x='height_bins', hue='severity', kind='line')
    # invert y axis in all n plots
    g.set_titles("Region: {col_name}")
    # Set super title and shift subplots down
    g.fig.suptitle(f"IC: {plot_column}")
    plt.subplots_adjust(top=0.85)
    plt.show()

# Plot remaining Fiber percentage
bin_below = 17
if reduce_below:
    bin_below = bin_below-2
df_remaining = df_plot.loc[(df_plot['height_bins'] >= 12) & (df_plot['height_bins'] <= bin_below)]

for plot_column in plot_columns:
    plt.figure()
    g = sns.relplot(data=df_remaining, col='brain_region', x=plot_column, y='height_bins', hue='subject')
    # invert y axis in all n plots
    for ax in g.axes[0]:
        ax.invert_yaxis()
    g.set_titles("Region: {col_name}")
    # Set super title and shift subplots down
    g.fig.suptitle(f"IC_remaining: {plot_column}")
    plt.subplots_adjust(top=0.85)
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_remaining_{plot_column}{reduce_below_str}.png"))
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_remaining_{plot_column}{reduce_below_str}.svg"))
    plt.show()

df_remaining_norm = df_remaining.copy()
df_max = df_remaining_norm.groupby(['brain_region', 'subject']).mean()
df_max = df_max.groupby('brain_region').max()
df_remaining_norm = df_remaining_norm.join(df_max, on='brain_region', rsuffix='_max')
df_remaining_norm['area_norm'] = df_remaining_norm['area_norm'] / df_remaining_norm['area_norm_max']
df_remaining_norm['density_norm'] = df_remaining_norm['density_norm'] / df_remaining_norm['density_norm_max']
df_out = (df_remaining_norm.groupby(['brain_region', 'subject']).mean()).reset_index()
df_out_std = (df_remaining_norm.groupby(['brain_region', 'subject']).std()).reset_index()
df_out = df_out.join(df_out_std, rsuffix='STD')
df_out.to_csv(os.path.join(dir_combined, f"IC_remaining_out_max{reduce_below_str}.csv"))

for plot_column in plot_columns:
    plt.figure()
    g = sns.catplot(data=df_remaining_norm, col='brain_region', y=plot_column, x='subject', kind='bar',
                    aspect=0.6, order=['Rey', 'Padme', 'Leia', 'Jyn', 'Vaiana', 'Merida', 'HH05'])
    # invert y axis in all n plots
    g.set_titles("Region: {col_name}")
    # Set super title and shift subplots down
    g.fig.suptitle(f"IC_remaining: {plot_column}")
    plt.subplots_adjust(top=0.85)
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_bar_remaining_{plot_column}{reduce_below_str}.png"))
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_bar_remaining_{plot_column}{reduce_below_str}.svg"))
    plt.show()

df_remaining_norm2 = df_remaining.copy()
df_max = df_remaining_norm2.loc[df_remaining_norm2['severity'] == 'Control'].groupby(['brain_region']).mean()
df_remaining_norm2 = df_remaining_norm2.join(df_max, on='brain_region', rsuffix='_max')
df_remaining_norm2['area_norm'] = df_remaining_norm2['area_norm'] / df_remaining_norm2['area_norm_max']
df_remaining_norm2['density_norm'] = df_remaining_norm2['density_norm'] / df_remaining_norm2['density_norm_max']
df_out2 = (df_remaining_norm2.groupby(['brain_region', 'subject']).mean()).reset_index()
df_out2_std = (df_remaining_norm2.groupby(['brain_region', 'subject']).std()).reset_index()
df_out2 = df_out.join(df_out2_std, rsuffix='STD')
df_out2.to_csv(os.path.join(dir_combined, f"IC_remaining_out_control{reduce_below_str}.csv"))

for plot_column in plot_columns:
    plt.figure()
    g = sns.catplot(data=df_remaining_norm2, col='brain_region', y=plot_column, x='subject', kind='bar',
                    aspect=0.6, order=['Rey', 'Padme', 'Leia', 'Jyn', 'Vaiana', 'Merida', 'HH05'])
    # invert y axis in all n plots
    g.set_titles("Region: {col_name}")
    # Set super title and shift subplots down
    g.fig.suptitle(f"IC_remaining: {plot_column}")
    plt.subplots_adjust(top=0.85)
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_bar2_remaining_{plot_column}{reduce_below_str}.png"))
    plt.savefig(os.path.join(dir_combined, f"directinal-analysis_bar2_remaining_{plot_column}{reduce_below_str}.svg"))
    plt.show()