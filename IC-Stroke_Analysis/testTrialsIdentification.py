import os.path
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.signal import deconvolve
import scipy as sc
import pandas as pd


def calculate_robot_min(sig):
    print(sig)
    # s = sig[::-1]
    id_s = sig.idxmin()
    print(id_s)
    return id_s


def check_start(sig):
    if 'start_x' in sig.values:
        st_c = True
        # id_c = sig.where(sig =='start_x').index # working but the index ist komisch
        # id_c = sig[sig == 'start_x'].index.to_list()
        id_c = sig.loc[sig == 'start_x'].index
        print(id_c)
    else:
        st_c = False
        id_c = float("NaN")

    return id_c


root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/FilesForKinematic/'

analogDF = pd.read_pickle(os.path.join(root_path, 'analog_20200609_Padme_Brain_Injury11.pkl'))
df_markers = pd.read_pickle(os.path.join(root_path, 'markers_20200609_Padme_Brain_Injury11.pkl'))

lightOn = df_markers.index[df_markers['meta', 'event_analog'] == 'light_on']  # without .loc
lightOff = df_markers.index[df_markers['meta', 'event_analog'] == 'light_off']

order = 4
samplingFreq = 3000
lowPassCutOff = 10
wn = lowPassCutOff / (samplingFreq / 2)

if analogDF.shape[1] == 9:
    kukaTrigger = analogDF['Voltage.Kuka_Trigger']
    kukaID = analogDF['Voltage.Kuka_ID']
    # b, a = sc.signal.butter(order, wn, 'low', analog=True) not really necessary
    # filteredkukaID = sc.signal.filtfilt(b, a, kukaID)
    kukaTrigAdj = kukaTrigger.iloc[::10].copy()
'''
a = df_markers['WRB'].melt(var_name="xyz", value_name="Timing", ignore_index=False)
sns.lineplot(data=a, x=a.index, y='Timing', hue='xyz')
sns.lineplot(data=kukaTrigAdj * 200)
plt.show()
'''
df = df_markers['WRB']
df.loc[:, 'y'] = df.loc[:, 'y'] * -1
df.loc[:, 'z'] = df.loc[:, 'z'] * -1

thX = 0.8 * df_markers['WRB', 'x'].max()  # frame 5584 - wrist aligned with the bar
thY = 0.8 * df_markers['WRB', 'y'].max() * -1  # same frame as for x
thZ = 0.9 * df_markers['WRB', 'z'].max() * -1  # same frame as for x

df['x'].loc[df['x'] <= thX] = thX
df['y'].loc[df['y'] <= thY] = thY
df['z'].loc[df['z'] <= thZ] = thZ
'''
a = df.melt(var_name="xyz", value_name="Timing", ignore_index=False)
sns.lineplot(data=a, x=a.index, y='Timing', hue='xyz')
sns.lineplot(data=kukaTrigAdj * 200)
plt.savefig('graph.svg')
plt.show()
'''
peaksX, _ = sc.signal.find_peaks(df['x'], height=df['x'].max() * 0.95)
# merge a series to a dataframe
# transform it to df,
# adjust column names to match the multilevel dataframe names=('marker', 'xyz') = levels
df_peaksX = df['x'][peaksX].to_frame()
df_peaksX.columns = pd.MultiIndex.from_tuples([('meta', 'peaks_x')], names=('marker', 'xyz'))
df_markers = df_markers.join(df_peaksX)
'''
plt.figure()
a = df.melt(var_name="xyz", value_name="Timing", ignore_index=False)
# sns.lineplot(data=a, x=a.index, y='Timing', hue='xyz')
plt.plot(df['x'].to_numpy())
plt.plot(peaksX, df['x'][peaksX], "x")
# plt.plot(lightOn.to_numpy().astype(int)/10000000, df['x'][lightOn.to_numpy().astype(int)/10000000],"x")
plt.show()
'''
# take light on and light off moments
lightOnOff = df_markers.index[
    (df_markers['meta', 'event_analog'] == 'light_on') | (
            df_markers['meta', 'event_analog'] == 'light_off')]  # without .loc
# exclude every second interval as it is the one between LightOff and LightOn
# lOnOff = pd.IntervalIndex.from_tuples(list(map(tuple, lightOnOff.to_numpy().reshape((-1,2)))))
# change_pointX = df.groupby(pd.cut(df.index, lOnOff, include_lowest=True))['x'].apply(lambda x: local_min(x))


# df_markers['meta', 'Moments'].loc[df_markers.index == test_df.to_frame().index] = 'beginningOfMovement' # works changes value only where I want it

# Calculate each time the signal in x passes the threshold
signal_x = df_markers[('WRB', 'x')] > thX
changes_x = signal_x ^ signal_x.shift()
sig_changes = changes_x.to_frame()
frames_of_change = sig_changes.index[sig_changes[('WRB', 'x')] == True].to_frame()
start_x = frames_of_change.iloc[::2]
end_x = frames_of_change.iloc[1::2]

df_markers.loc[start_x.index, ('meta', 'event_analog')] = 'start_x'
df_markers.loc[end_x.index, ('meta', 'event_analog')] = 'end_x'
df_markers.loc[df_peaksX.index, ('meta', 'event_analog')] = 'grasp'

# Calculate the pulling moment, when the robot is at its min point
lOnOff = pd.IntervalIndex.from_tuples(list(map(tuple, lightOnOff.to_numpy().reshape((-1, 2)))))
robot_local_min = analogDF.groupby(pd.cut(analogDF.index, lOnOff, include_lowest=True))['Voltage.Kuka_ID'].apply(
    lambda x: calculate_robot_min(x))
new_robot_df = pd.DataFrame(index=robot_local_min, data={'robot_min': ['robot_min']}).resample('10ms').first()

pull_moment = new_robot_df.index[new_robot_df['robot_min'] == 'robot_min']  # without .loc
df_markers.loc[pull_moment, ('meta', 'event_analog')] = 'pulling'

# Split in columns - looses the multiindex

'''
test_df2 = df_markers[('meta', 'event_analog')]
test_df2 = pd.get_dummies(df_markers, columns=[('meta', 'event_analog')])
test_df2.columns = pd.MultiIndex.from_tuples(
    [('meta', 'end_x'), ('meta', 'grasp'), ('meta', 'light_off'),
     ('meta', 'light_on'), ('meta', 'pulling'),
     ('meta', 'start_x')],  names=('marker', 'xyz'))
df_markers = df_markers.join(test_df2)
'''

df_markers.loc[lightOn, ('meta', 'light_on')] = True
df_markers.loc[lightOff, ('meta', 'light_off')] = True
df_markers.loc[start_x.index, ('meta', 'start_x')] = True
df_markers.loc[end_x.index, ('meta', 'end_x')] = True
df_markers.loc[df_peaksX.index, ('meta', 'grasp')] = True
df_markers.loc[pull_moment, ('meta', 'pull')] = True

# Check if the start_x is between light on and off otherwise look for it


'''
# First try to check if beginning of movement is between the light on and off
# n = df_markers['meta']
# df_markers.loc[df_markers[('meta', 'event_analog')]=='start_x', ('meta', 'event_analog')].index
actual_start = n.groupby(pd.cut(n.index, lOnOff, include_lowest=True))['event_analog'].apply(lambda x: check_start(x))
# new_df = actual_start.dropna().to_frame().set_index('event_analog')

# df_markers.loc[new_df.index, ('meta', 'event_analog')] = 'start_moving'
#df_markers.loc[actual_start, ('meta', 'event_analog')] = 'start_moving'
'''

meta_df = df_markers['meta']
cc = meta_df.groupby(['trial_analog'])['start_x'].first().to_frame().reset_index()
cc = cc.fillna(False)
cc.columns = pd.MultiIndex.from_tuples([('meta', 'trial_analog'), ('meta', 'start_movement')], names=('marker', 'xyz'))
# df_markers = df_markers.join(cc, on=[('meta', 'trial_analog')], lsuffix='meta')
# cd = df_markers.join(cc, on=[('meta', 'trial_analog')], how='left').fillna(method='ffill')

# works only with merge, but looses the index
# df_markers.reset_index().merge(cc, how="left").set_index('index')

ddf = df_markers.copy().reset_index()
new_dff = pd.merge(ddf, cc, on=[('meta', 'trial_analog')], how='left').set_index('index')

# try for the outside intervals
ct = new_dff[('meta', 'trial_analog')].where(new_dff[('meta', 'start_movement')] == False)

ct = new_dff[('meta', 'trial_analog')].where(new_dff[('meta', 'start_movement')] == False)[0]

mm = new_dff['meta']
# gets all trials
crt = mm.groupby([mm.index, 'trial_analog'])['start_movement'].first().to_frame().reset_index()
# gets only the false ones
cd = crt[crt['start_movement'] == False]
cd = crt[crt['start_movement'] is None]


'''

import os.path
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.signal import deconvolve
import scipy as sc
import pandas as pd
import ruptures as rpt


def local_min(sig, to_plot=False):
    x = sig.to_numpy()
    algo = rpt.Dynp(model="l1").fit(x)
    bkps = algo.predict(n_bkps=10)
    if to_plot:
        rpt.show.display(x, bkps, figsize=(10, 6))
        plt.title('Change Point Detection: Dynamic Programming Search Method')
        plt.show()
    id = sig.index[bkps[0]]
    # print(id)
    return id


def calculate_peak_y(sig):
    peaks_y = sc.signal.find_peaks(sig, height=sig.min() * 0.85)[0]
    return peaks_y


def calculate_peak_z(sig):
    peaks_z = sc.signal.find_peaks(sig, height=sig.min() * 0.75)[0]
    return peaks_z

def calculate_robot_min(sig):
    print(sig)
    # s = sig[::-1]
    id_s = sig.idxmin()
    print(id_s)
    return id_s


def check_start(sig):
    if 'start_x' in sig.values:
        st_c = True
        # id_c = sig.where(sig =='start_x').index # working but the index ist komisch
        # id_c = sig[sig == 'start_x'].index.to_list()
        id_c = sig.loc[sig == 'start_x'].index
        print(id_c)
    else:
        st_c = False
        id_c = float("NaN")

    return id_c


root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/FilesForKinematic/'

analogDF = pd.read_pickle(os.path.join(root_path, 'analog_20200609_Padme_Brain_Injury11.pkl'))
df_markers = pd.read_pickle(os.path.join(root_path, 'markers_20200609_Padme_Brain_Injury11.pkl'))

lightOn = df_markers.index[df_markers['meta', 'event_analog'] == 'light_on']  # without .loc
lightOff = df_markers.index[df_markers['meta', 'event_analog'] == 'light_off']

order = 4
samplingFreq = 3000
lowPassCutOff = 10
wn = lowPassCutOff / (samplingFreq / 2)

if analogDF.shape[1] == 9:
    kukaTrigger = analogDF['Voltage.Kuka_Trigger']
    kukaID = analogDF['Voltage.Kuka_ID']
    # b, a = sc.signal.butter(order, wn, 'low', analog=True) not really necessary
    # filteredkukaID = sc.signal.filtfilt(b, a, kukaID)
    kukaTrigAdj = kukaTrigger.iloc[::10].copy()

a = df_markers['WRB'].melt(var_name="xyz", value_name="Timing", ignore_index=False)
sns.lineplot(data=a, x=a.index, y='Timing', hue='xyz')
sns.lineplot(data=kukaTrigAdj * 200)
plt.show()

df = df_markers['WRB']
df.loc[:, 'y'] = df.loc[:, 'y'] * -1
df.loc[:, 'z'] = df.loc[:, 'z'] * -1

thX = 0.8 * df_markers['WRB', 'x'].max()  # frame 5584 - wrist aligned with the bar
thY = 0.8 * df_markers['WRB', 'y'].max() * -1  # same frame as for x
thZ = 0.9 * df_markers['WRB', 'z'].max() * -1  # same frame as for x

df['x'].loc[df['x'] <= thX] = thX
df['y'].loc[df['y'] <= thY] = thY
df['z'].loc[df['z'] <= thZ] = thZ

a = df.melt(var_name="xyz", value_name="Timing", ignore_index=False)
sns.lineplot(data=a, x=a.index, y='Timing', hue='xyz')
sns.lineplot(data=kukaTrigAdj * 200)
plt.savefig('graph.svg')
plt.show()

peaksX, _ = sc.signal.find_peaks(df['x'], height=df['x'].max() * 0.95)
# merge a series to a dataframe
# transform it to df,
# adjust column names to match the multilevel dataframe names=('marker', 'xyz') = levels
df_peaksX = df['x'][peaksX].to_frame()
df_peaksX.columns = pd.MultiIndex.from_tuples([('meta', 'peaks_x')], names=('marker', 'xyz'))
df_markers = df_markers.join(df_peaksX)

plt.figure()
a = df.melt(var_name="xyz", value_name="Timing", ignore_index=False)
# sns.lineplot(data=a, x=a.index, y='Timing', hue='xyz')
plt.plot(df['x'].to_numpy())
plt.plot(peaksX, df['x'][peaksX], "x")
# plt.plot(lightOn.to_numpy().astype(int)/10000000, df['x'][lightOn.to_numpy().astype(int)/10000000],"x")
plt.show()

# take light on and light off moments
lightOnOff = df_markers.index[
    (df_markers['meta', 'event_analog'] == 'light_on') | (
                df_markers['meta', 'event_analog'] == 'light_off')]  # without .loc
# exclude every second interval as it is the one between LightOff and LightOn
# lOnOff = pd.IntervalIndex.from_tuples(list(map(tuple, lightOnOff.to_numpy().reshape((-1,2)))))
# change_pointX = df.groupby(pd.cut(df.index, lOnOff, include_lowest=True))['x'].apply(lambda x: local_min(x))


# df_markers['meta', 'Moments'].loc[df_markers.index == test_df.to_frame().index] = 'beginningOfMovement' # works changes value only where I want it

# Calculate each time the signal passes the threshold
signal_x = df_markers[('WRB', 'x')] > thX
changes_x = signal_x ^ signal_x.shift()
sig_changes = changes_x.to_frame()
frames_of_change = sig_changes.index[sig_changes[('WRB', 'x')] == True].to_frame()
start_x = frames_of_change.iloc[::2]
end_x = frames_of_change.iloc[1::2]

df_markers.loc[lightOn, ('meta', 'light_on')] = True
df_markers.loc[lightOff, ('meta', 'light_off')] = True
df_markers.loc[start_x.index, ('meta', 'start_x')] = True
df_markers.loc[end_x.index, ('meta', 'end_x')] = True
df_markers.loc[df_peaksX.index, ('meta', 'grasp')] = True

check_df = df_markers['meta']
check_df = check_df[check_df['grasp'].notna()]

df_markers.loc[start_x.index, ('meta', 'event_analog')] = 'start_x'
df_markers.loc[df_peaksX.index, ('meta', 'event_analog')] = 'grasp'

# Calculate each time the signal passes the threshold
lOnOff = pd.IntervalIndex.from_tuples(list(map(tuple, lightOnOff.to_numpy().reshape((-1, 2)))))
robot_local_min = analogDF.groupby(pd.cut(analogDF.index, lOnOff, include_lowest=True))['Voltage.Kuka_ID'].apply(
    lambda x: calculate_robot_min(x))
new_thing = pd.DataFrame(index=robot_local_min, data={'robot_min': ['robot_min']}).resample('10ms').first()

pull_moment = new_thing.index[new_thing['robot_min'] == 'robot_min']  # without .loc
df_markers.loc[pull_moment, ('meta', 'event_analog')] = 'pulling'
df_markers.loc[pull_moment, ('meta', 'pull')] = True

# Check if the start_x is between light on and off otherwise look for it

n = df_markers['meta']

# df_markers.loc[df_markers[('meta', 'event_analog')]=='start_x', ('meta', 'event_analog')].index
actual_start = n.groupby(pd.cut(n.index, lOnOff, include_lowest=True))['event_analog'].apply(lambda x: check_start(x))
# new_df = actual_start.dropna().to_frame().set_index('event_analog')

# df_markers.loc[new_df.index, ('meta', 'event_analog')] = 'start_moving'
df_markers.loc[actual_start, ('meta', 'event_analog')] = 'start_moving'

meta_df = df_markers['meta']

cc = meta_df.groupby(['trial_analog'])['start_x'].first().to_frame()

cc.columns = pd.MultiIndex.from_tuples([('meta', 'start_movement')], names=('mov', 'times'))
df_markers = df_markers.join(cc)
'''
