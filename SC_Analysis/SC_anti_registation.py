import SimpleITK as sitk
import os
import pandas as pd
import sc_functions as fun
from skimage import io

'''
SKRIPT
'''

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

regInfo = pd.read_csv(os.path.join(os.path.join(root_dir, 'ReferenceFiles'), 'AntiRegInfo.csv'))

inputMoving = os.path.join(root_dir, 'AnnotationsAntiProcessedReg')
outputReg = os.path.join(root_dir, 'RegistrationResultsAnti')
if not os.path.exists(outputReg):
    os.mkdir(outputReg)

if 'atlas_path' not in regInfo:
    inputDetections = os.path.join(root_dir, 'DetectionsProcessedReg')
    input_Atlas = os.path.join(root_dir, 'SC_AtlasPadded')
    regInfo['atlas_path'] = input_Atlas + os.sep + regInfo["segment"] + ".png"

short_df = regInfo.loc[(regInfo['monkey'] != "Merida") & (regInfo['monkey'] != "Padme")]

for i, row in enumerate(short_df.itertuples()):
    if row.status == False:
        fixedImage = sitk.ReadImage(row.atlas_path, sitk.sitkFloat32)
        movingImage = sitk.ReadImage(row.ann_path, sitk.sitkFloat32)
        image_name = row.ann_name.split('.')[0]

        rigidT, bSplT = fun.reg_function(fixedImage, movingImage, outputReg, image_name)

        det_image = io.imread(row.det_path)

        det_image_sitk = sitk.GetImageFromArray(det_image)
        fun.transformation_function(fixedImage, rigidT, bSplT, det_image_sitk, outputReg, image_name)
        regInfo.loc[i,'status'] = True


