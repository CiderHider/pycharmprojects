import os
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import sc_functions as fun

root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

createRegPlots = False
sigma_val_filter = 15
all_animals = True

outputFigures = os.path.join(root_path, 'Figures', 'RegistrationAnti')
if not os.path.exists(outputFigures):
    os.mkdir(outputFigures)

outputLaminas = os.path.join(root_path, 'LaminaResultsAnti')
if not os.path.exists(outputLaminas):
    os.mkdir(outputLaminas)

# Read file containing general info about the monkeys and merge it with the infos obtianed from the registration
monkeyInfo = pd.read_csv(
    os.path.join(os.path.join(root_path, 'ReferenceFiles'), 'monkey_histo_reference_sheet_anti.csv'), sep=',')
monkeyInfo = monkeyInfo.dropna()
regInfo = pd.read_csv(os.path.join(os.path.join(root_path, 'ReferenceFiles'), 'SegmentsAll_Anti.csv'), sep=';')
regInfo['file_name'] = regInfo['monkey'] + '_' + regInfo['slide'] + '_' + regInfo['color']
regInfo = regInfo.join(monkeyInfo.set_index(["name", 'color']), on=['monkey', 'color'])
regInfo = regInfo.rename(columns={'index': 'monkey'})
# regInfo = regInfo.loc[(regInfo['monkey'] == "Padme")]

# Take list of the registration resulted files and and add it to a datagrame
file_list = glob.glob(os.path.join(os.path.join(root_path, 'RegistrationResultsAnti'), '*.tif'))
df = pd.DataFrame(file_list, columns=['reg_path'])
df['file_name'] = df['reg_path'].apply(lambda x: x.split('/')[-1].split('.')[0])

# join images df with the reference list
reg_df = regInfo.join(df.set_index(["file_name"]), on=['file_name'])
reg_df = reg_df[reg_df['reg_path'].notna()]

# Compute sum of the images belonging to the same monkey, segment and color
grouping = ['monkey', 'segment', 'color']
small_df = reg_df.groupby(grouping).apply(lambda x: fun.sum_goupped_images(x))
result_df = small_df.copy().reset_index()
result_df.rename(columns={0: 'sum_seg'}, inplace=True)

# Read the corresponding atlas masks needed for the plotting
atlas_masks = os.path.join(root_path, 'SC_AtlasMasks')
result_df['atlas_path'] = atlas_masks + os.sep + "W_" + result_df["segment"] + ".png"
result_df['atlas_cont'] = atlas_masks + os.sep + "M_" + result_df["segment"] + ".png"

print('Creating summed image out of registered images...')
# Cut the sum images with the corresponding atlas mask
result_df[['masked_images', 'gauss_masked_image']] = result_df.apply(
    lambda x: fun.apply_atlas_mask(x, sigma_val_filter, outputLaminas), axis=1)

# Calculate the max value per monkey per color for the plotting
result_df['max_val_img'] = result_df.apply(lambda x: np.max(x.masked_images), axis=1)
result_df['max_gauss'] = result_df.apply(lambda x: np.max(x.gauss_masked_image), axis=1)

ss = result_df.groupby(['monkey', 'color'])['max_gauss'].apply(lambda x: x.max()).to_frame().reset_index()
ss.rename(columns={"max_gauss": "max_color_gauss"}, inplace=True)
result_df = result_df.join(ss.set_index(["monkey", 'color']), on=['monkey', 'color'])

# Add the path towards the summed registration results
result_df['reg_result_path'] = outputLaminas + os.sep + result_df['monkey'] + '_' + result_df['segment'] + '_' + \
                               result_df['color'] + '.tif'

# Plot the result all or specific animal and color
if all_animals:
    result_df.apply(lambda x: fun.plot_results_gm(x, outputLaminas, createRegPlots, True), axis=1)
else:
    print('Give the name of the animal to be plotted:')
    given_animal = str(input())
    part_df = result_df.loc[result_df['monkey'] == given_animal]
    part_df.apply(lambda x: fun.plot_results_gm(x, outputLaminas, createRegPlots, True), axis=1)

# Discard arrays saved in the dataframe
result_df.drop(['sum_seg', 'masked_images', 'gauss_masked_image'], axis=1, inplace=True)
result_df.to_csv(os.path.join(os.path.join(root_path, 'ReferenceFiles'), 'LaminaRegInfo.csv'), index=False)
