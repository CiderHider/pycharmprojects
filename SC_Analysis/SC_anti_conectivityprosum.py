import os
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import sc_functions as fun
from skimage import feature
from skimage.morphology import (erosion, dilation, opening, closing)
from skimage.morphology import disk  # noqa
import seaborn as sns


def dil_func(input_df):
    img = io.imread(input_df)
    greymask = np.where(img > 0, 1, 0)
    dd = np.sum(dilation(greymask, disk(3)))
    return dd


def calc_density_GM(input_df):
    img = io.imread(input_df['reg_path'])
    bin_img = np.where(img > 0, 1, 0)
    mask = io.imread(input_df['atlas_path'])
    density_img = np.sum(bin_img) / np.count_nonzero(mask)
    dil_img = input_df['dens_dil_img']
    density_dil_img = np.sum(dil_img) / np.count_nonzero(mask)
    dif_img = density_dil_img / density_img
    return pd.Series({'no_filter': density_img, 'filter': density_dil_img, 'percent_filter': dif_img})


def plot_func_gm_density(df_data, brain_reg, output_folder, to_plot):
    df_data = df_data.sort_values(by=['segment'])
    df_data = df_data.loc[df_data['color'] == brain_reg]
    ticks_array = [0, 1]
    outputName = brain_reg + '_percentageDensity'

    if to_plot:
        h = sns.barplot(data=df_data, y='segment', x='percent_filter', hue='severity',
                         hue_order=['Healthy', 'Mild', 'Moderate'], palette=fun.define_colormap('M1'), ci=80)
        h.set_xticks(ticks_array)
        h.set(title=outputName)
        h.tick_params(labelsize=15)

        plt.savefig(os.path.join(output_folder, (outputName + '.svg')), format='svg')
        plt.show()
    return;


root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

output_folder = os.path.join(root_path, 'TestConnectivity')
if not os.path.exists(output_folder):
    os.mkdir(output_folder)

file_list = glob.glob(os.path.join(os.path.join(root_path, 'LaminaResultsAnti'), '*.tif'))
df = pd.DataFrame(file_list, columns=['reg_path'])
df['file_name'] = df['reg_path'].apply(lambda x: x.split('/')[-1].split('.')[0])
df['monkey'] = df['file_name'].apply(lambda x: x.split('_')[0])
df['segment'] = df['file_name'].apply(lambda x: x.split('_')[1])
df['color'] = df['file_name'].apply(lambda x: x.split('_')[2])

# Read the corresponding atlas masks needed for the plotting
atlas_masks = os.path.join(root_path, 'SC_AtlasMasks')
df['atlas_path'] = atlas_masks + os.sep + "W_" + df["segment"] + ".png"

csvFilePath = os.path.join(root_path, 'ReferenceFiles')
monkeyInfo = pd.read_csv(os.path.join(csvFilePath, 'Monkey_Reference_sheet.csv'), sep=',')


disk_size = 5

# tried in one function and failed, don't know why -.-
df['dens_dil_img'] = df['reg_path'].apply(lambda x: dil_func(x))
df[['no_filter', 'filter', 'percent_filter']] = df.apply(lambda x: calc_density_GM(x), axis=1)

'''
to_plot_red = df.loc[df['region']== 'red']
to_plot_red = to_plot_red.sort_values(by=['segment'])
h = sns.barplot(data=to_plot_red, y='segment', x='percent_filter', hue='monkey',ci=None)
plt.show()
'''

ss = df.groupby(['monkey', 'color'])['percent_filter'].apply(lambda x: x.max()).to_frame().reset_index()
ss.rename(columns={"percent_filter": "max_percent"}, inplace=True)
df = df.join(ss.set_index(["monkey", 'color']), on=['monkey', 'color'])

df['norm_density'] = df.percent_filter / df.max_percent

df = df.join(monkeyInfo.set_index(['monkey']), on=['monkey'])
df = df.sort_values(by=['segment'])



to_plot_red = df.loc[df['color'] == 'red']
h = sns.barplot(data=to_plot_red, y='segment', x='percent_filter', hue='severity',
                hue_order=['Healthy', 'Mild', 'Moderate'], palette=fun.define_colormap('M1'),  ci=None)
plt.show()

plot_func_gm_density(df, 'red', output_folder, True)
plot_func_gm_density(df, 'green', output_folder, True)
'''
h = sns.lineplot(data=df_data, x='segment', y=datatype, hue='severity',
                 hue_order=['Healthy', 'Mild', 'Moderate'], palette=colormap, ci=80)
# g.set_yticks(ticks_array)
h.set_yticks(ticks_array)
h.set(title=outputName)
h.tick_params(labelsize=20)
'''
# percentage of pixel in the mask

'''
test_image = file_list[12]

img = io.imread(test_image)

plt.imshow(img, cmap='gray')
io.imsave(os.path.join(output_folder,'img.png'), img, check_contrast=False)
plt.show()

binary_mappimg = np.where(img>0,1,0)
plt.imshow(binary_mappimg, cmap='gray')
io.imsave(os.path.join(output_folder,'img_bin.png'), binary_mappimg, check_contrast=False)
plt.show()

dil_img = dilation(img, disk(5))
plt.imshow(dil_img, cmap='gray')
io.imsave(os.path.join(output_folder,'img_dil.png'), dil_img, check_contrast=False)
plt.show()

dil_img_binary = dilation(binary_mappimg, disk(5))
plt.imshow(dil_img_binary, cmap='gray')
io.imsave(os.path.join(output_folder,'img_dilbin.png'), dil_img_binary, check_contrast=False)
plt.show()
'''
