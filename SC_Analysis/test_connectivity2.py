import os
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.io as io
import sc_functions as fun
from skimage import feature
from skimage.morphology import (erosion, dilation, opening, closing)
from skimage.morphology import disk  # noqa
import seaborn as sns
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler


def dil_func(input_df):
    img = io.imread(input_df)
    greymask = np.where(img > 0, 1, 0)
    dd = np.sum(dilation(greymask, disk(3)))
    return dd


def coord_white_pixels(input_df):
    img = io.imread(input_df)
    greymask = np.where(img > 0, 1, 0)
    out=np.nonzero(greymask)
    out_both = np.argwhere(greymask == 1)
    return pd.Series({'coord_x': out[0], 'coord_y': out[1],'coord_both':out_both})


def calc_density_GM(input_df):
    img = io.imread(input_df['reg_path'])
    bin_img = np.where(img > 0, 1, 0)
    mask = io.imread(input_df['atlas_path'])
    density_img = np.sum(bin_img) / np.count_nonzero(mask)
    dil_img = input_df['dens_dil_img']
    density_dil_img = np.sum(dil_img) / np.count_nonzero(mask)
    dif_img = density_dil_img / density_img
    return pd.Series({'no_filter': density_img, 'filter': density_dil_img, 'percent_filter': dif_img})


def plot_func_gm_density(df_data, brain_reg, output_folder, to_plot):
    df_data = df_data.sort_values(by=['segment'])
    df_data = df_data.loc[df_data['color'] == brain_reg]
    ticks_array = [0, 1]
    outputName = brain_reg + '_percentageDensity'

    if to_plot:
        h = sns.barplot(data=df_data, y='segment', x='percent_filter', hue='severity',
                         hue_order=['Healthy', 'Mild', 'Moderate'], palette=fun.define_colormap('M1'), ci=80)
        h.set_xticks(ticks_array)
        h.set(title=outputName)
        h.tick_params(labelsize=15)

        plt.savefig(os.path.join(output_folder, (outputName + '.svg')), format='svg')
        plt.show()
    return;


root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

output_folder = os.path.join(root_path, 'TestConnectivity')
if not os.path.exists(output_folder):
    os.mkdir(output_folder)

file_list = glob.glob(os.path.join(os.path.join(root_path, 'LaminaResultsAnti'), '*.tif'))
df = pd.DataFrame(file_list, columns=['reg_path'])
df['file_name'] = df['reg_path'].apply(lambda x: x.split('/')[-1].split('.')[0])
df['monkey'] = df['file_name'].apply(lambda x: x.split('_')[0])
df['segment'] = df['file_name'].apply(lambda x: x.split('_')[1])
df['color'] = df['file_name'].apply(lambda x: x.split('_')[2])

# Read the corresponding atlas masks needed for the plotting
atlas_masks = os.path.join(root_path, 'SC_AtlasMasks')
df['atlas_path'] = atlas_masks + os.sep + "W_" + df["segment"] + ".png"

csvFilePath = os.path.join(root_path, 'ReferenceFiles')
monkeyInfo = pd.read_csv(os.path.join(csvFilePath, 'Monkey_Reference_sheet.csv'), sep=',')




df[['coord_x','coord_y','coord_both']]  = df['reg_path'].apply(lambda x: coord_white_pixels(x))

Y = df.iloc[3, 8]
clustering = DBSCAN(eps=2,  min_samples=5).fit(Y)

'''
from sklearn.decomposition import PCA
pca = PCA(n_components=2).fit(Y)
pca_2d = pca.transform(Y)
for i in range(0, pca_2d.shape[0]):
    if clustering.labels_[i] == 0:
        c1 = plt.scatter(pca_2d[i,0],pca_2d[i,1],c='r',marker='+')
    elif clustering.labels_[i] == 1:
        c2 = plt.scatter(pca_2d[i,0],pca_2d[i,1],c='g', marker='o')
    elif clustering.labels_[i] == -1:
        c3 = plt.scatter(pca_2d[i,0],pca_2d[i,1],c='b', marker='*')
plt.legend([c1, c2, c3], ['Cluster 1', 'Cluster 2', 'Noise'])
plt.title('DBSCAN finds 2 clusters and noise')
plt.show()

'''

# Compute DBSCAN
db = DBSCAN(eps=10, min_samples=30).fit(Y)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

import matplotlib.pyplot as plt

# Black removed and is used for noise instead.
unique_labels = set(labels)
colors = [plt.cm.Spectral(each) for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = labels == k

    xy =Y[class_member_mask & core_samples_mask]
    plt.plot(
        xy[:, 0],
        xy[:, 1],
        "o",
        markerfacecolor=tuple(col),
        markeredgecolor="k",
        markersize=14,
    )

    xy = Y[class_member_mask & ~core_samples_mask]
    plt.plot(
        xy[:, 0],
        xy[:, 1],
        "o",
        markerfacecolor=tuple(col),
        markeredgecolor="k",
        markersize=6,
    )

plt.title("Estimated number of clusters: %d" % n_clusters_)
plt.show()