import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
import re
import sc_functions as fun

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/LESION/'

csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
monkey_reference = pd.read_csv(os.path.join(csvFilePath, 'monkey_ref_sheet.csv'),sep=';')

data_folder = os.path.join(root_dir, 'FileInjections')

file_list = glob.glob(os.path.join(data_folder, '*.csv'))

dfResult = pd.DataFrame()
for file in file_list:
    df = pd.DataFrame()
    rawData = pd.read_csv(file, sep=',')
    file_path, file_name = os.path.split(file)
    contourName = np.unique(np.array(rawData[rawData.columns[1]]))

    df['Region'] = contourName
    df['Animal'] = (re.search('(.*)_Annotations.csv', file_name)).group(1)

    areaStructure = np.zeros(contourName.shape)
    for i in range(contourName.size):
        areaStructure[i] = rawData.loc[rawData[rawData.columns[1]] == contourName[i],
                                       rawData.columns[7]].sum() * 300 / np.power(10, 9)
    df['Area'] = areaStructure

    dfResult = pd.concat([dfResult, df])

dfResult=dfResult[dfResult['Region'].str.contains("Injection")]
dfResult['Region']=dfResult['Region'].map(lambda x: x.rstrip('Injection'))
dfResult['Region']=dfResult['Region'].map(lambda x: x.rstrip('Injections'))

#sns.catplot(x="Animal", y="Area", hue="Region", kind="bar",data=dfResult)
'''

sns.set_theme(style="whitegrid")
g = sns.catplot(
    data=dfResult, kind="bar",
    x="Animal", y="Area", hue="Region",
    palette="magma", alpha=.9
)
g.despine(left=True)
g.set_axis_labels("", "Volume")
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#g.legend.set_title("")

plt.savefig(os.path.join(root_dir, 'Figures',"injVolume.svg"), format="svg")
'''


dfResult = dfResult.join(monkey_reference.set_index('monkey'), on='Animal')

colormap=fun.define_colormap('Lesion')
ax = sns.barplot(data=dfResult, x='Region', y='Area', ci=None, palette=colormap)
ax = sns.swarmplot(data=dfResult, x='Region', y='Area',
                   size=15,color=(192 / 255, 192 / 255, 192 / 255, 1.0))

ax.set_yticks(np.linspace(0, 40, num=3))
ax.tick_params(labelsize=15)
ax.set(title='Volume injected')
#plt.show()
plt.savefig(os.path.join(root_dir, 'Figures', 'InjectionVolume.svg'), dpi=100)

plt.show()
#dfResult.to_excel(os.path.join(root_dir, "ReferenceFiles", "injection_volume.xlsx"),index = False, header=True)