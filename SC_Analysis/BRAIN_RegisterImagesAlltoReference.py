import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd
import skimage.io as io


def plot_reg(status, img1, img2, out, ig_name, s_img):
    if status:
        fig = plt.figure()
        plt.imshow(sitk.GetArrayViewFromImage(img1), cmap=plt.cm.Greys_r, alpha=0.7)
        plt.imshow(sitk.GetArrayViewFromImage(img2), cmap=plt.cm.Greys_r, alpha=0.5)
        plt.savefig(os.path.join(out, (ig_name + s_img)), format='svg')
        plt.axis('off')
        #plt.show()
        #sitk.WriteImage(simg3, os.path.join(output_reg, (out_name + '.png')))



root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN'

input_images = os.path.join(root_dir, 'MovingImages')
image_list = glob.glob(os.path.join(input_images, '*.png'))

output_result = os.path.join(root_dir, 'Aligned')
if not os.path.exists(output_result):
    os.mkdir(output_result)

output_result_noreg = os.path.join(root_dir, 'AlignedNoReg')
if not os.path.exists(output_result_noreg):
    os.mkdir(output_result_noreg)

output_reg = os.path.join(root_dir, 'RegistrationResults','Aligned')
if not os.path.exists(output_reg):
    os.mkdir(output_reg)


fixed = sitk.ReadImage(os.path.join(input_images,'Merida_DS-64_Filter-Mean-5_Brain_035.png'), sitk.sitkFloat32)

for img in image_list:
    file_path, file_name = os.path.split(img)
    moving = sitk.ReadImage(img, sitk.sitkFloat32)
    image_name = file_name.split('.')[0]

    status = True  # if all the plots should be outputed or not
    initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                          moving,
                                                          sitk.Similarity2DTransform(),
                                                          sitk.CenteredTransformInitializerFilter.GEOMETRY)
    # Test to see how they look as overlay
    moving_resampled = sitk.Resample(moving, fixed,
                                     initial_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
    plot_reg(status, moving_resampled, fixed, output_reg, image_name, 'NotRegistred.svg')

    mm = sitk.Cast(moving_resampled, sitk.sitkUInt8)
    sitk.WriteImage(mm, os.path.join(output_result_noreg, (image_name + '.png')))

    registration_method = sitk.ImageRegistrationMethod()

    # Similarity metric settings.
    registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.01)
    registration_method.SetInterpolator(sitk.sitkLinear)

    # Optimizer settings.
    registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                      numberOfIterations=1000,
                                                      convergenceMinimumValue=1e-6,
                                                      convergenceWindowSize=500)
    registration_method.SetOptimizerScalesFromPhysicalShift()

    # Setup for the multi-resolution framework.
    # registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[6, 2, 1])
    # registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
    # registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

    # Don't optimize in-place, we would possibly like to run this cell multiple times.
    registration_method.SetInitialTransform(initial_transform, inPlace=False)

    rigid_transformation = registration_method.Execute(fixed, moving)
    print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
    print('Optimizer\'s stopping condition, {0}'.format(
        registration_method.GetOptimizerStopConditionDescription()))

    moving_rigid = sitk.Resample(moving, fixed, rigid_transformation, sitk.sitkLinear, 0.0, moving.GetPixelID())
    plot_reg(status, moving_rigid, fixed, output_reg, image_name, 'regRigid.svg')

    mm = sitk.Cast(moving_rigid, sitk.sitkUInt8)
    sitk.WriteImage(mm, os.path.join(output_result, (image_name + '.png')))
