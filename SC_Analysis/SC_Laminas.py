import os
import glob
import re
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from numpy import load
from skimage import io
import skimage.color
import tkinter as tk
from tkinter import filedialog
import seaborn as sns



def mask_operation(density_image, lamina_mask, var_mask, operation='mean'):
    """
    Parameters
    ----------
    density_image : np.ndarray
        Numpy array containing image data

    lamina_mask : np.ndarray
        Numpy array containing 8 bit integer masks

    var_mask: int
        Integer with the mask value of the lamina_mask to be applied

    operation: String | ('mean', 'sum')
        String indicating whether sum or mean of the masked values should be returned. Default: 'mean'

    Returns
    -------
    result: float
        Result of operation on the masked image
    """
    mask_single = np.where(lamina_mask == var_mask, 1, 0)
    image_clean = density_image * mask_single
    if operation == 'mean':
        result = np.sum(image_clean)/np.count_nonzero(mask_single)
    elif operation == 'sum':
        result = np.sum(image_clean)
    else:
        raise Exception('Operation passed to function mask_operation() not found. ')

    return result


def plot_ipsi_contra_segments(df, subject_key='name', subject_list=[], show_plots=True, save_path="", plot_xlims=1,
                              secondary_iterator_key='brain_region'):
    """Plots and/or saves back-to-back horizontal barplots of contra_x and ispi_x columns according to the segments column in DF

    Parameters
    ----------
    df : pandas.Dataframe()
        Pandas Dataframe containing 'name' and 'segments' column as well as matched numeric data columns of contra_x and ispi_x

    subject_key: Key in df
        Subject key to iterate over. Default: 'name'

    subject_list : List of strings
        . List of subjects in df(subject_col) to plot. If subject_list is empty, all subjects are taken into account.
        
    show_plots: Boolean
        Shows plots if set to true. Default: True
        
    save_path: Path String
        Saves figures into passed path. Saves them to current folder if nothing is passed.

    plot_xlims: Number
            Plot xlimits=[Number, 0, Number] Default: [1, 0, 1]
    
    secondary_iterator_key: Key in df
            secondary key to iterate over. Default: 'brain_region'

    Returns
    -------
    void
    """

    # Copy Dataframe to avoid chaning original dataframe
    df_copy = df.copy()

    # Create Save Dir
    if save_path:
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        print(f"Saving Plots to Path: {save_path}")

    if not subject_list:
        subject_list = df_copy[subject_key].unique().tolist()

    # Define Ipsi and contralesional columns and *(-1) ipsilesional values for plotting
    ipsi_list = [x for x in df_copy.columns.tolist() if 'ipsi_' in x]
    df_copy[ipsi_list] = df_copy[ipsi_list] * -1
    plotting_list = [(re.search('ipsi_(.*)', x)).group(1) for x in ipsi_list]

    # Xlimit and Interval
    assert isinstance(plot_xlims, (int, float)), "plot_xlims must be a int or float"
    x_limits = [-0.05, 0.05] # changed the initial numbers
    x_ticks = np.linspace(x_limits[0], x_limits[1], num=5)
    print(x_limits)
    print(x_ticks)
    # Plotting
    for subject in subject_list:
        for region in df_copy[secondary_iterator_key].unique().tolist():
            df_plot = df_copy.loc[(df_copy[subject_key] == subject) & (df_copy[secondary_iterator_key] == region)]
            if df_plot.empty:
                print(f"Could not find subject: {subject}. Check the passed subject_list!")
                continue

            for lamina_plot in plotting_list:
                col_list = [f"contra_{lamina_plot}", f"ipsi_{lamina_plot}"]
                if not pd.Series(col_list).isin(df_plot).all():
                    print(f"Could not find contra and ipsi columns for: {subject} - {lamina_plot}")
                    continue

                plt.figure()
                fig = sns.barplot(data=df_plot, x=f"contra_{lamina_plot}", y='segment', color='#FA525B')  # , lw=0) # fig might be useless... define with with plt figure
                fig = sns.barplot(data=df_plot, x=f"ipsi_{lamina_plot}", y='segment', color='#F0DF0D')  # , lw=0)

                fig.set(xlabel="Signal Intensity / Lamina Area", ylabel="Spinal Cord Segment",
                        title=f"{subject} - {region} Projection Density in Lamina: {lamina_plot.upper()}", xlim=x_limits, xticks=x_ticks)
                # x_ticks = plt.gca().get_xticks().astype(np.int)

                plt.xticks(x_ticks, labels=np.abs(x_ticks))

                if save_path:
                    plt.savefig(os.path.join(save_path, f"{subject}_site-{region}_lamina-{lamina_plot}.svg"))

                if show_plots:
                    plt.show()
                else:
                    plt.close()

def normalizer(df, method='max'):
    """
    Column wise normalisation of numeric part of dataframe.

    Parameters
    ----------
    df: pd.Dataframe()
        Dataframe to be normalized
    method: String
        'max': normalisation by max - X / max(abs(X))
        'min-max': normalized by min-max - (X-min(X)) / (max(X)-min(X))
    Returns
    -------
    df_norm: pd.Dataframe()
        normalized Dataframe
    """
    df_norm = df.copy()
    df_numeric = df_norm.select_dtypes(np.number)
    cols_numeric = df_numeric.columns.to_list()
    if method == 'max':
        df_norm[cols_numeric] = df_numeric/df_numeric.abs().max()
    elif method == 'max-min':
        df_norm[cols_numeric] = (df_numeric-df_numeric.min())/(df_numeric.max()-df_numeric.min())
    else:
        raise Exception('Passed normalizer method not implemented')
    return df_norm


# plot_individually = False
# split_sides = True
save_pickle = False
show_plots = False

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'
csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
colorInfo = pd.read_csv(os.path.join(csvFilePath, 'colorsInSCMf.csv'), sep=';')
df_reference = pd.read_csv(os.path.join(csvFilePath, 'monkey_histo_reference_sheet.csv'))

regInfo = pd.read_csv(os.path.join(os.path.join(root_dir, 'ReferenceFiles'), 'RegInfo.csv'), sep=',')

# Collect Data in Dataframe
if not os.path.exists(root_dir):
    root_dir = os.path.expanduser("~")

# Get data to analyze
root = tk.Tk()
root.withdraw()
# root_dir = os.path.join(root_dir, 'RegistrationResults')
# filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=root_dir)

# Collect Data
df_all = pd.DataFrame()

output_path = os.path.join(root_dir, 'Figures')
if not os.path.exists(output_path):
    os.mkdir(output_path)

laminaList = glob.glob(os.path.join(root_dir, 'SC_LaminaBothSidesPadded', '*.png'))

inputFiles = os.path.join(root_dir, 'RegistrationResults')
file_list = glob.glob(os.path.join(inputFiles, '*.png'))

df_reg_images = pd.DataFrame(
    columns=['file_name', 'name', 'segment', 'brain_region', 'wmgm', 'image', 'lamina_mask_path', 'easy_check'])

for file in file_list:
    file_name = os.path.split(file)[1]
    monkey_name = file_name.split('_')[0]
    exIdx = int(regInfo[regInfo['det_name'] == file_name].index.values)
    segment = regInfo.loc[exIdx]['segment']
    brain_region = file_name.split('_')[2]
    wmgm = file_name.split('_')[3]

    easy_check = f"{monkey_name}{segment}{brain_region}{wmgm}"
    data = io.imread(file)

    if easy_check in df_reg_images['easy_check'].tolist():
        img_prev = df_reg_images.loc[df_reg_images['easy_check'] == easy_check, 'image'].values[0]
        img_updated = img_prev + data
        df_reg_images.loc[df_reg_images['easy_check'] == easy_check, 'image'] = [img_updated]
    else:
        df_reg_images = df_reg_images.append(
            {'file_name': file_name, 'name': monkey_name, 'segment': segment, 'image': [data],
             'brain_region': brain_region, 'wmgm': wmgm, 'easy_check': easy_check,
             'lamina_mask_path': os.path.join(root_dir, 'SC_LaminaBothSidesPadded', (segment + '.png'))},
            ignore_index=True)

# Get Lamina Masks
df_reg_images['lamina_mask'] = df_reg_images['lamina_mask_path'].apply(lambda x: skimage.img_as_ubyte(io.imread(x)))

# Expand dataframe to include laminas
df_reg_images = df_reg_images.reindex(columns=df_reg_images.columns.tolist() + colorInfo['Segment'].tolist())

# Calculate mean of signal in each Lamina per Segment
for row in colorInfo.itertuples():
    mask_value = row.Number
    # Get mean signal for row.lamina_mask in each Segment
    df_reg_images[row.Segment] = df_reg_images.apply(
        lambda x: mask_operation(x.image, x.lamina_mask, mask_value), axis=1)

df_reg_images = df_reg_images.sort_values(by=['segment'])

df_reg_images['max_sig'] = df_reg_images.apply(lambda x: np.max(x.image), axis=1)


# if plot_individually:
#     df_plot = df_reg_images.melt(id_vars=['segment'], value_vars=colorInfo['Segment'].tolist(), var_name='lamina_region')
#
#     g = sns.FacetGrid(data=df_plot, row='lamina_region', row_order=colorInfo['Segment'].tolist())
#     g.map_dataframe(sns.barplot, x='value', y='segment', orient='h')
#     g.savefig(os.path.join(figure_output_path, f"{monkey_name}_{color}_Laminas.png"))

df_reg_images = df_reg_images.drop(columns=['image', 'lamina_mask'])
df_all = pd.concat([df_all, df_reg_images], axis=0)
# plt.show()

# Join with reference sheet
alignment = ['name', 'brain_region']
df_all = df_all.join(df_reference.set_index(alignment), on=alignment).reindex()

# Plot Combination
output_path = os.path.join(root_dir, 'Figures', 'Combined')
if not os.path.exists(output_path):
    os.mkdir(output_path)
df_plot = df_all.melt(id_vars=['name', 'segment', 'brain_region', 'wmgm'], value_vars=colorInfo['Segment'].tolist(),
                      var_name='lamina_region', value_name='summed_signal')
g = sns.FacetGrid(data=df_plot, row='lamina_region', col='name', hue='brain_region', row_order=colorInfo['Segment'].tolist(), sharex=False)
g.map_dataframe(sns.barplot, x='summed_signal', y='segment', orient='h')
g.savefig(os.path.join(output_path, f"Combined_Laminas.png"))

# Save pickle
if save_pickle:
    df_all.to_pickle(os.path.join(output_path, f"Combined_Laminas.pkl"))

"""
Plotting
"""
monkey_list = df_all['name'].unique().tolist()

# Get lamina Columns
lamina_columns = [x for x in df_all.columns.tolist() if 'ipsi_' in x or 'contra_' in x]

# Combination Injection Vol Normalized
df_normalized = df_all.copy()
df_normalized[lamina_columns] = df_normalized[lamina_columns].div(df_normalized['injection_vol'], axis=0)

# Plot injection volume normalized ipsi_contra plots:
# max_normalized = max(df_normalized[lamina_columns].max())
# max_normalized = np.ceil(max_normalized/5)*5
normalized_df = normalizer(df_normalized)
output_path = os.path.join(root_dir, 'Figures', 'Normalized')
plot_ipsi_contra_segments(df_normalized, subject_list=monkey_list, show_plots=show_plots, save_path=output_path)

# Plot ipsi_contra plots:
# max_laminas = max(df_all[lamina_columns].max())
# max_laminas = np.ceil(max_laminas/50)*50
df_laminas = normalizer(df_all)
output_path = os.path.join(root_dir, 'Figures', 'Laminas')
plot_ipsi_contra_segments(df_laminas, subject_list=monkey_list, show_plots=show_plots, save_path=output_path)

# Group normalisation
severity_normalized = df_normalized.groupby(['severity', 'segment', 'brain_region']).mean().reset_index()
severity_laminas = df_laminas.groupby(['severity', 'segment', 'brain_region']).mean().reset_index()

#
output_path = os.path.join(root_dir, 'Figures', 'Severity', 'Normalized')
plot_ipsi_contra_segments(severity_normalized, subject_key='severity', show_plots=show_plots, save_path=output_path)
output_path = os.path.join(root_dir, 'Figures', 'Severity', 'Laminas')
plot_ipsi_contra_segments(severity_laminas, subject_key='severity', show_plots=show_plots, save_path=output_path)




'''
# plot_individually = False
# split_sides = True
save_pickle = False
show_plots = False
monkey_list = ['Merida', 'Vaiana', 'Jyn', 'Leia', 'Padme']
color_list = ['M1', 'PMv']

# root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'
csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
colorInfo = pd.read_csv(os.path.join(csvFilePath, 'colorsInSC.csv'))
df_reference = pd.read_csv(os.path.join(csvFilePath, 'monkey_histo_reference_sheet.csv'))
# Collect Data in Dataframe
if not os.path.exists(root_dir):
    root_dir = os.path.expanduser("~")

# Get data to analyze
root = tk.Tk()
root.withdraw()
root_dir = filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=root_dir)



# Collect Data
df_all = pd.DataFrame()
for monkey_name in monkey_list:
    for color in color_list:
        region = 'GM'
        folderType = color + region

        # segInfo = pd.read_csv(os.path.join(csvFilePath, f"{monkey_name}_Segments.csv"), sep=';')

        output_path = os.path.join(root_dir, 'Figures', monkey_name)
        if not os.path.exists(output_path):
            os.mkdir(output_path)

        laminaList = glob.glob(os.path.join(root_dir, 'SC_LaminaBothSides', '*.png'))

        inputFiles = os.path.join(root_dir, f"RegFiles{folderType}", monkey_name)
        file_list = glob.glob(os.path.join(inputFiles, '*.npy'))



        df_reg_images = pd.DataFrame(columns=['name', 'segment', 'color', 'region', 'image', 'lamina_mask', 'easy_check'])

        for file in file_list:
            file_name = os.path.split(file)[1]
            segment = (file_name.split('_'))[0]
            easy_check = f"{segment}{color}{region}"
            data = load(file)
            data = data.__abs__()
            if easy_check in df_reg_images['easy_check'].tolist():
                img_prev = df_reg_images.loc[df_reg_images['easy_check'] == easy_check, 'image'].values[0]
                img_updated = img_prev + data
                df_reg_images.loc[df_reg_images['easy_check'] == easy_check, 'image'] = [img_updated]
            else:
                df_reg_images = df_reg_images.append({'name': monkey_name, 'segment': segment, 'image': data,
                                                      'color': color, 'region': region, 'easy_check': easy_check,
                                                      'lamina_mask': np.nan}, ignore_index=True)

        # Get Lamina Masks
        for image in laminaList:
            file_name = os.path.split(image)[1]
            segment = (file_name.split('.'))[0]

            mask = skimage.color.rgb2gray(skimage.color.rgba2rgb(io.imread(image)))
            mask = skimage.img_as_ubyte(mask)
            df_reg_images.loc[df_reg_images['segment'] == segment, 'lamina_mask'] = [mask]


        # Expand dataframe to include laminas
        df_reg_images = df_reg_images.reindex(columns=df_reg_images.columns.tolist() + colorInfo['Segment'].tolist())

        # Calculate mean of signal in each Lamina per Segment
        for row in colorInfo.itertuples():
            mask_value = row.Number
            # Get mean signal for row.lamina_mask in each Segment
            df_reg_images[row.Segment] = df_reg_images.apply(lambda x: mask_operation(x.image, x.lamina_mask, mask_value), axis=1)

        df_reg_images = df_reg_images.sort_values(by=['segment'])

        df_reg_images['max_sig'] = df_reg_images.apply(lambda x: np.max(x.image), axis=1)

        # if plot_individually:
        #     df_plot = df_reg_images.melt(id_vars=['segment'], value_vars=colorInfo['Segment'].tolist(), var_name='lamina_region')
        #
        #     g = sns.FacetGrid(data=df_plot, row='lamina_region', row_order=colorInfo['Segment'].tolist())
        #     g.map_dataframe(sns.barplot, x='value', y='segment', orient='h')
        #     g.savefig(os.path.join(figure_output_path, f"{monkey_name}_{color}_Laminas.png"))

        df_reg_images = df_reg_images.drop(columns=['image', 'lamina_mask'])
        df_all = pd.concat([df_all, df_reg_images], axis=0)
        # plt.show()

# Join with reference sheet
alignment = ['name', 'color']
df_all = df_all.join(df_reference.set_index(alignment), on=alignment).reindex()

# Plot Combination
output_path = os.path.join(root_dir, 'Figures', 'Combined')
if not os.path.exists(output_path):
    os.mkdir(output_path)
df_plot = df_all.melt(id_vars=['name', 'segment', 'color', 'brain_region'], value_vars=colorInfo['Segment'].tolist(), var_name='lamina_region', value_name='summed_signal')
g = sns.FacetGrid(data=df_plot, row='lamina_region', col='name', hue='color', row_order=colorInfo['Segment'].tolist())
g.map_dataframe(sns.barplot, x='summed_signal', y='segment', orient='h')
g.savefig(os.path.join(output_path, f"Combined_Laminas.png"))

# Save pickle
if save_pickle:
    df_all.to_pickle(os.path.join(output_path, f"Combined_Laminas.pkl"))

'''