import os
import glob
import pandas as pd
import sc_functions as fun


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

# root_dir = r'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\HistoProcessedData\SC'
# root_dir = fun.check_file_path(root_dir)

# Load the .csv file with information about the segments
csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
segInfo = pd.read_csv(os.path.join(csvFilePath, 'SegmentsAll_Anti.csv'), sep=';')

# Calculate the max hight and width over all images - for the later padding
image_list_forDim = glob.glob(os.path.join(os.path.join(root_dir, 'AnnotationsAntiToBeProcessed'), '*.png'))
max_height, max_width = fun.max_dimentions(image_list_forDim)

# Compute dataframe for the annotation and images
image_list_ann, output_dir_ann = fun.set_folder_path(root_dir, 'AnnotationsAnti')
segInfo['ann_name'] = segInfo['monkey'] + "_" + segInfo['slide'] + "_" + segInfo['color'] + ".png"
segInfo['det_name'] = segInfo['monkey'] + "_" + segInfo['slide'] + "_" + segInfo['color'] + ".tif"

segInfo['ann_path_input'] = os.path.join(root_dir, 'AnnotationsAntiToBeProcessed') + os.sep + segInfo['ann_name']
segInfo['ann_path'] = os.path.join(root_dir, 'AnnotationsAntiProcessedReg') + os.sep + segInfo['ann_name']
segInfo['det_path_input'] = os.path.join(root_dir, 'DetectionsAntiToBeProcess') + os.sep + segInfo['det_name']
segInfo['det_path'] = os.path.join(root_dir, 'DetectionsAntiProcessedReg') + os.sep + segInfo['det_name']


# Adjust the annotations bw, flip and pad
segInfo = fun.preprocess_annotations_anti('Annotations', segInfo, max_height, max_width)

fun.preprocess_detections_anti('Detections', segInfo, max_height, max_width)


segInfo.to_csv(os.path.join(csvFilePath, 'AntiRegInfo.csv'), index=False)


'''
# Used for testing

exIdx = 3

inputImage = io.imread(segInfo.loc[exIdx, 'det_path_input'])
flipStatus = segInfo.loc[exIdx]['transformation']
status = segInfo.loc[exIdx]['keep']
# Convert the image to black and white and flip them
bw_image = fun.flip_and_bw_transform(inputImage, flipStatus, 'Detections')  # Flip image in the function
h, w = bw_image.shape
cmImgH, cmImgW = ndimage.measurements.center_of_mass(bw_image)
cmImgH = segInfo.loc[exIdx, 'cm_imgH']
cmImgW = segInfo.loc[exIdx, 'cm_imgW']
padded_image = fun.padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)
'''





'''
exIdx = 3

inputImage = io.imread(segInfo.loc[exIdx, 'ann_path_input'])
flipStatus = segInfo.loc[exIdx]['transformation']
status = segInfo.loc[exIdx]['keep']
# Convert the image to black and white and flip them
bw_image = fun.flip_and_bw_transform(inputImage[:,:,0], flipStatus, 'Annotations')  # Flip image in the function
h, w = bw_image.shape
cmImgH, cmImgW = ndimage.measurements.center_of_mass(bw_image)
segInfo.loc[exIdx, 'cm_imgH'] = cmImgH
segInfo.loc[exIdx, 'cm_imgW'] = cmImgW
padded_image = fun.padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)

out = padded_image.astype(np.uint8)
io.imsave(segInfo.loc[exIdx, 'ann_path'], padded_image, check_contrast=False)


'''



# Adjust the detections bw, flip and pad

'''
exIdx = 2

inputImage = io.imread(segInfo.loc[exIdx, 'det_path_input'])
inputImage = np.where(inputImage < 10, inputImage, 0)

flipStatus = segInfo.loc[exIdx]['transformation']
status = segInfo.loc[exIdx]['keep']
# Convert the image to black and white and flip them
bw_image = fun.flip_and_bw_transform(inputImage, flipStatus, 'Detections')  # Flip image in the function
out = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(bw_image)), sitk.sitkUInt8)
sitk.WriteImage(out, segInfo.loc[exIdx, 'det_path'])
'''
'''
h, w = bw_image.shape
cmImgH, cmImgW = ndimage.measurements.center_of_mass(bw_image)
segInfo.loc[exIdx, 'cm_imgH'] = cmImgH
segInfo.loc[exIdx, 'cm_imgW'] = cmImgW
padded_image = fun.padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)


'''

'''
segInfo['cm_imgH'] = np.nan
segInfo['cm_imgW'] = np.nan

exIdx = 2


inputImage = sitk.ReadImage(segInfo.loc[exIdx, 'ann_path_input'])
flipStatus = segInfo.loc[exIdx]['transformation']
status = segInfo.loc[exIdx]['keep']
# Convert the image to black and white and flip them
bw_image = fun.flip_and_bw_transform(inputImage, flipStatus, 'Annotations')  # Flip image in the function
h, w = bw_image.shape
cmImgH, cmImgW = ndimage.measurements.center_of_mass(bw_image)
segInfo.loc[exIdx, 'cm_imgH'] = cmImgH
segInfo.loc[exIdx, 'cm_imgW'] = cmImgW
padded_image = fun.padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)
out = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(padded_image)), sitk.sitkUInt8)
sitk.WriteImage(out, segInfo.loc[exIdx, 'ann_path'])


for row in segInfo.itertuples(index=False):
    print(segInfo.index)
    
'''