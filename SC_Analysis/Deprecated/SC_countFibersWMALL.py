import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
from numpy import loadtxt
import re


def plot_func(datapoints, dataline, datatype, ticksarray, outputFolder, outputName):
    colorMild = (245 / 255, 183 / 255, 177 / 255, 1.0)
    colorModerate = (169 / 255, 50 / 255, 38 / 255, 1.0)
    colorHealthy = (98 / 255, 101 / 255, 103 / 255, 1.0)
    colormap = np.array([colorHealthy, colorMild, colorModerate])

    fig = plt.figure()
    g = sns.scatterplot(data=datapoints, x='Segment', y=datatype, hue='Severity', style="Animal",
                        hue_order=['Healthy', 'Mild', 'Moderate'], palette=colormap, s=100)
    h = sns.lineplot(data=dataline, x='Segment', y=datatype, hue='Severity',
                     hue_order=['Healthy', 'Mild', 'Moderate'], palette=colormap)
    g.set_yticks(ticksarray)
    h.set_yticks(ticksarray)
    plt.savefig(os.path.join(outputFolder,(outputName+'.svg')), format='svg')
    plt.show()
    return;



root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'

csvFilePath =os.path.join(root_dir, 'ReferenceFiles')
monkeyInfo = pd.read_csv(os.path.join(csvFilePath,'Monkey_Reference_sheet.csv'), sep=',')
monkeys = monkeyInfo['Monkey']

save_folder = os.path.join(root_dir, 'Figures')
if not os.path.exists(save_folder):
    os.makedirs(save_folder)

df = pd.DataFrame()

for monkey_name in monkeys:
    csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
    segInfo = pd.read_csv(os.path.join(csvFilePath, (monkey_name+'_Segments.csv')), sep=';')
    data_folder = os.path.join(root_dir, 'Annotations', monkey_name)

    save_folder = os.path.join(root_dir, 'Figures')
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    file_list = glob.glob(os.path.join(data_folder, '*.txt'))
    for file in file_list:
        df_temp = pd.read_csv(file, sep='\t')
        file_path, file_name = os.path.split(file)

        df_temp['Animal'] = monkey_name
        parts = file_name.split('_')
        df_temp['FileName'] = parts[len(parts) - 1].split('.')[0]

        slideName = parts[len(parts) - 1].split('.')[0]
        exIdx = int(segInfo[segInfo['Slide'] == slideName].index.values)
        df_temp['Segment'] = segInfo.loc[exIdx]['Segment']

        monkeyIndex = int(monkeyInfo[monkeyInfo['Monkey'] == monkey_name].index.values)
        df_temp['Severity'] = monkeyInfo.loc[monkeyIndex]['Severity']

        df = df.append(df_temp)
'''

dfNew = df[['Animal','Segment','FileName','Severity','Name','Num M1Fibers','Num PMvFibers']]
dfNew = dfNew.loc[df['Name'].isin(['WhiteMatterLeft','WhiteMatterRight'])]
dfNew['Num M1Fibers'] = np.where(dfNew['Name'] == 'WhiteMatterRight',
                                           dfNew['Num M1Fibers'] * (-1),
                                           dfNew['Num M1Fibers'])
dfNew['Num PMvFibers'] = np.where(dfNew['Name'] == 'WhiteMatterRight',
                                           dfNew['Num PMvFibers'] * (-1),
                                           dfNew['Num PMvFibers'])


#CALCULATE FOR THE RED FIBERS
dd=dfNew.groupby(['Segment','Animal','Severity','Name'])['Num M1Fibers'].mean().unstack().reset_index()

m1Fibers = dd.groupby(['Animal','Severity'])['WhiteMatterLeft'].max().reset_index()
m1Fibers = m1Fibers.rename(columns={'WhiteMatterLeft': 'MaxLeft'})
m1Fibers = pd.merge(dd, m1Fibers,  how='left', on=['Animal','Severity'])

m1Fibers["Ipsi"]=m1Fibers["WhiteMatterRight"].div(m1Fibers["MaxLeft"])
m1Fibers["Contra"]=m1Fibers["WhiteMatterLeft"].div(m1Fibers["MaxLeft"])

meanWMred = pd.merge(m1Fibers.groupby(['Severity', 'Segment'])['WhiteMatterLeft'].mean().reset_index(),
                     m1Fibers.groupby(['Severity','Segment'])['WhiteMatterRight'].mean().reset_index(),
                     how='left', on=['Severity', 'Segment'])

meanM1contra = m1Fibers.groupby(['Severity', 'Segment'])['Contra'].mean().reset_index()
meanM1ipsi = m1Fibers.groupby(['Severity', 'Segment'])['Ipsi'].mean().reset_index()
meanM1=pd.merge(meanM1contra, meanM1ipsi, how='left', on=['Severity', 'Segment'])

plot_func(m1Fibers, meanWMred, "WhiteMatterLeft", [0, 2500], save_folder, 'WMRed_ContraCounts')
plot_func(m1Fibers, meanM1contra, "Contra", [], save_folder, 'WMRed_Contra')
plot_func(m1Fibers, meanWMred, "WhiteMatterRight", [0, -2500], save_folder, 'WMRed_IpsiCounts')
plot_func(m1Fibers, meanM1ipsi, "Ipsi", [-1, -0.8, -0.6, -0.4, -0.2], save_folder, 'WMRed_Ipsi')


#CALCULATE FOR THE GREEN FIBERS
dd=dfNew.groupby(['Segment','Animal','Severity','Name'])['Num PMvFibers'].mean().unstack().reset_index()

pmvFibers = dd.groupby(['Animal','Severity'])['WhiteMatterLeft'].max().reset_index().rename(columns={'WhiteMatterLeft': 'MaxLeft'})
pmvFibers = pd.merge(dd, pmvFibers,  how='left', on=['Animal','Severity'])

pmvFibers["Ipsi"]=pmvFibers["WhiteMatterRight"].div(pmvFibers["MaxLeft"])
pmvFibers["Contra"]=pmvFibers["WhiteMatterLeft"].div(pmvFibers["MaxLeft"])

meanWPMvred = pd.merge(pmvFibers.groupby(['Severity', 'Segment'])['WhiteMatterLeft'].mean().reset_index(),
                       pmvFibers.groupby(['Severity','Segment'])['WhiteMatterRight'].mean().reset_index(),
                       how='left', on=['Severity', 'Segment'])

meanPMvcontra = pmvFibers.groupby(['Severity', 'Segment'])['Contra'].mean().reset_index()
meanPMvipsi = pmvFibers.groupby(['Severity', 'Segment'])['Ipsi'].mean().reset_index()
meanM1 = pd.merge(meanPMvcontra, meanPMvipsi, how='left', on=['Severity', 'Segment'])


plot_func(pmvFibers, meanWPMvred, "WhiteMatterLeft", [0, 700], save_folder, 'WMGreen_ContraCounts')
plot_func(pmvFibers, meanPMvcontra, "Contra", [], save_folder, 'WMGreen_Contra')
plot_func(pmvFibers, meanWPMvred, "WhiteMatterRight", [0, -700], save_folder, 'WMGreen_IpsiCounts')
plot_func(pmvFibers, meanPMvipsi, "Ipsi", [-1, -0.8, -0.6, -0.4, -0.2], save_folder, 'WMGreen_Ipsi')
'''