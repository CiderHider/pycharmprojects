import os
import glob
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import SimpleITK as sitk


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'

csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
segInfo = pd.read_csv(os.path.join(csvFilePath, 'RegInfo.csv'), sep=',')
segInfo_needed = segInfo[['segment','det_name']].set_index('det_name')

outputFigures = os.path.join(root_dir, 'Figures', 'Registration')
if not os.path.exists(outputFigures):
    os.mkdir(outputFigures)

inputFiles = os.path.join(root_dir, 'RegistrationResults','TestSum')
file_list = glob.glob(os.path.join(inputFiles, '*.png'))
df = pd.DataFrame(columns=['name', 'path', 'monkey', 'slide', 'region', 'wmgm', 'reg_result'])

for image in file_list:
    file_path, file_name = os.path.split(image)
    animal_name, slide, region, wmgm, extra = file_name.split('_')

    dataImage = sitk.ReadImage(image, sitk.sitkUInt8)
    data = sitk.GetArrayViewFromImage(dataImage)
    # data=dataImage np.where((dataImage > 0), 1, 0)

    df = df.append({'name': file_name, 'path': image, 'monkey': animal_name,
                    'slide': slide, 'region': region, 'wmgm': wmgm,
                    'reg_result': data}, ignore_index=True)

result_df=df.set_index('name').join(segInfo_needed)

# Calculate sum pro segment pro monkey pro region and wmgm
cs_df = result_df.groupby(['monkey','segment','region','wmgm'])['reg_result'].apply(lambda x: x.sum()).reset_index()
'''
plt.imshow(cs_df.loc[1,['reg_result']][0], cmap="hot")
plt.show()
'''

# Add smoothing filter for the resulted sums
cs_df['smooth_result'] = cs_df['reg_result'].apply(lambda x: gaussian_filter(x, sigma=25))
'''
plt.imshow(aa_cut, cmap="hot")
plt.show()
'''

# Calculate parameters for plotting: max value for the scaling and color for the heatmap
cs_df['max_res'] = cs_df['smooth_result'].apply(lambda x: x.max())
cs_df['plot_color'] = "magma"
cs_df.loc[cs_df['wmgm'] == "GM", ['plot_color']] = "hot"

# Add atlas layout to the dataframe
atlasPath = os.path.join(root_dir, 'SC_AtlasMasks')
cs_df['atlas_path'] = atlasPath + os.sep + "M_"+ cs_df["segment"] + ".png"
cs_df['atlas_image'] = cs_df['atlas_path'].apply(lambda x:sitk.GetArrayViewFromImage(sitk.ReadImage(x, sitk.sitkUInt16)))
cs_df['output_name']= cs_df[['monkey', 'segment', 'region','wmgm']].agg('_'.join, axis=1)

id=0
fig = plt.figure()
aa= cs_df.loc[id,['smooth_result']][0]
aa_cut = aa[300:aa.shape[0]-300,300:aa.shape[1]-300]
atl = sitk.GetArrayViewFromImage(sitk.ReadImage(cs_df.loc[id,['atlas_path']][0], sitk.sitkUInt16))
atl_cut = atl[300:atl.shape[0]-300,300:atl.shape[1]-300]
plt.imshow(aa_cut, cmap=cs_df['plot_color'][id])
plt.imshow(atl_cut, cmap="gray", alpha=0.5)
plt.axis('off')
plt.savefig(os.path.join(outputFigures, (cs_df['output_name'][id] + '.svg')), format='svg')
plt.show()

csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
# Save everything in a .csv file
cs_df.to_csv(os.path.join(csvFilePath, 'RegPlotInfo.csv'), index=False)
#cs_df.to_pickle(os.path.join(csvFilePath,"RegPlotInfo.pkl"))


