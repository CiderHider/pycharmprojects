import os
import SimpleITK as sitk
import matplotlib.pyplot as plt

def displaySidebySide(fixed_npa,moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    plt.show()



root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/TestDataRegistration'
OUTPUT_DIR = root_dir
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

pathFixedImage = os.path.join(root_dir, 'C2.png')
pathMovingImage = os.path.join(root_dir, 'SC023_labels.png')
testingImage = os.path.join(root_dir, 'SegGC2.png')

fixed = sitk.ReadImage(pathFixedImage, sitk.sitkFloat32)
moving = sitk.ReadImage(pathMovingImage, sitk.sitkFloat32)
detImg = sitk.ReadImage(testingImage, sitk.sitkFloat32)
file_path, file_name = os.path.split(pathMovingImage)
parts = file_name.split('_')
imageName = parts[0]

fixIm = sitk.GetArrayFromImage(fixed)
fSize = fixed.GetSize()
mSize = sitk.GetArrayFromImage(moving).shape
displaySidebySide(sitk.GetArrayViewFromImage(fixed), sitk.GetArrayViewFromImage(moving))


# Apply initial alignment

initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                      moving,
                                                      sitk.Similarity2DTransform(),
                                                      sitk.CenteredTransformInitializerFilter.GEOMETRY)
#Test to see how they look as overlay
moving_resampled = sitk.Resample(moving, fixed,
                                 initial_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
plt.savefig(os.path.join(OUTPUT_DIR,(imageName + 'NotRegistred.svg')), format='svg')
plt.axis('off')
plt.show()


registration_method = sitk.ImageRegistrationMethod()

# Similarity metric settings.
registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
registration_method.SetMetricSamplingPercentage(0.01)
registration_method.SetInterpolator(sitk.sitkLinear)

# Optimizer settings.
registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                  numberOfIterations=500,
                                                  convergenceMinimumValue=1e-6,
                                                  convergenceWindowSize=500)
registration_method.SetOptimizerScalesFromPhysicalShift()

# Setup for the multi-resolution framework.
registration_method.SetShrinkFactorsPerLevel(shrinkFactors =[6, 2, 1])
registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

# Don't optimize in-place, we would possibly like to run this cell multiple times.
registration_method.SetInitialTransform(initial_transform, inPlace=False)

rigidTransformation = registration_method.Execute(fixed, moving)
print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
print('Optimizer\'s stopping condition, {0}'.format(registration_method.GetOptimizerStopConditionDescription()))

movingRigid = sitk.Resample(moving, fixed, rigidTransformation, sitk.sitkLinear, 0.0, moving.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(movingRigid), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
plt.savefig(os.path.join(OUTPUT_DIR,(imageName + 'regRigid.svg')), format='svg')
plt.axis('off')
plt.show()



# BSPLINE
transformDomainMeshSize = [2] * fixed.GetDimension()
tx = sitk.BSplineTransformInitializer(fixed, transformDomainMeshSize)

print(f"Initial Number of Parameters: {tx.GetNumberOfParameters()}")

R = sitk.ImageRegistrationMethod()
R.SetMetricAsJointHistogramMutualInformation()

R.SetOptimizerAsGradientDescentLineSearch(learningRate=5.0,
                                          numberOfIterations=100,
                                          convergenceMinimumValue=1e-4,
                                          convergenceWindowSize=5)

R.SetInterpolator(sitk.sitkLinear)

R.SetInitialTransformAsBSpline(tx, inPlace=True, scaleFactors=[1, 2, 5])
R.SetShrinkFactorsPerLevel([4, 2, 1])
R.SetSmoothingSigmasPerLevel([4, 2, 1])

bSplineTransformation = R.Execute(fixed, movingRigid)

print("-------")
print(bSplineTransformation)
print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
print(f" Iteration: {R.GetOptimizerIteration()}")
print(f" Metric value: {R.GetMetricValue()}")

movingRegBspline = sitk.Resample(movingRigid, fixed, bSplineTransformation, sitk.sitkLinear, 0.0, moving_resampled.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(movingRegBspline), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
plt.savefig(os.path.join(OUTPUT_DIR, (imageName + 'RegBspline.svg')), format='svg')
plt.show()

# Apply transformation to a different image

resamplerRigid = sitk.ResampleImageFilter()
resamplerRigid.SetReferenceImage(fixed)
resamplerRigid.SetInterpolator(sitk.sitkLinear)
resamplerRigid.SetDefaultPixelValue(50)
resamplerRigid.SetTransform(rigidTransformation)

outRigidApplied = resamplerRigid.Execute(detImg)

resamplerBSpline= sitk.ResampleImageFilter()
resamplerBSpline.SetReferenceImage(fixed)
resamplerBSpline.SetInterpolator(sitk.sitkLinear)
resamplerBSpline.SetDefaultPixelValue(50)
resamplerBSpline.SetTransform(bSplineTransformation)

outBSplineApplied = resamplerBSpline.Execute(outRigidApplied)

simg1 = sitk.Cast(sitk.RescaleIntensity(fixed), sitk.sitkUInt8)
simg2 = sitk.Cast(sitk.RescaleIntensity(outRigidApplied), sitk.sitkUInt8)
simg3 = sitk.Cast(sitk.RescaleIntensity(outBSplineApplied), sitk.sitkUInt8)

fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
plt.imshow(sitk.GetArrayViewFromImage(simg2), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(simg3), cmap=plt.cm.Greys_r, alpha=0.7)
plt.axis('off')
plt.savefig(os.path.join(OUTPUT_DIR, (imageName + 'detections.svg')), format='svg')
plt.show()





