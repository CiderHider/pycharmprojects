import os
import glob
import numpy as np
from skimage import io
import skimage.color
import matplotlib.pyplot as plt
import cv2
from skimage import feature
from skimage.morphology import (erosion, dilation, opening, closing)
from skimage.morphology import disk  # noqa


inputFixed = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/TestDataRegistration/SC023_labels.png'
outputFixed = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/TestDataRegistration'
fixedImage = cv2.cvtColor(cv2.imread(inputFixed), cv2.COLOR_BGR2GRAY).astype('int')
    #skimage.color.rgb2gray(io.imread(inputFixed)).astype('int')

if np.unique(fixedImage).size == 3:
    h, w = fixedImage.shape
    a = np.repeat(np.linspace(1, 20, num=h), w).reshape((h, w)).astype('int')
    #plt.imshow(a, cmap='gray')
    #plt.show()
    mask = np.where((fixedImage > 0) & (fixedImage < 255), 1, 0)
    segG = mask*a
    plt.imshow(segG, cmap='gray')

    resultedImage = segG.astype(np.uint8)
    #cv2.imwrite(os.path.join(outputFixed,"SegGC2.png"), resultedImage)
    skimage.io.imsave(os.path.join(outputFixed, "SegGC2.png"),  segG, check_contrast=False)


