import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd

def command_iteration(method, bspline_transform):
    if method.GetOptimizerIteration() == 0:
        # The BSpline is resized before the first optimizer
        # iteration is completed per level. Print the transform object
        # to show the adapted BSpline transform.
        print(bspline_transform)

    print(f"{method.GetOptimizerIteration():3} = {method.GetMetricValue():10.5f}")


def displaySidebySide(fixed_npa,moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    plt.show()

def registrationFunction(moving, fixed, detImage):
    # Apply initial alignment - rigid registration
    displaySidebySide(sitk.GetArrayViewFromImage(fixed), sitk.GetArrayViewFromImage(moving))

    initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                          moving,
                                                          sitk.Similarity2DTransform(),
                                                          sitk.CenteredTransformInitializerFilter.GEOMETRY)
    rigidReg = sitk.ImageRegistrationMethod()
    # Similarity metric settings.
    rigidReg.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
    rigidReg.SetMetricSamplingStrategy(rigidReg.RANDOM)
    rigidReg.SetMetricSamplingPercentage(0.01)
    rigidReg.SetInterpolator(sitk.sitkLinear)

    # Optimizer settings.
    rigidReg.SetOptimizerAsGradientDescent(learningRate=0.5,
                                           numberOfIterations=500,
                                           convergenceMinimumValue=1e-6,
                                           convergenceWindowSize=500)
    rigidReg.SetOptimizerScalesFromPhysicalShift()
    # Setup for the multi-resolution framework.
    rigidReg.SetShrinkFactorsPerLevel(shrinkFactors=[6, 2, 1])
    rigidReg.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
    rigidReg.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

    # Don't optimize in-place, we would possibly like to run this cell multiple times.
    rigidReg.SetInitialTransform(initial_transform, inPlace=False)
    rigidTransformation = rigidReg.Execute(fixed, moving)
    print('Final metric value: {0}'.format(rigidReg.GetMetricValue()))
    print('Optimizer\'s stopping condition, {0}'.format(
        rigidReg.GetOptimizerStopConditionDescription()))

    movingRigid = sitk.Resample(moving, fixed, rigidTransformation, sitk.sitkLinear, 0.0,
                                moving.GetPixelID())
    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(movingRigid), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.axis('off')
    plt.savefig(os.path.join(outputReg, (imageName + 'Rigid.svg')), format='svg')
    plt.show()

    # BSpline Registration
    transformDomainMeshSize = [2] * fixed.GetDimension()
    txBSpline = sitk.BSplineTransformInitializer(fixed, transformDomainMeshSize)

    print(f"Initial Number of Parameters: {txBSpline.GetNumberOfParameters()}")

    bSplineRegistration = sitk.ImageRegistrationMethod()
    bSplineRegistration.SetMetricAsJointHistogramMutualInformation()

    bSplineRegistration.SetOptimizerAsGradientDescentLineSearch(learningRate=5.0,
                                                                numberOfIterations=100,
                                                                convergenceMinimumValue=1e-4,
                                                                convergenceWindowSize=5)

    bSplineRegistration.SetInterpolator(sitk.sitkLinear)

    bSplineRegistration.SetInitialTransformAsBSpline(txBSpline, inPlace=True, scaleFactors=[1, 2, 5])
    bSplineRegistration.SetShrinkFactorsPerLevel([4, 2, 1])
    bSplineRegistration.SetSmoothingSigmasPerLevel([4, 2, 1])

    outputTransfBSpline = bSplineRegistration.Execute(fixed, movingRigid)

    print("-------")
    print(outputTransfBSpline)
    print(f"Optimizer stop condition: {bSplineRegistration.GetOptimizerStopConditionDescription()}")
    print(f" Iteration: {bSplineRegistration.GetOptimizerIteration()}")
    print(f" Metric value: {bSplineRegistration.GetMetricValue()}")

    movingBspline = sitk.Resample(movingRigid, fixed, outputTransfBSpline, sitk.sitkLinear, 0.0, movingRigid.GetPixelID())
    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(movingBspline), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.axis('off')
    plt.savefig(os.path.join(outputReg, (imageName + '.svg')), format='svg')
    plt.show()

    # Apply transformation to a different image

    resamplerRigid = sitk.ResampleImageFilter()
    resamplerRigid.SetReferenceImage(fixed)
    resamplerRigid.SetInterpolator(sitk.sitkLinear)
    resamplerRigid.SetDefaultPixelValue(50)
    resamplerRigid.SetTransform(outputTransfBSpline)

    out = resamplerRigid.Execute(detImage)
    simg1 = sitk.Cast(sitk.RescaleIntensity(fixed), sitk.sitkUInt8)
    simg2 = sitk.Cast(sitk.RescaleIntensity(out), sitk.sitkUInt8)
    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(out), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.axis('off')
    plt.savefig(os.path.join(outputRegDet, (imageName + '.svg')), format='svg')
    plt.show()

    sitk.WriteImage(simg2, os.path.join(outputRegFiles, (imageName + '.png')))

    return

monkey_list = ['Merida']#, 'Leia', 'Jyn', 'Padme']
folderList = ['M1GM']#'M1WM','PMvWM','PMvGM']

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'


for folderType in folderList:
    for monkey_dir in monkey_list:
        csvFilePath = os.path.join(root_dir,'ReferenceFiles')
        regInfo = pd.read_csv(os.path.join(csvFilePath, (monkey_dir+folderType+'_RegInfo.csv')))

        inputDetections = os.path.join(root_dir,('Detections'+folderType+'PaddedFlipped'), monkey_dir)

        outputReg = os.path.join(root_dir,'RegistrationResults','OutputRegAnn', monkey_dir)
        if not os.path.exists(outputReg):
            os.mkdir(outputReg)

        outputRegDet = os.path.join(root_dir,'RegistrationResults', 'OutputRegDet',monkey_dir)
        if not os.path.exists(outputRegDet):
            os.mkdir(outputRegDet)

        outputRegFiles = os.path.join(root_dir,'RegistrationResults', ('RegFiles'+folderType), monkey_dir)
        if not os.path.exists(outputRegFiles):
            os.mkdir(outputRegFiles)

        detections_list = glob.glob(os.path.join(inputDetections, '*.png'))
        for dd in detections_list:
            print("in loop")
            file_path, file_name = os.path.split(dd)
            parts = file_name.split('_')
            imageName = parts[0]

            exIdx = int(regInfo[regInfo['Name'] == imageName].index.values)
            segmentName = regInfo.loc[exIdx]['Segment']

            moving = sitk.ReadImage(regInfo.loc[exIdx]['PathAtlas'], sitk.sitkFloat32)
            fixed = sitk.ReadImage(regInfo.loc[exIdx]['PathInput'], sitk.sitkFloat32)
            detImage = sitk.ReadImage(regInfo.loc[exIdx]['PathDet'], sitk.sitkFloat32)

            registrationFunction(moving, fixed, detImage)


