import sklearn.datasets
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
from numpy import loadtxt
import re

monkey_name = 'Merida'
generalPath = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'


csvFilePath = generalPath+'ReferenceFiles/'
segInfo = pd.read_csv(csvFilePath+monkey_name+'_Segments.csv', sep=';')

data_folder = generalPath+'Annotations/'+monkey_name
file_list = glob.glob(os.path.join(data_folder, '*.txt'))

save_folder = generalPath+'Figures/'
if not os.path.exists(save_folder):
    os.makedirs(save_folder)

save_files = generalPath+'PlotFiles/'
if not os.path.exists(save_files):
    os.makedirs(save_files)

df = pd.DataFrame()

#for folder in folder_list:
file_list = glob.glob(os.path.join(data_folder, '*.txt'))

for file in file_list:
    df_temp = pd.read_csv(file, sep='\t')
    file_path, file_name = os.path.split(file)

    #df_temp['Segment'] = file.rsplit('/', 2)[1]
    df_temp['Animal'] = monkey_name
    parts = file_name.split('_')
    df_temp['FileName'] = parts[len(parts) - 1].split('.')[0]

    slideName = parts[len(parts) - 1].split('.')[0]
    exIdx = int(segInfo[segInfo['Slide']==slideName].index.values)
    df_temp['Segment'] = segInfo.loc[exIdx]['Segment']

    df = df.append(df_temp)

dWhiteMatter = df.loc[df['Name'].isin(['WhiteMatterLeft','WhiteMatterRight'])]

aa = dWhiteMatter.groupby(['Segment','Name'])['Num M1Fibers'].mean().unstack().reset_index()
mm = pd.melt(aa, id_vars=['Segment'], value_vars=['WhiteMatterLeft','WhiteMatterRight'])
mm["valueNorm"]=mm["value"].div(max(mm["value"]))

g = sns.FacetGrid(data=mm, col="Name")
g.map_dataframe(sns.barplot, x="Segment", y="valueNorm")
g.add_legend()
g.set_titles(monkey_name+" White Matter Red CST Fibers")
plt.savefig(save_folder+monkey_name+"WM_Red.svg",format="svg")
plt.show()

aa["Ipsi"]=aa["WhiteMatterLeft"].div(max(aa["WhiteMatterLeft"]))
aa["Contra"]=aa["WhiteMatterRight"].div(max(aa["WhiteMatterLeft"]))*(-1)

aa['Animal'] = monkey_name
aa['Color'] = 'RED'


pmvF = dWhiteMatter.groupby(['Segment','Name'])['Num PMvFibers'].mean().unstack().reset_index()
mmPmv = pd.melt(pmvF, id_vars=['Segment'], value_vars=['WhiteMatterLeft','WhiteMatterRight'])
mmPmv["valueNorm"]=mmPmv["value"].div(max(mmPmv["value"]))

g = sns.FacetGrid(data=mmPmv, col="Name")
g.map_dataframe(sns.barplot, x="Segment", y="valueNorm")
g.add_legend()
g.set_titles(monkey_name+" White Matter Green CST Fibers")
plt.savefig(save_folder+monkey_name+'WM_Green.svg',format='svg')
plt.show()

pmvF["Ipsi"]=pmvF["WhiteMatterLeft"].div(max(pmvF["WhiteMatterLeft"]))
pmvF["Contra"]=pmvF["WhiteMatterRight"].div(max(pmvF["WhiteMatterLeft"]))*(-1)

pmvF['Animal'] = monkey_name
pmvF['Color'] = 'GREEN'


font_color = '#525252'
hfont = {'fontname':'DejaVu Sans'}
facecolor = '#eaeaf2'
color_red = '#fd625e'
color_blue = '#01b8aa'
index = aa['Segment']
column0 = aa['Ipsi']
column1 = aa['Contra']
title0 = 'Contra'
title1 = 'Ipsi'

fig, axes = plt.subplots(figsize=(10, 5), facecolor=facecolor, ncols=2, sharey=True)
fig.tight_layout()
axes[0].barh(index, column0, align='center', color=color_red, zorder=10)
axes[0].set_title(title0, fontsize=18, pad=15, color=color_red, **hfont)
axes[1].barh(index, column1, align='center', color=color_blue, zorder=10)
axes[1].set_title(title1, fontsize=18, pad=15, color=color_blue, **hfont)

# If you have positive numbers and want to invert the x-axis of the left plot
axes[0].invert_xaxis()
# To show data from highest to lowest
plt.gca().invert_yaxis()

axes[0].set(yticks=index, yticklabels=index)
axes[0].yaxis.tick_left()
axes[0].tick_params(axis='y', colors='white') # tick color

axes[1].set_xticks([-1, -0.8, -0.6, -0.4, -0.2])


plt.savefig(save_folder+monkey_name+'WM_Redl.svg', format='svg')
plt.show()



font_color = '#525252'
hfont = {'fontname':'DejaVu Sans'}
facecolor = '#eaeaf2'
color_red = '#fd625e'
color_blue = '#01b8aa'
index = pmvF['Segment']
column0 = pmvF['Ipsi']
column1 = pmvF['Contra']
title0 = 'Contra'
title1 = 'Ipsi'

fig, axes = plt.subplots(figsize=(10, 5), facecolor=facecolor, ncols=2, sharey=True)
fig.tight_layout()
axes[0].barh(index, column0, align='center', color=color_red, zorder=10)
axes[0].set_title(title0, fontsize=18, pad=15, color=color_red, **hfont)
axes[1].barh(index, column1, align='center', color=color_blue, zorder=10)
axes[1].set_title(title1, fontsize=18, pad=15, color=color_blue, **hfont)

# If you have positive numbers and want to invert the x-axis of the left plot
axes[0].invert_xaxis()
# To show data from highest to lowest
plt.gca().invert_yaxis()

axes[0].set(yticks=index, yticklabels=index)
axes[0].yaxis.tick_left()
axes[0].tick_params(axis='y', colors='white') # tick color

axes[1].set_xticks([-1, -0.8, -0.6, -0.4, -0.2])


plt.savefig(save_folder+monkey_name+'WM_Greenl.svg', format='svg')
plt.show()

