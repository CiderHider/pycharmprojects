import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd

def command_iteration(method, bspline_transform):
    if method.GetOptimizerIteration() == 0:
        # The BSpline is resized before the first optimizer
        # iteration is completed per level. Print the transform object
        # to show the adapted BSpline transform.
        print(bspline_transform)

    print(f"{method.GetOptimizerIteration():3} = {method.GetMetricValue():10.5f}")


def command_multi_iteration(method):
    # The sitkMultiResolutionIterationEvent occurs before the
    # resolution of the transform. This event is used here to print
    # the status of the optimizer from the previous registration level.
    if R.GetCurrentLevel() > 0:
        print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
        print(f" Iteration: {R.GetOptimizerIteration()}")
        print(f" Metric value: {R.GetMetricValue()}")

    print("--------- Resolution Changing ---------")

def displaySidebySide(fixed_npa,moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    plt.show()


monkey_list = ['Merida']#, 'Leia', 'Jyn', 'Padme']
folderList = ['M1GM']#'M1WM','PMvWM','PMvGM']

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'


for folderType in folderList:
    for monkey_dir in monkey_list:
        csvFilePath = os.path.join(root_dir,'ReferenceFiles')
        regInfo = pd.read_csv(os.path.join(csvFilePath, (monkey_dir+folderType+'_RegInfo.csv')))

        inputDetections = os.path.join(root_dir,('Detections'+folderType+'PaddedFlipped'), monkey_dir)

        outputReg = os.path.join(root_dir,'RegistrationResults','OutputRegAnn', monkey_dir)
        if not os.path.exists(outputReg):
            os.mkdir(outputReg)

        outputRegDet = os.path.join(root_dir,'RegistrationResults', 'OutputRegDet',monkey_dir)
        if not os.path.exists(outputRegDet):
            os.mkdir(outputRegDet)

        outputRegFiles = os.path.join(root_dir,'RegistrationResults', ('RegFiles'+folderType), monkey_dir)
        if not os.path.exists(outputRegFiles):
            os.mkdir(outputRegFiles)

        detections_list = glob.glob(os.path.join(inputDetections, '*.png'))
        for dd in detections_list:
            file_path, file_name = os.path.split(dd)
            parts = file_name.split('_')
            imageName = parts[0]

            exIdx = int(regInfo[regInfo['Name'] == imageName].index.values)
            segmentName = regInfo.loc[exIdx]['Segment']

            moving = sitk.ReadImage(regInfo.loc[exIdx]['PathAtlas'], sitk.sitkFloat32)
            fixed = sitk.ReadImage(regInfo.loc[exIdx]['PathInput'], sitk.sitkFloat32)
            detImage = sitk.ReadImage(regInfo.loc[exIdx]['PathDet'], sitk.sitkFloat32)

            initialTx = sitk.CenteredTransformInitializer(fixed, moving,
                                                          sitk.AffineTransform(
                                                              fixed.GetDimension()),
                                                          sitk.CenteredTransformInitializerFilter.GEOMETRY)

            R = sitk.ImageRegistrationMethod()

            R.SetShrinkFactorsPerLevel([6, 4, 2])
            R.SetSmoothingSigmasPerLevel([6, 1, 1])

            R.SetMetricAsJointHistogramMutualInformation(50)
            R.MetricUseFixedImageGradientFilterOff()

            R.SetOptimizerAsGradientDescent(learningRate=1.0,
                                            numberOfIterations=100,
                                            estimateLearningRate=R.EachIteration)
            R.SetOptimizerScalesFromPhysicalShift()

            R.SetInitialTransform(initialTx)
            R.SetInterpolator(sitk.sitkLinear)

            # R.AddCommand(sitk.sitkIterationEvent, lambda: command_iteration(R))
            # R.AddCommand(sitk.sitkMultiResolutionIterationEvent,
            #             lambda: command_multiresolution_iteration(R))

            outTx = R.Execute(fixed, moving)

            print("-------")
            print(outTx)
            print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
            print(f" Iteration: {R.GetOptimizerIteration()}")
            print(f" Metric value: {R.GetMetricValue()}")

            moving_resampled = sitk.Resample(moving, fixed, outTx, sitk.sitkLinear, 0.0, moving.GetPixelID())
            fig = plt.figure()
            plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
            plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
            plt.axis('off')
            plt.savefig(os.path.join(outputReg,(imageName+'.svg')), format='svg')
            plt.show()

            # Apply transformation to a different image

            resampler = sitk.ResampleImageFilter()
            resampler.SetReferenceImage(fixed)
            resampler.SetInterpolator(sitk.sitkLinear)
            resampler.SetDefaultPixelValue(50)
            resampler.SetTransform(outTx)

            out = resampler.Execute(detImage)
            simg1 = sitk.Cast(sitk.RescaleIntensity(fixed), sitk.sitkUInt8)
            simg2 = sitk.Cast(sitk.RescaleIntensity(out), sitk.sitkUInt8)
            fig = plt.figure()
            plt.imshow(sitk.GetArrayViewFromImage(out), cmap=plt.cm.Greys_r, alpha=0.5)
            plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
            plt.axis('off')
            plt.savefig(os.path.join(outputRegDet, (imageName + '.svg')), format='svg')
            plt.show()
            sitk.WriteImage(simg2, os.path.join(outputRegFiles, (imageName+'.png')))
