import os
import glob
import cv2
import pandas as pd
import numpy as np
from scipy import ndimage
from skimage import io
import skimage.color



monkey_dir = ['Merida','Vaiana','Leia','Padme','Jyn']
root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
#root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'



df = pd.DataFrame(columns=["filename", "segment", "width", "height", "comW", "comH"])
for monkey in monkey_dir:
    input_dir = os.path.join(root_dir, 'AnnotationsToBeFlipped', monkey)
    output_dir = os.path.join(root_dir, 'AnnotationsFlippedPadded', monkey)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
    segInfo = pd.read_csv(os.path.join(csvFilePath, (monkey + '_Segments.csv')), sep=';')

    file_list = glob.glob(os.path.join(input_dir, '*.png'))

    for image in file_list:
        #image = file_list[1]
        inputImage = skimage.color.rgb2gray(skimage.color.rgba2rgb(io.imread(image)))
        file_path, file_name = os.path.split(image)
        parts = file_name.split('_')
        imageName = parts[len(parts) - 2].split('.')[0]
        exIdx = int(segInfo[segInfo['Slide'] == imageName].index.values)
        name = segInfo.loc[exIdx]['Slide']
        segment = segInfo.loc[exIdx]['Segment']

        h, w = inputImage.shape
        cmImgH, cmImgW = ndimage.measurements.center_of_mass(inputImage)

        df = df.append({"filename": name, "segment": segment, "width": w, "height": h,
                        "comW": cmImgW, "comH": cmImgH}, ignore_index=True)



maxWidth = df["width"].max() + 500 # 3986
maxHeight = df["height"].max() + 500 # 3125

'''
for image in file_list:
    inputImage = skimage.color.rgb2gray(skimage.color.rgba2rgb(io.imread(image)))
    #cv2.cvtColor(cv2.imread(image), cv2.COLOR_BGR2GRAY)
    file_path, file_name = os.path.split(image)

    imageName = file_name.split('_')[0]
    exIdx = int(segInfo[segInfo['Slide'] == imageName].index.values)
    flipStatus = segInfo.loc[exIdx]['Transformation']

    if flipStatus == 'flip':
        inputImage = np.flip(inputImage, 1)

    imgIndex = int(df[df['filename'] == imageName].index.values)
    paddingMatrix = np.zeros((maxHeight, maxWidth))

    yoff = round(maxHeight/2-df.loc[imgIndex]['comH'])
    xoff = round(maxWidth/2-df.loc[imgIndex]['comW'])
    h = df.loc[imgIndex]['height']
    w = df.loc[imgIndex]['width']

    resultedImage = paddingMatrix.copy()
    resultedImage[yoff:yoff+h, xoff:xoff+w] = inputImage
    skimage.io.imsave(os.path.join(output_dir, file_name), skimage.util.img_as_ubyte(resultedImage))
    #cv2.imwrite(os.path.join(output_dir, file_name), resultedImage)
'''
