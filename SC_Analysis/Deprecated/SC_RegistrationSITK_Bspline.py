import SimpleITK as sitk
import os
import matplotlib.pyplot as plt

def command_iteration(method, bspline_transform):
    if method.GetOptimizerIteration() == 0:
        # The BSpline is resized before the first optimizer
        # iteration is completed per level. Print the transform object
        # to show the adapted BSpline transform.
        print(bspline_transform)

    print(f"{method.GetOptimizerIteration():3} = {method.GetMetricValue():10.5f}")


def command_multi_iteration(method):
    # The sitkMultiResolutionIterationEvent occurs before the
    # resolution of the transform. This event is used here to print
    # the status of the optimizer from the previous registration level.
    if R.GetCurrentLevel() > 0:
        print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
        print(f" Iteration: {R.GetOptimizerIteration()}")
        print(f" Metric value: {R.GetMetricValue()}")

    print("--------- Resolution Changing ---------")

def displaySidebySide(fixed_npa,moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    plt.show()



root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/TestDataRegistration'
OUTPUT_DIR = root_dir
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

pathFixedImage = os.path.join(root_dir, 'C5.png')
pathMovingImage = os.path.join(root_dir, 'SC057_labels.png')

fixed = sitk.ReadImage(pathFixedImage, sitk.sitkFloat32)
moving = sitk.ReadImage(pathMovingImage, sitk.sitkFloat32)


transformDomainMeshSize = [2] * fixed.GetDimension()
tx = sitk.BSplineTransformInitializer(fixed, transformDomainMeshSize)

print(f"Initial Number of Parameters: {tx.GetNumberOfParameters()}")

R = sitk.ImageRegistrationMethod()
R.SetMetricAsJointHistogramMutualInformation()

R.SetOptimizerAsGradientDescentLineSearch(learningRate=5.0,
                                          numberOfIterations=100,
                                          convergenceMinimumValue=1e-4,
                                          convergenceWindowSize=5)

R.SetInterpolator(sitk.sitkLinear)

R.SetInitialTransformAsBSpline(tx, inPlace=True, scaleFactors=[1, 2, 5])

#R.SetOptimizerScalesFromPhysicalShift()
#R.SetInitialTransform(tx)
R.SetShrinkFactorsPerLevel([4, 2, 1])
R.SetSmoothingSigmasPerLevel([4, 2, 1])

outTx = R.Execute(fixed, moving)

print("-------")
print(outTx)
print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
print(f" Iteration: {R.GetOptimizerIteration()}")
print(f" Metric value: {R.GetMetricValue()}")

moving_resampled = sitk.Resample(moving, fixed, outTx, sitk.sitkLinear, 0.0, moving.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
plt.savefig(os.path.join(OUTPUT_DIR,'regAD.svg'), format='svg')
plt.show()

# Apply transformation to a different image

resampler = sitk.ResampleImageFilter()
resampler.SetReferenceImage(fixed)
resampler.SetInterpolator(sitk.sitkLinear)
resampler.SetDefaultPixelValue(50)
resampler.SetTransform(outTx)

testingImagePath = os.path.join(root_dir, 'SegG.png')
ttOut = sitk.ReadImage(testingImagePath, sitk.sitkFloat32)
out = resampler.Execute(ttOut)
simg1 = sitk.Cast(sitk.RescaleIntensity(fixed), sitk.sitkUInt8)
simg2 = sitk.Cast(sitk.RescaleIntensity(out), sitk.sitkUInt8)
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(out), cmap=plt.cm.Greys_r, alpha=0.8)
plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
plt.show()
sitk.WriteImage(simg2, os.path.join(OUTPUT_DIR, 'registeredtest.png'))