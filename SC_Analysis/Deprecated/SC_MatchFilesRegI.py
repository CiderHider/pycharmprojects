import os
import glob
import SimpleITK as sitk
import pandas as pd
from pathlib import Path


monkey_list = ['Vaiana', 'Merida']#, 'Leia', 'Jyn', 'Padme']
region = 'M1'
wmgm = 'WM'
folderList = ['M1WM','M1GM','PMvWM','PMvGM']

root_dir= 'D:\DianaRepos\pythonProject\data\Registration'

for folderType in folderList:
    for monkey_dir in monkey_list:
        inputFixed = os.path.join(root_dir, 'AnnotationsFlippedPadded', monkey_dir)
        inputMoving = os.path.join(root_dir, 'SC_Atlas')

        inputDetections = os.path.join(root_dir, ('Detections' + folderType + 'PaddedFlipped'), monkey_dir)
        csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
        segInfo = pd.read_csv(os.path.join(csvFilePath,(monkey_dir+'_Segments.csv')), sep=';')

        # Create a dataframe for the moving data
        pdMoving = pd.DataFrame(columns=['Name', 'Segment', 'Path'])
        atlas_list = glob.glob(os.path.join(inputMoving, '*.png'))

        for atl in atlas_list:
            file_path, file_name = os.path.split(atl)
            parts = file_name.split('.')
            pdMoving = pdMoving.append({'Name': file_name, 'Segment': parts[0],
                                        'Path': os.path.join(atl)}, ignore_index=True)

        # Create a dataframe for the annotation data
        pdFixed = pd.DataFrame()
        fixed_list = glob.glob(os.path.join(inputFixed, '*.png'))
        pdFixed = pd.DataFrame(columns=['Name', 'Segment', 'PathInput','PathAtlas'])

        for mm in fixed_list:
            file_path, file_name = os.path.split(mm)
            parts = file_name.split('_')
            imageName = parts[0]

            exIdx = int(segInfo[segInfo['Slide'] == imageName].index.values)
            segmentName = segInfo.loc[exIdx]['Segment']

            atlasIndex = int(pdMoving[pdMoving['Segment'] == segmentName].index.values)
            atlasPath = pdMoving.loc[atlasIndex]['Path']

            pdFixed = pdFixed.append({'Name': imageName, 'Segment': segmentName,
                                      'PathInput': os.path.join(mm), 'PathAtlas': atlasPath}, ignore_index=True)

        # Create a dataframe for the detections data
        pdDet = pd.DataFrame()
        detection_list = glob.glob(os.path.join(inputDetections, '*.png'))
        pdDet = pd.DataFrame(columns=['Name', 'PathDet'])

        for dd in detection_list:
            file_path, file_name = os.path.split(dd)
            parts = file_name.split('_')
            imageName = parts[0]
            pdDet = pdDet.append({'Name': imageName,
                                  'PathDet': os.path.join(dd)}, ignore_index=True)

        dfDataImage = pdFixed.merge(pdDet, on="Name", how='inner')

        dfDataImage.to_csv(os.path.join(csvFilePath, (monkey_dir+folderType+'_RegInfo.csv')))