import os
import glob
import SimpleITK as sitk
import pandas as pd
import numpy as np
from numpy import asarray
import matplotlib.pyplot as plt
from pathlib import Path
from numpy import save

root_dir= 'D:\DianaRepos\pythonProject\data\Registration'
inputFixed = os.path.join(root_dir, 'Test','FixedImage.png')
inputMoving = os.path.join(root_dir,'Test','MovingImage.png')
inputDet = os.path.join(root_dir,'Test','SegG.png')
outputResult = os.path.join(root_dir,'Test')

movingImage = sitk.ReadImage(inputMoving, sitk.sitkUInt8)
fixedImage = sitk.ReadImage(inputFixed, sitk.sitkUInt8)
detImage = sitk.ReadImage(inputDet, sitk.sitkUInt8)

parameterMapVector = sitk.VectorOfParameterMap()
parameterMapVector.append(sitk.GetDefaultParameterMap("affine"))

elastixImageFilter = sitk.ElastixImageFilter()
elastixImageFilter.SetFixedImage(fixedImage)
elastixImageFilter.SetMovingImage(movingImage)
elastixImageFilter.SetParameterMap(parameterMapVector)
elastixImageFilter.Execute()

resultImage = elastixImageFilter.GetResultImage()
transformParameterMap = elastixImageFilter.GetTransformParameterMap()



transformixImageFilter = sitk.TransformixImageFilter()
transformixImageFilter.SetTransformParameterMap(transformParameterMap)
transformixImageFilter.SetMovingImage(detImage)
transformixImageFilter.Execute()
sitk.WriteImage(transformixImageFilter.GetResultImage(), "result.tif")

aa = sitk.GetArrayFromImage(transformixImageFilter.GetResultImage())
bb = transformixImageFilter.GetResultImage()
plt.imshow(aa, cmap='gray')
plt.show()