import os
import glob
import SimpleITK as sitk
import pandas as pd
from numpy import save

root_dir= 'D:\DianaRepos\pythonProject\data\Registration'
inputFixed = os.path.join(root_dir,'Test','FixedImage.png')
inputMoving = os.path.join(root_dir,'Test','MovingImage.png')
inputDet = os.path.join(root_dir,'Test','SegG.png')
outputResult = os.path.join(root_dir,'Test')


movingImage = sitk.ReadImage(inputMoving, sitk.sitkUInt8)
fixedImage = sitk.ReadImage(inputFixed, sitk.sitkUInt8)
detImage = sitk.ReadImage(inputDet, sitk.sitkUInt8)

# Initialize registration
elastixImageFilter = sitk.ElastixImageFilter()

# Set registration images
elastixImageFilter.SetMovingImage(movingImage)
elastixImageFilter.SetFixedImage(fixedImage)

parameterMapVector = sitk.VectorOfParameterMap()
# parameterMapVector.append(sitk.GetDefaultParameterMap("rigid"))
parameterMapVector.append(sitk.GetDefaultParameterMap("affine"))
parameterMapVector.append(sitk.GetDefaultParameterMap("bspline"))
elastixImageFilter.SetParameterMap(parameterMapVector)

# Registration
elastixImageFilter.Execute()
result = elastixImageFilter.GetResultImage()
transformParameterMap = elastixImageFilter.GetTransformParameterMap()

# Write result image low image quality due to multiple transformations
resultImage = sitk.Cast(elastixImageFilter.GetResultImage(), sitk.sitkUInt8)
igN = "RegistrationResult.tif"
sitk.WriteImage(resultImage, os.path.join(outputResult, igN))

transformedNewImage = sitk.Transformix(detImage, transformParameterMap)
sitk.WriteImage(transformedNewImage, os.path.join(outputResult, "Alignment.png"))



'''
arrayResultReg = sitk.GetArrayFromImage(transformedNewImage)

fname = segmentName + '_' + imageName + '_'+folderType+'.npy'
save(os.path.join(outputRegFiles, fname), arrayResultReg)
'''
