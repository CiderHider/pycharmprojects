

import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd

def displaySidebySide(fixed_npa,moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    plt.show()


def reg_function(fixed, moving, detImage,segName):
    """
        Parameters
        ----------
        fixed : ATLAS
            Numpy array containing image data

        moving : ANNOTATION IMAGES
            Numpy array containing 8 bit integer masks

        detImage: DETECTION IMAGES
            Integer with the mask value of the lamina_mask to be applied

        -----------------
        Function to calculate the registration between the fixed and the moving images.
        1) First create an initial rigid transformation
        2) Get the rigid registred moving image (RMI)
        3) Create a bspline transformation
        4) Register the resulted RMI and register it using the bspline transformation
        5) Apply the rigid transformation on the detection images
        6) Apply the bspline transformation on the rigid transformed detection images
    """
    # Apply initial alignment

    initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                          moving,
                                                          sitk.Similarity2DTransform(),
                                                          sitk.CenteredTransformInitializerFilter.GEOMETRY)
    # Test to see how they look as overlay
    moving_resampled = sitk.Resample(moving, fixed,
                                     initial_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.savefig(os.path.join(outputReg, (imageName + 'NotRegistred.svg')), format='svg')
    plt.axis('off')
    plt.show()

    registration_method = sitk.ImageRegistrationMethod()

    # Similarity metric settings.
    registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.01)
    registration_method.SetInterpolator(sitk.sitkLinear)

    # Optimizer settings.
    registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                      numberOfIterations=500,
                                                      convergenceMinimumValue=1e-6,
                                                      convergenceWindowSize=500)
    registration_method.SetOptimizerScalesFromPhysicalShift()

    # Setup for the multi-resolution framework.
    registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[6, 2, 1])
    registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
    registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

    # Don't optimize in-place, we would possibly like to run this cell multiple times.
    registration_method.SetInitialTransform(initial_transform, inPlace=False)

    rigidTransformation = registration_method.Execute(fixed, moving)
    print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
    print('Optimizer\'s stopping condition, {0}'.format(
        registration_method.GetOptimizerStopConditionDescription()))

    movingRigid = sitk.Resample(moving, fixed, rigidTransformation, sitk.sitkLinear, 0.0, moving.GetPixelID())
    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(movingRigid), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.savefig(os.path.join(outputReg, (imageName + 'regRigid.svg')), format='svg')
    plt.axis('off')
    plt.show()

    # BSPLINE
    transformDomainMeshSize = [2] * fixed.GetDimension()
    tx = sitk.BSplineTransformInitializer(fixed, transformDomainMeshSize)

    print(f"Initial Number of Parameters: {tx.GetNumberOfParameters()}")

    R = sitk.ImageRegistrationMethod()
    R.SetMetricAsJointHistogramMutualInformation()

    R.SetOptimizerAsGradientDescentLineSearch(learningRate=5.0,
                                              numberOfIterations=100,
                                              convergenceMinimumValue=1e-4,
                                              convergenceWindowSize=5)

    R.SetInterpolator(sitk.sitkLinear)

    R.SetInitialTransformAsBSpline(tx, inPlace=True, scaleFactors=[1, 2, 5])
    R.SetShrinkFactorsPerLevel([4, 2, 1])
    R.SetSmoothingSigmasPerLevel([4, 2, 1])

    bSplineTransformation = R.Execute(fixed, movingRigid)

    print("-------")
    print(bSplineTransformation)
    print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
    print(f" Iteration: {R.GetOptimizerIteration()}")
    print(f" Metric value: {R.GetMetricValue()}")

    movingRegBspline = sitk.Resample(movingRigid, fixed, bSplineTransformation, sitk.sitkLinear, 0.0,
                                     moving_resampled.GetPixelID())
    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(movingRegBspline), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.axis('off')
    plt.savefig(os.path.join(outputReg, (imageName + 'RegBspline.svg')), format='svg')
    plt.show()

    # Apply transformation to a different image

    resamplerRigid = sitk.ResampleImageFilter()
    resamplerRigid.SetReferenceImage(fixed)
    resamplerRigid.SetInterpolator(sitk.sitkLinear)
    resamplerRigid.SetDefaultPixelValue(50)
    resamplerRigid.SetTransform(rigidTransformation)

    outRigidApplied = resamplerRigid.Execute(detImage)

    resamplerBSpline = sitk.ResampleImageFilter()
    resamplerBSpline.SetReferenceImage(fixed)
    resamplerBSpline.SetInterpolator(sitk.sitkLinear)
    resamplerBSpline.SetDefaultPixelValue(50)
    resamplerBSpline.SetTransform(bSplineTransformation)

    outBSplineApplied = resamplerBSpline.Execute(outRigidApplied)

    simg1 = sitk.Cast(sitk.RescaleIntensity(fixed), sitk.sitkUInt8)
    simg2 = sitk.Cast(sitk.RescaleIntensity(outRigidApplied), sitk.sitkUInt8)
    simg3 = sitk.Cast(sitk.RescaleIntensity(outBSplineApplied), sitk.sitkUInt8)

    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.imshow(sitk.GetArrayViewFromImage(simg2), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(simg3), cmap=plt.cm.Greys_r, alpha=0.7)
    plt.axis('off')
    plt.savefig(os.path.join(outputReg, (imageName + 'detections.svg')), format='svg')
    plt.show()
    sitk.WriteImage(simg3, os.path.join(outputRegFiles, (segName + '_'+imageName + '.png')))

    del initial_transform, moving_resampled, registration_method, rigidTransformation, movingRigid
    del R, tx, bSplineTransformation, movingRegBspline
    del resamplerRigid, resamplerBSpline, outRigidApplied, outBSplineApplied, simg3



monkey_list = ['VaianaTest']#, 'Leia', 'Jyn', 'Padme']
folderList = ['M1GM']#'M1WM','PMvWM','PMvGM']

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'


for folderType in folderList:
    for monkey_dir in monkey_list:

        csvFilePath = os.path.join(root_dir,'ReferenceFiles')
        regInfo = pd.read_csv(os.path.join(csvFilePath, (monkey_dir+folderType+'_RegInfo.csv')))

        inputDetections = os.path.join(root_dir,('Detections'+folderType+'PaddedFlipped'), monkey_dir)

        outputReg = os.path.join(root_dir,'RegistrationResults','OutputRegistration', monkey_dir)
        if not os.path.exists(outputReg):
            os.mkdir(outputReg)

        outputRegFiles = os.path.join(root_dir,'RegistrationResults', ('RegFiles'+folderType), monkey_dir)
        if not os.path.exists(outputRegFiles):
            os.mkdir(outputRegFiles)

        detections_list = glob.glob(os.path.join(inputDetections, '*.png'))
        for dd in detections_list:
            print("in loop")
            file_path, file_name = os.path.split(dd)
            parts = file_name.split('_')
            imageName = parts[1]

            exIdx = int(regInfo[regInfo['Name'] == imageName].index.values)
            segmentName = regInfo.loc[exIdx]['Segment']

            fixedImage = sitk.ReadImage(regInfo.loc[exIdx]['PathAtlas'], sitk.sitkFloat32)
            movingImage = sitk.ReadImage(regInfo.loc[exIdx]['PathInput'], sitk.sitkFloat32)
            detectionsImage = sitk.ReadImage(regInfo.loc[exIdx]['PathDet'], sitk.sitkFloat32)

            reg_function(fixedImage, movingImage, detectionsImage,segmentName)