import os
import glob
import cv2
import pandas as pd

monkey_dir = 'Padme'
root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
#root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

input_dir =  os.path.join(root_dir, 'AnnotationsToBeFlipped', monkey_dir)
output_dir = os.path.join(root_dir, 'AnnotationsFlippedBWReg' + monkey_dir)

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

csvFilePath = os.path.join(root_dir,  'ReferenceFiles')
segInfo = pd.read_csv(os.path.join(csvFilePath,(monkey_dir+'_Segments.csv')), sep=';')

file_list = glob.glob(os.path.join(input_dir, '*.png'))

for image in file_list:
    inputImage = cv2.imread(image)
    file_path, file_name = os.path.split(image)
    parts = file_name.split('_')
    imageName = parts[len(parts) - 2].split('.')[0]
    exIdx = int(segInfo[segInfo['Slide'] == imageName].index.values)
    flipStatus = segInfo.loc[exIdx]['Transformation']

    if flipStatus == 'flip':
        flippedImg = cv2.flip(inputImage, 1)
        resImg = cv2.cvtColor(flippedImg, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(os.path.join(output_dir, file_name), resImg)
    else:
        resImg = cv2.cvtColor(inputImage, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(os.path.join(output_dir, file_name), resImg)


