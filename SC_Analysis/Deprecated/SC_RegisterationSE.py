import os
import glob
import SimpleITK as sitk
import pandas as pd
from numpy import save


monkey_list = ['Vaiana', 'Merida']#, 'Leia', 'Jyn', 'Padme']
region = 'M1'
wmgm = 'WM'
folderList = ['M1WM','M1GM','PMvWM','PMvGM']

root_dir= 'D:\DianaRepos\pythonProject\data\Registration'

for folderType in folderList:
    for monkey_dir in monkey_list:
        csvFilePath = os.path.join(root_dir,'ReferenceFiles')
        regInfo = pd.read_csv(os.path.join(csvFilePath, (monkey_dir+folderType+'_RegInfo.csv')))

        inputDetections = os.path.join(root_dir,('Detections'+folderType+'PaddedFlipped'), monkey_dir)

        outputReg = os.path.join(root_dir,'OutputRegAnn', monkey_dir)
        if not os.path.exists(outputReg):
            os.mkdir(outputReg)

        outputRegDetRed = os.path.join(root_dir,'OutputRegDet',monkey_dir)
        if not os.path.exists(outputRegDetRed):
            os.mkdir(outputRegDetRed)

        outputRegFiles = os.path.join(root_dir, ('RegFiles'+folderType), monkey_dir)
        if not os.path.exists(outputRegFiles):
            os.mkdir(outputRegFiles)

        detections_list = glob.glob(os.path.join(inputDetections, '*.png'))
        for dd in detections_list:
            file_path, file_name = os.path.split(dd)
            parts = file_name.split('_')
            imageName = parts[0]

            exIdx = int(regInfo[regInfo['Name'] == imageName].index.values)
            segmentName = regInfo.loc[exIdx]['Segment']

            movingImage = sitk.ReadImage(regInfo.loc[exIdx]['PathAtlas'], sitk.sitkUInt8)
            fixedImage = sitk.ReadImage(regInfo.loc[exIdx]['PathInput'], sitk.sitkUInt8)
            detImage = sitk.ReadImage(regInfo.loc[exIdx]['PathDet'], sitk.sitkUInt8)

            # Initialize registration
            elastixImageFilter = sitk.ElastixImageFilter()

            # Set registration images
            elastixImageFilter.SetMovingImage(movingImage)
            elastixImageFilter.SetFixedImage(fixedImage)

            parameterMapVector = sitk.VectorOfParameterMap()
            # parameterMapVector.append(sitk.GetDefaultParameterMap("rigid"))
            parameterMapVector.append(sitk.GetDefaultParameterMap("affine"))
            # parameterMapVector.append(sitk.GetDefaultParameterMap("bspline"))
            elastixImageFilter.SetParameterMap(parameterMapVector)

            # Registration
            elastixImageFilter.Execute()
            result = elastixImageFilter.GetResultImage()
            transformParameterMap = elastixImageFilter.GetTransformParameterMap()

            # Write result image low image quality due to multiple transformations
            resultImage = sitk.Cast(elastixImageFilter.GetResultImage(), sitk.sitkUInt8)
            #igN = imageName + ".tif"
            #sitk.WriteImage(resultImage, os.path.join(outputReg, igN))

            transformedNewImage = sitk.Transformix(detImage, transformParameterMap)
            #sitk.WriteImage(transformedNewImage, os.path.join(outputRegDetRed, igN))

            arrayResultReg = sitk.GetArrayFromImage(transformedNewImage)

            fname = segmentName + '_' + imageName + '_'+folderType+'.npy'
            save(os.path.join(outputRegFiles, fname), arrayResultReg)
