import numpy as np
from skimage.color import rgb2gray
from skimage.data import stereo_motorcycle, vortex
from skimage.transform import warp
from skimage.registration import optical_flow_tvl1, optical_flow_ilk
from skimage import io
import skimage.color
import os
import matplotlib.pyplot as plt
import cv2
from skimage import feature
from skimage.morphology import (erosion, dilation, opening, closing)
from skimage.morphology import disk  # noqa
from skimage.registration import optical_flow_ilk
import pyelastix


inputFixed = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/Test/FixedImage.png'
inputMoving = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/Test/MovingImage.png'

outputFixed = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/Test'
fixedImage = cv2.cvtColor(cv2.imread(inputFixed), cv2.COLOR_BGR2GRAY).astype('int')
    #skimage.color.rgb2gray(io.imread(inputFixed)).astype('int')


reference_image = skimage.color.rgb2gray((io.imread(inputFixed)))
moving_image = skimage.color.rgb2gray((io.imread(inputMoving)))
'''
# --- Convert the images to gray level: color is not supported.


flow = optical_flow_ilk(moving_image, reference_image)
'''

# Get params and change a few values
params = pyelastix.get_default_params()
params.MaximumNumberOfIterations = 200
params.FinalGridSpacingInVoxels = 10

# Apply the registration (im1 and im2 can be 2D or 3D)
im1_deformed, field = pyelastix.register(reference_image, moving_image, params)