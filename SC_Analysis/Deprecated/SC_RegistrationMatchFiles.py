import os
import glob
import pandas as pd


monkey_list = ['VaianaTest']#, 'Leia', 'Jyn', 'Padme']

folderList = ['M1GM']#['M1WM','M1GM','PMvWM','PMvGM']

root_dir= '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'


for folderType in folderList:
    for monkey_dir in monkey_list:
        # Input moving and fixed images
        inputMoving = os.path.join(root_dir, 'AnnotationsFlippedPadded', monkey_dir)
        inputFixed = os.path.join(root_dir, 'SC_AtlasPadded')

        # Input detections
        inputDetections = os.path.join(root_dir, ('Detections' + folderType + 'PaddedFlipped'), monkey_dir)

        # Input .csv with information about the segments
        csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
        segInfo = pd.read_csv(os.path.join(csvFilePath,(monkey_dir+'_Segments.csv')), sep=';')

        # Create a dataframe for the atlas data
        pdAtlas = pd.DataFrame(columns=['Name', 'Segment', 'Path'])
        atlas_list = glob.glob(os.path.join(inputFixed, '*.png'))

        for atl in atlas_list:
            file_path, file_name = os.path.split(atl)
            parts = file_name.split('.')
            pdAtlas = pdAtlas.append({'Name': file_name, 'Segment': parts[0],
                                      'Path': os.path.join(atl)}, ignore_index=True)

        # Create a dataframe for the annotation data
        pdMoving = pd.DataFrame()
        annotations_list = glob.glob(os.path.join(inputMoving, '*.png'))
        pdMoving = pd.DataFrame(columns=['Name', 'Segment', 'PathInput', 'PathAtlas'])

        for mm in annotations_list:
            file_path, file_name = os.path.split(mm)
            parts = file_name.split('_')
            imageName = parts[1]

            exIdx = int(segInfo[segInfo['Slide'] == imageName].index.values)
            segmentName = segInfo.loc[exIdx]['Segment']

            atlasIndex = int(pdAtlas[pdAtlas['Segment'] == segmentName].index.values)
            atlasPath = pdAtlas.loc[atlasIndex]['Path']

            pdMoving = pdMoving.append({'Name': imageName, 'Segment': segmentName,
                                        'PathInput': os.path.join(mm), 'PathAtlas': atlasPath}, ignore_index=True)

        # Create a dataframe for the detections data
        pdDet = pd.DataFrame()
        detection_list = glob.glob(os.path.join(inputDetections, '*.png'))
        pdDet = pd.DataFrame(columns=['Name', 'PathDet'])

        for dd in detection_list:
            file_path, file_name = os.path.split(dd)
            parts = file_name.split('_')
            imageName = parts[1]
            pdDet = pdDet.append({'Name': imageName,
                                  'PathDet': os.path.join(dd)}, ignore_index=True)

        dfDataImage = pdMoving.merge(pdDet, on="Name", how='inner')

        dfDataImage.to_csv(os.path.join(csvFilePath, (monkey_dir+folderType+'_RegInfo.csv')))


'''       
for folderType in folderList:
    for monkey_dir in monkey_list:
        # Input moving and fixed images
        inputMoving = os.path.join(root_dir, 'AnnotationsFlippedPadded', monkey_dir)
        inputFixed = os.path.join(root_dir, 'SC_AtlasPadded')

        # Input detections
        inputDetections = os.path.join(root_dir, ('Detections' + folderType + 'PaddedFlipped'), monkey_dir)

        # Input .csv with information about the segments
        csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
        segInfo = pd.read_csv(os.path.join(csvFilePath,(monkey_dir+'_Segments.csv')), sep=';')

        # Create a dataframe for the atlas data
        pdAtlas = pd.DataFrame(columns=['Name', 'Segment', 'Path'])
        atlas_list = glob.glob(os.path.join(inputFixed, '*.png'))

        for atl in atlas_list:
            file_path, file_name = os.path.split(atl)
            parts = file_name.split('.')
            pdAtlas = pdAtlas.append({'Name': file_name, 'Segment': parts[0],
                                      'Path': os.path.join(atl)}, ignore_index=True)

        # Create a dataframe for the annotation data
        pdMoving = pd.DataFrame()
        annotations_list = glob.glob(os.path.join(inputMoving, '*.png'))
        pdMoving = pd.DataFrame(columns=['Name', 'Segment', 'PathInput', 'PathAtlas'])

        for mm in annotations_list:
            file_path, file_name = os.path.split(mm)
            parts = file_name.split('_')
            imageName = parts[0]

            exIdx = int(segInfo[segInfo['Slide'] == imageName].index.values)
            segmentName = segInfo.loc[exIdx]['Segment']

            atlasIndex = int(pdAtlas[pdAtlas['Segment'] == segmentName].index.values)
            atlasPath = pdAtlas.loc[atlasIndex]['Path']

            pdMoving = pdMoving.append({'Name': imageName, 'Segment': segmentName,
                                        'PathInput': os.path.join(mm), 'PathAtlas': atlasPath}, ignore_index=True)

        # Create a dataframe for the detections data
        pdDet = pd.DataFrame()
        detection_list = glob.glob(os.path.join(inputDetections, '*.png'))
        pdDet = pd.DataFrame(columns=['Name', 'PathDet'])

        for dd in detection_list:
            file_path, file_name = os.path.split(dd)
            parts = file_name.split('_')
            imageName = parts[0]
            pdDet = pdDet.append({'Name': imageName,
                                  'PathDet': os.path.join(dd)}, ignore_index=True)

        dfDataImage = pdMoving.merge(pdDet, on="Name", how='inner')

        dfDataImage.to_csv(os.path.join(csvFilePath, (monkey_dir+folderType+'_RegInfo.csv')))
        '''