import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt

def command_iteration(method):
    if (method.GetOptimizerIteration() == 0):
        print(f"\tLevel: {method.GetCurrentLevel()}")
        print(f"\tScales: {method.GetOptimizerScales()}")
    print(f"#{method.GetOptimizerIteration()}")
    print(f"\tMetric Value: {method.GetMetricValue():10.5f}")
    print(f"\tLearningRate: {method.GetOptimizerLearningRate():10.5f}")
    if (method.GetOptimizerConvergenceValue() != sys.float_info.max):
        print(f"\tConvergence Value: {method.GetOptimizerConvergenceValue():.5e}")


def command_multiresolution_iteration(method):
    print(f"\tStop Condition: {method.GetOptimizerStopConditionDescription()}")
    print("============= Resolution Change =============")


def displaySidebySide(fixed_npa,moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    plt.show()



root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/TestDataRegistration'
OUTPUT_DIR = root_dir
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

pathFixedImage = os.path.join(root_dir, 'C5.png')
pathMovingImage = os.path.join(root_dir, 'SC057_labels.png')
testingImage = os.path.join(root_dir, 'SegG.png')

fixed = sitk.ReadImage(pathFixedImage, sitk.sitkFloat32)
moving = sitk.ReadImage(pathMovingImage, sitk.sitkFloat32)
detImg = sitk.ReadImage(testingImage, sitk.sitkFloat32)

displaySidebySide(sitk.GetArrayViewFromImage(fixed),sitk.GetArrayViewFromImage(moving))


# Try a affine registration

initialTx = sitk.CenteredTransformInitializer(fixed, moving,
                                              sitk.AffineTransform(fixed.GetDimension()))

R = sitk.ImageRegistrationMethod()

R.SetShrinkFactorsPerLevel([6, 4, 2])
R.SetSmoothingSigmasPerLevel([6, 1, 1])

R.SetMetricAsJointHistogramMutualInformation(200)

R.MetricUseFixedImageGradientFilterOff()

R.SetOptimizerAsGradientDescent(learningRate=1.0,
                                numberOfIterations=100,
                                estimateLearningRate=R.EachIteration)
R.SetOptimizerScalesFromPhysicalShift()

R.SetInitialTransform(initialTx)
R.SetInterpolator(sitk.sitkLinear)

#R.AddCommand(sitk.sitkIterationEvent, lambda: command_iteration(R))
#R.AddCommand(sitk.sitkMultiResolutionIterationEvent,
#             lambda: command_multiresolution_iteration(R))

final_transform = R.Execute(fixed, moving)

print("-------")
print(final_transform)
print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
print(f" Iteration: {R.GetOptimizerIteration()}")
print(f" Metric value: {R.GetMetricValue()}")


moving_resampled = sitk.Resample(moving, fixed, final_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
plt.show()
'''
# Apply transformation to a different image

resampler = sitk.ResampleImageFilter()
resampler.SetReferenceImage(fixed)
resampler.SetInterpolator(sitk.sitkLinear)
resampler.SetDefaultPixelValue(50)
resampler.SetTransform(final_transform)

out = resampler.Execute(detImg)
simg1 = sitk.Cast(sitk.RescaleIntensity(fixed), sitk.sitkUInt8)
simg2 = sitk.Cast(sitk.RescaleIntensity(out), sitk.sitkUInt8)
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(out), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
#plt.savefig(os.path.join(outputRegDet, (imageName + '.svg')), format='svg')
plt.show()

'''


'''
#Try displacement field
displacementField = sitk.Image(fixed.GetSize(), sitk.sitkVectorFloat64)
displacementField.CopyInformation(fixed)
displacementTx = sitk.DisplacementFieldTransform(displacementField)
del displacementField
displacementTx.SetSmoothingGaussianOnUpdate(varianceForUpdateField=0.0,
                                            varianceForTotalField=1.5)

R.SetMovingInitialTransform(final_transform)
R.SetInitialTransform(displacementTx, inPlace=True)

R.SetMetricAsANTSNeighborhoodCorrelation(4)
R.MetricUseFixedImageGradientFilterOff()

R.SetShrinkFactorsPerLevel([3, 2, 1])
R.SetSmoothingSigmasPerLevel([2, 1, 1])

R.SetOptimizerScalesFromPhysicalShift()
R.SetOptimizerAsGradientDescent(learningRate=1,
                                numberOfIterations=300,
                                estimateLearningRate=R.EachIteration)

displ_transform = R.Execute(fixed, moving)

print("-------")
print(displacementTx)
print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
print(f" Iteration: {R.GetOptimizerIteration()}")
print(f" Metric value: {R.GetMetricValue()}")

moving_resampled = sitk.Resample(moving, fixed, displ_transform)
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
plt.show()

'''



# Previous Registration

'''
initial_transform = sitk.CenteredTransformInitializer(fixed_image,
                                                      moving_image,
                                                      sitk.Euler2DTransform(),
                                                      sitk.CenteredTransformInitializerFilter.GEOMETRY)

moving_resampled = sitk.Resample(moving_image, fixed_image,
                                 initial_transform, sitk.sitkLinear, 0.0, moving_image.GetPixelID())

fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed_image), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
plt.show()



registration_method = sitk.ImageRegistrationMethod()

# Similarity metric settings.
registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=100)
registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
registration_method.SetMetricSamplingPercentage(0.01)

registration_method.SetInterpolator(sitk.sitkLinear)

# Optimizer settings.
registration_method.SetOptimizerAsGradientDescent(learningRate=0.5, numberOfIterations=500, convergenceMinimumValue=1e-6, convergenceWindowSize=500)
registration_method.SetOptimizerScalesFromPhysicalShift()

# Setup for the multi-resolution framework.
registration_method.SetShrinkFactorsPerLevel(shrinkFactors = [4,2,1])
registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[2,1,0])
registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

# Don't optimize in-place, we would possibly like to run this cell multiple times.
registration_method.SetInitialTransform(initial_transform, inPlace=False)

final_transform = registration_method.Execute(fixed_image, moving_image)
print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
print('Optimizer\'s stopping condition, {0}'.format(registration_method.GetOptimizerStopConditionDescription()))

moving_resampled = sitk.Resample(moving_image, fixed_image, final_transform, sitk.sitkLinear, 0.0, moving_image.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed_image), cmap=plt.cm.Greys_r, alpha=0.3)
plt.axis('off')
plt.show()


sitk.WriteImage(moving_resampled, os.path.join(OUTPUT_DIR, 'RIRE_training_001_mr_T1_resampled.mha'))
sitk.WriteTransform(final_transform, os.path.join(OUTPUT_DIR, 'RIRE_training_001_CT_2_mr_T1.tfm'))
'''


