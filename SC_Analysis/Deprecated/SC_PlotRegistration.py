import os
import glob
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
from numpy import load
import numpy as np
from scipy.ndimage import convolve
import seaborn as sns
import skimage
from skimage import io
from scipy.ndimage import gaussian_filter

monkey_list= ['Merida']#,'Vaiana','Leia','Jyn','Padme']
region = 'GM'
folderList = ['M1GM']#,'PMvGM','M1WM','PMvWM']
root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'

atlasPath = os.path.join(root_path, 'SC_AtlasMasks')
atlas_list = glob.glob(os.path.join(atlasPath, '*.png'))

dfAtlas = pd.DataFrame(columns=['Name', 'Segment', 'Path'])
for image in atlas_list:
    file_path, file_name = os.path.split(image)
    checkMask = file_name.split('_')[0]
    if checkMask == 'M':
        segment = file_name.split('.')[0].split('_')[1]
        dfAtlas = dfAtlas.append({'Name': file_name, 'Segment': segment, 'Path': image}, ignore_index=True)

for folderType in folderList:
    for monkey_dir in monkey_list:
        csvFilePath =os.path.join(root_path, 'ReferenceFiles')
        segInfo = pd.read_csv(os.path.join(csvFilePath, (monkey_dir+'_Segments.csv')), sep=';')

        inputFiles =os.path.join(root_path, 'RegistrationResults', ('RegFiles' + folderType), monkey_dir)
        file_list = glob.glob(os.path.join(inputFiles, '*.npy'))

        outputFigures = os.path.join(root_path,'Figures', monkey_dir)
        if not os.path.exists(outputFigures):
            os.mkdir(outputFigures)

        df = pd.DataFrame(columns=['Name', 'Segment', 'DataReg'])
        for file in file_list:
            file_path, file_name = os.path.split(file)
            parts = file_name.split('_')
            segment = parts[0]
            data = load(file)
            data = data.__abs__()
            df = df.append({'Name': file_name, 'Segment': segment,'DataReg': data}, ignore_index=True)

        segm = df['Segment'].unique()
        temp_df = pd.DataFrame(columns=['Segment', 'MaxConv'])

        for ii in segm:
            tt = df.loc[df['Segment'] == ii, ['DataReg']].sum(axis=1).sum()
            a = gaussian_filter(tt, sigma=25)
            temp_df = temp_df.append({'Segment': ii, 'MaxConv': a.max()}, ignore_index=True)

        for ii in segm:
            toPlot = df.loc[df['Segment'] == ii, ['DataReg']].sum(axis=1).sum()
            a = gaussian_filter(toPlot, sigma=25)
            column = temp_df["MaxConv"]
            maxElem = column.max()
            #maxElem = 250
            #sns.heatmap(a, vmin=0, vmax=maxElem, cmap='hot')

            maskIndex = int(dfAtlas[dfAtlas['Segment'] == ii].index.values)

            atlasMask = skimage.util.img_as_ubyte(skimage.color.rgb2gray(io.imread(dfAtlas.loc[maskIndex]['Path'])))
            '''
            hPlot, wPlot = a.shape
            paddingMatrix = np.zeros((hPlot, wPlot))

            hm, wk = atlasMask.shape

            yoff = round((hPlot - hm) / 2)
            xoff = round((wPlot - wk) / 2)

            resultedImage = paddingMatrix.copy()
            resultedImage[yoff:yoff + hm, xoff:xoff + wk] = atlasMask
            atL = resultedImage.copy()
            '''
            if folderType == 'M1WM' or folderType == 'PMvWM':
                plt.imshow(a, cmap="magma", interpolation="nearest", vmin=0, vmax=a.max())
                plt.imshow(atlasMask, cmap="gray", alpha=0.4)
            else:
                plt.imshow(a, cmap="hot", interpolation="nearest", vmin=0, vmax=a.max())
                plt.imshow(atlasMask, cmap="gray", alpha=0.4)
                plt.axis('off')

            plt.savefig(os.path.join(outputFigures, ('Reg' + folderType + ii + '.png')), format='png')

