import sklearn.datasets
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
from numpy import loadtxt
import re

generalPath = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
csvFilePath = generalPath+'ReferenceFiles/'
monkeyInfo = pd.read_csv(csvFilePath+'Monkey_Reference_sheet.csv', sep=',')
monkeys = monkeyInfo['Monkey']

save_folder = generalPath+'Figures/'
if not os.path.exists(save_folder):
    os.makedirs(save_folder)

df = pd.DataFrame()

for monkey_name in monkeys:
    csvFilePath = generalPath+'ReferenceFiles/'
    segInfo = pd.read_csv(csvFilePath+monkey_name+'_Segments.csv', sep=';')

    data_folder = generalPath+'Detections/'+monkey_name
    file_list = glob.glob(os.path.join(data_folder, '*.txt'))

    save_folder = generalPath+'Figures/'
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    for file in file_list:
        df_temp = pd.read_csv(file, sep='\t')
        file_path, file_name = os.path.split(file)

        df_temp['Animal'] = monkey_name
        parts = file_name.split('_')
        df_temp['FileName'] = parts[len(parts) - 1].split('.')[0]

        slideName = parts[len(parts) - 1].split('.')[0]
        exIdx = int(segInfo[segInfo['Slide'] == slideName].index.values)
        df_temp['Segment'] = segInfo.loc[exIdx]['Segment']

        monkeyIndex = int(monkeyInfo[monkeyInfo['Monkey'] == monkey_name].index.values)
        df_temp['Severity']=monkeyInfo.loc[monkeyIndex]['Severity']
        df_temp['CY3_Dens'] = df_temp['CY3: Mean'] * df_temp['Area']
        df_temp['FITC_Dens'] = df_temp['FITC: Mean'] * df_temp['Area']

        df = df.append(df_temp)

dfNew = df[['Animal','Segment','FileName','Severity','Parent','Name','CY3_Dens','FITC_Dens']]
dfNew = dfNew.loc[df['Parent'].isin(['GreyMatterLeft','GreyMatterRight'])]

dfNew['CY3_Dens'] = np.where(dfNew['Name'] == 'GreyMatterRight',
                                           dfNew['CY3_Dens'] * (-1),
                                           dfNew['CY3_Dens'])
dfNew['FITC_Dens'] = np.where(dfNew['Name'] == 'GreyMatterRight',
                                           dfNew['FITC_Dens'] * (-1),
                                           dfNew['FITC_Dens'])


#CALCULATE FOR THE RED FIBERS
dd = dfNew.loc[dfNew['Name'] == 'M1FibersG']
mm=dd.groupby(['Segment','Animal','Severity','Parent'])['CY3_Dens'].sum().unstack().reset_index()

m1Fibers = mm.groupby(['Animal','Severity'])['GreyMatterLeft'].max().reset_index()

m1Fibers = m1Fibers.rename(columns={'GreyMatterLeft': 'MaxLeft'})
m1Fibers = pd.merge(mm, m1Fibers,  how='left', on=['Animal','Severity'])

m1Fibers["Ipsi"]=m1Fibers["GreyMatterRight"].div(m1Fibers["MaxLeft"])
m1Fibers["Contra"]=m1Fibers["GreyMatterLeft"].div(m1Fibers["MaxLeft"])

meanWMred = pd.merge(m1Fibers.groupby(['Severity', 'Segment'])['GreyMatterLeft'].mean().reset_index(),
                     m1Fibers.groupby(['Severity','Segment'])['GreyMatterRight'].mean().reset_index(),
                     how='left', on=['Severity', 'Segment'])

meanM1contra = m1Fibers.groupby(['Severity', 'Segment'])['Contra'].mean().reset_index()
meanM1ipsi = m1Fibers.groupby(['Severity', 'Segment'])['Ipsi'].mean().reset_index()
meanM1=pd.merge(meanM1contra, meanM1ipsi, how='left', on=['Severity', 'Segment'])

colorMild=(245/255, 183/255, 177/255, 1.0)
colorModerate=(169/255, 50/255, 38/255, 1.0)
colorHealthy=( 98/255, 101/255, 103/255,1.0)
colormap = np.array([colorHealthy,colorMild, colorModerate])


fig = plt.figure()
g=sns.scatterplot(data=m1Fibers,  x='Segment', y='GreyMatterLeft', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
h=sns.lineplot(data=meanWMred, x='Segment', y='GreyMatterLeft', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GMRed_ContraCounts.svg', format='svg')
plt.show()
plt.show()


fig = plt.figure()
sns.scatterplot(data=m1Fibers,  x='Segment', y='Contra', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
sns.lineplot(data=meanM1contra, x='Segment', y='Contra', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GMRed_Contra.svg', format='svg')
plt.show()


fig = plt.figure()
g=sns.scatterplot(data=m1Fibers,  x='Segment', y='GreyMatterRight', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
h=sns.lineplot(data=meanWMred, x='Segment', y='GreyMatterRight', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GMRed_IpsiCounts.svg', format='svg')
plt.show()


fig = plt.figure()
g=sns.scatterplot(data=m1Fibers,  x='Segment', y='Ipsi', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
h=sns.lineplot(data=meanM1ipsi, x='Segment', y='Ipsi', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GmRed_Ipsi.svg', format='svg')
plt.show()


#CALCULATE FOR THE GREEN FIBERS
dd = dfNew.loc[dfNew['Name'] == 'PMvFibersG']
pp=dd.groupby(['Segment','Animal','Severity','Parent'])['FITC_Dens'].sum().unstack().reset_index()

pmvFibers = pp.groupby(['Animal','Severity'])['GreyMatterLeft'].max().reset_index().rename(columns={'GreyMatterLeft': 'MaxLeft'})
pmvFibers = pd.merge(pp, pmvFibers,  how='left', on=['Animal','Severity'])

pmvFibers["Ipsi"]=pmvFibers["GreyMatterRight"].div(pmvFibers["MaxLeft"])
pmvFibers["Contra"]=pmvFibers["GreyMatterLeft"].div(pmvFibers["MaxLeft"])



meanWPMvred = pd.merge(pmvFibers.groupby(['Severity', 'Segment'])['GreyMatterLeft'].mean().reset_index(),
                       pmvFibers.groupby(['Severity','Segment'])['GreyMatterRight'].mean().reset_index(),
                       how='left', on=['Severity', 'Segment'])

meanPMvcontra = pmvFibers.groupby(['Severity', 'Segment'])['Contra'].mean().reset_index()
meanPMvipsi = pmvFibers.groupby(['Severity', 'Segment'])['Ipsi'].mean().reset_index()
meanM1=pd.merge(meanPMvcontra, meanPMvipsi, how='left', on=['Severity', 'Segment'])

colorMild=(245/255, 183/255, 177/255, 1.0)
colorModerate=(169/255, 50/255, 38/255, 1.0)
colorHealthy=( 98/255, 101/255, 103/255,1.0)
colormap = np.array([colorHealthy,colorMild, colorModerate])

fig = plt.figure()
g=sns.scatterplot(data=pmvFibers,  x='Segment', y='GreyMatterLeft', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
h=sns.lineplot(data=meanWPMvred, x='Segment', y='GreyMatterLeft', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GMGreen_ContraCounts.svg', format='svg')
plt.show()
plt.show()


fig = plt.figure()
sns.scatterplot(data=pmvFibers,  x='Segment', y='Contra', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
sns.lineplot(data=meanPMvcontra, x='Segment', y='Contra', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GMGreen_Contra.svg', format='svg')
plt.show()


fig = plt.figure()
g=sns.scatterplot(data=pmvFibers,  x='Segment', y='GreyMatterRight', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
h=sns.lineplot(data=meanWPMvred, x='Segment', y='GreyMatterRight', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GMGreen_IpsiCounts.svg', format='svg')
plt.show()


fig = plt.figure()
g=sns.scatterplot(data=pmvFibers,  x='Segment', y='Ipsi', hue='Severity',  style="Animal", hue_order =['Healthy','Mild','Moderate'],palette=colormap,s=100)
h=sns.lineplot(data=meanPMvipsi, x='Segment', y='Ipsi', hue='Severity', hue_order =['Healthy', 'Mild', 'Moderate'], palette=colormap)
plt.savefig(save_folder+'GMGreen_Ipsi.svg', format='svg')
plt.show()
