import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd
import skimage.io as io
import skimage as sk


def plot_reg(status, img1, img2, out, ig_name, s_img):
    if status:
        fig = plt.figure()
        plt.imshow(sitk.GetArrayViewFromImage(img1), cmap=plt.cm.Greys_r, alpha=0.7)
        plt.imshow(sitk.GetArrayViewFromImage(img2), cmap=plt.cm.Greys_r, alpha=0.5)
        plt.savefig(os.path.join(out, (ig_name + s_img)), format='svg')
        plt.axis('off')
        plt.show()
        #sitk.WriteImage(simg3, os.path.join(output_reg, (out_name + '.png')))

def same_height_images(ref_img, input_image,out_f,img_name):
    max_height, max_width = ref_img.shape
    ht, wt = input_image.shape

    max_width = max_width+100
    max_height = max_height+50
    padding_matrix = np.zeros((max_height, max_width))
    xoff = round((max_width - wt) / 2)
    print(xoff)
    resulted_image = padding_matrix.copy()
    resulted_image[0:ht, xoff:wt + xoff] = input_image
    # image_ubyte = sk.util.img_as_ubyte(out_img)
    io.imsave(os.path.join(out_f, 'PreProcessed', (img_name + '.png')), sk.img_as_float64(resulted_image),
              check_contrast=False)


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN'

monkey_dir = 'Vaiana'
input_images = os.path.join(root_dir, monkey_dir, 'Input')
image_list = glob.glob(os.path.join(input_images, '*.png'))

output_reg = os.path.join(root_dir, monkey_dir)
if not os.path.exists(output_reg):
    os.mkdir(output_reg)

#moving = io.imread(image_list[20])
#fixed = io.imread(image_list[37])

fixed = io.imread(os.path.join(root_dir, monkey_dir,'Vaiana_Reference_50.tif'),as_gray=True)
fixedff = sk.img_as_float64(fixed)
#moving = io.imread(image_list[2])

#fixed = sitk.ReadImage(os.path.join(input_images,'Merida_Reg_DS-64_Filter-Mean-5_Brain_039.png'))

for img in image_list:
    file_path, file_name = os.path.split(img)
    img_name = file_name.split('.')[0]
    #moving = sitk.ReadImage(img)

    same_height_images(fixed, io.imread(img,as_gray=True), output_reg,img_name)



