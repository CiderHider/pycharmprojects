import os
import glob
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import SimpleITK as sitk

monkey_dir= 'Merida'
region = 'GM'
folderType = 'M1GM'
root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'

atlasPath = os.path.join(root_path, 'SC_AtlasMasks')
atlas_list = glob.glob(os.path.join(atlasPath, '*.png'))

dfAtlas = pd.DataFrame(columns=['Name', 'Segment', 'Path'])
for image in atlas_list:
    file_path, file_name = os.path.split(image)
    checkMask = file_name.split('_')[0]
    if checkMask == 'M':
        segment = file_name.split('.')[0].split('_')[1]
        dfAtlas = dfAtlas.append({'Name': file_name, 'Segment': segment, 'Path': image}, ignore_index=True)


csvFilePath =os.path.join(root_path, 'ReferenceFiles')
segInfo = pd.read_csv(os.path.join(csvFilePath, (monkey_dir+'_Segments.csv')), sep=';')

inputFiles =os.path.join(root_path, 'RegistrationResults', ('RegFiles' + folderType), monkey_dir)
file_list = glob.glob(os.path.join(inputFiles, '*.png'))

outputFigures = os.path.join(root_path, 'Figures', monkey_dir)
if not os.path.exists(outputFigures):
    os.mkdir(outputFigures)

df = pd.DataFrame(columns=['Name', 'Segment','DataImg', 'DataReg'])
for image in file_list:
    file_path, file_name = os.path.split(image)
    parts = file_name.split('_')
    segment = parts[0]
    dataImage= sitk.ReadImage(image, sitk.sitkUInt8)
    dataImage = sitk.GetArrayViewFromImage(dataImage)
    data =dataImage# np.where((dataImage > 0), 1, 0)

    df = df.append({'Name': file_name, 'Segment': segment,
                    'DataImg': dataImage, 'DataReg': data}, ignore_index=True)

segm = df['Segment'].unique()
temp_df = pd.DataFrame(columns=['Segment', 'MaxConv'])

for ii in segm:
    tt = df.loc[df['Segment'] == ii, ['DataReg']].sum(axis=1).sum()
    a = gaussian_filter(tt, sigma=25)
    temp_df = temp_df.append({'Segment': ii, 'MaxConv': a.max()}, ignore_index=True)

for ii in segm:
    toPlot = df.loc[df['Segment'] == ii, ['DataReg']].sum(axis=1).sum()
    a = gaussian_filter(toPlot, sigma=25)
    column = temp_df["MaxConv"]
    maxElem = column.max()

    maskIndex = int(dfAtlas[dfAtlas['Segment'] == ii].index.values)

    #atlasMask = skimage.util.img_as_ubyte(skimage.color.rgb2gray(io.imread(dfAtlas.loc[maskIndex]['Path'])))
    atlasMask = sitk.GetArrayViewFromImage(sitk.ReadImage(dfAtlas.loc[maskIndex]['Path'], sitk.sitkUInt16))
    aCut = a[250:a.shape[0]-250,250:a.shape[1]-250]
    atlasCut = atlasMask[250:a.shape[0]-250,250:a.shape[1]-250]

    if folderType == 'M1WM' or folderType == 'PMvWM':
        plt.imshow(aCut, cmap="magma", vmax=aCut.max())
        plt.imshow(atlasCut, cmap="gray", alpha=0.8)
        plt.axis('off')
    else:
        fig = plt.figure()
        plt.imshow(aCut, cmap="hot", vmax=aCut.max())
        plt.imshow(atlasCut, cmap="gray", alpha=0.5)
        plt.axis('off')
        plt.savefig(os.path.join(outputFigures, ('Reg' + folderType + ii + '.svg')), format='svg')
        plt.show()



