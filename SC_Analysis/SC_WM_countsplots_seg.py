import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
from numpy import loadtxt
import re

colorMild = (245 / 255, 183 / 255, 177 / 255, 1.0)
colorModerate = (169 / 255, 50 / 255, 38 / 255, 1.0)
colorHealthy = (98 / 255, 101 / 255, 103 / 255, 1.0)
colormap = np.array([colorHealthy, colorMild, colorModerate])


def get_df_info(df_input):
    df_input['file_name'] = df_input['file_path'].split('/')[-1].split('.')[0]
    df_input['monkey'] = df_input['file_name'].split('_')[0]
    df_input['file_nr'] = df_input['file_name'].split('_')[1]
    return df_input


def plot_func_wm(df_data, datatype, output_folder,to_plot):
    max_nr = 3000
    df_data = df_data.sort_values(by=['segment'])

    brain_region=df_data['region'].unique()[0]
    brain_side = df_data['side'].unique()[0]
    '''
    if brain_region =='PMv':
        df_data = df_data.loc[(df_data['monkey'] != 'Jyn')]
    '''
    if datatype == "value":
        if brain_side == "contra":
            ticks_array =[0, max_nr]
        else:
            ticks_array = [-max_nr, 0]
    else:
        if brain_side == "contra":
            ticks_array =[0, 1]
        else:
            ticks_array = [-1, 0]

    outputName = brain_region+'_'+brain_side+'_'+datatype

    if to_plot:
        fig = plt.figure()
        #g = sns.scatterplot(data=df_data, x='segment', y=datatype, hue='severity',
        #                    hue_order=['Healthy', 'Mild', 'Moderate'], palette=colormap, s=100)

        h = sns.lineplot(data=df_data, x='segment', y=datatype, hue='severity',
                         hue_order=['Healthy', 'Mild', 'Moderate'], palette=colormap, ci=80)
        #g.set_yticks(ticks_array)
        h.set_yticks(ticks_array)
        h.set(title=outputName)
        h.tick_params(labelsize=20)

        plt.savefig(os.path.join(output_folder, (outputName + '.svg')), format='svg')
        plt.show()
    return;


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'

csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
monkeyInfo = pd.read_csv(os.path.join(csvFilePath, 'Monkey_Reference_sheet.csv'), sep=',')

segInfoPath = os.path.join(root_dir, 'ReferenceFiles')
segInfo = pd.read_csv(os.path.join(segInfoPath, 'SegmentsAllOriginal.csv'), sep=';')

# refInfo = segInfo.join(monkeyInfo.set_index(["name", 'color']), on=['monkey', 'color'])

save_folder = os.path.join(root_dir, 'FiguresWM')
if not os.path.exists(save_folder):
    os.makedirs(save_folder)

file_list = glob.glob(os.path.join(os.path.join(root_dir, 'Annotations'), '*.txt'))
df = pd.DataFrame(file_list, columns=['file_path'])
df = df.apply(lambda x: get_df_info(x), axis=1)

df_annotations = pd.DataFrame()
for iter, row in enumerate(df.itertuples()):
    df_temp = pd.read_csv(row.file_path, sep='\t')
    df_temp['file_name'] = row.file_name
    df_temp['monkey'] = row.monkey
    df_temp['slide'] = row.file_nr
    df_annotations = df_annotations.append(df_temp)

df_annotations = df_annotations.join(segInfo.set_index(['monkey', 'slide']), on=['monkey', 'slide'])
df_annotations = df_annotations.join(monkeyInfo.set_index(['monkey']), on='monkey')

dfNew = df_annotations[['monkey', 'segment', 'Name', 'Num M1Fibers', 'Num PMvFibers']]

df_right = dfNew.loc[df_annotations['Name'].isin(['WhiteMatterRight'])].rename(
    columns={"Num M1Fibers": "M1_ipsi", "Num PMvFibers": "PMv_ipsi"}).drop(columns=['Name'])
df_left = dfNew.loc[df_annotations['Name'].isin(['WhiteMatterLeft'])].rename(
    columns={"Num M1Fibers": "M1_contra", "Num PMvFibers": "PMv_contra"}).drop(columns=['Name'])

# df_right = df_right.groupby(['monkey','segment'])["M1_ipsi"].mean().unstack().reset_index()
df_right = df_right.melt(id_vars=["monkey", "segment"], var_name="side", value_name="value")
df_left = df_left.melt(id_vars=["monkey", "segment"], var_name="side", value_name="value")

df_nn = pd.concat([df_left.set_index(['monkey', 'segment']), df_right.set_index(['monkey', 'segment'])]).reset_index()
df_nn[['region', 'side']] = df_nn.side.str.split("_", expand=True, )

#dd = df_nn.groupby(['monkey', 'segment', 'side', 'region'])['value'].mean().unstack().reset_index()

dd = df_nn.copy()
#dd = dd.melt(id_vars=['monkey', 'segment', 'side'], var_name="region", value_name="value")

# why bb is not working?
# don't really like this method but nothing else at this moment

# aa = df_left.join(df_right.set_index(['monkey','segment']), on=['monkey','segment'])
# bb=pd.merge(df_left, df_right,  on=['monkey','segment'], how="right")

'''
df_ann = pd.concat([df_left.set_index(['monkey', 'segment']), df_right.set_index(['monkey', 'segment'])],
                   axis=1).reset_index()
df_ann = df_ann.melt(id_vars=["monkey", "segment"], var_name="side", value_name="value")
df_ann[['region', 'side']] = df_ann.side.str.split("_", expand=True, )
'''
calc_df = pd.DataFrame()
calc_df['max_val'] = dd.groupby(['monkey', 'region'])['value'].max()
calc_df['min_val'] = dd.groupby(['monkey', 'region'])['value'].min()
calc_df['max_min_val'] = calc_df['max_val'] - calc_df['min_val']
calc_df = calc_df.reset_index()

max_animals = dd.groupby(['region'])['value'].max()
min_animals = dd.groupby(['region'])['value'].min()

monkeyInfo = monkeyInfo.melt(id_vars=["monkey", "short_name", "lesion_date", "severity"], var_name="inj_location",
                             value_name="volume")
monkeyInfo['inj_location'] = np.where(monkeyInfo['inj_location'] == "inj_vol_m1", "M1", "PMv")

df_all = dd.join(calc_df.set_index(['monkey', 'region']), on=['monkey', 'region'])
df_all = df_all.join(monkeyInfo.set_index(['monkey', "inj_location"]), on=['monkey', 'region'])

df_all["general_max"] = np.where(df_all['region'] == 'M1',round(max_animals.iloc[0]),round(max_animals.iloc[1]))
df_all["general_min"] = np.where(df_all['region'] == 'M1',round(min_animals.iloc[0]),round(min_animals.iloc[1]))

df_all['max_norm'] = np.where(df_all['side'] == 'contra', df_all['value'] / df_all['max_val'],
                              df_all['value'] / df_all['max_val'] * (-1))
df_all['minmax_norm'] = np.where(df_all['side'] == 'contra', (df_all['value']-df_all['min_val']) / df_all['max_min_val'],
                                 (df_all['value']-df_all['min_val']) / df_all['max_min_val'] * (-1))

df_all['one_max_norm']= np.where(df_all['side'] == 'contra', df_all['value'] / df_all['general_max'],
                                 df_all['value'] / df_all['general_max'] * (-1))

df_all['one_minmax_norm']= np.where(df_all['side'] == 'contra',
                                    (df_all['value']-df_all['general_min']) / (df_all['general_max']-df_all['general_min']),
                                    ((df_all['value']-df_all['general_min']) / (df_all['general_max']-df_all['general_min']))* (-1))

df_all['inj_div'] = df_all['value'] / df_all['volume']
max_inj = df_all.groupby(['monkey', 'region'])['inj_div'].max().reset_index()
max_inj = max_inj.rename(columns={"inj_div": "max_inj_div"})
df_all = df_all.join(max_inj.set_index(['monkey', "region"]), on=['monkey', "region"])

df_all['inj_norm'] = np.where(df_all['side'] == 'contra', df_all['inj_div'] / df_all['max_inj_div'],
                              df_all['inj_div'] / df_all['max_inj_div'] * (-1))
df_all['value'] = np.where(df_all['side'] == 'contra', df_all['value'], df_all['value'] * (-1))


#what_to_plot = ['value','max_norm','minmax_norm','inj_norm','one_max_norm','one_minmax_norm']
what_to_plot = ['minmax_norm']
to_plot=True
for ii in what_to_plot:
    df_all.groupby(['side','region']).apply(lambda x: plot_func_wm(x, ii, save_folder,to_plot))

