
import sklearn.datasets
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
import re

colNN = ['AnimalName', 'Region', 'Area']

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/LESION/'

data_folder = os.path.join(root_dir,'Files_Annotations','Rey')
# = os.walk(data_folder)

folder_list = [f.path for f in os.scandir(data_folder) if f.is_dir()]

for folder in folder_list:
    file_list = glob.glob(os.path.join(folder, '*.csv'))

    df = pd.DataFrame()
    for file in file_list:
        df_temp = pd.read_csv(file, sep='\t')
        file_path, file_name = os.path.split(file)
        cols = [1, 7]
        if df_temp.empty:
            print('DataFrame is empty!')
            continue
        else:

            df_temp = df_temp[df_temp.columns[cols]]
            df_temp['Animal'] = folder.rsplit('/', 1)[1]
            slideNr = file.rsplit('/', 1)[1]
            sli = (re.findall("\d+", slideNr))
            df_temp['SlideNr'] = int(sli[0])

            df = df.append(df_temp)


    dfStroke = df[df[df.columns[0]].str.contains("Stroke")]

    dfStroke = dfStroke.sort_values(by=['SlideNr'])