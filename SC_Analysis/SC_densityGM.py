import sklearn.datasets
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import glob
import re

monkey_name = 'Vaiana'


csvFilePath = '/Users/muscalu/Dropbox (Privat)/' \
            'IC_Stroke_WorkingDirectory/HistoProcessedData/SC/ReferenceFiles/'
segInfo = pd.read_csv(csvFilePath+monkey_name+'_Segments.csv', sep=';')

data_folder = '/Users/muscalu/Dropbox (Privat)/' \
            'IC_Stroke_WorkingDirectory/HistoProcessedData/SC/Detections/'+monkey_name
#data_folder = os.path.abspath(os.getcwd())+'/data/'+monkey_name+'/Detections/'
file_list = glob.glob(os.path.join(data_folder, '*.txt'))
#save_folder = os.path.abspath(os.getcwd())+'/data/Figures/'
save_folder = '/Users/muscalu/Dropbox (Privat)/' \
            'IC_Stroke_WorkingDirectory/HistoProcessedData/SC/Figures/'
if not os.path.exists(save_folder):
    os.makedirs(save_folder)
#monkey_name = data_folder.rsplit('/', 3)[1]

df = pd.DataFrame()
for file in file_list:
    df_temp = pd.read_csv(file, sep='\t')
    file_path, file_name = os.path.split(file)
    #df_temp['Segment'] = file.rsplit('/', 2)[1]
    df_temp['Animal'] = file.rsplit('/', 3)[1]

    parts = file_name.split('_')
    df_temp['FileName'] = parts[len(parts) - 1].split('.')[0]

    slideName = parts[len(parts) - 1].split('.')[0]
    exIdx = int(segInfo[segInfo['Slide'] == slideName].index.values)
    df_temp['Segment'] = segInfo.loc[exIdx]['Segment']
    df_temp['CY3_Dens']=df_temp['CY3: Mean'] * df_temp['Area']
    df_temp['FITC_Dens'] = df_temp['FITC: Mean'] * df_temp['Area']

    df = df.append(df_temp)

dGreyMatter = df.loc[df['Parent'].isin(['GreyMatterLeft','GreyMatterRight'])]


gg = dGreyMatter.groupby(['FileName','Segment','Parent','Name'])['CY3_Dens'].sum().reset_index()

m1F = gg.groupby(['Segment','Parent','Name'])['CY3_Dens'].mean().unstack().reset_index()
m1F["M1FibersGNorm"]=m1F["M1FibersG"].div(max(m1F["M1FibersG"]))

g = sns.FacetGrid(data=m1F, col="Parent")
g.map_dataframe(sns.barplot, x="Segment", y="M1FibersGNorm")
g.add_legend()
g.set_titles(monkey_name+" Grey Matter Red Density")
plt.savefig(save_folder+monkey_name+'GM_Red.svg',format='svg')
plt.show()


pmvgg = dGreyMatter.groupby(['FileName','Segment','Parent','Name'])['FITC_Dens'].sum().reset_index()
pmvF = pmvgg.groupby(['Segment','Parent','Name'])['FITC_Dens'].mean().unstack().reset_index()
pmvF["PMvFibersGNorm"]=pmvF["PMvFibersG"].div(max(pmvF["PMvFibersG"]))

g = sns.FacetGrid(data=pmvF, col="Parent")
g.map_dataframe(sns.barplot, x="Segment", y="PMvFibersGNorm")
g.add_legend()
g.set_titles(monkey_name+" Grey Matter Green Density")
plt.savefig(save_folder+monkey_name+'GM_Green.svg',format='svg')
plt.show()


pmvgg = dGreyMatter.groupby(['FileName','Segment','Parent','Name'])['FITC_Dens'].sum().reset_index()
#test = pmvgg.pivot(index=pmvgg.columns.drop(['Name', 'FITC_Dens']).to_list(), columns=['Name']).reset_index()
g = sns.FacetGrid(data=pmvgg, col="Parent", row='Name')
g.map_dataframe(sns.barplot, x="FileName", y="FITC_Dens")
g.add_legend()
g.set_titles(monkey_name+" Grey Matter Green Density")
plt.savefig(save_folder+monkey_name+'GM_GreenS.svg',format='svg')
plt.show()

