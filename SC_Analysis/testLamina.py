import os
import glob
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from numpy import load
from skimage import io
import skimage.color
import tkinter as tk
import seaborn as sns
import sc_functions as fun


def calculate_density(density_image, operation='counts'):
    if operation == 'mean':
        result = np.sum(density_image) / np.count_nonzero(density_image)
    elif operation == 'sum':
        result = np.sum(density_image)
    elif operation == 'counts':
        result = np.count_nonzero(density_image)
    return result



# plot_individually = False
# split_sides = True
save_pickle = False
show_plots = False

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'
# Get data to analyze
root = tk.Tk()
root.withdraw()
# root_dir = os.path.join(root_dir, 'RegistrationResults')
# filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=root_dir)
# Collect Data in Dataframe
if not os.path.exists(root_dir):
    root_dir = os.path.expanduser("~")

# Prepare folder for export
output_path = os.path.join(root_dir, 'Figures')
if not os.path.exists(output_path):
    os.mkdir(output_path)

# Collect general information about the monkeys and lamina colours
csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
colorInfo = pd.read_csv(os.path.join(csvFilePath, 'colorsInSCMf.csv'), sep=';')
df_reference = pd.read_csv(os.path.join(csvFilePath, 'monkey_histo_reference_sheet_anti.csv'))

# Collect Data
df_reg_images = pd.DataFrame()

# Take the segment summed per images
df_files = pd.read_csv(os.path.join(csvFilePath, 'LaminaRegInfo.csv'), sep=',')
df_files['file_name'] = df_files['monkey'] + '_' + df_files['segment'] + '_' + df_files['color']
df_files['image'] = df_files['reg_result_path'].apply(lambda x: io.imread(x))

# Take the lamina masks
laminaList = glob.glob(os.path.join(root_dir, 'SC_LaminaBothSidesPadded', '*.png'))
df_laminas = pd.DataFrame(laminaList, columns=['lamina_mask_path'])
df_laminas['segment'] = df_laminas['lamina_mask_path'].apply(lambda x: x.split('/')[-1].split('.')[0])

# join the file df, atlas df, and general information dfs into one
df_reg_images = df_files.copy()
df_reg_images = df_reg_images.join(df_reference.set_index(['name', 'color']), on=['monkey', 'color'])
df_reg_images = df_reg_images.join(df_laminas.set_index('segment'), on='segment')

# Get Lamina Masks
df_reg_images['lamina_mask'] = df_reg_images['lamina_mask_path'].apply(lambda x: skimage.img_as_ubyte(io.imread(x)))

# Expand dataframe to include laminas
df_reg_images = df_reg_images.reindex(columns=df_reg_images.columns.tolist() + colorInfo['Segment'].tolist())

# Calculate mean of signal in each Lamina per Segment
for row in colorInfo.itertuples():
    mask_value = row.Number
    # Get mean signal for row.lamina_mask in each Segment
    df_reg_images[row.Segment] = df_reg_images.apply(
        lambda x: fun.mask_operation(x.image, x.lamina_mask, mask_value,"counts"), axis=1)

df_reg_images = df_reg_images.sort_values(by=['segment'])
df_reg_images['max_sig'] = df_reg_images.apply(lambda x: np.max(x.image), axis=1)

df_reg_images['counts_total'] = df_reg_images.apply(lambda x: calculate_density(x.image,'counts'), axis=1)

ss = df_reg_images.groupby(['monkey', 'color'])['counts_total'].apply(lambda x: x.max()).to_frame().reset_index()
ss.rename(columns={"counts_total": "max_counts_img"}, inplace=True)
df_reg_images = df_reg_images.join(ss.set_index(["monkey", 'color']), on=['monkey', 'color'])

"""
Plotting
"""
monkey_list = df_reg_images['monkey'].unique().tolist()

# Get lamina Columns
lamina_columns = [x for x in df_reg_images.columns.tolist() if 'ipsi_' in x or 'contra_' in x]

# Plot results normalize by the max
df_normalized = df_reg_images.copy()
df_normalized[lamina_columns] = df_normalized[lamina_columns].div(df_normalized['max_counts_img'], axis=0)

output_path = os.path.join(root_dir, 'Figures', 'TestCounts')
fun.plot_ipsi_contra_segments(df_normalized, subject_list=monkey_list, show_plots=show_plots, save_path=output_path,plot_xlims=0.5)



df_plot = df_normalized.copy()
output_path = os.path.join(root_dir, 'Figures', 'TestCountsBar')
fun.plot_lamina_bar(df_normalized,'M1',False,output_path)
fun.plot_lamina_bar(df_normalized,'PMv',False,output_path)
