import os
import glob
import matplotlib.pyplot as plt
import pandas as pd
import SimpleITK as sitk
from scipy import ndimage
import numpy as np
from PIL import Image
from skimage import io
import seaborn as sns
import re
from tkinter import filedialog
import tkinter as tk
from scipy.ndimage import gaussian_filter


def define_colormap(map_type):
    if map_type == 'M1':
        colorMild = (245 / 255, 183 / 255, 177 / 255, 1.0)
        colorModerate = (169 / 255, 50 / 255, 38 / 255, 1.0)
        colorHealthy = (98 / 255, 101 / 255, 103 / 255, 1.0)
        colormap = np.array([colorHealthy, colorMild, colorModerate])
    elif map_type == 'PMv':
        colorMild = (125 / 255, 230 / 255, 222 / 255, 1.0)
        colorModerate = (8 / 255, 137 / 255, 128 / 255, 1.0)
        colorHealthy = (98 / 255, 101 / 255, 103 / 255, 1.0)
        colormap = np.array([colorHealthy, colorMild, colorModerate])
    elif map_type == 'Lesion':
        colorMild = (25 / 255, 211 / 255, 197 / 255, 1.0)
        colorModerate = (240 / 255, 223 / 255, 13 / 255, 1.0)
        colorSevere = (250 / 255, 82 / 255, 91 / 255, 1.0)
        colormap = np.array([colorMild, colorModerate,colorSevere])
    return colormap;



def check_file_path(data_path):
    import tkinter as tk
    from tkinter import filedialog
    if not os.path.exists(data_path):
        data_path = os.path.expanduser("~")
        root = tk.Tk()
        root.withdraw()
        data_path = filedialog.askdirectory(title="Chose data directory with images to analyze", initialdir=data_path)
    return data_path


def max_dimentions(img_list):
    height_vector = np.zeros(len(img_list)).astype(int)
    width_vector = np.zeros(len(img_list)).astype(int)
    counter = 0
    for ii in img_list:
        width_vector[counter], height_vector[counter] = Image.open(ii).size
        counter += 1
    max_height = height_vector.max() + 500
    max_width = width_vector.max() + 500
    return max_height, max_width


def set_folder_path(root_dir, image_type):
    input_dir = os.path.join(root_dir, str(image_type + 'ToBeProcessed'))
    output_dir = os.path.join(root_dir, str(image_type + 'ProcessedReg'))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    image_list = glob.glob(os.path.join(input_dir, '*.png'))
    return image_list, output_dir


def create_ann_dfs(img_list, col_name):
    df = pd.DataFrame(img_list, columns=['path'])
    df['image_name'] = df['path'].apply(lambda x: os.path.basename(x))
    df[col_name] = df['image_name'].apply(lambda x: pd.Series(str(x).split("_")))
    df.drop('rest', axis=1, inplace=True)
    return df


def flip_and_bw_transform(in_img, flip_status, image_type):
    if flip_status == 'flip':
        print('inflip')
        in_img = np.fliplr(in_img)
        #in_img = sitk.Flip(in_img, [True, False, False])  # cv2.flip(inputImage, 1)

    #bw_image = sitk.GetArrayFromImage(sitk.VectorMagnitude(in_img))
    bw_image = in_img
    if image_type == 'Annotations':
        bw_image[bw_image == bw_image.max()] = 255
        bw_image[(bw_image > 0) & (bw_image < 255)] = 70
    else:
        bw_image[bw_image < 0] = 0
    return bw_image


def padding_func(input_image, max_height, max_width, ht, wt, cm_h, cm_w):
    padding_matrix = np.zeros((max_height, max_width))
    yoff = round(max_height / 2 - cm_h)
    xoff = round(max_width / 2 - cm_w)
    resulted_image = padding_matrix.copy()
    resulted_image[yoff:yoff + ht, xoff:xoff + wt] = input_image
    #output_image = resulted_image.astype(np.uint8)
    output_image = input_image
    return output_image


def preprocess_annotations_anti(folder_type, df_info, max_height, max_width):
    df_info['cm_imgH'] = np.nan
    df_info['cm_imgW'] = np.nan
    counter = 0
    for row in df_info.itertuples(index=False):
        inputImage = io.imread(row.ann_path_input)
        inputImage = inputImage[:,:,0]
        flipStatus = row.transformation
        status = row.keep
        if status:
            # Convert the image to black and white and flip them
            bw_image = flip_and_bw_transform(inputImage, flipStatus, folder_type)  # Flip image in the function
            h, w = bw_image.shape
            cmImgH, cmImgW = ndimage.measurements.center_of_mass(bw_image)
            df_info.loc[counter, 'cm_imgH'] = cmImgH
            df_info.loc[counter, 'cm_imgW'] = cmImgW
            padded_image = padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)
            #out = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(padded_image)), sitk.sitkUInt8)
            #sitk.WriteImage(out, row.ann_path)
            counter += 1
            io.imsave(row.ann_path, padded_image, check_contrast=False)
    return df_info


def preprocess_detections_anti(folder_type, df_info, max_height, max_width):
    for row in df_info.itertuples(index=False):
        inputImage = io.imread(row.det_path_input)
        flipStatus = row.transformation
        status = row.keep
        if status:
            bw_image = flip_and_bw_transform(inputImage, flipStatus, folder_type)  # Flip image in the function
            h, w = bw_image.shape
            cmImgH = row.cm_imgH
            cmImgW = row.cm_imgW
            padded_image = padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)
            io.imsave(row.det_path, padded_image, check_contrast=False)



def display_sidebyside(fixed_npa, moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    #plt.show()


def plot_reg(status, img1, img2, out, ig_name, s_img):
    if status:
        fig = plt.figure()
        plt.imshow(sitk.GetArrayViewFromImage(img1), cmap=plt.cm.Greys_r, alpha=0.5)
        plt.imshow(sitk.GetArrayViewFromImage(img2), cmap=plt.cm.Greys_r, alpha=0.3)
        plt.savefig(os.path.join(out, (ig_name + s_img)), format='svg')
        plt.axis('off')
        #plt.show()


def reg_function(fixed, moving, output_reg, image_name):
    """
        Parameters
        ----------
        fixed : ATLAS
            Numpy array containing image data

        moving : ANNOTATION IMAGES
            Numpy array containing 8 bit integer masks

        output_reg: PATH to OUTPUT FOLDER
            Str contains the path to the output folder

        image_name :
            Str contains the name for the outputed result

        -----------------
        Output
        ------
        rigid_transformation: RIGID TRANSFORMATION
            Transform

        bSplineTransformation: B-SPLINE TRANSFORMATION
            Transform

        -----------------
        Function to calculate the registration between the fixed and the moving images.
        1) First create an initial rigid transformation
        2) Get the rigid registred moving image (RMI)
        3) Create a bspline transformation
        4) Register the resulted RMI to the fix image using a bspline transformation
        5) Export the regid and b-spline transformation
    """
    # Apply initial alignment
    status = True # if all the plots should be outputed or not
    initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                          moving,
                                                          sitk.Similarity2DTransform(),
                                                          sitk.CenteredTransformInitializerFilter.GEOMETRY)
    # Test to see how they look as overlay
    moving_resampled = sitk.Resample(moving, fixed,
                                     initial_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
    plot_reg(status, moving_resampled, fixed, output_reg, image_name, 'NotRegistred.svg')

    registration_method = sitk.ImageRegistrationMethod()

    # Similarity metric settings.
    registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.01)
    registration_method.SetInterpolator(sitk.sitkLinear)

    # Optimizer settings.
    registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                      numberOfIterations=1000,
                                                      convergenceMinimumValue=1e-6,
                                                      convergenceWindowSize=500)
    registration_method.SetOptimizerScalesFromPhysicalShift()

    # Setup for the multi-resolution framework.
    registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[6, 2, 1])
    registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
    registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

    # Don't optimize in-place, we would possibly like to run this cell multiple times.
    registration_method.SetInitialTransform(initial_transform, inPlace=False)

    rigid_transformation = registration_method.Execute(fixed, moving)
    print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
    print('Optimizer\'s stopping condition, {0}'.format(
        registration_method.GetOptimizerStopConditionDescription()))

    moving_rigid = sitk.Resample(moving, fixed, rigid_transformation, sitk.sitkLinear, 0.0, moving.GetPixelID())
    plot_reg(status, moving_rigid, fixed, output_reg, image_name, 'regRigid.svg')

    # BSPLINE
    transformDomainMeshSize = [2] * fixed.GetDimension()
    tx = sitk.BSplineTransformInitializer(fixed, transformDomainMeshSize)

    print(f"Initial Number of Parameters: {tx.GetNumberOfParameters()}")

    R = sitk.ImageRegistrationMethod()
    R.SetMetricAsJointHistogramMutualInformation()

    R.SetOptimizerAsGradientDescentLineSearch(learningRate=5.0,
                                              numberOfIterations=100,
                                              convergenceMinimumValue=1e-4,
                                              convergenceWindowSize=5)

    R.SetInterpolator(sitk.sitkLinear)

    R.SetInitialTransformAsBSpline(tx, inPlace=True, scaleFactors=[1, 2, 5])
    R.SetShrinkFactorsPerLevel([4, 2, 1])
    R.SetSmoothingSigmasPerLevel([4, 2, 1])

    bSplineTransformation = R.Execute(fixed, moving_rigid)

    print("-------")
    print(bSplineTransformation)
    print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
    print(f" Iteration: {R.GetOptimizerIteration()}")
    print(f" Metric value: {R.GetMetricValue()}")

    movingRegBspline = sitk.Resample(moving_rigid, fixed, bSplineTransformation, sitk.sitkLinear, 0.0,
                                     moving_resampled.GetPixelID())
    plot_reg(status, movingRegBspline, fixed, output_reg, image_name, 'RegBspline.svg')

    return rigid_transformation, bSplineTransformation


def transformation_function(fixed_img, rigid_t, bspl_t, det_i, output_reg, out_name):
    """
            Parameters
            ----------
            fixed_img : ATLAS
                Numpy array containing image data

            rigid_t: RIGID TRANSFORMATION
                Transform

            bspl_t: B-SPLINE TRANSFORMATION
                Transform

            det_i: DETECTION IMAGES

            output_reg: PATH to OUTPUT FOLDER
                Str contains the path to the output folder

            out_name :
                Str contains the name for the outputed result

            -----------------
            Function to calculate the registration between the fixed and the moving images.
            1) Apply the rigid transformation on the detection images
            2) Apply the bspline transformation on the rigid transformed detection images
        """

    # Apply transformation to a different image
    resamplerRigid = sitk.ResampleImageFilter()
    resamplerRigid.SetReferenceImage(fixed_img)
    resamplerRigid.SetInterpolator(sitk.sitkLinear)
    resamplerRigid.SetDefaultPixelValue(50)
    resamplerRigid.SetTransform(rigid_t)

    outRigidApplied = resamplerRigid.Execute(det_i)

    resamplerBSpline = sitk.ResampleImageFilter()
    resamplerBSpline.SetReferenceImage(fixed_img)
    resamplerBSpline.SetInterpolator(sitk.sitkLinear)
    resamplerBSpline.SetDefaultPixelValue(50)
    resamplerBSpline.SetTransform(bspl_t)

    outBSplineApplied = resamplerBSpline.Execute(outRigidApplied)

    simg1 = sitk.Cast(sitk.RescaleIntensity(fixed_img), sitk.sitkUInt8)
    simg2 = sitk.Cast(sitk.RescaleIntensity(outRigidApplied), sitk.sitkUInt8)
    simg3 = sitk.Cast(sitk.RescaleIntensity(outBSplineApplied), sitk.sitkUInt8)

    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.imshow(sitk.GetArrayViewFromImage(simg2), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(simg3), cmap=plt.cm.Greys_r, alpha=0.7)
    plt.axis('off')
    plt.savefig(os.path.join(output_reg, (out_name + 'detections.svg')), format='svg')
    #plt.show()
    sitk.WriteImage(simg3, os.path.join(output_reg, (out_name + '.png')))
    io.imsave(os.path.join(output_reg, (out_name + '.tif')), sitk.GetArrayViewFromImage(simg3), check_contrast=False)


def sum_goupped_images(input_df):
    """
        Parameters
        ----------
        input_df : dataframe
            Input data to be summed

        Returns
        -------
        result: ndarray
            Resulted sum of the images
        """
    df = input_df.copy()
    df['new'] = df['reg_path'].apply(lambda x: io.imread(x))
    df['new'] = df.apply(lambda x: np.where(x['new'] > x['th_anti'], x['new'], 0), axis=1)
    # summed_image = df['new'].apply(lambda x: sum(x))
    # summed_image = df.groupby(grouping)['new'].apply(lambda x: sum(x)) # x.sum(axis=0)
    summed_image = df['new'].sum()
    return summed_image


def apply_atlas_mask(df_input, sigma_val, out_lm):
    image = df_input.sum_seg
    atlas_img = io.imread(df_input['atlas_path'])
    image_masked = image & atlas_img
    # Output the raw summed image
    out_name = df_input['monkey']+'_'+df_input['segment']+'_'+df_input['color']
    print(out_name)
    io.imsave(os.path.join(out_lm, (out_name + '.tif')), image_masked, check_contrast=False)

    gauss_image = gaussian_filter(image_masked, sigma=sigma_val)
    return pd.Series({'masked_images': image_masked, 'gauss_masked_image': gauss_image})


def plot_results_gm(input_df, out_lm, to_plot, filt):
    if to_plot:
        out_name = input_df['monkey'] + '_' + input_df['segment'] + '_' + input_df['color']
        if filt:
            image = input_df['gauss_masked_image']
            plot_max = input_df['max_color_gauss']
        else:
            image = input_df['masked_images']
            plot_max = 255

        atl = io.imread(input_df['atlas_cont'])
        plt.imshow(image, cmap="hot", vmin=0, vmax=plot_max-0.5*plot_max)
        plt.imshow(atl, cmap="gray", alpha=0.5)
        plt.axis('off')
        plt.savefig(os.path.join(out_lm, (out_name + '.svg')), format='svg')


def mask_operation(density_image, lamina_mask, var_mask, operation='mean'):
    """
    Parameters
    ----------
    density_image : np.ndarray
        Numpy array containing image data

    lamina_mask : np.ndarray
        Numpy array containing 8 bit integer masks

    var_mask: int
        Integer with the mask value of the lamina_mask to be applied

    operation: String | ('mean', 'sum')
        String indicating whether sum or mean of the masked values should be returned. Default: 'mean'

    Returns
    -------
    result: float
        Result of operation on the masked image
    """
    mask_single = np.where(lamina_mask == var_mask, 1, 0)
    image_clean = density_image * mask_single
    if operation == 'mean':
        result = np.sum(image_clean) / np.count_nonzero(mask_single)
    elif operation == 'sum':
        result = np.sum(image_clean)
    elif operation == 'counts':
        result = np.count_nonzero(image_clean)
    else:
        raise Exception('Operation passed to function mask_operation() not found. ')

    return result


def plot_ipsi_contra_segments(df, subject_key='monkey', subject_list=[], show_plots=True, save_path="", plot_xlims=0.6,
                              secondary_iterator_key='brain_region'):
    """Plots and/or saves back-to-back horizontal barplots of contra_x and ispi_x columns according to the segments column in DF

    Parameters
    ----------
    df : pandas.Dataframe()
        Pandas Dataframe containing 'name' and 'segments' column as well as matched numeric data columns of contra_x and ispi_x

    subject_key: Key in df
        Subject key to iterate over. Default: 'name'

    subject_list : List of strings
        . List of subjects in df(subject_col) to plot. If subject_list is empty, all subjects are taken into account.

    show_plots: Boolean
        Shows plots if set to true. Default: True

    save_path: Path String
        Saves figures into passed path. Saves them to current folder if nothing is passed.

    plot_xlims: Number
            Plot xlimits=[Number, 0, Number] Default: [1, 0, 1]

    secondary_iterator_key: Key in df
            secondary key to iterate over. Default: 'brain_region'

    Returns
    -------
    void
    """

    # Copy Dataframe to avoid chaning original dataframe
    df_copy = df.copy()

    # Create Save Dir
    if save_path:
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        print(f"Saving Plots to Path: {save_path}")

    if not subject_list:
        subject_list = df_copy[subject_key].unique().tolist()

    # Define Ipsi and contralesional columns and *(-1) ipsilesional values for plotting
    ipsi_list = [x for x in df_copy.columns.tolist() if 'ipsi_' in x]
    df_copy[ipsi_list] = df_copy[ipsi_list] * -1
    plotting_list = [(re.search('ipsi_(.*)', x)).group(1) for x in ipsi_list]

    # Xlimit and Interval
    assert isinstance(plot_xlims, (int, float)), "plot_xlims must be a int or float"

    x_limits = [-plot_xlims, plot_xlims]#[-0.05, 0.05]  # changed the initial numbers
    x_ticks = np.linspace(x_limits[0], x_limits[1], num=5)
    print(x_limits)
    print(x_ticks)
    # Plotting
    for subject in subject_list:
        for region in df_copy[secondary_iterator_key].unique().tolist():
            df_plot = df_copy.loc[(df_copy[subject_key] == subject) & (df_copy[secondary_iterator_key] == region)]
            if df_plot.empty:
                print(f"Could not find subject: {subject}. Check the passed subject_list!")
                continue

            for lamina_plot in plotting_list:
                col_list = [f"contra_{lamina_plot}", f"ipsi_{lamina_plot}"]
                if not pd.Series(col_list).isin(df_plot).all():
                    print(f"Could not find contra and ipsi columns for: {subject} - {lamina_plot}")
                    continue

                plt.figure()
                fig = sns.barplot(data=df_plot, x=f"contra_{lamina_plot}", y='segment',
                                  color='#FA525B')  # , lw=0) # fig might be useless... define with with plt figure
                fig = sns.barplot(data=df_plot, x=f"ipsi_{lamina_plot}", y='segment', color='#F0DF0D')  # , lw=0)

                fig.set(xlabel="Signal Intensity / Lamina Area", ylabel="Spinal Cord Segment",
                        title=f"{subject} - {region} Projection Density in Lamina: {lamina_plot.upper()}",
                        xlim=x_limits, xticks=x_ticks)
                # x_ticks = plt.gca().get_xticks().astype(np.int)

                plt.xticks(x_ticks, labels=np.abs(x_ticks))

                if save_path:
                    plt.savefig(os.path.join(save_path, f"{subject}_site-{region}_lamina-{lamina_plot}.svg"))

                if show_plots:
                    plt.show()
                else:
                    plt.close()


def plot_lamina_bar(df_input, brain_region, show_plots=False, save_path="", plot_lims=0.5):
    # Create Save Dir
    if save_path:
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        print(f"Saving Plots to Path: {save_path}")
    df_plot = df_input.copy()
    df_plot = df_plot.loc[df_plot['brain_region'] == brain_region]
    lamina_list = [x for x in df_input.columns.tolist() if 'contra_' in x]

    limits = [0, plot_lims]
    x_ticks = np.linspace(limits[0], limits[1], num=3)

    colormap = define_colormap(brain_region)
    for ii in lamina_list:
        fig = plt.figure()#figsize=(14, 10)
        h = sns.barplot(data=df_plot, y='segment', x=ii, hue='severity',
                        hue_order=['Control', 'Mild', 'Moderate'], palette=colormap, ci=None)
        h.set_xticks(x_ticks)
        h.tick_params(labelsize=15)
        output_name = brain_region + ii
        h.set(title=output_name)

        plt.savefig(os.path.join(save_path, (output_name + '.svg')), format='svg')

    if show_plots:
        plt.show()
    else:
        plt.close()
    return

'''
def test_plot_ipsi_contra_segments(df, subject_key='monkey', subject_list=[], show_plots=True, save_path="", plot_xlims=1,
                              secondary_iterator_key='brain_region'):


    # Copy Dataframe to avoid chaning original dataframe
    df_copy = df.copy()

    # Create Save Dir
    if save_path:
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        print(f"Saving Plots to Path: {save_path}")

    if not subject_list:
        subject_list = df_copy[subject_key].unique().tolist()

    # Define Ipsi and contralesional columns and *(-1) ipsilesional values for plotting
    ipsi_list = [x for x in df_copy.columns.tolist() if 'ipsi_' in x]
    df_copy[ipsi_list] = df_copy[ipsi_list] * -1
    plotting_list = [(re.search('ipsi_(.*)', x)).group(1) for x in ipsi_list]

    # Xlimit and Interval
    assert isinstance(plot_xlims, (int, float)), "plot_xlims must be a int or float"

    # Plotting
    for subject in subject_list:
        for region in df_copy[secondary_iterator_key].unique().tolist():
            df_plot = df_copy.loc[(df_copy[subject_key] == subject) & (df_copy[secondary_iterator_key] == region)]
            if df_plot.empty:
                print(f"Could not find subject: {subject}. Check the passed subject_list!")
                continue

            for lamina_plot in plotting_list:
                col_list = [f"contra_{lamina_plot}", f"ipsi_{lamina_plot}"]
                if not pd.Series(col_list).isin(df_plot).all():
                    print(f"Could not find contra and ipsi columns for: {subject} - {lamina_plot}")
                    continue

                plt.figure()
                fig = sns.barplot(data=df_plot, x=f"contra_{lamina_plot}", y='segment',
                                  color='#FA525B')  # , lw=0) # fig might be useless... define with with plt figure
                fig = sns.barplot(data=df_plot, x=f"ipsi_{lamina_plot}", y='segment', color='#F0DF0D')  # , lw=0)

                fig.set(xlabel="Signal Intensity / Lamina Area", ylabel="Spinal Cord Segment",
                        title=f"{subject} - {region} Projection Density in Lamina: {lamina_plot.upper()}",
                        )
                # x_ticks = plt.gca().get_xticks().astype(np.int)
                if save_path:
                    plt.savefig(os.path.join(save_path, f"{subject}_site-{region}_lamina-{lamina_plot}.svg"))

                if show_plots:
                    plt.show()
                else:
                    plt.close()
'''

def normalizer(df, method='max'):
    """
    Column wise normalisation of numeric part of dataframe.

    Parameters
    ----------
    df: pd.Dataframe()
        Dataframe to be normalized
    method: String
        'max': normalisation by max - X / max(abs(X))
        'min-max': normalized by min-max - (X-min(X)) / (max(X)-min(X))
    Returns
    -------
    df_norm: pd.Dataframe()
        normalized Dataframe
    """
    df_norm = df.copy()
    df_numeric = df_norm.select_dtypes(np.number)
    cols_numeric = df_numeric.columns.to_list()
    if method == 'max':
        df_norm[cols_numeric] = df_numeric / df_numeric.abs().max()
    elif method == 'max-min':
        df_norm[cols_numeric] = (df_numeric - df_numeric.min()) / (df_numeric.max() - df_numeric.min())
    else:
        raise Exception('Passed normalizer method not implemented')
    return df_norm
