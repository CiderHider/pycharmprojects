import os
import glob
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from numpy import load
from skimage import io
import skimage.color
import tkinter as tk
import seaborn as sns
import sc_functions as fun


def calculate_density(density_image, operation='mean'):
    if operation == 'mean':
        result = np.sum(density_image) / np.count_nonzero(density_image)
    elif operation == 'sum':
        result = np.sum(density_image)
    return result


# plot_individually = False
# split_sides = True
save_pickle = False
show_plots = False

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'
# Get data to analyze
root = tk.Tk()
root.withdraw()
# root_dir = os.path.join(root_dir, 'RegistrationResults')
# filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=root_dir)
# Collect Data in Dataframe
if not os.path.exists(root_dir):
    root_dir = os.path.expanduser("~")

# Prepare folder for export
output_path = os.path.join(root_dir, 'Figures')
if not os.path.exists(output_path):
    os.mkdir(output_path)

# Collect general information about the monkeys and lamina colours
csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
colorInfo = pd.read_csv(os.path.join(csvFilePath, 'colorsInSCMf.csv'), sep=';')
df_reference = pd.read_csv(os.path.join(csvFilePath, 'monkey_histo_reference_sheet_anti.csv'))

# Collect Data
df_reg_images = pd.DataFrame()

# Take the segment summed per images
df_files = pd.read_csv(os.path.join(csvFilePath, 'LaminaRegInfo.csv'), sep=',')
df_files['file_name'] = df_files['monkey'] + '_' + df_files['segment'] + '_' + df_files['color']
df_files['image'] = df_files['reg_result_path'].apply(lambda x: io.imread(x))

# Take the lamina masks
laminaList = glob.glob(os.path.join(root_dir, 'SC_LaminaBothSidesPadded', '*.png'))
df_laminas = pd.DataFrame(laminaList, columns=['lamina_mask_path'])
df_laminas['segment'] = df_laminas['lamina_mask_path'].apply(lambda x: x.split('/')[-1].split('.')[0])

# join the file df, atlas df, and general information dfs into one
df_reg_images = df_files.copy()
df_reg_images = df_reg_images.join(df_reference.set_index(['name', 'color']), on=['monkey', 'color'])
df_reg_images = df_reg_images.join(df_laminas.set_index('segment'), on='segment')

# Get Lamina Masks
df_reg_images['lamina_mask'] = df_reg_images['lamina_mask_path'].apply(lambda x: skimage.img_as_ubyte(io.imread(x)))

# Expand dataframe to include laminas
df_reg_images = df_reg_images.reindex(columns=df_reg_images.columns.tolist() + colorInfo['Segment'].tolist())

# Calculate mean of signal in each Lamina per Segment
for row in colorInfo.itertuples():
    mask_value = row.Number
    # Get mean signal for row.lamina_mask in each Segment
    df_reg_images[row.Segment] = df_reg_images.apply(
        lambda x: fun.mask_operation(x.image, x.lamina_mask, mask_value), axis=1)

df_reg_images = df_reg_images.sort_values(by=['segment'])
df_reg_images['max_sig'] = df_reg_images.apply(lambda x: np.max(x.image), axis=1)

# calculate density per whole image
df_reg_images['density_img'] = df_reg_images.apply(lambda x: calculate_density(x.image), axis=1)

# Calculate max density
ss = df_reg_images.groupby(['monkey', 'color'])['density_img'].apply(lambda x: x.max()).to_frame().reset_index()
ss.rename(columns={"density_img": "max_density_img"}, inplace=True)
df_reg_images = df_reg_images.join(ss.set_index(["monkey", 'color']), on=['monkey', 'color'])

# Calculate min density
ss = df_reg_images.groupby(['monkey', 'color'])['density_img'].apply(lambda x: x.min()).to_frame().reset_index()
ss.rename(columns={"density_img": "min_density_img"}, inplace=True)
df_reg_images = df_reg_images.join(ss.set_index(["monkey", 'color']), on=['monkey', 'color'])

df_reg_images = df_reg_images.drop(columns=['image', 'lamina_mask'])
df_all = df_reg_images.copy()

'''
"""
Plot Combination
"""
output_path = os.path.join(root_dir, 'Figures', 'CombinedAnti')
if not os.path.exists(output_path):
    os.mkdir(output_path)
df_plot = df_all.melt(id_vars=['monkey', 'segment', 'brain_region'], value_vars=colorInfo['Segment'].tolist(),
                      var_name='lamina_region', value_name='summed_signal')
g = sns.FacetGrid(data=df_plot, row='lamina_region', col='monkey', hue='brain_region',
                  row_order=colorInfo['Segment'].tolist(), sharex=False)
g.map_dataframe(sns.barplot, x='summed_signal', y='segment', orient='h')
g.savefig(os.path.join(output_path, f"Combined_Laminas.png"))

# Save pickle
if save_pickle:
    df_all.to_pickle(os.path.join(output_path, f"Combined_Laminas.pkl"))
'''

"""
Plotting
"""
monkey_list = df_all['monkey'].unique().tolist()

# Get lamina Columns
lamina_columns = [x for x in df_all.columns.tolist() if 'ipsi_' in x or 'contra_' in x]

# Plot results without normalisation
'''
df_normalized = df_all.copy()
#aa= df_normalized.loc[(df_normalized['monkey']=='Merida') & (df_normalized['color'] == 'red')]
output_path = os.path.join(root_dir, 'Figures', 'TestPlotLamina')
fun.test_plot_ipsi_contra_segments(df_normalized, subject_list=monkey_list, show_plots=show_plots, save_path=output_path)
'''
# Normalize by the max
df_normalized = df_all.copy()
df_normalized[lamina_columns] = df_normalized[lamina_columns].div(df_normalized['max_density_img'], axis=0)

#output_path = os.path.join(root_dir, 'Figures', 'TestMaxNorm')
#fun.plot_ipsi_contra_segments(df_normalized, subject_list=monkey_list, show_plots=show_plots, save_path=output_path)



df_plot = df_normalized.copy()
output_path = os.path.join(root_dir, 'Figures', 'TestMaxNormBar')
fun.plot_lamina_bar(df_normalized,'M1',False,output_path,0.4)
fun.plot_lamina_bar(df_normalized,'PMv',False,output_path,0.2)



'''
# Normalize by the minmax
df_normalized = df_all.copy()
df_normalized[lamina_columns] = df_normalized[lamina_columns] = (
    df_normalized[lamina_columns].sub(df_normalized['min_density_img'], axis=0)).div(
    df_normalized['max_density_img'] - df_normalized['min_density_img'], axis=0)
aa = (df_normalized[lamina_columns] - df_normalized['min_density_img'])
output_path = os.path.join(root_dir, 'Figures', 'TestMinMaxNorm')
fun.plot_ipsi_contra_segments(df_normalized, subject_list=monkey_list, show_plots=show_plots, save_path=output_path)
'''
