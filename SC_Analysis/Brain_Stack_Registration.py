import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd
import skimage.io as io
import skimage as sk

def create_info_dfs(img_list, col_name):
    df = pd.DataFrame(img_list, columns=['path'])
    df['image_name'] = df['path'].apply(lambda x: os.path.basename(x))
    df[col_name] = df['image_name'].apply(lambda x: pd.Series(str(x).split('.')[0].split('_')))
    #df.drop('rest', axis=1, inplace=True)
    return df


def plot_reg(status, img1, img2, out, ig_name, s_img):
    if status:
        fig = plt.figure()
        plt.imshow(sitk.GetArrayViewFromImage(img1), cmap=plt.cm.Greys_r, alpha=0.7)
        plt.imshow(sitk.GetArrayViewFromImage(img2), cmap=plt.cm.Greys_r, alpha=0)
        plt.savefig(os.path.join(out, (ig_name + s_img)), format='svg')
        plt.axis('off')
        #plt.show()
        #sitk.WriteImage(simg3, os.path.join(output_reg, (out_name + '.png')))


def rigid_registration(df_it, input_fixed,output_reg):
    for index, row in df_it.iterrows():
        path_fixed = os.path.join(input_fixed, row['fixed_name'])
        path_moving = os.path.join(output_reg, row['path'])
        fixed = sitk.ReadImage(path_fixed, sitk.sitkFloat32)
        mask = sitk.OtsuThreshold(fixed,1,0)
        fixed = sitk.N4BiasFieldCorrection(sitk.Cast(fixed,sitk.sitkFloat32), mask)

        moving = sitk.ReadImage(path_moving, sitk.sitkFloat32)
        mask = sitk.OtsuThreshold(moving,1,0)
        moving=sitk.N4BiasFieldCorrection(sitk.Cast(moving, sitk.sitkFloat32), mask)

        name_output_reg = row['out_name']
        status = True  # if all the plots should be outputed or not
        initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                              moving,
                                                              sitk.Similarity2DTransform(),
                                                              sitk.CenteredTransformInitializerFilter.GEOMETRY)
        # Test to see how they look as overlay
        moving_resampled = sitk.Resample(moving, fixed,
                                         initial_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
        # plot_reg(status, moving_resampled, fixed, output_reg, name_output_reg, 'NotRegistred.svg')

        registration_method = sitk.ImageRegistrationMethod()

        # Similarity metric settings.
        registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
        registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
        registration_method.SetMetricSamplingPercentage(0.01)
        registration_method.SetInterpolator(sitk.sitkLinear)

        # Optimizer settings.
        registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                          numberOfIterations=1000,
                                                          convergenceMinimumValue=1e-6,
                                                          convergenceWindowSize=500)
        registration_method.SetOptimizerScalesFromPhysicalShift()

        # Setup for the multi-resolution framework.
        # registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[6, 2, 1])
        # registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
        # registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

        # Don't optimize in-place, we would possibly like to run this cell multiple times.
        registration_method.SetInitialTransform(initial_transform, inPlace=False)

        rigid_transformation = registration_method.Execute(fixed, moving)
        print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
        print('Optimizer\'s stopping condition, {0}'.format(
            registration_method.GetOptimizerStopConditionDescription()))

        moving_rigid = sitk.Resample(moving, fixed, rigid_transformation, sitk.sitkLinear, 0.0, moving.GetPixelID())
        plot_reg(status, moving_rigid, fixed, output_reg, name_output_reg, 'regRigid.svg')

        mm = sitk.Cast(moving_rigid, sitk.sitkUInt8)
        sitk.WriteImage(mm, os.path.join(input_fixed, (name_output_reg + '.png')))


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN'

input_images = os.path.join(root_dir, 'MovingImages')
input_fixed_imgp = os.path.join(root_dir, 'FixedImagesP')
input_fixed_imgn = os.path.join(root_dir, 'FixedImagesN')

image_list = glob.glob(os.path.join(input_images, '*.png'))

output_reg = os.path.join(root_dir, 'RegistrationResults')
if not os.path.exists(output_reg):
    os.mkdir(output_reg)



reference_image_name = 'Merida_DS-64_Filter-Mean-5_Brain_035.png'
reference_image_slide = reference_image_name.split('.')[0].split('_')[-1]

df_images = create_info_dfs(image_list, ['monkey_name', 'ds', 'filter', 'type', 'slide'])

df_images['slide_nr'] = df_images['slide'].astype(int)


df_images = df_images.set_index('slide').sort_index()

df_images['reg_order'] = df_images['slide_nr'].astype(int) - int(reference_image_slide)

df_images.loc[df_images['reg_order'] < 0, 'slide_nr_for_reg'] = df_images['slide_nr']+1
df_images.loc[df_images['reg_order'] > 0, 'slide_nr_for_reg'] = df_images['slide_nr']-1
df_images.loc[df_images['reg_order'] == 0, 'slide_nr_for_reg'] = 0

df_images['fixed_name'] = df_images['monkey_name']+'_Brain_'+df_images['slide_nr_for_reg'].astype(int).astype(str)+'_RigidRegistered.png'
df_images.loc[df_images['reg_order'] == 1, 'fixed_name']= reference_image_name
df_images.loc[df_images['reg_order'] == -1, 'fixed_name']= reference_image_name

df_images['out_name']=df_images['monkey_name']+'_Brain_'+df_images['slide_nr'].astype(str)+'_RigidRegistered'

df_p = df_images[df_images['reg_order'] > 0]
df_p = df_p.sort_values(by='reg_order')

df_n = df_images[df_images['reg_order'] < 0]
df_n = df_n.sort_values(by='reg_order',ascending=False)


rigid_registration(df_p, input_fixed_imgp, output_reg)
rigid_registration(df_n, input_fixed_imgn, output_reg)




