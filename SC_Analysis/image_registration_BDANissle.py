import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd
import skimage.io as io
import skimage as sk


def plot_reg(status, img1, img2, out, ig_name, s_img):
    if status:
        fig = plt.figure()
        plt.imshow(sitk.GetArrayViewFromImage(img1), cmap=plt.cm.Greys_r, alpha=0.7)
        plt.imshow(sitk.GetArrayViewFromImage(img2), cmap=plt.cm.Greys_r, alpha=0.5)
        plt.savefig(os.path.join(out, (ig_name + s_img)), format='svg')
        plt.axis('off')
        plt.show()
        #sitk.WriteImage(simg3, os.path.join(output_reg, (out_name + '.png')))


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/MonkeyDataSimon/Test'


input_images = os.path.join(root_dir)
image_list = glob.glob(os.path.join(input_images, '*.jpg'))

output_reg = os.path.join(root_dir)
if not os.path.exists(output_reg):
    os.mkdir(output_reg)

#moving = io.imread(image_list[20])
#fixed = io.imread(image_list[37])

nissle_img = io.imread(os.path.join(root_dir,'Nissle_s64.jpg'))[:,:,1]
nissle_img = nissle_img.astype('float32')
fixed = sitk.GetImageFromArray(nissle_img)
'''
bdaimg=io.imread(os.path.join(root_dir,'BDA_s64.jpg'))[:,:,1]
plt.imshow(bdaimg,cmap='gray')
plt.show()
 '''

bda_img = io.imread(os.path.join(root_dir,'BDA_s64.jpg'))[:,:,1]
bda_img = bda_img.astype('float32')
moving = sitk.GetImageFromArray(bda_img)

# Apply initial alignment

initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                      moving,
                                                      sitk.Similarity2DTransform(),
                                                      sitk.CenteredTransformInitializerFilter.GEOMETRY)
#Test to see how they look as overlay
moving_resampled = sitk.Resample(moving, fixed,
                                 initial_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(moving_resampled), cmap=plt.cm.Greys_r, alpha=0.5)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.3)
#plt.savefig(os.path.join(OUTPUT_DIR,(imageName + 'NotRegistred.svg')), format='svg')
plt.axis('off')
plt.show()

registration_method = sitk.ImageRegistrationMethod()

# Similarity metric settings.
registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
registration_method.SetMetricSamplingPercentage(0.01)
registration_method.SetInterpolator(sitk.sitkLinear)

# Optimizer settings.
registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                  numberOfIterations=500,
                                                  convergenceMinimumValue=1e-6,
                                                  convergenceWindowSize=500)
registration_method.SetOptimizerScalesFromPhysicalShift()

# Setup for the multi-resolution framework.
registration_method.SetShrinkFactorsPerLevel(shrinkFactors =[6, 2, 1])
registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

# Don't optimize in-place, we would possibly like to run this cell multiple times.
registration_method.SetInitialTransform(initial_transform, inPlace=False)

rigidTransformation = registration_method.Execute(fixed, moving)
print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
print('Optimizer\'s stopping condition, {0}'.format(registration_method.GetOptimizerStopConditionDescription()))

movingRigid = sitk.Resample(moving, fixed, rigidTransformation, sitk.sitkLinear, 0.0, moving.GetPixelID())
fig = plt.figure()
plt.imshow(sitk.GetArrayViewFromImage(movingRigid), cmap=plt.cm.Greys_r, alpha=0.3)
plt.imshow(sitk.GetArrayViewFromImage(fixed), cmap=plt.cm.Greys_r, alpha=0.5)
#plt.savefig(os.path.join(OUTPUT_DIR,(imageName + 'regRigid.svg')), format='svg')
plt.axis('off')
plt.show()



