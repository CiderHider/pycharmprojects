import SimpleITK as sitk
import sys
import os
import matplotlib.pyplot as plt
import glob
import numpy as np
import pandas as pd


def display_sidebyside(fixed_npa, moving_npa):
    fig = plt.figure()
    plt.subplots(1, 2, figsize=(10, 8))

    # Draw the fixed image in the first subplot.
    plt.subplot(1, 2, 1)
    plt.imshow(fixed_npa, cmap=plt.cm.Greys_r)
    plt.title('fixed image')
    plt.axis('off')

    # Draw the moving image in the second subplot.
    plt.subplot(1, 2, 2)
    plt.imshow(moving_npa, cmap=plt.cm.Greys_r)
    plt.title('moving image')
    plt.axis('off')
    #plt.show()


def plot_reg(status, img1, img2, out, ig_name, s_img):
    if status:
        fig = plt.figure()
        plt.imshow(sitk.GetArrayViewFromImage(img1), cmap=plt.cm.Greys_r, alpha=0.5)
        plt.imshow(sitk.GetArrayViewFromImage(img2), cmap=plt.cm.Greys_r, alpha=0.3)
        plt.savefig(os.path.join(out, (ig_name + s_img)), format='svg')
        plt.axis('off')
        #plt.show()


def reg_function(fixed, moving, output_reg, image_name):
    """
        Parameters
        ----------
        fixed : ATLAS
            Numpy array containing image data

        moving : ANNOTATION IMAGES
            Numpy array containing 8 bit integer masks

        output_reg: PATH to OUTPUT FOLDER
            Str contains the path to the output folder

        image_name :
            Str contains the name for the outputed result

        -----------------
        Output
        ------
        rigid_transformation: RIGID TRANSFORMATION
            Transform

        bSplineTransformation: B-SPLINE TRANSFORMATION
            Transform

        -----------------
        Function to calculate the registration between the fixed and the moving images.
        1) First create an initial rigid transformation
        2) Get the rigid registred moving image (RMI)
        3) Create a bspline transformation
        4) Register the resulted RMI to the fix image using a bspline transformation
        5) Export the regid and b-spline transformation
    """
    # Apply initial alignment
    status = True # if all the plots should be outputed or not
    initial_transform = sitk.CenteredTransformInitializer(fixed,
                                                          moving,
                                                          sitk.Similarity2DTransform(),
                                                          sitk.CenteredTransformInitializerFilter.GEOMETRY)
    # Test to see how they look as overlay
    moving_resampled = sitk.Resample(moving, fixed,
                                     initial_transform, sitk.sitkLinear, 0.0, moving.GetPixelID())
    plot_reg(status, moving_resampled, fixed, output_reg, image_name, 'NotRegistred.svg')

    registration_method = sitk.ImageRegistrationMethod()

    # Similarity metric settings.
    registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=200)
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.01)
    registration_method.SetInterpolator(sitk.sitkLinear)

    # Optimizer settings.
    registration_method.SetOptimizerAsGradientDescent(learningRate=0.5,
                                                      numberOfIterations=500,
                                                      convergenceMinimumValue=1e-6,
                                                      convergenceWindowSize=500)
    registration_method.SetOptimizerScalesFromPhysicalShift()

    # Setup for the multi-resolution framework.
    registration_method.SetShrinkFactorsPerLevel(shrinkFactors=[6, 2, 1])
    registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[6, 2, 1])
    registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()

    # Don't optimize in-place, we would possibly like to run this cell multiple times.
    registration_method.SetInitialTransform(initial_transform, inPlace=False)

    rigid_transformation = registration_method.Execute(fixed, moving)
    print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
    print('Optimizer\'s stopping condition, {0}'.format(
        registration_method.GetOptimizerStopConditionDescription()))

    moving_rigid = sitk.Resample(moving, fixed, rigid_transformation, sitk.sitkLinear, 0.0, moving.GetPixelID())
    plot_reg(status, moving_rigid, fixed, output_reg, image_name, 'regRigid.svg')

    # BSPLINE
    transformDomainMeshSize = [2] * fixed.GetDimension()
    tx = sitk.BSplineTransformInitializer(fixed, transformDomainMeshSize)

    print(f"Initial Number of Parameters: {tx.GetNumberOfParameters()}")

    R = sitk.ImageRegistrationMethod()
    R.SetMetricAsJointHistogramMutualInformation()

    R.SetOptimizerAsGradientDescentLineSearch(learningRate=5.0,
                                              numberOfIterations=100,
                                              convergenceMinimumValue=1e-4,
                                              convergenceWindowSize=5)

    R.SetInterpolator(sitk.sitkLinear)

    R.SetInitialTransformAsBSpline(tx, inPlace=True, scaleFactors=[1, 2, 5])
    R.SetShrinkFactorsPerLevel([4, 2, 1])
    R.SetSmoothingSigmasPerLevel([4, 2, 1])

    bSplineTransformation = R.Execute(fixed, moving_rigid)

    print("-------")
    print(bSplineTransformation)
    print(f"Optimizer stop condition: {R.GetOptimizerStopConditionDescription()}")
    print(f" Iteration: {R.GetOptimizerIteration()}")
    print(f" Metric value: {R.GetMetricValue()}")

    movingRegBspline = sitk.Resample(moving_rigid, fixed, bSplineTransformation, sitk.sitkLinear, 0.0,
                                     moving_resampled.GetPixelID())
    plot_reg(status, movingRegBspline, fixed, output_reg, image_name, 'RegBspline.svg')

    return rigid_transformation, bSplineTransformation


def transformation_function(fixed_img, rigid_t, bspl_t, det_i, output_reg, out_name):
    """
            Parameters
            ----------
            fixed_img : ATLAS
                Numpy array containing image data

            rigid_t: RIGID TRANSFORMATION
                Transform

            bspl_t: B-SPLINE TRANSFORMATION
                Transform

            det_i: DETECTION IMAGES

            output_reg: PATH to OUTPUT FOLDER
                Str contains the path to the output folder

            out_name :
                Str contains the name for the outputed result

            -----------------
            Function to calculate the registration between the fixed and the moving images.
            1) Apply the rigid transformation on the detection images
            2) Apply the bspline transformation on the rigid transformed detection images
        """

    # Apply transformation to a different image
    resamplerRigid = sitk.ResampleImageFilter()
    resamplerRigid.SetReferenceImage(fixed_img)
    resamplerRigid.SetInterpolator(sitk.sitkLinear)
    resamplerRigid.SetDefaultPixelValue(50)
    resamplerRigid.SetTransform(rigid_t)

    outRigidApplied = resamplerRigid.Execute(det_i)

    resamplerBSpline = sitk.ResampleImageFilter()
    resamplerBSpline.SetReferenceImage(fixed_img)
    resamplerBSpline.SetInterpolator(sitk.sitkLinear)
    resamplerBSpline.SetDefaultPixelValue(50)
    resamplerBSpline.SetTransform(bspl_t)

    outBSplineApplied = resamplerBSpline.Execute(outRigidApplied)

    simg1 = sitk.Cast(sitk.RescaleIntensity(fixed_img), sitk.sitkUInt8)
    simg2 = sitk.Cast(sitk.RescaleIntensity(outRigidApplied), sitk.sitkUInt8)
    simg3 = sitk.Cast(sitk.RescaleIntensity(outBSplineApplied), sitk.sitkUInt8)

    fig = plt.figure()
    plt.imshow(sitk.GetArrayViewFromImage(simg1), cmap=plt.cm.Greys_r, alpha=0.3)
    plt.imshow(sitk.GetArrayViewFromImage(simg2), cmap=plt.cm.Greys_r, alpha=0.5)
    plt.imshow(sitk.GetArrayViewFromImage(simg3), cmap=plt.cm.Greys_r, alpha=0.7)
    plt.axis('off')
    plt.savefig(os.path.join(output_reg, (out_name + 'detections.svg')), format='svg')
    #plt.show()
    sitk.WriteImage(simg3, os.path.join(output_reg, (out_name + '.png')))

    # del initial_transform, moving_resampled, registration_method, rigidTransformation, movingRigid
    # del R, tx, bSplineTransformation, movingRegBspline
    # del resamplerRigid, resamplerBSpline, outRigidApplied, outBSplineApplied, simg3


'''
SKRIPT
'''

root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

regInfo = pd.read_csv(os.path.join(os.path.join(root_dir, 'ReferenceFiles'), 'RegInfo.csv'))

inputMoving = os.path.join(root_dir, 'AnnotationsProcessedReg')
outputReg = os.path.join(root_dir, 'RegistrationResults')
if not os.path.exists(outputReg):
    os.mkdir(outputReg)

if 'atlas_path' not in regInfo:
    inputDetections = os.path.join(root_dir, 'DetectionsProcessedReg')
    input_Atlas = os.path.join(root_dir, 'SC_AtlasPadded')
    regInfo['atlas_path'] = input_Atlas + os.sep + regInfo["segment"] + ".png"
    regInfo['moving_path'] = inputMoving + os.sep + regInfo["ann_name"]
    regInfo['detreg_path'] = inputDetections + os.sep + regInfo["det_name"]


moving_list = glob.glob(os.path.join(inputMoving, '*.png'))
# take only specific animal
# to be implemented ...
# moving_list = glob.glob(os.path.join(inputMoving, 'Merida*.png'))
for itr in moving_list:
    print("in loop")
    file_path, file_name = os.path.split(itr)

    annotations_list = regInfo.index[regInfo['ann_name'] == file_name].tolist()

    if not annotations_list:
        continue
    else:
        exIdx = annotations_list[0]

    segmentName = regInfo.loc[exIdx]['segment']

    fixedImage = sitk.ReadImage(regInfo.loc[exIdx]['atlas_path'], sitk.sitkFloat32)
    movingImage = sitk.ReadImage(regInfo.loc[exIdx]['moving_path'], sitk.sitkFloat32)

    parts = file_name.split('.')
    imageName = parts[0]

    rigidT, bSplT = reg_function(fixedImage, movingImage, outputReg, imageName)

    index_detections = regInfo.index[regInfo['ann_name'] == file_name].tolist()
    for ii in index_detections:
        imgDetName = regInfo.loc[ii]['det_name'].split('.')[0]
        detectionsImage = sitk.ReadImage(regInfo.loc[ii]['detreg_path'], sitk.sitkFloat32)
        transformation_function(fixedImage, rigidT, bSplT, detectionsImage, outputReg, imgDetName)
