import sklearn.datasets
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob


def clac_lesion_vol(small_df):
    lesion_vol = small_df['Area µm^2'].sum() * (small_df['distance']*small_df['space']) / np.power(10, 9)
    return lesion_vol


root_dir = '/Users/muscalu/Dropbox (Privat)/Projekt_ANCE'

ref_df =pd.read_csv(os.path.join(root_dir, 'ReferenceFile.csv'), sep=',')

file_list = glob.glob(os.path.join(os.path.join(root_dir, 'MouseStrokeData'), '*.csv'))

df_annotations = pd.DataFrame()
for iter, row in enumerate(file_list):
    file_path, file_name = os.path.split(row)
    df_temp = pd.read_csv(row, sep=',')
    df_temp['mouse_name'] = file_name.split('_')[0]

    df_annotations = df_annotations.append(df_temp)

df_annotations = df_annotations[['Image','mouse_name','Area µm^2']]

df_annotations = df_annotations.join(ref_df.set_index('name'), on='mouse_name').sort_values('Image')


df_all = df_annotations.groupby(['mouse_name']).apply(lambda x: clac_lesion_vol(x)).to_frame().reset_index().rename(columns={0:"volume mm^3"})
'''
#lesion_volume1 = df_all.groupby(['mouse_name'])['Area µm^2'].apply(lambda x: x*(40*4)/10**9)
#lesion_volume2 = df_all.groupby(['mouse_name'])['Area µm^2'].sum() * 300 / np.power(10, 9)
#lesion_volume = lesion_volume.to_frame().reset_index().rename(columns={"Area µm^2":"Volume mm^3"})

'''
sns.set_theme(style="whitegrid")
g = sns.catplot(
    data=df_all, kind="bar",
    x="mouse_name", y="volume mm^3",
    palette="magma", alpha=.9
)
g.despine(left=True)
g.set_axis_labels("", "volume mm^3")
plt.title('Lesion Volume', fontsize=30)
plt.xlabel(None)
plt.xticks(fontsize=20)
plt.ylabel("Volume mm^3" , fontsize=20)
plt.yticks(fontsize=15)
sns.despine(bottom=True)
ax = plt.gca()
box = ax.get_position()
ax.set_position([box.x0*1.5, box.y0, box.width, box.height*0.9])
plt.grid(False)
#
plt.savefig("lesion_Volume.svg", format="svg")

plt.show()