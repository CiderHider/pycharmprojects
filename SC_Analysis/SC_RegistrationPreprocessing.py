import os
import glob
import pandas as pd
import SimpleITK as sitk
from scipy import ndimage
import numpy as np
from PIL import Image


def check_file_path(data_path):
    import tkinter as tk
    from tkinter import filedialog

    if not os.path.exists(data_path):
        data_path = os.path.expanduser("~")
        root = tk.Tk()
        root.withdraw()
        data_path = filedialog.askdirectory(title="Chose data directory with images to analyze", initialdir=data_path)
    return data_path


def max_dimentions(img_list):
    height_vector = np.zeros(len(img_list)).astype(int)
    width_vector = np.zeros(len(img_list)).astype(int)

    counter = 0
    for ii in img_list:
        width_vector[counter], height_vector[counter] = Image.open(ii).size
        counter += 1
    max_height = height_vector.max() + 500
    max_width = width_vector.max() + 500
    return max_height, max_width


def create_info_dfs(img_list, col_name):
    df = pd.DataFrame(img_list, columns=['path'])
    df['image_name'] = df['path'].apply(lambda x: os.path.basename(x))
    df[col_name] = df['image_name'].apply(lambda x: pd.Series(str(x).split("_")))
    df.drop('rest', axis=1, inplace=True)
    return df


def flip_and_bw_transform(in_img, flip_status, image_type):
    if flip_status == 'flip':
        print('inflip')
        in_img = sitk.Flip(in_img, [True, False, False])  # cv2.flip(inputImage, 1)

    bw_image = sitk.GetArrayFromImage(sitk.VectorMagnitude(in_img))
    if image_type == 'Annotations':
        bw_image[bw_image == bw_image.max()] = 255
        bw_image[(bw_image > 0) & (bw_image < 255)] = 70
    else:
        bw_image[bw_image > 0] = 255
    return bw_image


def padding_func(input_image, max_height, max_width, ht, wt, cm_h, cm_w):
    padding_matrix = np.zeros((maxHeight, maxWidth))
    yoff = round(max_height / 2 - cm_h)
    xoff = round(max_width / 2 - cm_w)
    resulted_image = padding_matrix.copy()
    resulted_image[yoff:yoff + ht, xoff:xoff + wt] = input_image
    output_image = resulted_image.astype(np.uint8)
    return output_image


def set_folder_path(image_type):
    input_dir = os.path.join(root_dir, str(image_type + 'ToBeProcessed'))
    output_dir = os.path.join(root_dir, str(image_type + 'ProcessedReg'))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    image_list = glob.glob(os.path.join(input_dir, '*.png'))
    return image_list, output_dir


def preprocess_annotations(folder_type, df_info, max_height, max_width):
    df_info['cm_imgH'] = np.nan
    df_info['cm_imgW'] = np.nan
    image_list, out_d = set_folder_path(folder_type)
    for image_path in image_list:
        inputImage = sitk.ReadImage(image_path)
        file_path, file_name = os.path.split(image_path)
        exIdx = df_info.index[(df_info['ann_name'] == file_name)].tolist().pop()
        flipStatus = df_info.loc[exIdx]['transformation']
        status = df_info.loc[exIdx]['keep']
        if status:
            # Convert the image to black and white and flip them
            bw_image = flip_and_bw_transform(inputImage, flipStatus, folder_type)  # Flip image in the function
            h, w = bw_image.shape
            cmImgH, cmImgW = ndimage.measurements.center_of_mass(bw_image)
            df_info.loc[exIdx, 'cm_imgH'] = cmImgH
            df_info.loc[exIdx, 'cm_imgW'] = cmImgW
            padded_image = padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)
            out = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(padded_image)), sitk.sitkUInt8)
            sitk.WriteImage(out, os.path.join(out_d, file_name))

    return df_info


def preprocess_detections(folder_type, df_info, max_height, max_width):
    image_list, out_d = set_folder_path(folder_type)
    for image_path in image_list:
        inputImage = sitk.ReadImage(image_path)
        file_path, file_name = os.path.split(image_path)
        exIdx = df_info.index[(df_info['det_name'] == file_name)].tolist().pop()
        flipStatus = df_info.loc[exIdx]['transformation']
        status = df_info.loc[exIdx]['keep']
        if status:
            bw_image = flip_and_bw_transform(inputImage, flipStatus, folder_type)  # Flip image in the function
            h, w = bw_image.shape
            cmImgH = df_info.loc[exIdx, 'cm_imgH']
            cmImgW = df_info.loc[exIdx, 'cm_imgW']
            padded_image = padding_func(bw_image, max_height, max_width, h, w, cmImgH, cmImgW)

            out = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(padded_image)), sitk.sitkUInt8)
            sitk.WriteImage(out, os.path.join(out_d, file_name))

    return df_info


"""
SKRIPT
"""

# root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

root_dir = r'F:\Dropbox (Personal)\IC_Stroke_WorkingDirectory\HistoProcessedData\SC'
root_dir = check_file_path(root_dir)

# Load the .csv file with information about the segments
csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
segInfo = pd.read_csv(os.path.join(csvFilePath, 'SegmentsAll.csv'), sep=';')

folder_list = ['Annotations', 'Detections']

# Calculate the max hight and width over all images
image_list_forDim = glob.glob(os.path.join(os.path.join(root_dir, 'AnnotationsToBeProcessed'), '*.png'))
maxHeight, maxWidth = max_dimentions(image_list_forDim)

# Calculate dataframe for the annotation images
image_listA, output_dirA = set_folder_path('Annotations')
ann_df = create_info_dfs(image_listA, ['monkey', 'slide', 'rest'])
ann_df.rename(columns={'path': 'annotations_path', 'image_name': 'ann_name'}, inplace=True)
ann_df = pd.merge(ann_df, segInfo, on=["slide", "monkey"])

# Calculate dataframe for the detection images
image_listD, output_dirD = set_folder_path('Detections')
det_df = create_info_dfs(image_listD, ['monkey', 'slide', 'region', 'wmgm', 'rest'])
det_df.rename(columns={'path': 'det_path', 'image_name': 'det_name'}, inplace=True)

# Preprocess the annotations and the detections: black and white, flip and pad
ann_df = preprocess_annotations("Annotations", ann_df, maxHeight, maxWidth)
det_df = pd.merge(ann_df, det_df, on=["slide", "monkey"])

info_df = preprocess_detections("Detections", det_df, maxHeight, maxWidth)

# Save everything in a .csv file
info_df.to_csv(os.path.join(csvFilePath, 'RegInfo.csv'), index=False)
