import sklearn.datasets
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
import sc_functions as fun


def def_colormap5():
    base_5 = ['#372367',
              '#19D3C5',
              '#FA525B',
              '#969899',
              ]
    colormap = np.array(base_5)
    return colormap


def get_df_info(df_input):
    df_input['file_name'] = df_input['file_path'].split('/')[-1].split('.')[0]
    df_input['monkey'] = df_input['file_name'].split('_')[0]
    return df_input


def calculate_ic_stroke(df_input):
    dd = df_input[['region', 'volume']].set_index('region').transpose()
    stroke_total = dd['Stroke'].to_numpy()[0]
    dd['ICStroke'] = dd['Stroke'] - dd[dd.columns.difference(["Stroke"])].sum(axis=1)
    dd['Pallidum'] = dd['IGPStroke']+dd['EGPStroke']
    dd['Thalamus'] = dd['ThStroke']
    dd['Striatum'] = dd['Stroke']-dd['ICStroke']-dd['Pallidum']- dd['Thalamus']
    #dd['other'] = dd[
    #    dd.columns.difference(["Stroke", 'IGPStroke', 'EGPStroke', 'ThStroke', 'ICStroke'])].sum(axis=1)
    dd.drop(dd.columns.difference(['ICStroke', 'Thalamus', 'Pallidum', 'Striatum']), 1, inplace=True)
    dd['stroke_total'] = stroke_total
    return dd


def plot_stroke_in_other_structures(df_input, to_plot):
    if to_plot:
        df_stroke_pie = df_input[df_input.columns.difference(['stroke_total'])].set_index('monkey')
        df_stroke_pie = df_stroke_pie.reindex(sorted(df_stroke_pie.columns), axis=1).reset_index()
        # Plot the lesion percentage in other structures in form of pie charts
        labels = df_stroke_pie.columns[1:]
        for ind in df_stroke_pie.index:
            # df_stroke.iloc[ind, 1:].plot(kind='pie',colors=def_colormap5, autopct='%1.1f%%')
            plt.pie(df_stroke_pie.iloc[ind, 1:], labels=labels, colors=def_colormap5())
            plt.savefig(os.path.join(root_dir, 'Figures', (df_stroke_pie.iloc[ind, 0] + 'plot.svg')), dpi=100)
            plt.show()


def percentage_growth(l):
    s = 0
    res = [0]
    for i in range(len(l) - 1):
        s += l[i]
        res.append(s / sum(l))
    return res


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/LESION/'
csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
monkey_reference = pd.read_csv(os.path.join(csvFilePath, 'monkey_ref_sheet.csv'), sep=';')

plot_piechart = True

file_list = glob.glob(os.path.join(os.path.join(root_dir, 'Files_Stroke'), '*.csv'))

df = pd.DataFrame(file_list, columns=['file_path'])
df = df.apply(lambda x: get_df_info(x), axis=1)

df_stroke = pd.DataFrame()
for iter, row in enumerate(df.itertuples()):
    df_temp = pd.read_csv(row.file_path, sep=';')
    df_temp.drop(df_temp.columns.difference(['Contour Name', 'Area (µm²)']), 1, inplace=True)

    df_temp = df_temp.groupby('Contour Name')['Area (µm²)'].apply(
        lambda x: x.sum() * 300 / np.power(10, 9)).reset_index()
    df_temp['file_name'] = row.file_name
    df_temp['monkey'] = row.monkey
    monkey = row.monkey
    df_stroke = df_stroke.append(df_temp)

df_stroke = df_stroke.rename(columns={'Contour Name': 'region', 'Area (µm²)': 'volume'})

df_stroke = df_stroke.groupby('monkey').apply(lambda x: calculate_ic_stroke(x)).reset_index()
df_stroke.columns = df_stroke.columns.str.rstrip('Stroke')
df_stroke.drop(columns='level_1', inplace=True)

# Plot the stroke percentage as pie chart
plot_stroke_in_other_structures(df_stroke, plot_piechart)

# Prepare df for plotting the other measurements
df_stroke = df_stroke.set_index('monkey').join(monkey_reference.set_index('monkey'), on=['monkey']).reset_index()

print('Display model precision')
df_all = df_stroke.copy()
df_all = pd.melt(df_all, id_vars=['monkey', 'severity', 'stroke_total'], value_vars=['IC', 'Thalamus', 'Pallidum', 'Striatum'],
                 ignore_index=False)
df_all.rename(columns={'variable': 'region'}, inplace=True)
colormap = fun.define_colormap('Lesion')
df_all['value_percentage'] = np.divide(df_all['value'], df_all['stroke_total']) * 100

ax = sns.barplot(data=df_all, x='region', y='value_percentage', ci=None, color=(192 / 255, 192 / 255, 192 / 255, 1.0))
ax = sns.swarmplot(x='region', y='value_percentage', data=df_all, color=".25",
                   size=8, hue='severity', hue_order=['Mild', 'Moderate', 'Severe'], palette=colormap)
ax.set_yticks(np.linspace(0, 100, num=3))
ax.tick_params(labelsize=15)
ax.set(title='Model precision')
plt.savefig(os.path.join(root_dir, 'Figures', 'ModelPrecision.svg'), dpi=100)
plt.show()

print('Display plot of each lesion volume')
# Bar plot of each lesion volume
h = sns.barplot(data=df_stroke.sort_values('stroke_total'), x='short_name', y='stroke_total',
                palette=colormap, hue='severity')
h.set_yticks(np.linspace(0, 100, num=3))
h.tick_params(labelsize=10)
h.set(title='Lesion Volume')
plt.savefig(os.path.join(root_dir, 'Figures', 'LesionVolume.svg'), dpi=100)
plt.show()

print('Display plot of lesion volume per groupe')
# Bar plot of lesion volume per group
bx = sns.barplot(data=df_stroke.sort_values('severity'), x='severity', y='stroke_total', ci=None, palette=colormap)
bx = sns.swarmplot(data=df_stroke.sort_values('severity'), x='severity', y='stroke_total',
                   color=(192 / 255, 192 / 255, 192 / 255, 1.0), size=12)
bx.set_yticks(np.linspace(0, 100, num=3))
bx.tick_params(labelsize=15)
bx.set(title='Lesion Volume')
plt.savefig(os.path.join(root_dir, 'Figures', 'LesionVolumeShort.svg'), dpi=100)
plt.show()

'''
# https://github.com/nanorobocop/Nested-Pie-Chart/blob/master/nested_pie_chart.ipynb
ar = pd.melt(df_stroke, id_vars=['monkey'],
             value_vars=['IC', 'Pallidum','Thalamus', 'Striatum'], value_name='region_stroke', var_name='regions')

'''
# Create the sunburst diagram for all the lesion levels
df_stroke = df_stroke.sort_values(by=['severity', 'stroke_total'])
part_df_stroke = df_stroke.iloc[:, :5]
aa = df_stroke[['monkey', 'IC', 'Thalamus', 'Pallidum', 'Striatum']]
layers = ['Rey', 'Padme', 'Jyn', 'Leia', 'HH05', 'HH11', 'HH06', 'HH03']

plt.axis("equal")
# figure(figsize=(8, 6), dpi=80)
for i, layer in enumerate(layers):
    radius = i + 2
    width = 0.8
    frame = aa.loc[aa['monkey'] == layer]
    frame = frame.drop(columns=['monkey']).to_numpy()
    colors = def_colormap5()  # cmap(percentage_growth(frame))
    #labels = ['IC', 'IGP', 'Th', 'oth']
    plt.pie(frame.reshape(-1), colors=colors, radius=radius, wedgeprops=dict(width=width, edgecolor='w'),
            labeldistance=0.8)

plt.savefig(os.path.join(root_dir, 'Figures', 'NestedPieChart.svg'))

plt.show()
