import os
import glob
import skimage.io as io
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import SimpleITK as sitk
import skimage.io as io


root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
# root_path = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

regInfo = pd.read_csv(os.path.join(os.path.join(root_path, 'ReferenceFiles'), 'AntiRegInfo.csv'), sep=',')

outputFigures = os.path.join(root_path, 'Figures', 'RegistrationAnti')
if not os.path.exists(outputFigures):
    os.mkdir(outputFigures)

file_list = glob.glob(os.path.join(os.path.join(root_path, 'RegistrationResultsAnti'), '*.png'))
df = pd.DataFrame(columns=['det_name', 'det_path_l', 'reg_result'])

for num, image in enumerate(file_list):
    file_path, file_name = os.path.split(image)
    dataImage = io.imread(image)
    # threshold
    threshold = 10
    dataImage = np.where(dataImage > threshold, dataImage, 0)
    df_temp = pd.DataFrame(index=[num], data={'det_name': file_name, 'det_path_l': image,
                                              'reg_result': [dataImage]})  # columns=df.columns)#,
    # df_temp['det_name'] = file_name
    # df_temp['reg_result'] = [dataImage]
    df = pd.concat([df, df_temp], axis=0)

# df['i_r']=df['det_path_l'].apply(lambda x: io.imread(x))

# result_df = pd.merge(regInfo, df, on="det_name")
result_df = df.set_index('det_name').join(regInfo.set_index('ann_name'))

# Calculate sum pro segment pro monkey pro region
# Define groups for segment wise calculation
grouping = ['monkey', 'segment', 'color']
# Concatenate passed series to 3d array and sum along concatenation axis
cs_df = result_df.groupby(grouping)['reg_result'].apply(lambda x: x.to_numpy().sum(axis=0)).reset_index()
'''
plt.imshow(cs_df.loc[1,['reg_result']][0], cmap="hot")
plt.show()
'''
# Add smoothing filter for the resulted sums
cs_df['smooth_result'] = cs_df['reg_result'].apply(lambda x: gaussian_filter(x, sigma=50))

'''
plt.imshow(aa_cut, cmap="hot")
plt.show()
'''

# Calculate parameters for plotting: max value for the scaling and color for the heatmap
cs_df['max_res'] = cs_df['smooth_result'].apply(lambda x: x.max())
cs_df['plot_color'] = "hot"

# Add atlas layout to the dataframe
atlasPath = os.path.join(root_path, 'SC_AtlasMasks')
cs_df['atlas_path'] = atlasPath + os.sep + "M_" + cs_df["segment"] + ".png"
cs_df['atlas_image'] = cs_df['atlas_path'].apply(
    lambda x: sitk.GetArrayViewFromImage(sitk.ReadImage(x, sitk.sitkUInt16)))
cs_df['output_name'] = cs_df[['monkey', 'segment', 'color']].agg('_'.join, axis=1)

ed = 500
for index, row in cs_df.iterrows():
    aa = cs_df.loc[index, ['smooth_result']][0]
    aa_cut = aa[ed:aa.shape[0] - ed, ed:aa.shape[1] - ed]
    # atl = cs_df.loc[index, ['atlas_image']][0]
    atl = sitk.GetArrayViewFromImage(sitk.ReadImage(cs_df.loc[index, ['atlas_path']][0], sitk.sitkUInt16))
    atl_cut = atl[ed:atl.shape[0] - ed, ed:atl.shape[1] - ed]
    plt.imshow(aa_cut, cmap=cs_df.loc[index, ['plot_color']][0])
    plt.imshow(atl_cut, cmap="gray", alpha=0.5)
    plt.axis('off')
    plt.savefig(os.path.join(outputFigures, (cs_df['output_name'][index] + '.svg')), format='svg')
