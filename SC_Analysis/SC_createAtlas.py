import os
import glob
import numpy as np
from skimage import io
import skimage.color
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage import feature
from skimage.morphology import (erosion, dilation, opening, closing)
from skimage.morphology import disk  # noqa
import skimage.io as io


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'
#root_dir = '/home/aaron/Dropbox/IC_Stroke_WorkingDirectory/HistoProcessedData/SC'

input_atlas = os.path.join(root_dir, 'SC_LaminaBothSides')
output_atlas = os.path.join(root_dir, 'SC_LaminaBothSidesPadded')
if not os.path.exists(output_atlas):
    os.makedirs(output_atlas)

input_dir = os.path.join(root_dir, 'SC_LaminaBothSidesPadded')
output_dir_masks = os.path.join(root_dir, 'SC_AtlasMasks')
if not os.path.exists(output_dir_masks):
    os.makedirs(output_dir_masks)

# Padd the atlas images
atlas_list = glob.glob(os.path.join(input_atlas, '*.png'))

maxWidth = 3986
maxHeight = 3125

for atlasImage in atlas_list:
    inputImage = skimage.util.img_as_ubyte(skimage.color.rgb2gray((io.imread(atlasImage))))
    file_path, file_name = os.path.split(atlasImage)
    segment = file_name.split('_')[0]
    h, w = inputImage.shape
    cmImgH, cmImgW = ndimage.measurements.center_of_mass(inputImage)

    paddingMatrix = np.zeros((maxHeight, maxWidth))

    yoff = round(maxHeight / 2 - cmImgH)
    xoff = round(maxWidth / 2 - cmImgW)

    resultedImage = paddingMatrix.copy()
    resultedImage[yoff:yoff + h, xoff:xoff + w] = inputImage
    resultedImage = resultedImage.astype(np.uint8)
    skimage.io.imsave(os.path.join(output_atlas, file_name), resultedImage, check_contrast=False)


# Calculate different types of masks for the atlas
file_list = glob.glob(os.path.join(input_dir, '*.png'))

for image in file_list:
    #image = file_list[1]
    inputImage = skimage.color.rgb2gray((io.imread(image)))
    file_path, file_name = os.path.split(image)

    inputImage[(inputImage > 0) & (inputImage < 255)] = 70
    #plt.imshow(inputImage,cmap='gray')
    #plt.show()
    skimage.io.imsave(os.path.join(output_dir_masks, file_name), inputImage, check_contrast=False)


    #Apply erosion to fill in the small holes between the laminas
    erodedImg = erosion(inputImage, disk(2))
    #skimage.io.imsave(os.path.join(output_dir_masks, ('cc'+file_name)), erodedImg, check_contrast=False)
    edges = feature.canny(erodedImg, sigma=3)
    #edges[edges*1 > 0] = 255
    edges = skimage.util.img_as_ubyte(edges).astype(np.uint8)
    dilated = dilation(edges, disk(3))
    #plt.imshow(edges, cmap="gray")
    #skimage.io.imsave(os.path.join(output_dir_masks, ('bk'+file_name)), edges, check_contrast=False)
    skimage.io.imsave(os.path.join(output_dir_masks, ('M_' + file_name)), dilated, check_contrast=False)

    maskAtlas = inputImage
    maskAtlas[maskAtlas == 255] = 0
    maskAtlas[maskAtlas != 0] = 255
    skimage.io.imsave(os.path.join(output_dir_masks, ('W_' + file_name)), maskAtlas, check_contrast=False)



