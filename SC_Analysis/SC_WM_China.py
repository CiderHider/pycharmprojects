import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import os
import glob
from numpy import loadtxt
import re

colorMild = (245 / 255, 183 / 255, 177 / 255, 1.0)
colorModerate = (169 / 255, 50 / 255, 38 / 255, 1.0)
colorHealthy = (98 / 255, 101 / 255, 103 / 255, 1.0)
colormap = np.array([colorHealthy, colorMild, colorModerate])


def get_df_info(df_input):
    df_input['file_name'] = df_input['file_path'].split('/')[-1].split('.')[0]
    df_input['monkey'] = df_input['file_name'].split('_')[0]
    df_input['file_nr'] = df_input['file_name'].split('_')[1]
    return df_input


def search_minmaxval(x, val):
    if x == 'M1':
        out = val[0]
    elif x == 'PMd':
        out = val[1]
    elif x == 'PMv':
        out = val[2]
    return out


def plot_func_wm(df_data, datatype, output_folder, to_plot,lim_plot=0.5):
    max_nr = 3000
    df_data = df_data.sort_values(by=['segment'])

    brain_region = df_data['brain_region'].unique()[0]
    brain_side = df_data['side'].unique()[0]
    '''
    if brain_region =='PMv':
        df_data = df_data.loc[(df_data['monkey'] != 'Jyn')]
    '''
    if datatype == "value":
        if brain_side == "contra":
            ticks_array = [0, max_nr]
        else:
            ticks_array = [-max_nr, 0]
    else:
        if brain_side == "contra":
            ticks_array = [0,lim_plot]
        else:
            ticks_array = [-lim_plot, 0]

    outputName = brain_region + '_' + brain_side + '_' + datatype

    if to_plot:
        fig = plt.figure()
        #g = sns.scatterplot(data=df_data, x='segment', y=datatype, hue='severity',
        #                   hue_order=['Healthy', 'Mild', 'Moderate'], palette=colormap, s=100)

        h = sns.lineplot(data=df_data, x='segment', y=datatype, hue='severity',
                         hue_order=['Healthy', 'Mild', 'Moderate'], palette=colormap, ci=80)
        #g.set_yticks(ticks_array)
        h.set_yticks(ticks_array)
        h.set(title=outputName)
        h.tick_params(labelsize=20)

        plt.savefig(os.path.join(output_folder, (outputName + '.svg')), format='svg')
        plt.show()
    return;


root_dir = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/SC/'

csvFilePath = os.path.join(root_dir, 'ReferenceFiles')
monkeyInfo = pd.read_csv(os.path.join(csvFilePath, 'monkey_histo_reference_sheet.csv'), sep=',')

segInfoPath = os.path.join(root_dir, 'ReferenceFiles')
segInfo = pd.read_csv(os.path.join(segInfoPath, 'SegmentsAllOriginal.csv'), sep=';')

china_data = True

# refInfo = segInfo.join(monkeyInfo.set_index(["name", 'color']), on=['monkey', 'color'])

save_folder = os.path.join(root_dir, 'FiguresWM')
if not os.path.exists(save_folder):
    os.makedirs(save_folder)

file_list = glob.glob(os.path.join(os.path.join(root_dir, 'Annotations'), '*.txt'))
df = pd.DataFrame(file_list, columns=['file_path'])
df = df.apply(lambda x: get_df_info(x), axis=1)

df_annotations = pd.DataFrame()
for iter, row in enumerate(df.itertuples()):
    df_temp = pd.read_csv(row.file_path, sep='\t')
    df_temp['file_name'] = row.file_name
    df_temp['monkey'] = row.monkey
    monkey = row.monkey
    df_temp['slide'] = row.file_nr

    if monkey.startswith('HH'):
        try:
            df_temp['Num M1Fibers'] = df_temp['Num RedFibersRight'] + df_temp['Num RedFibersLeft']
            df_temp['Num PMvFibers'] = df_temp['Num GreenFibersRight'] + df_temp['Num GreenFibersLeft']
        except:
            df_temp['Num RedFibersRight']=0
            df_temp['Num GreenFibersRight']=0
            df_temp['Num GreenFibersLeft'] = 0
            df_temp['Num M1Fibers'] = df_temp['Num RedFibersRight'] + df_temp['Num RedFibersLeft']
            df_temp['Num PMvFibers'] = df_temp['Num GreenFibersRight'] + df_temp['Num GreenFibersLeft']
    df_annotations = df_annotations.append(df_temp)


df_annotations = df_annotations.join(segInfo.set_index(['monkey', 'slide']), on=['monkey', 'slide'])

dfNew = df_annotations[['monkey', 'segment', 'Name', 'Num M1Fibers', 'Num PMvFibers']]

df_right = dfNew.loc[df_annotations['Name'].isin(['WhiteMatterRight'])].rename(
    columns={"Num M1Fibers": "red_ipsi", "Num PMvFibers": "green_ipsi"}).drop(columns=['Name'])
df_left = dfNew.loc[df_annotations['Name'].isin(['WhiteMatterLeft'])].rename(
    columns={"Num M1Fibers": "red_contra", "Num PMvFibers": "green_contra"}).drop(columns=['Name'])

# df_right = df_right.groupby(['monkey','segment'])["M1_ipsi"].mean().unstack().reset_index()
df_right = df_right.melt(id_vars=["monkey", "segment"], var_name="side", value_name="value")
df_left = df_left.melt(id_vars=["monkey", "segment"], var_name="side", value_name="value")

df_nn = pd.concat([df_left.set_index(['monkey', 'segment']), df_right.set_index(['monkey', 'segment'])]).reset_index()
df_nn[['color', 'side']] = df_nn.side.str.split("_", expand=True, )

df_nn = df_nn.join(monkeyInfo.set_index(['monkey', 'color']), on=['monkey', 'color'])

dd = df_nn.copy()

calc_df = pd.DataFrame()
calc_df['max_val'] = dd.groupby(['monkey', 'brain_region'])['value'].max()
calc_df['min_val'] = dd.groupby(['monkey', 'brain_region'])['value'].min()
calc_df['max_min_val'] = calc_df['max_val'] - calc_df['min_val']
calc_df = calc_df.reset_index()

max_animals = dd.groupby(['brain_region'])['value'].max()
min_animals = dd.groupby(['brain_region'])['value'].min()

df_all = df_nn.join(calc_df.set_index(['monkey', 'brain_region']), on=['monkey', 'brain_region'])

df_all["general_max"] = df_all['brain_region'].apply(lambda x: search_minmaxval(x, max_animals))
df_all["general_min"] = df_all['brain_region'].apply(lambda x: search_minmaxval(x, min_animals))

df_all['max_norm'] = np.where(df_all['side'] == 'contra', df_all['value'] / df_all['max_val'],
                              df_all['value'] / df_all['max_val'] * (-1))

df_all['one_max_norm'] = np.where(df_all['side'] == 'contra', df_all['value'] / df_all['general_max'],
                                  df_all['value'] / df_all['general_max'] * (-1))

'''
# Normalise by the injection intake volume
df_all['inj_div'] = df_all['value'] / df_all['injection_vol']
max_inj = df_all.groupby(['monkey', 'brain_region'])['inj_div'].max().reset_index()
max_inj = max_inj.rename(columns={"inj_div": "max_inj_div"})
df_all = df_all.join(max_inj.set_index(['monkey', "brain_region"]), on=['monkey', "brain_region"])
df_all['inj_norm'] = np.where(df_all['side'] == 'contra', df_all['inj_div'] / df_all['max_inj_div'],
                              df_all['inj_div'] / df_all['max_inj_div'] * (-1))
df_all['value'] = np.where(df_all['side'] == 'contra', df_all['value'], df_all['value'] * (-1))
'''

# Normalise by the fibers density
df_all['fibers_div'] = df_all['value'] / df_all['ic_dens']
max_icdens = df_all.groupby(['monkey', 'brain_region'])['fibers_div'].max().reset_index()
max_icdens = max_icdens.rename(columns={"fibers_div": "max_fibers_div"})
df_all = df_all.join(max_icdens.set_index(['monkey', "brain_region"]), on=['monkey', "brain_region"])
df_all['icfibers_norm'] = np.where(df_all['side'] == 'contra', df_all['fibers_div'] / df_all['max_fibers_div'],
                                   df_all['fibers_div'] / df_all['max_fibers_div'] * (-1))

#what_to_plot = ['value','max_norm','inj_norm','one_max_norm']
what_to_plot = ['icfibers_norm']
to_plot=True
for ii in what_to_plot:
    df_all.groupby(['side','brain_region']).apply(lambda x: plot_func_wm(x, ii, save_folder,to_plot,1))

