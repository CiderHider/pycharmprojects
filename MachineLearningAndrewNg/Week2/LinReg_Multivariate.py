import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import os

# Parameters
n_samples = 1000
alpha = 0.01  # Learning rate
alpha_adjustment_factor = 10
n_iterations = 100

# Chose columns to train with
test_columns = ['bedrooms', 'floors', 'sqft_living', 'yr_built', 'bathrooms']

# Read example data
script_path = os.path.dirname(os.path.realpath(__file__))
data_file_name = 'kc_house_data.csv'
data_path = os.path.join(script_path, data_file_name)
df = pd.read_csv(data_path)

# Prepare Training data set
df = df.head(n_samples)
y = df['price']
x = df[test_columns].apply(stats.zscore)
x.insert(0, "x0", np.ones(len(df.index)))

# Batch Regression Variables
n_features = len(x.columns)
theta = np.zeros(n_features)
theta_temp = np.zeros(n_features)
cost = np.zeros(n_iterations)

# Batch Multivariate Regression
for step in range(n_iterations):
    # hypothesis: theta(T)*x
    for j in range(n_features):
        theta_temp[j] = theta[j] - alpha * sum((x.dot(theta) - y) * x[x.columns[j]])
    theta = theta_temp

    # Cost calculation
    cost[step] = 1 / (2 * n_samples) * sum((x.dot(theta) - y) ** 2)

    # Convergence test (optional)
    # if step >= 1 and (cost[step-1] - cost[step]) < 0.001:
    #     break

    # Alpha adjustment (optional)
    if step >= 1 and (cost[step - 1] - cost[step]) < 0:
        alpha = alpha / alpha_adjustment_factor
        print(f'New alpha: {alpha}')

# Plot Cost
plt.plot(cost)
plt.show()
