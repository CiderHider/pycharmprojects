import numpy as np
import random as rd
import matplotlib.pyplot as plt
from scipy import stats
# Generate random test data
data_x = np.array([rd.uniform(2000, 10000) for _ in range(100)])
data_y = np.array([yi + rd.uniform(-1000, 1000) for yi in data_x])
plt.plot(data_x, data_y, 'ro')
plt.show()

x = stats.zscore(data_x)
y = data_y #stats.zscore(data_y)
n_samples = len(data_x)
d0 = 0
d1 = 0
alpha = 100
alpha_adjustment_factor = 10
n_iterations = 20
cost = np.empty(n_iterations)
for step in range(n_iterations):
    #Partial Derivatives 1/n omitted
    d0_temp = d0 - alpha * sum(d0 + d1 * x - y)
    d1_temp = d1 - alpha * sum((d0 + d1 * x - y) * x)
    d0 = d0_temp
    d1 = d1_temp
    cost[step] = 1/n_samples * sum((d0 + d1*x - y)**2)
    # Convergence test
    # if step >= 1 and (cost[step-1] - cost[step]) < 0.001:
    #     break

    # Alpha adjustment
    if step >= 1 and (cost[step-1] - cost[step]) < 0:
        alpha = alpha/alpha_adjustment_factor
        print(f'New alpha: {alpha}')

plt.plot(cost)
plt.show()
