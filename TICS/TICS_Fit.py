import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy as sp
from scipy.stats import norm
import cma
from TICS_Fit_Fun import *

# Load Data
df = pd.read_csv('./data/KS014_train.csv')  # Load .csv file into a pandas DataFrame

df['signed_contrast'] = df['contrast'] * df['position']  # We define a new column for "signed contrasts"
df.drop(columns='stim_probability_left', inplace=True)  # Stimulus probability has no meaning for training sessions

print('Total # of trials: ' + str(len(df['trial_num'])))
print('Sessions: ' + str(np.unique(df['session_num'])))
df.head()

# Plot 2nd session
fig, ax = scatterplot_psychometric_data(df, session_num=2)
plt.show()

# Plot 15th session (last training session)
fig, ax = scatterplot_psychometric_data(df, session_num=15)
plt.show()

# Psychometric
ax = plot_psychometric_data(df, 2)
plt.show()

ax = plot_psychometric_data(df, 15)
plt.show()

# Psychometric plus model
theta0 = (0, 20, 0.2, 0.5)  # Arbitrary parameter values - try different ones
session_num = 15
ax = plot_psychometric_data(df, session_num)
psychofun_plot(theta0, ax)
plt.show()

# Define hard parameter bounds (log likelyhood function [bias/mean, threshold/inverse slope, laps rate, bias when lapse])
# 1-100 folie 11/53 --> near perfect distinction of one to the other condition
# feed into alrogithm
lb = np.array([-100, 1, 0, 0])
ub = np.array([100, 100, 1, 1])
bounds = [lb, ub]

# Define plausible range --> exprience
# Use to initialize function
plb = np.array([-25, 5, 0.05, 0.2])
pub = np.array([25, 25, 0.40, 0.8])

# Pick session data
session_num = 14
df_session = df[df['session_num'] == session_num]

# Define objective function: negative log-likelihood
opt_fun = lambda theta_: -psychofun_loglike(theta_, df_session)

# Generate random starting point for the optimization inside the plausible box
theta0 = np.random.uniform(low=plb, high=pub)

# Initialize CMA-ES algorithm
opts = cma.CMAOptions()
opts.set("bounds", bounds)
opts.set("tolfun", 1e-5)

# Run optimization
res = cma.fmin(opt_fun, theta0, 0.5, opts)

print('')
print('Returned parameter vector: ' + str(res[0]))
print('Negative log-likelihood at solution: ' + str(res[1]))

ax = plot_psychometric_data(df_session, session_num)
psychofun_plot(res[0], ax)
plt.show()

lb = np.array([0, -100, 1, 0, 0])
ub = np.array([1, 100, 100, 1, 1])
bounds = [lb, ub]

plb = np.array([0.05, -25, 5, 0.05, 0.2])
pub = np.array([0.2, 25, 25, 0.45, 0.8])

df_session = df[df['session_num'] == session_num]
# df_session = df[(df['session_num'] == session_num) & (df['trial_num'] > 300)]
opt_fun = lambda theta_: -psychofun_repeatlast_loglike(theta_, df_session)

theta0 = np.random.uniform(low=plb, high=pub)
opts = cma.CMAOptions()
opts.set("bounds", bounds)
opts.set("tolfun", 1e-5)
res_repeatlast = cma.fmin(opt_fun, theta0, 0.5, opts)

print('')
print('Returned parameter vector: ' + str(res_repeatlast[0]))
print('Negative log-likelihood at solution: ' + str(res_repeatlast[1]))

ax = plot_psychometric_data(df_session, session_num)
# psychofun_plot(res[0],ax)
plt.show()

# AIC BIC
Nmodels = 2
nll = np.zeros(Nmodels)
nparams = np.zeros(Nmodels)

results = [res, res_repeatlast]  # Store all optimization output in a vector

for i in range(0, len(results)):
    nll[i] = results[i][1]  # The optimization algorithm received the *negative* log-likelihood
    nparams[i] = len(results[i][0])

ntrials = len(df['signed_contrast'])

aic = 2 * nll + 2 * nparams
bic = 2 * nll + nparams * np.log(ntrials)

print('Model comparison results (for all metrics, lower is better)\n')
print('Negative log-likelihoods: ' + str(nll))
print('AIC: ' + str(aic))
print('BIC: ' + str(bic))

# # ADVANCED MODEL
# theta0 = (0, 20, 0.1, 0.5, 1, 20, 0.1, 0.5)
# ll = psychofun_timevarying_loglike(theta0, df[df['session_num'] == session_num])
# lb = np.array([-100,1,0,0,-100,1,0,0])
# ub = np.array([100,100,1,1,100,100,1,1])
# bounds = [lb,ub]
#
# plb = np.array([-25,5,0.05,0.2,-25,5,0.05,0.2])
# pub = np.array([25,25,0.45,0.8,25,25,0.45,0.8])
#
# session_num = 14
# df_session = df[df['session_num'] == session_num]
# # df_session = df[(df['session_num'] == session_num) & (df['trial_num'] > 300)]
# opt_fun = lambda theta_: -psychofun_timevarying_loglike(theta_,df_session)
#
# theta0 = np.random.uniform(low=plb,high=pub)
# opts = cma.CMAOptions()
# opts.set("bounds",bounds)
# opts.set("tolfun",1e-5)
# res_time = cma.fmin(opt_fun, theta0, 0.5, opts)
#
# print('')
# print('Returned parameter vector: ' + str(res_time[0]))
# print('Negative log-likelihood at solution: ' + str(res_time[1]))
#
# fig = plt.figure(figsize=(9,4))
# ax = plot_psychometric_data(df_session,fig, session_num)
# #psychofun_plot(res[0],ax)
# plt.show()

# Functions
