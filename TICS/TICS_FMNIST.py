"""
Accompanying script 1 of the course
"Trends in Computational Neuroscience"

michael.schartner@unige.ch
"""

# Task A: Dimesionality reduction of the MNIST_784 dataset
# with the UMAP algorithm.
# If you are new to python, 
# try to run this script line by line 
# in an interactive python session (ipython).
# If a python package is missing, say it's called "pack", try installing it via 'pip install pack' 
# in a terminal.

# Modified from https://umap-learn.readthedocs.io/en/latest/auto_examples/plot_mnist_example.html#sphx-glr-auto-examples-plot-mnist-example-p

# 1b: Create and describe a UMAP plot for any non-MNIST dataset you find in the net.
# E.g. fashion MNIST, the first 10**6 integers expressed in prime base, ...
# Look e.g. at https://umap-learn.readthedocs.io/en/latest/auto_examples/index.html

# import required libraries
import numpy as np
import umap
from sklearn.datasets import fetch_openml
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.patches as mpatches
import matplotlib
import os

plt.ion()  # switch on interactive plotting

# set some plotting option
sns.set(context="paper", style="white")

# load (and save) fashion Mnist
if os.path.exists('f_mnist_data10000.npy'):
    data = np.load('f_mnist_data10000.npy', allow_pickle=True)
    target = np.load('f_mnist_target10000.npy', allow_pickle=True)
else:
    print('downloading Fashion-MNIST ...')
    # download the fashion-MNIST dataset
    f_mnist = fetch_openml("fashion-MNIST", version=1)
    np.save('f_mnist_data10000.npy', f_mnist.data[:10000])
    np.save('f_mnist_target10000.npy', f_mnist.target[:10000])
    data = np.load('f_mnist_data10000.npy', allow_pickle=True)
    target = np.load('f_mnist_target10000.npy', allow_pickle=True)

# Plot image number 1000 to exemplify a data point and check label
fig0, ax0 = plt.subplots()
ax0.set_title('Example data point - label: %s' % target[1000])
plt.imshow(data[1000].reshape([28, 28]), axes=ax0)
plt.show()

# Apply the umap algorithm to reduce the 28**2-dimensional points to 2 D
# (this takes a minute or so; to speed it up we'll only use 10k datapoints, not all 70k)
reducer = umap.UMAP(random_state=42)
print('Embedding ...')
embedding = reducer.fit_transform(data)

# Getting the data-labels as a number for color
color = target.astype(int)

# Plot the results
fig, ax = plt.subplots(figsize=(12, 10))
plt.scatter(embedding[:, 0], embedding[:, 1], c=color, cmap="Spectral", s=0.1)
plt.setp(ax, xticks=[], yticks=[])
plt.title("Fashion MNIST data embedded into two dimensions by UMAP", fontsize=18)

# construct a legend manually
cmap = matplotlib.cm.get_cmap('Spectral')
norm = matplotlib.colors.Normalize(vmin=0, vmax=9)
clothing = ['T-Shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle Boot']
patches = [mpatches.Patch(color=cmap(norm(num)), label=name) for num, name in enumerate(clothing)]
plt.legend(handles=patches)
plt.show()
