# Imports
import os
import glob
import re
import tkinter as tk
from tkinter import filedialog
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def cantrip_scaler(base_die, n_base_dice=1, n_added_dice=1, scaler_die=None, damage_mod=0):
    base_damage = (base_die + 1) / 2
    if scaler_die:
        scale_damage = (scaler_die + 1) / 2
    else:
        scale_damage = base_damage

    base_damage_array = np.array([base_damage * n_base_dice] * 20)
    cantrip_saling_array = np.array([0] * 4 + [1] * 6 + [2] * 6 + [3] * 4) * scale_damage
    exp_damage = base_damage_array + cantrip_saling_array * n_added_dice + damage_mod
    return exp_damage


# Non Adjusted Spell Damage
df = pd.DataFrame({'Player Level': np.arange(20) + 1})
df['Eldrich Blast'] = cantrip_scaler(10)
df['Fire Bolt'] = cantrip_scaler(10)
df['Mind Sliver'] = cantrip_scaler(6)
df['Mind Sliver (H)'] = cantrip_scaler(4)
df['Mind Sliver (A)'] = cantrip_scaler(4, scaler_die=6)
df['Ray of Frost'] = cantrip_scaler(8)
df['Chill Touch'] = cantrip_scaler(8)

# Plot stuff
df_melt = df.melt('Player Level', var_name='Spell', value_name='Expected Damage')
plt.figure()
g = sns.lineplot(data=df_melt, x='Player Level', y='Expected Damage', hue='Spell')
g.set(ylim=(0, None))
plt.show()
