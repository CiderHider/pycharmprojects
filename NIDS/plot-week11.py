"""
NAME: Plot Script for co-localisation Exercise

DESCRIPTION: Plots Manders Coefficient with input from the experiment/control
data from the BIOP-JACoP Fiji plugin. (Results Table saved as CSV)

REQUIRED PACKAGES:
numpy~=1.19.2
pandas~=1.1.3
seaborn~=0.11.0
matplotlib~=3.3.2

AUTHOR: Aaron Brändli
DATE: 2020.11.29
"""

import os
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import tkinter as tk
from tkinter.filedialog import askopenfilename

# Set search strings ~ organelle names
search_string_1 = "Mitochondria"
search_string_2 = "RE"

# Set initial dir for convenience
target_dir = "/home/aaron/Documents/week11/Results"
if os.path.exists(target_dir):
    initial_dir = target_dir
else:
    initial_dir = os.path.dirname(os.path.realpath(__file__))

# Get File to analyze
root = tk.Tk()
root.withdraw()
filename_exp = askopenfilename(title=" Select Experimental Data CSV file to analyze",
                               initialdir=initial_dir, filetypes=(("CSV Data", "*.csv*"),))
filename_ctrl = askopenfilename(title=" Select Control Data CSV file to analyze",
                                initialdir=initial_dir, filetypes=(("CSV Data", "*.csv*"),))
root.destroy()

# Read CSV files and add experiment/control column
df_exp = pd.read_csv(filename_exp)
df_exp["Condition"] = "Experiment"
df_ctrl = pd.read_csv(filename_ctrl)
df_ctrl["Condition"] = "Control"

# Merge dataframes and add organelle column
df = pd.concat([df_exp, df_ctrl])
df["Organelle"] = np.where(df["Image A"].str.contains(search_string_1), search_string_1, search_string_2)

# Get Values of interest into usable form
df_melted = df.melt(id_vars=["Organelle", "Condition"], value_vars=["Thresholded M1", "Thresholded M2"])
# Set NaN to 0
df_melted["value"] = df_melted["value"].fillna(0)

# Plot data
g = sns.FacetGrid(df_melted, col="variable", row="Condition")
g.map_dataframe(sns.boxplot, x="Organelle", y="value")
g.map_dataframe(sns.swarmplot, x="Organelle", y="value", color=".25")
g.set_axis_labels("Organelle", "Manders Coefficient")
g.set_titles(col_template="{col_name}", row_template="{row_name}")
g.set(ylim=(0, 1), yticks=np.linspace(0, 1, 5))
plt.show()

# Save
fig_save_path = os.path.join(os.path.split(filename_exp)[0], "Manders_plot.png")
g.savefig(fig_save_path)
