# NAME: Plot Script for Tiling and Tracking Exercise
#
# DESCRIPTION: Plot the mean intensity over time of different tracts
#
# AUTHOR: Aaron Brändli
# DATE: 2020-11-22
#

# Imports
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# Read data and put into Pandas Dataframe
script_path = os.path.dirname(os.path.realpath(__file__))
data_file_name = 'results/SpotsStats.csv'
data_path = os.path.join(script_path, data_file_name)
df = pd.read_csv(data_path)

# Find unique tracks
IDs = df['TRACK_ID'].unique()

# Plot unique tracks over time (frames)
plt.figure(figsize=(12, 12))
plt.title('Mean intensity across time')
plt.xlabel('time (frames)')
plt.ylabel('Median Intensity')
for ID in IDs:
    b_mask = (df['TRACK_ID'] == ID)
    mean_intensity = df['MEAN_INTENSITY'][b_mask]
    x_range = df['FRAME'][b_mask]
    plt.plot(x_range, mean_intensity, label = str(ID))
plt.legend()
plt.savefig('Mean_Intensity.png')
plt.show()


