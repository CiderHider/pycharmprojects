# NAME: Plot 3D distances
#
# DESCRIPTION: Plot 3D distances from the inner and outer cell membrane borders in Conditions A and B respectively
#
# AUTHOR: Aaron Brändli
# DATE: 2020-12-14
#

# Imports
import os
import glob
import re
import tkinter as tk
from tkinter import filedialog

import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from scipy.stats import ttest_ind


# Read data and put into Pandas Dataframe
# script_path = os.path.dirname(os.path.realpath(__file__))
script_path = "/home/aaron/Dropbox/Documents/Work/Courses/ImageProcessing2/Project/Results"

# If user predefined path does not work replace path with home directory as initial dir
if not os.path.exists(script_path):
    script_path = os.path.expanduser("~")

# Get directory to analyze
root = tk.Tk()
root.withdraw()
data_folder = filedialog.askdirectory(title="Chose data directory with csv files to analyze", initialdir=script_path)

# Get csv list in data directory
file_list = glob.glob(os.path.join(data_folder, "*.csv"))

# Initialize empty dataframe
df_import = pd.DataFrame()

# Loop through files to be analyzed and append the mto the dataframe
for file in file_list:
    df_temp = pd.read_csv(file)
    _, file_name = os.path.split(file)
    # Augment dataframe with 'Condition', 'FileNr' and 'Border' information
    df_temp["Condition"] = (re.search("Cond(.*) ", file_name)).group(1)
    df_temp["FileNr"] = (re.search("\((.*)\)", file_name)).group(1)
    df_temp["Border"] = (re.search("\)_(.*)\.", file_name)).group(1)
    df_import = df_import.append(df_temp)

# Extract relevant columns
df = df_import[["Condition", "FileNr", "Border", "Mean", "Min", "Max", "X", "Y", "Z"]]

# Adjust from converted 16 bit back to 32 bit format
df[["Mean", "Min", "Max"]] = df[["Mean", "Min", "Max"]] / 65535

# Plot data
g = sns.FacetGrid(df, col="Border")
g.map_dataframe(sns.boxplot, x="Condition", y="Min")
# g.map_dataframe(sns.swarmplot, x="Condition", y="Min", color=".25") # Too many points to plot using stripplot instead
g.map_dataframe(sns.stripplot, x="Condition", y="Min", color=".25")
g.set_titles(col_template="{col_name}", row_template="{row_name}")
# g.set(ylim=(0, 1), yticks=np.linspace(0, 1, 5))
plt.show()

# Save figure as .png
fig_save_path = os.path.join(os.path.split(file_list[0])[0], "Distance_plot.png")
g.savefig(fig_save_path)

df_out = df.loc[df['Border'] == "Out"]
df_in = df.loc[df['Border'] == "In"]

in_test = ttest_ind(*df_in.groupby('Condition')['Min'].apply(lambda x:list(x)))

out_test = ttest_ind(*df_out.groupby('Condition')['Min'].apply(lambda x:list(x)))
