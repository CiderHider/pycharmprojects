import numpy as np
from matplotlib import pyplot as plt

plt.figure(figsize=(12, 3))
plt.plot(np.median(first_file_data, axis=1), label = 'median')
plt.title('Median of inflammation score across days for each subject')
plt.xlabel('Days')
plt.ylabel('Inflammation Level')
plt.legend()

plt.figure(figsize=(12, 3))
plt.plot(np.min(first_file_data, axis=1), label = 'min')
plt.title('Minimum inflammation score across days for each subject')
plt.xlabel('Days')
plt.ylabel('Inflammation Level')
plt.legend()

plt.figure(figsize=(12, 3))
plt.plot(np.max(first_file_data, axis=1), label = 'max')
plt.title('Maximum inflammation score across days for each subject')
plt.xlabel('Days')
plt.ylabel('Inflammation Level')
plt.legend()


plotlist = ['median', 'min', 'max']
plt.figure(figsize=(12, 3))
plt.plot(np.median(first_file_data, axis=1), label = 'median')
plt.title('Median of inflammation score across days for each subject')
plt.xlabel('Days')
plt.ylabel('Inflammation Level')
plt.legend()

# Exercise
# Substitute the ... above with the right words between subject(s) and day(s)
# Create a new figure, and plot on that figure the minimum, median and maximum inflammation score across patients for each day (i.e. day is on the x-axis)
# Hint: use np.min and np.max
# Add an appropriate label for each plot, and display the legend
# For a 2D array, the boxplot function will produce a boxplot of each column.