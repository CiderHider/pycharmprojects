import os
import glob
import re
import tkinter as tk
from tkinter import filedialog
import numpy as np
from PIL import Image
from tifffile import TiffFile
import nibabel as nib
import pandas as pd
import registration_functions

# Settings and Variables
#data_path = '/media/aaron/Squirrel500/00_Merida/MeridaBDAAnnotation/MeridaBDAAnnotation/output/DirectRegistration'
root_path = '/Users/muscalu/Dropbox (Privat)/IC_Stroke_WorkingDirectory/HistoProcessedData/BRAIN/INPUT'
data_path = os.path.join(root_path,'All_Anterior_Histo_Images')
img_size = [1000, 700]
z_size = 300
nifti_units = 3  # NIFTI_UNITS_MICRON : 3
mri_start = 31 # 38
mri_stop = 56 # 56
monkey = 'Merida'
reset_dyn_range = False
user_output = True

# Collect Data in Dataframe
if not os.path.exists(data_path):
    data_path = os.path.expanduser("~")

# Get data to analyze
root = tk.Tk()
root.withdraw()
#data_folder = filedialog.askdirectory(title="Chose data directory with tif files to analyze", initialdir=data_path)
data_folder = data_path
parent_folder = data_folder.split(os.path.sep)[-1]
files = glob.glob(os.path.join(data_folder, '*.tif'))
files.sort()

name_template = os.path.split(files[0])[1]
filter_str = (re.search('Filter-(.*)_', name_template)).group(1)

if user_output: print(f"Processing dataset with {filter_str}")

nifti, image_stack = registration_functions.tiffs_to_nii(files, output_img_size=img_size,
                                                         voxel_depth=z_size, nifti_unit=nifti_units,
                                                         reset_dyn_range=reset_dyn_range)

df_assoc = registration_functions.associate_tiff_to_MRI(files, mri_start, mri_stop)

str_reset_dyn_range = ''
if reset_dyn_range:
    str_reset_dyn_range = '_RND'
    if user_output: print(f"Adjusting dynamic Range")

file_prefix = f"{monkey}_{parent_folder}_{filter_str}{str_reset_dyn_range}"
nifti_path = os.path.join(data_folder, f"{file_prefix}_Histo.nii.gz")
df_assoc.to_csv(os.path.join(data_folder, f"{file_prefix}_association.csv"))
nib.save(nifti, os.path.join(data_folder, f"{file_prefix}_Histo.nii.gz"))

if user_output: print(f"Saved to Nifti to {nifti_path}")
