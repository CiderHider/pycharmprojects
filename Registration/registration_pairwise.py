import SimpleITK as sitk
from pathlib import Path

# Folders
this_dir = Path.cwd()
image_folder = 'registrationcrop/Image1crops'
image_dir = Path(this_dir, image_folder)

# Images
movingImage = sitk.ReadImage(str(Path(image_dir, '1_small.tif')))
movingImageL = sitk.ReadImage(str(Path(image_dir, '1.tif')))
# ResampleImageFilter() to scale images ???
fixedImage = sitk.ReadImage(str(Path(image_dir, '1a.tif')))

# Initialize registration
elastixImageFilter = sitk.ElastixImageFilter()

# Set registration images
elastixImageFilter.SetMovingImage(movingImage)
elastixImageFilter.SetFixedImage(fixedImage)

# Set Parameters
# p = sitk.ParameterMap()
# p['Registration'] = ['MultiResolutionRegistration']
# p['Transform'] = ['TranslationTransform']

parameterMapVector = sitk.VectorOfParameterMap()
parameterMapVector.append(sitk.GetDefaultParameterMap("affine"))
parameterMapVector.append(sitk.GetDefaultParameterMap("bspline"))
elastixImageFilter.SetParameterMap(parameterMapVector)


# Registration
elastixImageFilter.Execute()
result = elastixImageFilter.GetResultImage()
transformParameterMap = elastixImageFilter.GetTransformParameterMap()

# Write result image low image quality due to multiple transformations
sitk.WriteImage(elastixImageFilter.GetResultImage(), str(Path(image_dir, "result_elastix.tif")))

# Transformations

transformixImageFilter = sitk.TransformixImageFilter()
transformixImageFilter.SetTransformParameterMap(transformParameterMap)

# images = ['1_small.tif', 'b', 'c']

# for image in images:
transformixImageFilter.SetMovingImage(movingImage)
transformixImageFilter.Execute()
sitk.WriteImage(transformixImageFilter.GetResultImage(), str(Path(image_dir, "result_transform.tif")))

transformixImageFilter.SetMovingImage(movingImageL)
transformixImageFilter.Execute()
sitk.WriteImage(transformixImageFilter.GetResultImage(), str(Path(image_dir, "result_transform_Large.tif")))